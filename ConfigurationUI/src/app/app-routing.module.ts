import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DashboardComponent } from './dashboard/dashboard.component';
import { ApplicationConfigComponent } from './application-config/application-config.component';
import { UserConfigComponent } from './user-config/user-config.component';
import { ApiComponent } from './api/api.component';
// import { BacknetComponent } from './backnet/backnet.component';

const routes: Routes = [
  { path: '', component: DashboardComponent },
  { path: '',   redirectTo: '/dashboard', pathMatch: 'full' },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'ApiComponent', component: ApiComponent }
  // { path: 'BacknetComponent', component: BacknetComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
