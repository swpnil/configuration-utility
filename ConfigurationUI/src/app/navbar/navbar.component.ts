import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

dashName: any;
apiName: any;
apiNameVal: boolean = false;

  popupDash = localStorage.getItem('dashBoardPopupDashboard');
  ipDash = localStorage.getItem('ipAddressDashboard');
  portDash = localStorage.getItem('portDashboard');


  popupAPI = localStorage.getItem('APIPopup');
  ipAPI = localStorage.getItem('ipAddressAPI');
  portAPI = localStorage.getItem('portAPI');

  logoutDashBoard(){
    localStorage.removeItem('dashBoardPopupDashboard');
    localStorage.removeItem( 'ipAddressDashboard');
    localStorage.removeItem( 'portDashboard');
  }

  logoutAPI(){
    localStorage.removeItem('APIPopup');
    localStorage.removeItem( 'ipAddressDashboard');
    localStorage.removeItem( 'portDashboard');
  }


  constructor() { }

  ngOnInit() {
    // localStorage.setItem('dashBoardPopupDashboard', 'false');
    // localStorage.setItem('ipAddressDashboard', "ipAddress");
    // localStorage.setItem('portDashboard', "port");


    // localStorage.setItem('APIPopup', 'false');

    // localStorage.setItem('ipAddressAPI', "ipAddress");
    // localStorage.setItem('portAPI', "port");

    // if(this.popupDash!= null && this.ipDash != null && this.portDash != null){
    //   this.dashName = "Dashboard"
    // }
    // if(this.popupAPI!= null && this.ipAPI != null && this.portAPI != null){
    //   this.apiName = "API"
    //   this.apiNameVal = true;
    // }
  }

}
