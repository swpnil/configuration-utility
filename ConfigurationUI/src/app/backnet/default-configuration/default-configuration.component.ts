import { Component, OnInit, ViewChild, ElementRef, NgZone, SimpleChanges } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { ToastrService } from 'ngx-toastr';

import {ApiService} from '../service/ApiService';
import { ConfigurationService } from './service/configuration.service';
import {BacknetHomeService} from '../backnet-home/service/backnet-home.service';
import {deviceComponent} from '../interface/device.comp.interface'

import { AgmCoreModule, MapsAPILoader, AgmMap, LatLngBounds, MouseEvent } from '@agm/core';
import {} from '@agm/core/services/google-maps-types';
declare let google: any;

@Component({
  selector: 'app-default-configuration',
  templateUrl: './default-configuration.component.html',
  styleUrls: ['./default-configuration.component.css']
})
export class DefaultConfigurationComponent implements OnInit {
  title = "Default Configuration Setting"

  molexProjectNameValue =["1-Manual Life Safety","2-Automatic-Life Safety", "3-Available", "4-Available", "5-Critical Equipment Control", "6-Minimum On/Off", "7-Available", "8-Manual Operator", "9-Available", "10-Available", "11-Available", "12-Available", "13-Available", "14-Available", "15-Available", "16-Available"]

  MonitoringDefaultPriorityVal=[""]

  public formConfiguration: FormGroup;
  _postsArrayConfiguration:any;

  isDataAvailable:boolean = false;
  _formDisabledState:boolean = true;

  ipPattern = "(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)";


  optionsApi:any;
  cancel() {
    // this.formConfiguration.get('DeviceId').setValue(this.optionsConfiguration.DeviceId);
    // this.formConfiguration.get('MolexApiUrl').setValue(this.optionsConfiguration.MolexApiUrl);
    // this.formConfiguration.get('MolexProjectName').setValue(this.optionsConfiguration.MolexProjectName);
    // this.formConfiguration.get('MonitoringDefaultPriority').setValue(this.optionsConfiguration.MonitoringDefaultPriority);
   }

  private ConfigurationComponentSer: DefaultConfigurationComponent[] = [];
  private ConfigurationComponentObservable : Observable<DefaultConfigurationComponent[]> ;

  feedbackForm: FormGroup;
  buildings = []

  constructor(private fb: FormBuilder, private apiService : ApiService, private configurationService: ConfigurationService, private backnetHomeService: BacknetHomeService, private http: Http,private tostr: ToastrService) {
    this.ConfigurationComponentObservable = this.configurationService.get_Configuration();
   }

  baseUrlJSON = "http://localhost:3000";
  baseUrlSave = "http://localhost:3000"
  private ConfigurationComponent  = [];

  get_ConfigurationComponent(){
    this.http.get(this.baseUrlJSON + '/data').subscribe((res)=>{
        console.log(res.json());
        this.ConfigurationComponent = res.json();
        console.log(this.ConfigurationComponent);
    });
}

  isDisabled = true;
  currentIPAddress:any;
  currentPort:any;
  showBacNetLoginPopUP:boolean;
  currentIPBacnet:any;
  currentPortBacnet:any;

  ngOnInit() {
    this.currentIPBacnet = localStorage.getItem('ipAddressBacNet');
    this.currentPortBacnet = localStorage.getItem('portBacNet');
    if (this.currentIPBacnet == null && this.currentPortBacnet == null) {
      console.log("if");
      this.showBacNetLoginPopUP = true;
    }
    setTimeout(() => {
      this.isDataAvailable = true;
      this.initConfiguration();
    }, 1000);

  }

  configClicked(value){
    // console.log("dsdsdsdsdsd", this._postJson.ConfigurationObject.DaylightSavingStatus);
    if(value == "setConfiguration")
    {
      this.tostr.success('Success! BACnet Configuration is set as default successfully.');
      this.backnetHomeService.SetBACnetDefaultConfigurations();
    }
    else
    {
      this.tostr.success('Success! Default BACnet Configuration restored successfully.');
      this.backnetHomeService.RestoreBACnetDefaultConfigurations();
    }
  }

  baseUrl:any;
  loginModelStatus:any
  loginModelStatusValue:boolean = true;
  isConnectedValue:boolean =true;
  localStorageValue:boolean;
  buildingConfiguration:boolean = false;

  initConfiguration(){
    // this.configurationService.initLoadApp();
    this.backnetHomeService.getProjectsData();

    this.backnetHomeService.isConnected.subscribe( result => {
      console.log("isConnected", result)
      this.isConnectedValue = result
      this.loginModelStatusValue = !result;
      if (result) {
        this.getDataConfig();
        this.getConfiguration();
        this.subscribeInputFields();
      }
    });
  }

  getSmartCoreValue: any;
  SmartCore: any;
  buildingValue:any;
  optionsSmartCore:any;
  _postsArraySmartCore:any;
  _SmartCoreoptions:any;
  // systemValues:any;


    get repliesControl() {
      // console.log("this.feedbackForm", this.feedbackForm)
      return this.feedbackForm.get('buildings');
    }

  _postJson:any;
  getDataConfig(): void{
    // const resultJson = this.configurationService.getData()
    // this._postJson = resultJson;
    const resultJson = this.backnetHomeService.getGWServiceData();
    // this._postJson = resultJson.JsonData;
    this._postJson = resultJson.result.data[0];
    console.log("Debug", this._postJson );
    console.log("this._postJson$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$", this._postJson );
}

optionsConfiguration:any;

setConfigurationVal;
restoreConfigurationVal;

getConfiguration(): void{
    const resultArray = this.configurationService.getConfiguration()
    this._postsArrayConfiguration = resultArray;
    var data = [];
    data.push(resultArray);
    this._postsArrayConfiguration = data;
    console.log("this._postsArrayConfiguration", this._postsArrayConfiguration)
    this.optionsConfiguration = this._postsArrayConfiguration[0];

    this.setConfigurationVal = this.optionsConfiguration.setConfiguration
    this.restoreConfigurationVal = this.optionsConfiguration.restoreConfiguration

    console.log("this.optionsConfiguration", this.optionsConfiguration)
   }


  get fConfiguration() { return this.formConfiguration.controls; }

submitConfigurationComp():void{
  console.log("this.baseUrlSave", this.baseUrlSave);

  // this._postJson.IsConfigurationLogEnable = value;
  // this._postJson = this._postJson.IsConfigurationLogEnable;
  console.log("this._postJson", this._postJson);

  /* this.http.post(this.baseUrlSave, this._postJson)
          .subscribe(
            data  => {
            console.log("POST Request is successful ", data);
            // this.addContactOnFalse(this.currentIPAddress, this.currentPort);
            },
            error  => {
            console.log("Error", error);
            }
          ); */
}

  subscribeInputFields():void {
    // this.formConfiguration.valueChanges.subscribe(
    //   (data) => {
    //     this._postJson.IsConfigurationLogEnable = data;
    //     this._formDisabledState = this.formConfiguration.valid;
    //     }
    //   );
  }
  onSearchChange(searchValue : string ) {
    console.log(searchValue);
  }

}
