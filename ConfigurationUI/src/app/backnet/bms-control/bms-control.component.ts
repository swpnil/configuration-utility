import { Component, OnInit, ViewChild, ElementRef, NgZone, SimpleChanges, ViewChildren, Input} from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { ToastrService } from 'ngx-toastr';

import { BmsService } from './service/bms.service';

import {BacknetHomeService} from '../backnet-home/service/backnet-home.service';
// import { PasswordSettingDirective } from './directive/password-setting.directive';

import {} from '@agm/core/services/google-maps-types';
declare let google: any;

@Component({
  selector: 'app-bms-control',
  templateUrl: './bms-control.component.html',
  styleUrls: ['./bms-control.component.css']
})
export class BmsControlComponent implements OnInit {
molexProjectNameValue =["Celsius", "Fahrenheit"]
buttonDisabled: boolean = true;

typeBCT= "password";
showBCT = false;
 toggleBCTShow()
  {
      this.showBCT = !this.showBCT;
      if (this.showBCT){
          this.typeBCT = "text";
      }
      else {
          this.typeBCT = "password";
      }
  }

showPassword = false;
 toggleShowPassword()
  {
      this.showPassword = !this.showPassword;
      if (this.showPassword){
          // this.typeAPI = "text";
          this.formBms.controls["BACnetPassword"].disable();
      }
      else {
          // this.typeAPI = "password";
          this.formBms.controls["BACnetPassword"].enable();
      }
  }

  DataPublishClick(value){
    console.log("Temperature changed",value);
    if(value == "Fahrenheit")
      this._postJson.BmsControl.IsTempRequiredInFahrenheit = true;
    else
      this._postJson.BmsControl.IsTempRequiredInFahrenheit = false;
    // this.order.type=value;
  }



public formBms: FormGroup;
  _postsArrayBms:any;

  isDataAvailable:boolean = false;
  _formDisabledState:boolean = true;
  title = "Bms Control Settings"

  ipPattern = "(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)";


  optionsApi:any;
  cancel() {
    this.formBms.get('IsBACnetPasswordRequired').setValue(this.optionsBms.IsBACnetPasswordRequired);
    this.formBms.get('WriteAccess').setValue(this.optionsBms.WriteAccess);
    this.formBms.get('BACnetPassword').setValue(this.optionsBms.BACnetPassword);
    this.formBms.get('TimeSynchronization').setValue(this.optionsBms.TimeSynchronization);
    this.formBms.get('Scheduling').setValue(this.optionsBms.Scheduling);
    this.formBms.get('MapIndividualSensors').setValue(this.optionsBms.MapIndividualSensors);
    this.formBms.get('MapIndividualFixture').setValue(this.optionsBms.MapIndividualFixture);
    this.formBms.get('CharacterEncodingTypes').setValue(this.optionsBms.CharacterEncodingTypes);
    this.formBms.get('IsTempRequiredInFahrenheit').setValue(this.optionsBms.IsTempRequiredInFahrenheit);
    // this.formBms.get('LocalBmsNumber').setValue(this.optionsBms.LocalBmsNumber);
   }

  private BmsComponentSer: BmsControlComponent[] = [];
  private BmsComponentObservable : Observable<BmsControlComponent[]> ;

  feedbackForm: FormGroup;
  buildings = []

  constructor(private fb: FormBuilder, private bmsService: BmsService, private http: Http, private backnetHomeService:BacknetHomeService,private tostr: ToastrService) {
    this.BmsComponentObservable = this.backnetHomeService.get_Bms();
   }

  baseUrlJSON = "http://localhost:3000";
  baseUrlSave = "http://localhost:3000";

  private BmsInterfaceComponent  = [];

  get_BmsComponent(){
    this.http.get(this.baseUrlJSON + '/data').subscribe((res)=>{
        console.log(res.json());
        this.BmsInterfaceComponent = res.json();
        console.log(this.BmsInterfaceComponent);
    });
}

  isDisabled = true;
  currentIPAddress:any;
  currentPort:any;

  showBacNetLoginPopUP:boolean;
  currentIPBacnet:any;
  currentPortBacnet:any;

  ngOnInit() {
    this.currentIPBacnet = localStorage.getItem('ipAddressBacNet');
    this.currentPortBacnet = localStorage.getItem('portBacNet');
    if (this.currentIPBacnet == null && this.currentPortBacnet == null) {
      console.log("if");
      this.showBacNetLoginPopUP = true;
    }
    // this.backnetHomeService.initLoadApp();

    this.backnetHomeService.getProjectsData();
    // console.log("1111111111111",this.myDiv.nativeElement);
    // console.log("2222222222222222222",this.myDiv2.nativeElement);

    // this.currentIPAddress = localStorage.getItem('ipAddress');
    // this.currentPort = localStorage.getItem('port');
    setTimeout(() => {
      // this.initBms();
      this.formBms = this.fb.group({
        IsBACnetPasswordRequired: ['', [Validators.required]],
        BACnetPassword: ['', [Validators.required]],
        WriteAccess: ['', [Validators.required]],
        TimeSynchronization: ['', [Validators.required]],
        Scheduling: ['', [Validators.required]],
        MapIndividualSensors: ['', [Validators.required]],
        MapIndividualFixture: ['', [Validators.required]],
        CharacterEncodingTypes: ['', [Validators.required]],
        IsTempRequiredInFahrenheit: ['', [Validators.required]],
        // LocalBmsNumber: ['', [Validators.required]],
      });
    }, 1000);

    this.backnetHomeService.isConnected.subscribe( result => {
      if (result) {
        setTimeout(() => {
          this.isDataAvailable = true;
          this.getDataConfig();
          this.getBms();
          this.subscribeInputFields();
        }, 1000);
      }
    });

  }

  enaDisClicked(value){
    console.log("dsdsdsdsdsd", this._postJson.BmsObject.DaylightSavingStatus);
    this.isDisabled = value;
    this._postJson.BmsObject.DaylightSavingStatus = value;
  }

  LocalBmsNumberClick(value){
    console.log(value);
    this._postJson.IsBACnetPasswordRequired.LocalBmsNumber = value;
    // this.order.type=value;
  }

  selectStatus(selectStatus, selectedNum){
    let array = this._postJson.BmsInterface.IPAddressList;

    for (let i = 0; i < array.length; i++) {
      if(selectedNum == i){
        this._postJson.BmsInterface.IPAddressList[i].IsSelected = selectStatus;
      }else{
        this._postJson.BmsInterface.IPAddressList[i].IsSelected = false;
      }
    }
    // this._postJson.BmsInterface.IPAddressList[i].IsSelected = selectStatus;
    // this.order.type=value;
  }

  baseUrl:any;
  loginModelStatus:any
  loginModelStatusValue:boolean = true;
  isConnectedValue:boolean =true;
  localStorageValue:boolean;
  buildingConfiguration:boolean = false;

  initBms(){
    this.bmsService.initLoadApp();
    this.bmsService.isConnected.subscribe( result => {
      console.log("isConnected", result)
      this.isConnectedValue = result
      this.loginModelStatusValue = !result;
      if (result) {
        this.getDataConfig();
        this.getBms();
        this.subscribeInputFields();
        // localStorage.setItem('dashBoardPopupDashboard', 'false');
        // localStorage.setItem('ipAddressDashboard', ipAddress);
        // localStorage.setItem('portDashboard', port);
      }
    });
  }

  getSmartCoreValue: any;
  SmartCore: any;
  buildingValue:any;
  optionsSmartCore:any;
  _postsArraySmartCore:any;
  _SmartCoreoptions:any;
  // systemValues:any;


    get repliesControl() {
      // console.log("this.feedbackForm", this.feedbackForm)
      return this.feedbackForm.get('buildings');
    }

  _postJson:any;
  getDataConfig(): void{
    // const resultJson = this.backnetHomeService.getData()
    // this._postJson = resultJson.JsonData;
    const resultJson = this.backnetHomeService.getJsonData();
    // this._postJson = resultJson.JsonData;
    this._postJson = resultJson.result.data[0];
    console.log("BMS", this._postJson );
    console.log("this._postJson$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$", this._postJson );
}

optionsBms:any;
IPAddressList:any;
getBms(): void{
    // const resultArray = this.backnetHomeService.getBms()
    this._postsArrayBms = this._postJson.BmsControl;
    var data = [];
    data.push(this._postsArrayBms);
    this._postsArrayBms = data;
    console.log("this._postsArrayBms", this._postsArrayBms)
    this.optionsBms = this._postsArrayBms[0];

    // this.IPAddressList = this._postsArrayBms[0].IPAddressList;
    // console.log("this.optionsBms", this.optionsBms)
    this.formBms.get('IsBACnetPasswordRequired').setValue(this.optionsBms.IsBACnetPasswordRequired);
    this.formBms.get('BACnetPassword').setValue(this.optionsBms.BACnetPassword);


    this.formBms.get('WriteAccess').setValue(this.optionsBms.WriteAccess);
    this.formBms.get('TimeSynchronization').setValue(this.optionsBms.TimeSynchronization);
    this.formBms.get('Scheduling').setValue(this.optionsBms.Scheduling);
    this.formBms.get('MapIndividualSensors').setValue(this.optionsBms.MapIndividualSensors);
    this.formBms.get('MapIndividualFixture').setValue(this.optionsBms.MapIndividualFixture);
    this.formBms.get('CharacterEncodingTypes').setValue(this.optionsBms.CharacterEncodingTypes);
    this.formBms.get('IsTempRequiredInFahrenheit').setValue(this.optionsBms.IsTempRequiredInFahrenheit);
    // this.formBms.get('LocalBmsNumber').setValue(this.optionsBms.LocalBmsNumber);
   }


  get fBms() { return this.formBms.controls; }

submitBmsComp():void{
  console.log("this.baseUrlSave", this.baseUrlSave);
  console.log("this._postJson", this._postJson);
  this.backnetHomeService.SaveBacNetConfiguration(this._postJson);
  this.tostr.success('Success! BACnet Configuration is updated successfully.');
}

  subscribeInputFields():void {
    this.formBms.valueChanges.subscribe(
      (data) => {
        this._postJson.BmsControl = data;
        this._formDisabledState = this.formBms.valid;
        }
      );
  }
  onSearchChange(searchValue : string ) {
    console.log(searchValue);
  }

}
