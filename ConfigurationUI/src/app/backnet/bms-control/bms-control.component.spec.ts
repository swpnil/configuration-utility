import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BmsControlComponent } from './bms-control.component';

describe('BmsControlComponent', () => {
  let component: BmsControlComponent;
  let fixture: ComponentFixture<BmsControlComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BmsControlComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BmsControlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
