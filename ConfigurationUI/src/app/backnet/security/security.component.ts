import { Component, OnInit, ViewChild, ElementRef, NgZone, SimpleChanges, ViewChildren, Input} from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { ToastrService } from 'ngx-toastr';

import { SecurityService } from './service/security.service';
import {BacknetHomeService} from '../backnet-home/service/backnet-home.service';
// import { PasswordSettingDirective } from './directive/password-setting.directive';

import {} from '@agm/core/services/google-maps-types';
declare let google: any;

@Component({
  selector: 'app-security',
  templateUrl: './security.component.html',
  styleUrls: ['./security.component.css']
})
export class SecurityComponent implements OnInit {

typeBCT= "password";
showBCT = false;
 toggleBCTShow()
  {
    console.log("HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH")
      this.showBCT = !this.showBCT;
      if (this.showBCT){
          this.typeBCT = "text";
      }
      else {
          this.typeBCT = "password";
      }
  }

typeAPI= "password";
showAPI = false;
 toggleAPIShow()
  {
    console.log("HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH")
      this.showAPI = !this.showAPI;
      if (this.showAPI){
          this.typeAPI = "text";
      }
      else {
          this.typeAPI = "password";
      }
  }

public formSecurity: FormGroup;
  _postsArraySecurity:any;

  isDataAvailable:boolean = false;
  _formDisabledState:boolean = true;
  title = "Security Object Settings"

  ipPattern = "(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)";


  optionsApi:any;
  cancel() {
    this.formSecurity.get('APIUserID').setValue(this.optionsSecurity.APIuserID);
    this.formSecurity.get('APIPassword').setValue(this.optionsSecurity.APIpassword);
    // this.formSecurity.get('LocalSecurityNumber').setValue(this.optionsSecurity.LocalSecurityNumber);
   }

  private SecurityComponentSer: SecurityComponent[] = [];
  private SecurityComponentObservable : Observable<SecurityComponent[]> ;

  feedbackForm: FormGroup;
  buildings = []

  constructor(private fb: FormBuilder, private securityService: SecurityService, private backnetHomeService:BacknetHomeService, private http: Http,private tostr: ToastrService) {
    this.SecurityComponentObservable = this.backnetHomeService.get_Security();
   }

  baseUrlJSON = "http://localhost:3000";
  baseUrlSave = "http://localhost:3000";

  private SecurityInterfaceComponent  = [];

  get_SecurityComponent(){
    this.http.get(this.baseUrlJSON + '/data').subscribe((res)=>{
        console.log(res.json());
        this.SecurityInterfaceComponent = res.json();
        console.log(this.SecurityInterfaceComponent);
    });
}

  isDisabled = true;
  currentIPAddress:any;
  currentPort:any;
  showBacNetLoginPopUP:boolean;
  currentIPBacnet:any;
  currentPortBacnet:any;

  ngOnInit() {

    this.backnetHomeService.getProjectsData();
    setTimeout(() => {
      // this.initSecurity();
      this.formSecurity = this.fb.group({
        APIUserID: ['', [Validators.required]],
        APIPassword: ['', [Validators.required]],
        // LocalSecurityNumber: ['', [Validators.required]],
      });
    }, 1000);

    this.backnetHomeService.isConnected.subscribe( result => {
      if (result) {
        setTimeout(() => {
          this.isDataAvailable = true;
          this.getDataConfig();
          this.getSecurity();
          this.subscribeInputFields();
        }, 1000);
      }
    });

  }

  enaDisClicked(value){
    console.log("dsdsdsdsdsd", this._postJson.SecurityObject.DaylightSavingStatus);
    this.isDisabled = value;
    this._postJson.SecurityObject.DaylightSavingStatus = value;
  }

  LocalSecurityNumberClick(value){
    console.log(value);
    this._postJson.BCTUserID.LocalSecurityNumber = value;
    // this.order.type=value;
  }

  selectStatus(selectStatus, selectedNum){
    let array = this._postJson.SecurityInterface.IPAddressList;

    for (let i = 0; i < array.length; i++) {
      if(selectedNum == i){
        this._postJson.SecurityInterface.IPAddressList[i].IsSelected = selectStatus;
      }else{
        this._postJson.SecurityInterface.IPAddressList[i].IsSelected = false;
      }
    }
    // this._postJson.SecurityInterface.IPAddressList[i].IsSelected = selectStatus;
    // this.order.type=value;
  }

  baseUrl:any;
  loginModelStatus:any
  loginModelStatusValue:boolean = true;
  isConnectedValue:boolean =true;
  localStorageValue:boolean;
  buildingConfiguration:boolean = false;

  initSecurity(){
    this.securityService.initLoadApp();
    this.securityService.isConnected.subscribe( result => {
      console.log("isConnected", result)
      this.isConnectedValue = result
      this.loginModelStatusValue = !result;
      if (result) {
        this.getDataConfig();
        this.getSecurity();
        this.subscribeInputFields();
        // localStorage.setItem('dashBoardPopupDashboard', 'false');
        // localStorage.setItem('ipAddressDashboard', ipAddress);
        // localStorage.setItem('portDashboard', port);
      }
    });
  }

  getSmartCoreValue: any;
  SmartCore: any;
  buildingValue:any;
  optionsSmartCore:any;
  _postsArraySmartCore:any;
  _SmartCoreoptions:any;
  // systemValues:any;


    get repliesControl() {
      // console.log("this.feedbackForm", this.feedbackForm)
      return this.feedbackForm.get('buildings');
    }

  _postJson:any;
  getDataConfig(): void{
    // const resultJson = this.backnetHomeService.getData()
    // this._postJson = resultJson.JsonData;
    const resultJson = this.backnetHomeService.getJsonData();
    // this._postJson = resultJson.JsonData;
    this._postJson = resultJson.result.data[0];
    console.log("Device", this._postJson );
    console.log("this._postJson$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$", this._postJson );
}

optionsSecurity:any;
IPAddressList:any;
getSecurity(): void{
    // const resultArray = this.backnetHomeService.getSecurity()
    this._postsArraySecurity = this._postJson.Security;
    var data = [];
    data.push( this._postsArraySecurity);
    this._postsArraySecurity = data;
    this.optionsSecurity = this._postsArraySecurity[0];
    this.formSecurity.get('APIUserID').setValue(this.optionsSecurity.APIuserID);
    this.formSecurity.get('APIPassword').setValue(this.optionsSecurity.APIpassword);
    // this.formSecurity.get('LocalSecurityNumber').setValue(this.optionsSecurity.LocalSecurityNumber);
   }


  get fSecurity() { return this.formSecurity.controls; }

submitSecurityComp():void{
  console.log("this.baseUrlSave", this.baseUrlSave);
  console.log("this._postJson", this._postJson);
  this.backnetHomeService.SaveBacNetConfiguration(this._postJson);
  this.tostr.success('Success! BACnet Configuration is updated successfully.');
}

  subscribeInputFields():void {
    this.formSecurity.valueChanges.subscribe(
      (data) => {
        this._postJson.Security = data;
        this._formDisabledState = this.formSecurity.valid;
        }
      );
  }
  onSearchChange(searchValue : string ) {
    console.log(searchValue);
  }

}
