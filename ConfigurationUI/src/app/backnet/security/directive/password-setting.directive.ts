import { Directive, ElementRef, EventEmitter, Output } from '@angular/core';

@Directive({
  selector: '[appPasswordSetting]'
})
export class PasswordSettingDirective {

   @Output() onSelect: EventEmitter<any> = new EventEmitter()

  // public onSelect = "heeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee"
  public abc: EventEmitter<any> = new EventEmitter();
  private _shown = false;

  constructor(private el: ElementRef) { 
    this.setup();
  }

  toggle(span: HTMLElement) {
    this._shown = !this._shown;

    this.onSelect.emit({'startTime': this._shown})
// console.log("this._shown", this._shown)
    // return this._shown;
    // this.abc.emit(this._shown);
    // if (this._shown) {
    //   this.el.nativeElement.setAttribute('type', 'text');
    //   span.innerHTML = 'Hide password';
    // } else {
    //   this.el.nativeElement.setAttribute('type', 'password');
    //   span.innerHTML = 'Show password';
    // }
  }

  setup() {
    const parent = this.el.nativeElement.parentNode;
    const span = document.createElement('span');
    span.innerHTML = `Show password`;
    span.addEventListener('click', (event) => {
       this.toggle(span);
      alert('you just clicked me, you need to toggle view')
    });
    parent.appendChild(span);
  }

  customDirective() {
    console.log('Inside customDirective()');
}

}
