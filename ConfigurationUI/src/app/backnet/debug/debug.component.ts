import { Component, OnInit, ViewChild, ElementRef, NgZone, SimpleChanges } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { ToastrService } from 'ngx-toastr';

import {ApiService} from '../service/ApiService';
import { DebugService } from './service/debug.service';
import {BacknetHomeService} from '../backnet-home/service/backnet-home.service';
import {deviceComponent} from '../interface/device.comp.interface'

import { AgmCoreModule, MapsAPILoader, AgmMap, LatLngBounds, MouseEvent } from '@agm/core';
import {} from '@agm/core/services/google-maps-types';
declare let google: any;


@Component({
  selector: 'app-debug',
  templateUrl: './debug.component.html',
  styleUrls: ['./debug.component.css']
})
export class DebugComponent implements OnInit {
  molexProjectNameValue =["1-Manual Life Safety","2-Automatic-Life Safety", "3-Available", "4-Available", "5-Critical Equipment Control", "6-Minimum On/Off", "7-Available", "8-Manual Operator", "9-Available", "10-Available", "11-Available", "12-Available", "13-Available", "14-Available", "15-Available", "16-Available"]

  MonitoringDefaultPriorityVal=[""]

  public formDebug: FormGroup;
  _postsArrayDebug:any;

  isDataAvailable:boolean = false;
  _formDisabledState:boolean = true;
  title = "Debug Object Settings"

  ipPattern = "(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)";


  optionsApi:any;
  cancel() {
    this.formDebug.get('DeviceId').setValue(this.optionsDebug.DeviceId);
    this.formDebug.get('MolexApiUrl').setValue(this.optionsDebug.MolexApiUrl);
    this.formDebug.get('MolexProjectName').setValue(this.optionsDebug.MolexProjectName);
    this.formDebug.get('MonitoringDefaultPriority').setValue(this.optionsDebug.MonitoringDefaultPriority);
   }

  private DebugComponentSer: DebugComponent[] = [];
  private DebugComponentObservable : Observable<DebugComponent[]> ;

  feedbackForm: FormGroup;
  buildings = []

  constructor(private fb: FormBuilder, private apiService : ApiService, private debugService: DebugService, private backnetHomeService: BacknetHomeService, private http: Http,private tostr: ToastrService) {
    this.DebugComponentObservable = this.backnetHomeService.get_Debug();
   }

  baseUrlJSON = "http://localhost:3000";
  baseUrlSave = "http://localhost:2000/saveConfiguration"

  private DebugComponent  = [];

  get_DebugComponent(){
    this.http.get(this.baseUrlJSON + '/data').subscribe((res)=>{
        console.log(res.json());
        this.DebugComponent = res.json();
        console.log(this.DebugComponent);
    });
}

  isDisabled = true;
  currentIPAddress:any;
  currentPort:any;

  showBacNetLoginPopUP:boolean;
  currentIPBacnet:any;
  currentPortBacnet:any;

  ngOnInit() {
    this.currentIPBacnet = localStorage.getItem('ipAddressBacNet');
    this.currentPortBacnet = localStorage.getItem('portBacNet');
    if (this.currentIPBacnet == null && this.currentPortBacnet == null) {
      console.log("if");
      this.showBacNetLoginPopUP = true;
    }

    // this.backnetHomeService.initLoadApp();
    this.backnetHomeService.getProjectsData();

    // this.currentIPAddress = localStorage.getItem('ipAddress');
    // this.currentPort = localStorage.getItem('port');
    setTimeout(() => {
      // this.initDebug();
      this.formDebug = this.fb.group({
        DeviceId: ['', [Validators.required]],
        MolexApiUrl: ['', [Validators.required]],
        MolexProjectName: ['', [Validators.required]],
        MonitoringDefaultPriority: ['', [Validators.required, Validators.minLength(5)]],
      });
    }, 1000);

    this.backnetHomeService.isConnected.subscribe( result => {
      if (result) {
        setTimeout(() => {
          this.isDataAvailable = true;
          this.getDataConfig();
          this.getDebug();
          this.subscribeInputFields();
        }, 1000);
      }
    });


  }

  enaDisClicked(value){
    console.log("dsdsdsdsdsd", this._postJson.DebugObject.DaylightSavingStatus);
    this.isDisabled = value;
    this._postJson.DebugObject.DaylightSavingStatus = value;
  }

  molexProjectNameClick(value){
    console.log(value);
    this._postJson.Debug.MolexProjectName = value;
    // this.order.type=value;
  }

  MonitoringDefaultPriority(value){
    console.log(value);
    this._postJson.Debug.MonitoringDefaultPriority = value;
    // this.order.type=value;
  }

  baseUrl:any;
  loginModelStatus:any
  loginModelStatusValue:boolean = true;
  isConnectedValue:boolean =true;
  localStorageValue:boolean;
  buildingConfiguration:boolean = false;

  initDebug(){
    this.debugService.initLoadApp();
    this.debugService.isConnected.subscribe( result => {
      console.log("isConnected", result)
      this.isConnectedValue = result
      this.loginModelStatusValue = !result;
      if (result) {
        this.getDataConfig();
        this.getDebug();
        this.subscribeInputFields();
        // localStorage.setItem('dashBoardPopupDashboard', 'false');
        // localStorage.setItem('ipAddressDashboard', ipAddress);
        // localStorage.setItem('portDashboard', port);
      }
    });
  }

  getSmartCoreValue: any;
  SmartCore: any;
  buildingValue:any;
  optionsSmartCore:any;
  _postsArraySmartCore:any;
  _SmartCoreoptions:any;
  // systemValues:any;


    get repliesControl() {
      // console.log("this.feedbackForm", this.feedbackForm)
      return this.feedbackForm.get('buildings');
    }

  _postJson:any;
  getDataConfig(): void{
    // const resultJson = this.backnetHomeService.getData()
    // this._postJson = resultJson.JsonData;
    const resultJson = this.backnetHomeService.getJsonData();
    // this._postJson = resultJson.JsonData;
    this._postJson = resultJson.result.data[0];
    console.log("Debug", this._postJson );
    console.log("this._postJson$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$", this._postJson );
}

optionsDebug:any;
IsDebugEnabled:any;
debugValue:any;
getDebug(): void{
    console.log("this.debugValue", this._postJson);
    this.IsDebugEnabled = this._postJson.IsDebugLogEnable;
    
   }


  get fDebug() { return this.formDebug.controls; }

saveDebugComp(value):void{
  console.log("Debug Log Value",value);
  this._postJson.IsDebugLogEnable = value;
}

submitDebugComp():void{
  console.log("this.baseUrlSave", this.baseUrlSave);
  console.log("this._postJson", this._postJson);
  this.backnetHomeService.SaveBacNetConfiguration(this._postJson);
  this.tostr.success('Success! BACnet Configuration is updated successfully.');
}

  subscribeInputFields():void {
    this.formDebug.valueChanges.subscribe(
      (data) => {
        this._postJson.IsDebugLogEnable = data;
        this._formDisabledState = this.formDebug.valid;
        }
      );
  }
  onSearchChange(searchValue : string ) {
    console.log(searchValue);
  }

}
