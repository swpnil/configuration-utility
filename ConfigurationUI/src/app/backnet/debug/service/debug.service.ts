import { Injectable, EventEmitter } from '@angular/core';
import { Http, Response} from '@angular/http';
// import {Observable} from 'rxjs/Observable';
import 'rxjs/Rx';

@Injectable({
  providedIn: 'root'
})
export class DebugService {
  public isConnected: EventEmitter<any> = new EventEmitter();
  configData: any;

  baseUrl:any;
  initLoadApp(){
    this.baseUrl = "http://localhost:3000/Data";
    console.log("this.baseUrl", this.baseUrl);
    // initLoadApp(ipAddress, port){
    // let baseUrl = this.getBaseUrlOnLoad(ipAddress, port)
    // let baseUrlSave = this.getBaseUrlOnSave(ipAddress, port)
    this.http
      .get(this.baseUrl)
      .map((response: Response) => {
        this.configData = response.json();
        console.log("response", response);
      }).subscribe(
        response => {
          this.isConnected.emit(true);
        },
        error => {
          this.isConnected.emit(false);
        }
      );

  }


  getData(): any{
    console.log("this.configData", this.configData)
    return (this.configData);
   }

   getDebug(): any{
    return (this.configData.IsDebugLogEnable);
   }


  get_Debug(): any{
    // return (this.configData.deviceObject);
   }

  constructor(private http : Http) { }

}
