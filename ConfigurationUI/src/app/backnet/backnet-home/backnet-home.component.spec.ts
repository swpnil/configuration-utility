import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BacknetHomeComponent } from './backnet-home.component';

describe('BacknetHomeComponent', () => {
  let component: BacknetHomeComponent;
  let fixture: ComponentFixture<BacknetHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BacknetHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BacknetHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
