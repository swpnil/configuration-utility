import { TestBed } from '@angular/core/testing';

import { BacknetHomeService } from './backnet-home.service';

describe('BacknetHomeService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BacknetHomeService = TestBed.get(BacknetHomeService);
    expect(service).toBeTruthy();
  });
});
