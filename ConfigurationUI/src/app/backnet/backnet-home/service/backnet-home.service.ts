import { Injectable, EventEmitter, ɵConsole } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';

// import {Observable} from 'rxjs/Observable';
import { HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/Rx';

@Injectable({
  providedIn: 'root'
})
export class BacknetHomeService {
  public isConnected: EventEmitter<any> = new EventEmitter();
  public isConnectedBacnet: EventEmitter<any> = new EventEmitter();
  public isConnectedBacnetPost: EventEmitter<any> = new EventEmitter();
  public isConnectedService: EventEmitter<any> = new EventEmitter();

  constructor(private http: Http){

   }

    configData: any;
    bacNetUrl:any;

    // user = { id : 1, name : 'Hello'};

    // ipAddress:any;
    // callServer() {
    //   console.log("Server Called");
    //   this.ipAddress = "192.168.120.02";
    //   const headers = new Headers();
    //   headers.append('Content-Type', 'application/x-www-form-urlencoded');

    //   let postParams = {
    //     type: 'insertbranch',
    //     vendorid: 1,
    //     imgcode: ' ',
    //     address: 'varachhaSurat',
    //     latitude: '0',
    //     longitude: '0',
    //     branchmobile: '9853624221',
    //     gstno: '0',
    //     abc: this.ipAddress
    // }

    //   this.http.put('http://192.168.80.103:5200/setIp', postParams, { headers: headers })
    //   .subscribe(data => {
    //     console.log("Server DATA", data);
    //   }, error => {
    //     console.log("Server ERROR", error);// Error getting the data
    //   });
    // }


    ipAddress:any;
    port:any;
    getbacNetData:any;
    getBacnetServerStarted(ipAddressBacnet, portBacnet) {
      console.log("Server Called");
      this.ipAddress = ipAddressBacnet;
      this.port = portBacnet;
      const headers = new Headers();
      headers.append('Content-Type', 'application/x-www-form-urlencoded');

      let postParams = {
        ip: this.ipAddress,
        port: this.port,
        setModuleName: "Bacnet"
      }

            // .set('Authorization', 'my-auth-token')
            // .set('Content-Type', 'application/json');

      this.http.put('http://localhost:888/setIPBacnet', JSON.stringify(postParams), { headers: headers })
      .subscribe(data => {
        this.getbacNetData = data.json();
        console.log("this.getbacNetData", this.getbacNetData)
        if ( this.getbacNetData.status == true) {
          console.log("Server DATA11111111111", data.json());
          this.isConnectedBacnet.emit(true);
        } else {
          this.isConnectedBacnet.emit(false);
          console.log("Server ERROR");// Error getting the data
        }

      });
    }


    getPostData:any;
    getPostDataSuccess:any;
    SaveBacNetConfiguration(getData) {
      console.log("Server Called", getData);
      this.getPostData = getData;
      const headers = new Headers();
      headers.append('Content-Type', 'application/json');

      let postParams = {
        Status : true,
        JsonData: this.getPostData,
        Error: ""
      }
      // .set('Authorization', 'my-auth-token')
      // .set('Content-Type', 'application/json');
      console.log("Server Called", postParams);
      console.log("Server Called", JSON.stringify(postParams));
      // console.log("Server Called", JSON.parse(postParams));

      // this.http.post('http://localhost:5200/SaveBacNetConfiguration', JSON.stringify(postParams), { headers: headers })
      this.http.post('http://localhost:888/SaveBacNetConfiguration', JSON.stringify(postParams) , { headers: headers })
      .subscribe(data => {
        this.getPostDataSuccess = data.json();
        console.log("this.getPostDataSuccess", this.getPostDataSuccess)
        if ( this.getPostDataSuccess.status == true) {
          console.log("Server DATA11111111111", data.json());
          this.isConnectedBacnetPost.emit(true);
        } else {
          this.isConnectedBacnetPost.emit(false);
          console.log("Server ERROR");// Error getting the data
        }

      });
    }


  getConfigData1(ipAddress, port){
    // this.callServer();
    console.log("ipAddress",ipAddress);
    console.log("port",port);
    // let baseUrl = this.getJsonDataOnLoad(ipAddress, port)
    // let baseUrl = "http://192.168.80.58:7004/GetBacNetConfiguration";
    // let baseUrl = "http://dummy.restapiexample.com/api/v1/employees";
    // let baseUrl = "https://www.reddit.com/r/javascript.json";

    let baseUrl = "http://localhost:2000/test";
    // let baseUrl = "./myJson.json";
    // let baseUrlSave = this.getJsonDataOnSave(ipAddress, port)
    console.log("type of baseUrl",baseUrl)
  // return this.http.get(baseUrl).map((res:Response) => {
  //     console.log("gygygygy",res)
  //     res.json()
  //   });

// this.http.get('http://localhost:3000/test').subscribe((data:any) => {
//     console.log('data', data)
// }, error => {
//     console.log("There was an error generating the proper GUID on the server", error);
// });

  //   this.http.get(baseUrl).subscribe(res => {
  //    console.log("redddddddddddddddddddddd", res)
  //  });

    this.http
    .get(baseUrl)
      .map((response: Response) => {
        this.configData = response;
        console.log("response", response);
      }).subscribe(
        response => {
          console.log("response192.168.80.93", response)
          console.log("response192.168.80.93", this.configData)
          this.isConnected.emit(true);

        },
        error => {
          this.isConnected.emit(false);
        }
      );

  }

  configBacnetData: any;
  getProject: any;
  getProjectsData() {
    this.getProject = 'http://localhost:888' + '/' + 'getBacnetData';

    console.log('this.baseUrl', this.baseUrl);
    this.http
    .get(this.getProject)
    .map((response: Response) => {
      this.configBacnetData = response.json();
      //  this.configBacnetData = response;
      }).subscribe(
        response => {
          // return this._getSuccesPortIP = true;
          // this.configBacnetData = true;
          console.log('response192.168.80.93', response);
          console.log('response192.168.80.93', this.configBacnetData);
          this.isConnected.emit(true);

        },
        error => {
          // this.error = error
          // this.configBacnetData = false;
          this.isConnected.emit(false);
        }
      );
  }


  configGWService: any;
  getProjectGW: any;
  SetBACnetDefaultConfigurations()
  {
    this.getProjectGW = 'http://localhost:888' + '/' + 'SetDefaultConfigurations';

    console.log('this.baseUrl', this.baseUrl);
    this.http
    .get(this.getProjectGW)
    .map((response: Response) => {
      //  this.configGWService = response;
      }).subscribe(
        response => {

        },
        error => {
        }
      );
  }

  RestoreBACnetDefaultConfigurations()
  {
    this.getProjectGW = 'http://localhost:888' + '/' + 'RestoreDefaultConfigurations';

    console.log('this.baseUrl', this.baseUrl);
    this.http
    .get(this.getProjectGW)
    .map((response: Response) => {
      //  this.configGWService = response;
      }).subscribe(
        response => {

        },
        error => {
        }
      );
  }

  GetGWServiceStatus() {
    this.getProjectGW = 'http://localhost:888' + '/' + 'GetGWServiceStatus';

    console.log('this.baseUrl', this.baseUrl);
    this.http
    .get(this.getProjectGW)
    .map((response: Response) => {
      this.configGWService = response.json();
      //  this.configGWService = response;
      }).subscribe(
        response => {
          // return this._getSuccesPortIP = true;
          // this.configGWService = true;
          console.log('response192.168.80.93', response);
          console.log('response192.168.80.93', this.configGWService);
          this.isConnectedService.emit(true);

        },
        error => {
          // this.error = error
          // this.configGWService = false;
          this.isConnectedService.emit(false);
        }
      );
  }

SetGatewayServiceStatus(servicestatus)
{
  console.log("Server Called", servicestatus);
  const headers = new Headers();
  headers.append('Content-Type', 'application/json');

  let postParams = {
    Status : true,
    JsonData: {"State":servicestatus},
    Error: ""
  }
  this.http.post('http://localhost:888/ToggleGWServiceStatus', JSON.stringify(postParams) , { headers: headers })
  .subscribe(data => {
    this.getPostDataSuccess = data.json();
    console.log("this.getPostDataSuccess", this.getPostDataSuccess)
    if ( this.getPostDataSuccess.status == true) {
      console.log("Server DATA11111111111", data.json());
      this.isConnectedBacnetPost.emit(true);
    } else {
      this.isConnectedBacnetPost.emit(false);
      console.log("Server ERROR");// Error getting the data
    }

  });
}

  baseUrl:any;
  getJsonDataOnLoad(ipAddress, port): any{

      console.log("getJsonData.ipAddress", typeof ipAddress, ipAddress)
      console.log("getJsonData.port", typeof port, port)
      this.baseUrl = "http://" + ipAddress + ":" + port + "/" + "GetBacNetConfiguration";

      console.log("this.baseUrl", this.baseUrl)
      // this.getConfigData1(this.baseUrl)
      // this.baseUrl = "http://localhost:5200/api/getConfig/";
      return (this.baseUrl);
    }

    // getJsonDataOnSave(ipAddress, port): any{
    //   console.log("getJsonData.ipAddress", ipAddress)
    //   console.log("getJsonData.port", port)
    //   this.baseUrlSave = "http://" + ipAddress + ":" + port + "/" + "api/saveConfig/";

    //   console.log("this.baseUrlSave", this.baseUrlSave)
    //   return (this.baseUrlSave);
    // }


  // GET Full JSON
  getJsonData(): any{
    // return (this.configData);
    return (this.configBacnetData);
   }

  getGWServiceData(): any{
    // return (this.configData);
    return (this.configGWService);
   }

   getData(): any{
     return (this.configData);
    }

    getNetwork(): any{
      console.log("this.00000000000000000000000000000000000000000000000000", this.configData.NetworkInterface);
      return (this.configData.NetworkInterface);
   }

   get_Network(): any{
    // return (this.configData.deviceObject);
   }

   get_Gateway(): any{
    // return (this.configData.deviceObject);
   }
   get_Device(): any{
    // return (this.configData.deviceObject);
   }
   get_Broadcast(): any{
    // return (this.configData.deviceObject);
   }
   get_Bms(): any{
    // return (this.configData.deviceObject);
   }
   get_Communication(): any{
    //return (this.configData.comm);
   }
   get_Security(): any{
    // return (this.configData.deviceObject);
   }
   get_Debug(): any{
    // return (this.configData.deviceObject);
   }
   get_Notification(): any{
    // return (this.configData.deviceObject);
   }


  //  login() {

  //   console.log('Login ************')
  //   // var ipAddressBacnet = "192.168.80.58";
  //   // var ipAddressPort = "7004";
  //   // var baseUrl = "http://localhost:2000/initial?ip="+ipAddressBacnet+"&port="+ipAddressPort;
  //   var url = "http://localhost:2000/abc";
  //   this.http
  //   .get(url)
  //   .map((response: Response) => {
  //     console.log('Response', response)
  //   });

  //  }


   initLoadApp(ipAddress, port):any{

    // this.callServer();
    this.baseUrl = "http://" + this.ipAddress + ":" + this.port + "/" + "GetBacNetConfiguration";
console.log("Base URL : ",this.baseUrl);

    // this.bacNetUrl = "/GetBacNetConfiguration";
    // console.log("this.baseUrl", this.baseUrl);
    // initLoadApp(ipAddress, port){
    // let baseUrl = this.getBaseUrlOnLoad(ipAddress, port)
    // let baseUrlSave = this.getBaseUrlOnSave(ipAddress, port)
    this.http
      .get(this.baseUrl)
      .map((response: Response) => {
        this.configData = response.json();
        console.log("response", this.configData);
      }).subscribe(
        response => {
          this.isConnected.emit(true);
        },
        error => {
         console.log("error");
          this.isConnected.emit(false);
        }
      );

  }

}
