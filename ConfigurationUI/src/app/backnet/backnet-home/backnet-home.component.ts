import { Component, OnInit, HostListener } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
// import sampleData from './data.json';

import {ApiService} from '../../service/api.service';
import {BacknetHomeService} from './service/backnet-home.service';

@Component({
  selector: 'app-backnet-home',
  templateUrl: './backnet-home.component.html',
  styleUrls: ['./backnet-home.component.css']
})
export class BacknetHomeComponent implements OnInit {

  @HostListener('window:resize', ['$event'])
  onResize() {
    this.myInnerHeight = window.innerHeight;
    console.log("!@@@@@@!@!!!!!!!!!!!!!!!!", this.myInnerHeight)
  }


  loginStatusBtn:boolean = false;
  onChange(val, id){
    if((val.target.value).length != 0){
      this.loginStatusBtn = true;
    }
    console.log("val", val.target.value)
    console.log("val", (val.target.value).length)
  }

  addToList(selectedParameter){
    this.isselected = selectedParameter;
  }

  isselected: any;
  show_dialog:boolean = true;
  isDataAvailable:boolean = false;
  showBacNetLoginPopUP:boolean;
  currentIPBacnet:any;
  currentPortBacnet:any;

  public myInnerHeight: any;

  ngOnInit() {

    this.myInnerHeight= window.innerHeight;
    console.log("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&", this.myInnerHeight)
    this.currentIPBacnet = localStorage.getItem('ipAddressBacNet');
    this.currentPortBacnet = localStorage.getItem('portBacNet');
    if (this.currentIPBacnet == null && this.currentPortBacnet == null) {
      console.log("if");
      this.showBacNetLoginPopUP = true;
    } else {
      console.log("else");
      this.showBacNetLoginPopUP = false;
      this.addBACnetAddress(this.currentIPBacnet, this.currentPortBacnet);
      this.router.navigateByUrl('/bacnet/networkInterface');
    }
      console.log("LOCAL STORAG", this.currentIPBacnet, this.currentPortBacnet);
      console.log("LOCAL STORAG", typeof this.currentIPBacnet, typeof this.currentPortBacnet);
  }

  constructor(private fb: FormBuilder, private apiService : ApiService, private backnetHomeService : BacknetHomeService, private http: Http, private router: Router) {
    this.onResize();
  }



  baseUrl: any;
  baseUrlSave: any;
  _postsJson : any;
  allConfigList = ["Gateway Settings", "Device Object Settings", "Notification Settings", "Broadcast Settings", "BMS Control Settings", "Communication Settings", "Security Settings"]
  isConnectedValue:boolean =true;

  addBACnetAddress(ipAddress,port){
    // this.backnetHomeService.login();
    console.log("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@",ipAddress);
    console.log("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@",port);

    this.baseUrl = "http://" + ipAddress + ":" + port + "/" + "api/getConfig/";

    // this.backnetHomeService.getConfigData1(ipAddress, port);
    this.backnetHomeService.getBacnetServerStarted(ipAddress, port);

    this.backnetHomeService.isConnectedBacnet.subscribe( result => {
      if (result) {

        setTimeout(() => {
          console.log("FFFFFFFFF");
          this.backnetHomeService.getProjectsData();
          this.getJsonConfig();
          // this.backnetHomeService.GetGWServiceStatus();
          // this.getGWServiceData();
          this.showBacNetLoginPopUP = false;
        },1000);
        localStorage.setItem('PopupBacNet', 'false');
        localStorage.setItem('ipAddressBacNet', ipAddress);
        localStorage.setItem('portBacNet', port);
      } else {
        this.isConnectedValue = false;
        this.showBacNetLoginPopUP = true;
      }
    });

    this.backnetHomeService.isConnectedService.subscribe( result => {
      console.log("isConnectedService", result)
      if (result) {
        setTimeout(() => {
          this.getJsonConfig();
          this.getGWServiceData();
        },1000);
      } else {
      }
    });
  }



  _postsJson1:any;
    getJsonConfig(){
          const resultJson = this.backnetHomeService.getJsonData();
          this._postsJson = resultJson;
          console.log("this._postsJson$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$", this._postsJson);

  };

  _postsService:any;
  getGWServiceData(){
    const resultJson = this.backnetHomeService.getGWServiceData();
    this._postsService = resultJson;
    console.log("this._postsJson$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$", this._postsService);
  }


  toggle() {
    this.show_dialog = !this.show_dialog;
  }

}
