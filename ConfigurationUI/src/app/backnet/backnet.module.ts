import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';


import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import {TooltipModule} from "ngx-tooltip";
import { TooltipDirective } from './directive/tooltip.directive';

import { BacknetRoutingModule } from './backnet.routing.module';

import { BacknetHomeComponent } from './backnet-home/backnet-home.component';
import { GatewayComponent } from './gateway/gateway.component';
import { DeviceComponent } from './device/device.component';
import { NotificationComponent } from './notification/notification.component';
import { BroadcastComponent } from './broadcast/broadcast.component';
import { BmsControlComponent } from './bms-control/bms-control.component';
import { CommunicationComponent } from './communication/communication.component';
import { SecurityComponent } from './security/security.component';
import { ObjectmappingComponent } from './objectmapping/objectmapping.component';
import { DebugComponent } from './debug/debug.component';
import { HelpComponent } from './help/help.component';
import { AboutComponent } from './about/about.component';
import { DefaultConfigurationComponent } from './default-configuration/default-configuration.component';
import { GateWayStatusComponent } from './gate-way-status/gate-way-status.component';
import { NetworkInterfaceComponent } from './network-interface/network-interface.component';
import { PasswordSettingDirective } from './security/directive/password-setting.directive';



@NgModule({
  declarations: [BacknetHomeComponent, GatewayComponent, DeviceComponent, NotificationComponent, BroadcastComponent, BmsControlComponent, CommunicationComponent, SecurityComponent, ObjectmappingComponent, DebugComponent, HelpComponent, AboutComponent, DefaultConfigurationComponent, GateWayStatusComponent, TooltipDirective, NetworkInterfaceComponent, PasswordSettingDirective],
  imports: [
    CommonModule,
    BacknetRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    TooltipModule,
    HttpClientModule

  ]
})
export class BacknetModule { }
