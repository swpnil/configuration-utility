import { Component, OnInit, ViewChild, ElementRef, NgZone, SimpleChanges, ViewChildren, Input} from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { ToastrService } from 'ngx-toastr';

import { BroadcastService } from './service/broadcast.service';
import {BacknetHomeService} from '../backnet-home/service/backnet-home.service';
// import { PasswordSettingDirective } from './directive/password-setting.directive';

import {} from '@agm/core/services/google-maps-types';
declare let google: any;

@Component({
  selector: 'app-broadcast',
  templateUrl: './broadcast.component.html',
  styleUrls: ['./broadcast.component.css']
})
export class BroadcastComponent implements OnInit {

buttonDisabled: boolean = true;
isDataAvailable:boolean=false;

typeBCT= "password";
showBBMD:any;
 toggleBBMDShow(value)
  {
      if (value == true){
          this.showBBMD = true;
        this._postJson.Broadcast.IsBBMD = true;
      }
      else {

          this.showBBMD = false;
        this._postJson.Broadcast.IsBBMD = false;
      }
  }

reRegisterDiv:any;

 reRegister(val)
  {
    console.log("val",val.target.checked)
      // this.reRegisterDiv = !this.reRegisterDiv;
    this.reRegisterDiv = val.target.checked;
    if (this.reRegisterDiv == true){
      console.log("this.reRegisterDiv", this.reRegisterDiv)
      this._postJson.Broadcast.FDReRegister = this.reRegisterDiv;
    }
    else {
        // this.typeAPI = "password";
      console.log("this.reRegisterDiv", this.reRegisterDiv)
      this._postJson.Broadcast.FDReRegister = this.reRegisterDiv;
    }
  }

public formBroadcast: FormGroup;
  _postsArrayBroadcast:any;

  _formDisabledState:boolean = true;
  title = "Broadcast Settings "

  ipPattern = "(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)";


  optionsApi:any;
  cancel() {
    // this.formBroadcast.get('FDIPAddress').setValue(this.optionsBroadcast.FDTModel.FDIPAddress);
    // this.formBroadcast.get('PortFDT').setValue(this.optionsBroadcast.FDTModel.PortFDT);
    // this.formBroadcast.get('TimeToLiveFDT').setValue(this.optionsBroadcast.FDTModel.TimeToLiveFDT);

    this.formBroadcast.get('FDIPAddress').setValue(this.optionsBroadcast.FDIPAddress);
    this.formBroadcast.get('FDUDPPort').setValue(this.optionsBroadcast.FDUDPPort);
    this.formBroadcast.get('FDTimeToLive').setValue(this.optionsBroadcast.FDTimeToLive);

     this.formBroadcast.get('IPAddress').setValue(this.optionsBroadcast.BDTList.IPAddress);
    this.formBroadcast.get('Port').setValue(this.optionsBroadcast.BDTList.Port);
    this.formBroadcast.get('BroadcastMask').setValue(this.optionsBroadcast.BDTList.BroadcastMask);

    this.formBroadcast.get('TimeSynchronization').setValue(this.optionsBroadcast.TimeSynchronization);
    this.formBroadcast.get('Scheduling').setValue(this.optionsBroadcast.Scheduling);
    this.formBroadcast.get('MapIndividualSensors').setValue(this.optionsBroadcast.MapIndividualSensors);
    this.formBroadcast.get('MapIndividualFixture').setValue(this.optionsBroadcast.MapIndividualFixture);
    this.formBroadcast.get('CharacterEncodingTypes').setValue(this.optionsBroadcast.CharacterEncodingTypes);
    this.formBroadcast.get('IsTempRequiredInFahrenheit').setValue(this.optionsBroadcast.IsTempRequiredInFahrenheit);
    // this.formBroadcast.get('LocalBroadcastNumber').setValue(this.optionsBroadcast.LocalBroadcastNumber);
   }

  private BroadcastComponentSer: BroadcastComponent[] = [];
  private BroadcastComponentObservable : Observable<BroadcastComponent[]> ;

  feedbackForm: FormGroup;
  buildings = []

  constructor(private fb: FormBuilder, private broadcastService: BroadcastService, private backnetHomeService: BacknetHomeService, private http: Http,private tostr: ToastrService) {
    this.BroadcastComponentObservable = this.backnetHomeService.get_Broadcast();
   }

  baseUrlJSON = "http://localhost:3000";
  baseUrlSave = "http://localhost:3000";

  private BroadcastInterfaceComponent  = [];

  get_BroadcastComponent(){
    this.http.get(this.baseUrlJSON + '/data').subscribe((res)=>{
        console.log(res.json());
        this.BroadcastInterfaceComponent = res.json();
        console.log(this.BroadcastInterfaceComponent);
    });
}

  isDisabled = true;
  currentIPAddress:any;
  currentPort:any;

  removeLanguague(languague, index){
    this.optionsBdtlist.splice(index, 1);
}

showBacNetLoginPopUP:boolean;
  currentIPBacnet:any;
  currentPortBacnet:any;

  ngOnInit() {
    this.currentIPBacnet = localStorage.getItem('ipAddressBacNet');
    this.currentPortBacnet = localStorage.getItem('portBacNet');
    if (this.currentIPBacnet == null && this.currentPortBacnet == null) {
      console.log("if");
      this.showBacNetLoginPopUP = true;
    }

    this.backnetHomeService.getProjectsData();

    // console.log("1111111111111",this.myDiv.nativeElement);
    // console.log("2222222222222222222",this.myDiv2.nativeElement);

    // this.currentIPAddress = localStorage.getItem('ipAddress');
    // this.currentPort = localStorage.getItem('port');
    setTimeout(() => {
      // this.initBroadcast();
      this.formBroadcast = this.fb.group({
        IPAddress: ['', [Validators.required]],
        Port: ['', [Validators.required]],
        BroadcastMask: ['', [Validators.required]],
        FDIPAddress: ['', [Validators.required]],
        PortFDT: ['', [Validators.required]],
        TimeToLiveFDT: ['', [Validators.required]],

        // FDIPAddress: ['', [Validators.required]],
        FDUDPPort: ['', [Validators.required]],
        FDTimeToLive: ['', [Validators.required]],

        TimeSynchronization: ['', [Validators.required]],
        Scheduling: ['', [Validators.required]],
        MapIndividualSensors: ['', [Validators.required]],
        MapIndividualFixture: ['', [Validators.required]],
        CharacterEncodingTypes: ['', [Validators.required]],
        IsTempRequiredInFahrenheit: ['', [Validators.required]],
        // LocalBroadcastNumber: ['', [Validators.required]],
      });
    }, 1000);

    this.backnetHomeService.isConnected.subscribe( result => {
      if (result) {
        setTimeout(() => {
          this.isDataAvailable = true;
          this.getDataConfig();
          this.getBroadcast();
          this.subscribeInputFields();
        }, 1000);
      }
    });

  }

  enaDisClicked(value){
    console.log("dsdsdsdsdsd", this._postJson.BroadcastObject.DaylightSavingStatus);
    this.isDisabled = value;
    this._postJson.BroadcastObject.DaylightSavingStatus = value;
  }

  LocalBroadcastNumberClick(value){
    console.log(value);
    this._postJson.FDTModel.IPAddress.LocalBroadcastNumber = value;
    // this.order.type=value;
  }

  selectStatus(selectStatus, selectedNum){
    let array = this._postJson.BroadcastInterface.IPAddressList;

    for (let i = 0; i < array.length; i++) {
      if(selectedNum == i){
        this._postJson.BroadcastInterface.IPAddressList[i].IsSelected = selectStatus;
      }else{
        this._postJson.BroadcastInterface.IPAddressList[i].IsSelected = false;
      }
    }
    // this._postJson.BroadcastInterface.IPAddressList[i].IsSelected = selectStatus;
    // this.order.type=value;
  }

  baseUrl:any;
  loginModelStatus:any
  loginModelStatusValue:boolean = true;
  isConnectedValue:boolean =true;
  localStorageValue:boolean;
  buildingConfiguration:boolean = false;

  initBroadcast(){
    this.broadcastService.initLoadApp();
    this.broadcastService.isConnected.subscribe( result => {
      console.log("isConnected", result)
      this.isConnectedValue = result
      this.loginModelStatusValue = !result;
      if (result) {
        this.getDataConfig();
        this.getBroadcast();
        this.subscribeInputFields();
        // localStorage.setItem('dashBoardPopupDashboard', 'false');
        // localStorage.setItem('ipAddressDashboard', ipAddress);
        // localStorage.setItem('portDashboard', port);
      }
    });
  }

  getSmartCoreValue: any;
  SmartCore: any;
  buildingValue:any;
  optionsSmartCore:any;
  _postsArraySmartCore:any;
  _SmartCoreoptions:any;
  // systemValues:any;


    get repliesControl() {
      // console.log("this.feedbackForm", this.feedbackForm)
      return this.feedbackForm.get('buildings');
    }

  _postJson:any;
  getDataConfig(): void{
    // const resultJson = this.backnetHomeService.getData()
    // this._postJson = resultJson.JsonData;
    const resultJson = this.backnetHomeService.getJsonData();
    // this._postJson = resultJson.JsonData;
    this._postJson = resultJson.result.data[0];
    console.log("BROADCAST", this._postJson );
    // this.showBBMD = this._postJson.Broadcast.ISBBMD;
}

optionsBroadcast:any;
IPAddressList:any;
optionsBdtlist:any;
getBroadcast(): void{
    // const resultArray = this.backnetHomeService.getBroadcast();
    this._postsArrayBroadcast = this._postJson.Broadcast;
    var data = [];
    data.push(this._postsArrayBroadcast);
    this._postsArrayBroadcast = data;
    console.log("this._postsArrayBroadcast", this._postsArrayBroadcast)
    this.optionsBroadcast = this._postsArrayBroadcast[0];

    console.log("this.optionsBroadcast", this.optionsBroadcast)
    this.optionsBdtlist = this._postsArrayBroadcast[0].BDTList;

    this.showBBMD = this.optionsBroadcast.IsBBMD;
    // this.reRegisterDiv = this.optionsBroadcast.FDTModel.FDReRegisterFDT;
    this.reRegisterDiv = this.optionsBroadcast.FDReRegister;
    console.log("this", this.showBBMD)
    console.log("this.reRegisterDiv", this.reRegisterDiv)


    console.log("this.optionsBroadcast@@@@@@@@@@@@@@@@@@@@@@@@@", this.optionsBroadcast);
    console.log("this.optionsBdtlist@@@@@@@@@@@@@@@@@@@@@@@@@", this.optionsBroadcast);
    // this.formBroadcast.get('FDIPAddress').setValue(this.optionsBroadcast.FDTModel.FDIPAddress);
    // this.formBroadcast.get('PortFDT').setValue(this.optionsBroadcast.FDTModel.PortFDT);
    // this.formBroadcast.get('TimeToLiveFDT').setValue(this.optionsBroadcast.FDTModel.TimeToLiveFDT);

    this.formBroadcast.get('FDIPAddress').setValue(this.optionsBroadcast.FDIPAddress);
    this.formBroadcast.get('FDUDPPort').setValue(this.optionsBroadcast.FDUDPPort);
    this.formBroadcast.get('FDTimeToLive').setValue(this.optionsBroadcast.FDTimeToLive);

    this.formBroadcast.get('IPAddress').setValue(this.optionsBroadcast.BDTList.IPAddress);
    this.formBroadcast.get('Port').setValue(this.optionsBroadcast.BDTList.Port);
    this.formBroadcast.get('BroadcastMask').setValue(this.optionsBroadcast.BDTList.BroadcastMask);

    this.formBroadcast.get('TimeSynchronization').setValue(this.optionsBroadcast.TimeSynchronization);
    this.formBroadcast.get('Scheduling').setValue(this.optionsBroadcast.Scheduling);
    this.formBroadcast.get('MapIndividualSensors').setValue(this.optionsBroadcast.MapIndividualSensors);
    this.formBroadcast.get('MapIndividualFixture').setValue(this.optionsBroadcast.MapIndividualFixture);
    this.formBroadcast.get('CharacterEncodingTypes').setValue(this.optionsBroadcast.CharacterEncodingTypes);
    this.formBroadcast.get('IsTempRequiredInFahrenheit').setValue(this.optionsBroadcast.IsTempRequiredInFahrenheit);
    // this.formBroadcast.get('LocalBroadcastNumber').setValue(this.optionsBroadcast.LocalBroadcastNumber);
   }


  get fBroadcast() { return this.formBroadcast.controls; }

submitBroadcastComp():void{
  console.log("this.baseUrlSave", this.baseUrlSave);
  console.log("this._postJson", this._postJson);
  this.backnetHomeService.SaveBacNetConfiguration(this._postJson);
  this.tostr.success('Success! BACnet Configuration is updated successfully.');
}

  subscribeInputFields():void {
    this.formBroadcast.valueChanges.subscribe(
      (data) => {
        this._postJson.Broadcast = data;
        this._formDisabledState = this.formBroadcast.valid;
        }
      );
  }
  onSearchChange(searchValue : string ) {
    console.log(searchValue);
  }


}
