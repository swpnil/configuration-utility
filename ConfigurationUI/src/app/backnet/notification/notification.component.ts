import { Component, OnInit, ViewChild, ElementRef, NgZone, SimpleChanges, ViewChildren, Input} from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { ToastrService } from 'ngx-toastr';

import { NotificationService } from './service/notification.service';

import {BacknetHomeService} from '../backnet-home/service/backnet-home.service';
// import { PasswordSettingDirective } from './directive/password-setting.directive';

import {} from '@agm/core/services/google-maps-types';
declare let google: any;

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.css']
})
export class NotificationComponent implements OnInit {

  molexAlarmTypeValue =["Alarm", "Event"];

  DataPublishClick(value){
    console.log(value);
    this._postJson.TimeDelay = value;
    // this.order.type=value;
  }

  floorAlarm(val)
  {
    console.log("val",val.target.checked)
    console.log("val",this._postJson)
      // this.floorLevelAlarm = !this.floorLevelAlarm;
    this.floorLevelAlarm = val.target.checked;
    if (this.floorLevelAlarm == true){
      console.log("this.floorLevelAlarm", this.floorLevelAlarm)
      this._postJson.Notification.IsFloorAlarm = this.floorLevelAlarm;
    }
    else {
        // this.typeAPI = "password";
      console.log("this.floorAlarmCheckBox", this.floorLevelAlarm)
      this._postJson.Notification.IsFloorAlarm = this.floorLevelAlarm;
    }
  }

  // this.buildingLevelAlarm = this._postJson.IsFloorAlarm;
  // this.buildingLevelAlarm = this._postJson.IsBuildingAlarm;

  buildingAlarm(val)
  {
    console.log("val",val.target.checked)
    console.log("val",this._postJson)
      // this.buildingLevelAlarm = !this.buildingLevelAlarm;
    this.buildingLevelAlarm = val.target.checked;
    if (this.buildingLevelAlarm == true){
      console.log("this.buildingLevelAlarm", this.buildingLevelAlarm)
      this._postJson.Notification.IsBuildingAlarm = this.buildingLevelAlarm;
    }
    else {
        // this.typeAPI = "password";
      console.log("this.floorAlarmCheckBox", this.buildingLevelAlarm)
      this._postJson.Notification.IsBuildingAlarm = this.buildingLevelAlarm;
    }
  }

  // toggleUserAlarm(val){
  //   console.log("VALL", val)
  //   showUserAlarm = val
  // }

  public showUserAlarm:boolean = false;
  public buttonNameUserAlarm:any = 'Show';
  toggleUserAlarm() {
    this.showUserAlarm = !this.showUserAlarm;
    // CHANGE THE NAME OF THE BUTTON.
    if(this.showUserAlarm)
      this.buttonNameUserAlarm = "Hide";
    else
      this.buttonNameUserAlarm = "Show";
  }

  buttonDisabled: boolean = true;



  public formNotification: FormGroup;
    _postsArrayNotification:any;

    isDataAvailable:boolean = false;
    _formDisabledState:boolean = true;
    title = "Notification Settings"

    ipPattern = "(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)";


    optionsApi:any;
    cancel() {
      this.formNotification.get('TimeDelay').setValue(this.optionsNotification.TimeDelay);
      this.formNotification.get('WriteAccess').setValue(this.optionsNotification.WriteAccess);
      this.formNotification.get('BACnetPassword').setValue(this.optionsNotification.BACnetPassword);
      this.formNotification.get('TimeSynchronization').setValue(this.optionsNotification.TimeSynchronization);
      this.formNotification.get('Scheduling').setValue(this.optionsNotification.Scheduling);
      this.formNotification.get('MapIndividualSensors').setValue(this.optionsNotification.MapIndividualSensors);
      this.formNotification.get('MapIndividualFixture').setValue(this.optionsNotification.MapIndividualFixture);
      this.formNotification.get('CharacterEncodingTypes').setValue(this.optionsNotification.CharacterEncodingTypes);
      this.formNotification.get('IsTempRequiredInFahrenheit').setValue(this.optionsNotification.IsTempRequiredInFahrenheit);
      // this.formNotification.get('LocalNotificationNumber').setValue(this.optionsNotification.LocalNotificationNumber);
     }

    private NotificationComponentSer: NotificationComponent[] = [];
    private NotificationComponentObservable : Observable<NotificationComponent[]> ;

    feedbackForm: FormGroup;
    buildings = []

    constructor(private fb: FormBuilder, private notificationService: NotificationService, private http: Http, private backnetHomeService:BacknetHomeService,private tostr: ToastrService) {
      this.NotificationComponentObservable = this.backnetHomeService.get_Notification();
     }

    baseUrlJSON = "http://localhost:3000";
    baseUrlSave = "http://localhost:3000";

    private NotificationInterfaceComponent  = [];

    get_NotificationComponent(){
      this.http.get(this.baseUrlJSON + '/data').subscribe((res)=>{
          console.log(res.json());
          this.NotificationInterfaceComponent = res.json();
          console.log(this.NotificationInterfaceComponent);
      });
  }

    isDisabled = true;
    currentIPAddress:any;
    currentPort:any;

    showBacNetLoginPopUP:boolean;
    currentIPBacnet:any;
    currentPortBacnet:any;

    alarmFloorValue: any;
    floorLevelAlarm: any;
    alarmBuildingValue: any;
    buildingLevelAlarm: any;


    ngOnInit() {

      this.currentIPBacnet = localStorage.getItem('ipAddressBacNet');
      this.currentPortBacnet = localStorage.getItem('portBacNet');
      if (this.currentIPBacnet == null && this.currentPortBacnet == null) {
        console.log("if");
        this.showBacNetLoginPopUP = true;
      }
      // this.backnetHomeService.initLoadApp();

      this.backnetHomeService.getProjectsData();
      // console.log("1111111111111",this.myDiv.nativeElement);
      // console.log("2222222222222222222",this.myDiv2.nativeElement);

      // this.currentIPAddress = localStorage.getItem('ipAddress');
      // this.currentPort = localStorage.getItem('port');
      setTimeout(() => {
        // this.initNotification();
        this.formNotification = this.fb.group({
          TimeDelay: ['', [Validators.required]],
          BACnetPassword: ['', [Validators.required]],
          WriteAccess: ['', [Validators.required]],
          TimeSynchronization: ['', [Validators.required]],
          Scheduling: ['', [Validators.required]],
          MapIndividualSensors: ['', [Validators.required]],
          MapIndividualFixture: ['', [Validators.required]],
          CharacterEncodingTypes: ['', [Validators.required]],
          IsTempRequiredInFahrenheit: ['', [Validators.required]],
          // LocalNotificationNumber: ['', [Validators.required]],
        });
      }, 1000);

      this.backnetHomeService.isConnected.subscribe( result => {
        if (result) {
          setTimeout(() => {
            this.isDataAvailable = true;
            this.getDataConfig();
            this.getNotification();
            this.subscribeInputFields();
          }, 1000);
        }
      });

    }

    enaDisClicked(value){
      console.log("dsdsdsdsdsd", this._postJson.NotificationObject.DaylightSavingStatus);
      this.isDisabled = value;
      this._postJson.NotificationObject.DaylightSavingStatus = value;
    }

    LocalNotificationNumberClick(value){
      console.log(value);
      this._postJson.TimeDelay.LocalNotificationNumber = value;
      // this.order.type=value;
    }

    selectStatus(selectStatus, selectedNum){
      let array = this._postJson.NotificationInterface.IPAddressList;

      for (let i = 0; i < array.length; i++) {
        if(selectedNum == i){
          this._postJson.NotificationInterface.IPAddressList[i].IsSelected = selectStatus;
        }else{
          this._postJson.NotificationInterface.IPAddressList[i].IsSelected = false;
        }
      }
      // this._postJson.NotificationInterface.IPAddressList[i].IsSelected = selectStatus;
      // this.order.type=value;
    }

    baseUrl:any;
    loginModelStatus:any
    loginModelStatusValue:boolean = true;
    isConnectedValue:boolean =true;
    localStorageValue:boolean;
    buildingConfiguration:boolean = false;

    initNotification(){
      this.notificationService.initLoadApp();
      this.notificationService.isConnected.subscribe( result => {
        console.log("isConnected", result)
        this.isConnectedValue = result
        this.loginModelStatusValue = !result;
        if (result) {
          this.getDataConfig();
          this.getNotification();
          this.subscribeInputFields();
          // localStorage.setItem('dashBoardPopupDashboard', 'false');
          // localStorage.setItem('ipAddressDashboard', ipAddress);
          // localStorage.setItem('portDashboard', port);
        }
      });
    }

    getSmartCoreValue: any;
    SmartCore: any;
    buildingValue:any;
    optionsSmartCore:any;
    _postsArraySmartCore:any;
    _SmartCoreoptions:any;
    // systemValues:any;


      get repliesControl() {
        // console.log("this.feedbackForm", this.feedbackForm)
        return this.feedbackForm.get('buildings');
      }

    _postJson:any;
    getDataConfig(): void{
      // const resultJson = this.backnetHomeService.getData();
      // this._postJson = resultJson.JsonData;
      const resultJson = this.backnetHomeService.getJsonData();
      // this._postJson = resultJson.JsonData;
      this._postJson = resultJson.result.data[0];
      console.log("Device", this._postJson );
      console.log("this._postJson$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$", this._postJson );
  }

  optionsNotification:any;
  IPAddressList:any;
  allZonesAlarm:any;
  userZones:any;
  generalZones:any;
  beaconZones:any;
  getNotification(): void{
      // const resultArray = this.backnetHomeService.getNotification()
      this._postsArrayNotification = this._postJson.Notification;
      console.log("this._postsArrayNotification", this._postJson);
      var data = [];
      data.push(this._postsArrayNotification);
      this._postsArrayNotification = data;
      console.log("this._postsArrayNotification", this._postsArrayNotification);
      this.optionsNotification = this._postsArrayNotification[0];

      this.buildingLevelAlarm = this._postJson.IsBuildingAlarm;
      this.floorLevelAlarm = this._postJson.IsFloorAlarm;
      if(this.floorLevelAlarm == true){
        this.alarmFloorValue = "Enable";
      }else{
        this.alarmFloorValue = "Disable";
      }

      if(this.buildingLevelAlarm == true){
        this.alarmBuildingValue = "Enable";
      }else{
        this.alarmBuildingValue = "Disable";
      }

      this.allZonesAlarm = this._postJson.Notification.AlarmEventVM.ZoneBasedObjects;

      var zoneData = [];
      zoneData.push(this.allZonesAlarm);
      this.allZonesAlarm = zoneData;
      this.userZones = zoneData[0].UserZone;
      this.generalZones = zoneData[0].GeneralZone;
      this.beaconZones = zoneData[0].BeaconZone;
      console.log("this.allZonesAlarm@#@#@#@#@#@#@", this._postJson.Notification.AlarmEventVM.ZoneBasedObjects);
      console.log("this.allZonesAlarm@#@#@#@#@#@#@", zoneData[0].BeaconZone);

      // this.IPAddressList = this._postsArrayNotification[0].IPAddressList;
      this.formNotification.get('TimeDelay').setValue(this.optionsNotification.TimeDelay);

      this.formNotification.get('BACnetPassword').setValue(this.optionsNotification.BACnetPassword);


      this.formNotification.get('WriteAccess').setValue(this.optionsNotification.WriteAccess);
      this.formNotification.get('TimeSynchronization').setValue(this.optionsNotification.TimeSynchronization);
      this.formNotification.get('Scheduling').setValue(this.optionsNotification.Scheduling);
      this.formNotification.get('MapIndividualSensors').setValue(this.optionsNotification.MapIndividualSensors);
      this.formNotification.get('MapIndividualFixture').setValue(this.optionsNotification.MapIndividualFixture);
      this.formNotification.get('CharacterEncodingTypes').setValue(this.optionsNotification.CharacterEncodingTypes);
      this.formNotification.get('IsTempRequiredInFahrenheit').setValue(this.optionsNotification.IsTempRequiredInFahrenheit);
      // this.formNotification.get('LocalNotificationNumber').setValue(this.optionsNotification.LocalNotificationNumber);
     }


    get fNotification() { return this.formNotification.controls; }

  submitNotificationComp():void{
    console.log("this.baseUrlSave", this.baseUrlSave);
    console.log("this._postJson", this._postJson);
    this.backnetHomeService.SaveBacNetConfiguration(this._postJson);
    this.tostr.success('Success! BACnet Configuration is updated successfully.');
  }

    subscribeInputFields():void {
      this.formNotification.valueChanges.subscribe(
        (data) => {
          this._postJson.Notification = data;
          this._formDisabledState = this.formNotification.valid;
          }
        );
    }
    onSearchChange(searchValue : string ) {
      console.log(searchValue);
    }

}
