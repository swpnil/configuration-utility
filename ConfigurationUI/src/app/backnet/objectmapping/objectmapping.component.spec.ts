import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ObjectmappingComponent } from './objectmapping.component';

describe('ObjectmappingComponent', () => {
  let component: ObjectmappingComponent;
  let fixture: ComponentFixture<ObjectmappingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ObjectmappingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ObjectmappingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
