import { Component, OnInit, ViewChild, ElementRef, NgZone, SimpleChanges } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { ToastrService } from 'ngx-toastr';

import {ApiService} from '../service/ApiService';
import { CommunicationService } from './service/communication.service';
import {BacknetHomeService} from '../backnet-home/service/backnet-home.service';
import {deviceComponent} from '../interface/device.comp.interface'

import { AgmCoreModule, MapsAPILoader, AgmMap, LatLngBounds, MouseEvent } from '@agm/core';
import {} from '@agm/core/services/google-maps-types';
declare let google: any;

@Component({
  selector: 'app-communication',
  templateUrl: './communication.component.html',
  styleUrls: ['./communication.component.css']
})
export class CommunicationComponent implements OnInit {
  
  molexProjectNameValue =["HTTP", "MQTT"]
  MonitoringDefaultPriorityVal=[""]

  public formCommunication: FormGroup;
  _postsArrayCommunication:any;

  isDataAvailable:boolean = false;

  _formDisabledState:boolean = true;
  title = "Communication Object Settings"

  ipPattern = "(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)";


  optionsApi:any;
  cancel() {
    this.formCommunication.get('DeviceId').setValue(this.optionsCommunication.DeviceId);
    this.formCommunication.get('MolexApiUrl').setValue(this.optionsCommunication.MolexApiUrl);
    this.formCommunication.get('MolexProjectName').setValue(this.optionsCommunication.MolexProjectName);
    this.formCommunication.get('MonitoringDefaultPriority').setValue(this.optionsCommunication.MonitoringDefaultPriority);
   }

  private CommunicationComponentSer: CommunicationComponent[] = [];
  private CommunicationComponentObservable : Observable<CommunicationComponent[]> ;

  feedbackForm: FormGroup;
  buildings = []

  constructor(private fb: FormBuilder, private apiService : ApiService, private communicationService: CommunicationService, private backnetHomeService:BacknetHomeService, private http: Http,private tostr: ToastrService){
    this.CommunicationComponentObservable = this.backnetHomeService.get_Communication();
   }

  baseUrlJSON = "http://localhost:3000";
  baseUrlSave = "http://localhost:3000"
  private CommunicationComponent  = [];

  get_CommunicationComponent(){
    this.http.get(this.baseUrlJSON + '/data').subscribe((res)=>{
        console.log(res.json());
        this.CommunicationComponent = res.json();
        console.log(this.CommunicationComponent);
    });
}

  isDisabled = true;
  currentIPAddress:any;
  currentPort:any;
  showBacNetLoginPopUP:boolean;
  currentIPBacnet:any;
  currentPortBacnet:any;

  ngOnInit() {
    this.currentIPBacnet = localStorage.getItem('ipAddressBacNet');
    this.currentPortBacnet = localStorage.getItem('portBacNet');
    if (this.currentIPBacnet == null && this.currentPortBacnet == null) {
      console.log("if");
      this.showBacNetLoginPopUP = true;
    }
    // this.backnetHomeService.initLoadApp();
    this.backnetHomeService.getProjectsData();
    // this.currentIPAddress = localStorage.getItem('ipAddress');
    // this.currentPort = localStorage.getItem('port');
    setTimeout(() => {
      // this.initCommunication();
      this.formCommunication = this.fb.group({
        DeviceId: ['', [Validators.required]],
        MolexApiUrl: ['', [Validators.required]],
        MolexProjectName: ['', [Validators.required]],
        MonitoringDefaultPriority: ['', [Validators.required, Validators.minLength(5)]],
      });
    }, 1000);

    this.backnetHomeService.isConnected.subscribe( result => {
      if (result) {
        setTimeout(() => {
          this.isDataAvailable = true;
          this.getDataConfig();
          this.getCommunication();
          this.subscribeInputFields();
        }, 1000);
      }
    });

  }

  enaDisClicked(value){
    console.log("dsdsdsdsdsd", this._postJson.CommunicationObject.DaylightSavingStatus);
    this.isDisabled = value;
    this._postJson.CommunicationObject.DaylightSavingStatus = value;
  }

  DataPublishClick(value){
    console.log(value);
    this._postJson.Communication.CurrentModule = value;
    // this.order.type=value;
  }

  MonitoringDefaultPriority(value){
    console.log(value);
    this._postJson.Communication.MonitoringDefaultPriority = value;
    // this.order.type=value;
  }

  baseUrl:any;
  loginModelStatus:any
  loginModelStatusValue:boolean = true;
  isConnectedValue:boolean =true;
  localStorageValue:boolean;
  buildingConfiguration:boolean = false;

  initCommunication(){
    this.communicationService.initLoadApp();
    this.communicationService.isConnected.subscribe( result => {
      console.log("isConnected", result)
      this.isConnectedValue = result
      this.loginModelStatusValue = !result;
      if (result) {
        this.getDataConfig();
        this.getCommunication();
        this.subscribeInputFields();
        // localStorage.setItem('dashBoardPopupDashboard', 'false');
        // localStorage.setItem('ipAddressDashboard', ipAddress);
        // localStorage.setItem('portDashboard', port);
      }
    });
  }

  getSmartCoreValue: any;
  SmartCore: any;
  buildingValue:any;
  optionsSmartCore:any;
  _postsArraySmartCore:any;
  _SmartCoreoptions:any;
  // systemValues:any;


    get repliesControl() {
      // console.log("this.feedbackForm", this.feedbackForm)
      return this.feedbackForm.get('buildings');
    }

  _postJson:any;
  getDataConfig(): void{
    // const resultJson = this.backnetHomeService.getData()
    // this._postJson = resultJson.JsonData;
    const resultJson = this.backnetHomeService.getJsonData();
    // this._postJson = resultJson.JsonData;
    this._postJson = resultJson.result.data[0];
    console.log("Device", this._postJson );
    console.log("this._postJson$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$", this._postJson );
}

optionsCommunication:any;
getCommunication(): void{
     //const resultArray = this.backnetHomeService.getCommunication()
    this._postsArrayCommunication = this._postJson.Communication;
    var data = [];
    data.push(this._postsArrayCommunication);
    this._postsArrayCommunication = data;
    console.log("this._postsArrayCommunication", this._postsArrayCommunication)
    this.optionsCommunication = this._postsArrayCommunication[0];

   }


  get fCommunication() { return this.formCommunication.controls; }

submitCommunicationComp():void{
  console.log("this.baseUrlSave", this.baseUrlSave);
  console.log("this._postJson", this._postJson);
  this.backnetHomeService.SaveBacNetConfiguration(this._postJson);
  this.tostr.success('Success! BACnet Configuration is updated successfully.');
}

  subscribeInputFields():void {
    this.formCommunication.valueChanges.subscribe(
      (data) => {
        this._postJson.Communication = data;
        this._formDisabledState = this.formCommunication.valid;
        }
      );
  }
  onSearchChange(searchValue : string ) {
    console.log(searchValue);
  }

}



