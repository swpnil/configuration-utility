import { Component, OnInit, ViewChild, ElementRef, NgZone, SimpleChanges } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { ToastrService } from 'ngx-toastr';

import {ApiService} from '../service/ApiService';
import { GatewayStatusService } from './service/gateway-status.service';
import {BacknetHomeService} from '../backnet-home/service/backnet-home.service';
import {deviceComponent} from '../interface/device.comp.interface'

import { AgmCoreModule, MapsAPILoader, AgmMap, LatLngBounds, MouseEvent } from '@agm/core';
import {} from '@agm/core/services/google-maps-types';
declare let google: any;

@Component({
  selector: 'app-gate-way-status',
  templateUrl: './gate-way-status.component.html',
  styleUrls: ['./gate-way-status.component.css']
})
export class GateWayStatusComponent implements OnInit {
  title = "Gateway Status Setting"

  ServiceStatus = ["Start Gateway Service", " Stop Gateway Service"]

  public formGatewayStatus: FormGroup;
  _postsArrayGatewayStatus:any;

  isDataAvailable:boolean = false;
  _formDisabledState:boolean = true;

  gatewayTxt:any;

  optionsApi:any;
  cancel() {
    // this.formGatewayStatus.get('DeviceId').setValue(this.optionsGatewayStatus.DeviceId);
    // this.formGatewayStatus.get('MolexApiUrl').setValue(this.optionsGatewayStatus.MolexApiUrl);
    // this.formGatewayStatus.get('MolexProjectName').setValue(this.optionsGatewayStatus.MolexProjectName);
    // this.formGatewayStatus.get('MonitoringDefaultPriority').setValue(this.optionsGatewayStatus.MonitoringDefaultPriority);
   }

  private GatewayStatusComponentSer: GateWayStatusComponent[] = [];
  private GatewayStatusComponentObservable : Observable<GateWayStatusComponent[]> ;

  feedbackForm: FormGroup;
  buildings = []

  constructor(private fb: FormBuilder, private apiService : ApiService, private gatewayStatusService: GatewayStatusService, private backnetHomeService:BacknetHomeService, private http: Http,private tostr: ToastrService) {
    this.GatewayStatusComponentObservable = this.gatewayStatusService.get_GatewayStatus();
   }

  baseUrlJSON = "http://localhost:3000";
  baseUrlSave = "http://localhost:3000"
  private GatewayStatusComponent  = [];

  get_GatewayStatusComponent(){
    this.http.get(this.baseUrlJSON + '/data').subscribe((res)=>{
        console.log(res.json());
        this.GatewayStatusComponent = res.json();
        console.log(this.GatewayStatusComponent);
    });
}

  currentIPAddress:any;
  currentPort:any;
  showBacNetLoginPopUP:boolean;
  currentIPBacnet:any;
  currentPortBacnet:any;

  ngOnInit() {
    this.currentIPBacnet = localStorage.getItem('ipAddressBacNet');
    this.currentPortBacnet = localStorage.getItem('portBacNet');
    if (this.currentIPBacnet == null && this.currentPortBacnet == null) {
      console.log("if");
      this.showBacNetLoginPopUP = true;
    }
    setTimeout(() => {
      this.initGatewayStatus();
      console.log("GGHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH")
      // this.formGatewayStatus = this.fb.group({
      //   DeviceId: ['', [Validators.required]],
      //   MolexApiUrl: ['', [Validators.required]],
      //   MolexProjectName: ['', [Validators.required]],
      //   MonitoringDefaultPriority: ['', [Validators.required, Validators.minLength(5)]],
      // });
    }, 1000);

  }


  baseUrl:any;
  loginModelStatus:any
  loginModelStatusValue:boolean = true;
  isConnectedValue:boolean =true;
  localStorageValue:boolean;
  buildingGatewayStatus:boolean = false;

  initGatewayStatus(){
    // this.gatewayStatusService.initLoadApp();

    this.backnetHomeService.GetGWServiceStatus();

    this.backnetHomeService.isConnectedService.subscribe( result => {
      console.log("isConnected", result);

      this.isConnectedValue = result
      this.loginModelStatusValue = !result;
      if (result) {
        this.getDataConfig();
        this.getGatewayStatus();
        this.subscribeInputFields();

        // localStorage.setItem('dashBoardPopupDashboard', 'false');
        // localStorage.setItem('ipAddressDashboard', ipAddress);
        // localStorage.setItem('portDashboard', port);
      }
    });
  }

  getSmartCoreValue: any;
  SmartCore: any;
  buildingValue:any;
  optionsSmartCore:any;
  _postsArraySmartCore:any;
  _SmartCoreoptions:any;
  // systemValues:any;


    get repliesControl() {
      // console.log("this.feedbackForm", this.feedbackForm)
      return this.feedbackForm.get('buildings');
    }

  _postJson:any;
  getDataConfig(): void{
    const resultJson = this.gatewayStatusService.getData()
    this._postJson = resultJson;

    console.log("this._postJson$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$", this._postJson );
}

optionsGatewayStatus:any;
getGatewayStatus(): void{

  const resultJson = this.backnetHomeService.getGWServiceData();
    // this._postsService = resultJson;
    // console.log("this._postsJson$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$", this._postsService);

    // const resultArray = this.gatewayStatusService.getGatewayStatus();
  this._postsArrayGatewayStatus = resultJson.result.data[0].State.toString();
  var data = [];
  data.push(this._postsArrayGatewayStatus);
  this._postsArrayGatewayStatus = data;
  this.optionsGatewayStatus = this._postsArrayGatewayStatus[0];
  this.gatewayTxt = resultJson.result.data[0].State;
console.log("GATEWAY STATUS",this.gatewayTxt);
  if(this.gatewayTxt == "Running")
  {
    this.IsSelected = 0;
  }
  else if(this.gatewayTxt == "Stopped")
  {
    this.IsSelected = 1;
  }
  else if(this.gatewayTxt == "Refresh")
  {
    this.IsSelected = 2;
  }
    console.log("this.IsSelected", this.IsSelected)
    // this.formGatewayStatus.get('DeviceId').setValue(this.optionsGatewayStatus.DeviceId);
    // this.formGatewayStatus.get('MolexApiUrl').setValue(this.optionsGatewayStatus.MolexApiUrl);
    // this.formGatewayStatus.get('MolexProjectName').setValue(this.optionsGatewayStatus.MolexProjectName);
    // this.formGatewayStatus.get('MonitoringDefaultPriority').setValue(this.optionsGatewayStatus.MonitoringDefaultPriority);
   }


  get fGatewayStatus() { return this.formGatewayStatus.controls; }

submitGatewayStatusComp(servicestatus):void{
this.backnetHomeService.SetGatewayServiceStatus(servicestatus);
}

  subscribeInputFields():void {
    // this.formGatewayStatus.valueChanges.subscribe(
    //   (data) => {
    //     this._postJson.IsGatewayStatusLogEnable = data;
    //     this._formDisabledState = this.formGatewayStatus.valid;
    //     }
    //   );
  }

  isDisabled:Boolean = true;
  IsSelected:any;
  currentStatus:any;

  selectStatus(selectStatus, selectedNum){
    console.log("selectStatus", selectStatus, selectedNum)
    // let array = this._postJson.gatewaySerConfiguration.GateWaySer;

    // Start Gateway Service", " Stop Gateway Service", " Refresh Status of Gateway Service"



    if(selectedNum == 0){
      this.IsSelected = 0;
      this.gatewayTxt = "Running"
      console.log(this.gatewayTxt)
      this.submitGatewayStatusComp(true);
      this.tostr.success('Success! BACnet Service started successfully.');

    }else if(selectedNum == 1){
      this.IsSelected = 1;
      this.gatewayTxt = "Stop"
      console.log(this.gatewayTxt)
      this.submitGatewayStatusComp(false);
      this.tostr.success('Success! BACnet Service stopped successfully.');

    }else if(selectedNum == 2){
      this.IsSelected = 2;
      this.gatewayTxt = "Refresh"
      console.log(this.gatewayTxt)

    }
  }


  onSearchChange(searchValue : string ) {
    console.log(searchValue);
  }

}
