import { TestBed } from '@angular/core/testing';

import { GatewayStatusService } from './gateway-status.service';

describe('GatewayStatusService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GatewayStatusService = TestBed.get(GatewayStatusService);
    expect(service).toBeTruthy();
  });
});
