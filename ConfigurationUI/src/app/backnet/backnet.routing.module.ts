import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BacknetHomeComponent } from './backnet-home/backnet-home.component';
import { GatewayComponent } from './gateway/gateway.component';
import { DeviceComponent } from './device/device.component';
import { NotificationComponent } from './notification/notification.component';
import { BroadcastComponent } from './broadcast/broadcast.component';
import { BmsControlComponent } from './bms-control/bms-control.component';
import { CommunicationComponent } from './communication/communication.component';
import { SecurityComponent } from './security/security.component';
import { ObjectmappingComponent } from './objectmapping/objectmapping.component';
import { DebugComponent } from './debug/debug.component';

import { HelpComponent } from './help/help.component';
import { AboutComponent } from './about/about.component';
import { DefaultConfigurationComponent } from './default-configuration/default-configuration.component';
import { GateWayStatusComponent } from './gate-way-status/gate-way-status.component';
import { NetworkInterfaceComponent } from './network-interface/network-interface.component';

const routes: Routes = [
    {
        path: 'bacnet',
        component: BacknetHomeComponent,
        children: [
            {
                path: 'Gateway Settings',
                component: GatewayComponent
            },
            {
                path: 'Device Object Settings',
                component: DeviceComponent
            },
            {
                path: 'Notification Settings',
                component: NotificationComponent
            },
            {
                path: 'Broadcast Settings',
                component: BroadcastComponent
            },
            {
                path: 'BMS Control Settings',
                component: BmsControlComponent
            },
            {
                path: 'Communication Settings',
                component: CommunicationComponent
            },
            {
                path: 'Security Settings',
                component: SecurityComponent
            },
            {
                path: 'objectMapping',
                component: ObjectmappingComponent
            },
            {
                path: 'debug',
                component: DebugComponent
            },
            {
                path: 'help',
                component: HelpComponent
            },
            {
                path: 'about',
                component: AboutComponent
            },
            {
                path: 'defaultConfiguration',
                component: DefaultConfigurationComponent
            },
            {
                path: 'gatewayStatus',
                component: GateWayStatusComponent
            },
            {
                path: 'networkInterface',
                component: NetworkInterfaceComponent
            }
        ]
    }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BacknetRoutingModule { }
