import { TestBed } from '@angular/core/testing';

import { DeviceObjectService } from './device-object.service';

describe('DeviceObjectService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DeviceObjectService = TestBed.get(DeviceObjectService);
    expect(service).toBeTruthy();
  });
});
