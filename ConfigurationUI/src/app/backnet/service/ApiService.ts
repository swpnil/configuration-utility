import { Injectable, EventEmitter } from '@angular/core';
import { Http, Response} from '@angular/http';
import { HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/Rx';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Authorization': 'my-auth-token'
  })
};

@Injectable({
  providedIn: 'root'
})
export class ApiService {
   public isConnected: EventEmitter<any> = new EventEmitter();
   public isConnectedBuilding: EventEmitter<any> = new EventEmitter();
   constructor(private http: Http){

   }

  getlatlng(address){
    return this.http.get('https://maps.googleapis.com/maps/api/geocode/json?address=' + address)
  }

  getPopupState:boolean = true;

  changStatus(value){
    this.getPopupState = value;
    console.log("dsdjdsdjdjsd", this.getPopupState);
  }


   baseUrl:any;
   baseUrlSave:any;
   baseUrlBuilding:any;

   configData: any;

  _getSuccesPortIP: Boolean;

  getConfigData1(ipAddress, port){
    // console.log("baseUrl",baseUrl)
    // console.log("type of baseUrl",typeof baseUrl)
    this.baseUrl = this.getJsonDataOnLoad(ipAddress, port)
    console.log("this.baseUrl", this.baseUrl)
    this.baseUrlSave = this.getJsonDataOnSave(ipAddress, port)

    this.http
      .get(this.baseUrl)
      .map((response: Response) => {
        this.configData = response.json();
        // console.log("response", response);
      }).subscribe(
        response => {
          // return this._getSuccesPortIP = true;
          // this.configData = true;
          console.log("response192.168.80.93", response)
          console.log("response192.168.80.93", this.configData)
          this.isConnected.emit(true);

        },
        error => {
          // this.error = error
          console.log("error###################################", error)
          console.log("error###################################", this.configData)
          // this.configData = false;
          this.isConnected.emit(false);
        }
      );

  }

  getJsonDataOnLoad(ipAddress, port): any{

    console.log("getJsonData.ipAddress", typeof ipAddress, ipAddress)
    console.log("getJsonData.port", typeof port, port)
    this.baseUrl = "http://" + ipAddress + ":" + port + "/" + "api/getConfig/";

    console.log("this.baseUrl", this.baseUrl)
    // this.getConfigData1(this.baseUrl)
    // this.baseUrl = "http://localhost:5200/api/getConfig/";
    return (this.baseUrl);
   }

  getJsonDataOnSave(ipAddress, port): any{
    console.log("getJsonData.ipAddress", ipAddress)
    console.log("getJsonData.port", port)
    this.baseUrlSave = "http://" + ipAddress + ":" + port + "/" + "api/saveConfig/";

    console.log("this.baseUrlSave", this.baseUrlSave)
    return (this.baseUrlSave);
   }


// GET Full JSON
  getJsonData(): any{
    return (this.configData);
   }

  getApi(): any{
    return (this.configData.apiConfig);
  }
  
  private handleError(error:Response){
     return Observable.throwError(error.statusText)
    //  Observable.throwError()
   }
}
