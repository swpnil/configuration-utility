import { Injectable, EventEmitter } from '@angular/core';
import { Http, Response} from '@angular/http';
// import {Observable} from 'rxjs/Observable';
import { HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/Rx';

@Injectable({
  providedIn: 'root'
})
export class DeviceObjectService {
  public isConnected: EventEmitter<any> = new EventEmitter();
  configData: any;

  baseUrl:any;
  initLoadApp(){
    this.baseUrl = "http://localhost:3000/Data";
    console.log("this.baseUrl", this.baseUrl);
    // initLoadApp(ipAddress, port){
    // let baseUrl = this.getBaseUrlOnLoad(ipAddress, port)
    // let baseUrlSave = this.getBaseUrlOnSave(ipAddress, port)
    this.http
      .get(this.baseUrl)
      .map((response: Response) => {
        this.configData = response.json();
        console.log("response", response);
      }).subscribe(
        response => {
          this.isConnected.emit(true);
        },
        error => {
          this.isConnected.emit(false);
        }
      );

  }


  getData(): any{
    console.log("this.configData", this.configData)
    return (this.configData);
   }

   getDevice(): any{
    //  console.log("DeviceObject", this.baseUrl + 'DeviceObject')
    return (this.configData.DeviceObject);
   }


  get_Device(): any{
    // return (this.configData.deviceObject);
   }

  // getBaseUrlOnLoad(ipAddress, port): any{
  //   this.baseUrl = "http://" + ipAddress + ":" + port + "/" + "api/getConfig/";
  //   return (this.baseUrl);
  //  }

  // getBaseUrlOnSave(ipAddress, port): any{
  //   this.baseUrlSave = "http://" + ipAddress + ":" + port + "/" + "api/saveConfig/";
  //   return (this.baseUrlSave);
  //  }

  constructor(private http : Http) { }

  // get_Device(){
  //     return this.http.get(this.baseUrl + '/deviceObject');
  // }
}
