import { Component, OnInit, ViewChild, ElementRef, NgZone, SimpleChanges } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { ToastrService } from 'ngx-toastr';
import {ApiService} from '../service/ApiService';
import { GatewayService } from './service/gateway.service';
import {BacknetHomeService} from '../backnet-home/service/backnet-home.service';
import {deviceComponent} from '../interface/device.comp.interface'

import { AgmCoreModule, MapsAPILoader, AgmMap, LatLngBounds, MouseEvent } from '@agm/core';
import {} from '@agm/core/services/google-maps-types';
declare let google: any;

@Component({
  selector: 'app-gateway',
  templateUrl: './gateway.component.html',
  styleUrls: ['./gateway.component.css']
})
export class GatewayComponent implements OnInit {
  molexProjectNameValue =["1-Manual Life Safety","2-Automatic-Life Safety", "3-Available", "4-Available", "5-Critical Equipment Control", "6-Minimum On/Off", "7-Available", "8-Manual Operator", "9-Available", "10-Available", "11-Available", "12-Available", "13-Available", "14-Available", "15-Available", "16-Available"]

  MonitoringDefaultPriorityVal=[""]

  public formGateway: FormGroup;
  _postsArrayGateway:any;
  isDataAvailable:boolean=false;

  _formDisabledState:boolean = true;
  title = "Gateway Object Settings"

  ipPattern = "(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)";


  optionsApi:any;
  cancel() {
    this.formGateway.get('DeviceId').setValue(this.optionsGateway.DeviceId);
    this.formGateway.get('MolexApiUrl').setValue(this.optionsGateway.MolexApiUrl);
    this.formGateway.get('MolexProjectId').setValue(this.optionsGateway.MolexProjectId);
    this.formGateway.get('MonitoringDefaultPriority').setValue(this.optionsGateway.MonitoringDefaultPriority);
   }

  private GatewayComponentSer: GatewayComponent[] = [];
  private GatewayComponentObservable : Observable<GatewayComponent[]> ;

  feedbackForm: FormGroup;
  buildings = []

  constructor(private fb: FormBuilder, private apiService : ApiService, private gatewayService: GatewayService, private backnetHomeService: BacknetHomeService, private http: Http,private tostr: ToastrService) {
    this.GatewayComponentObservable = this.backnetHomeService.get_Gateway();
   }

  baseUrlJSON = "http://localhost:3000";
  baseUrlSave = "http://localhost:3000"
  private GatewayComponent  = [];

  get_GatewayComponent(){
    this.http.get(this.baseUrlJSON + '/data').subscribe((res)=>{
        console.log(res.json());
        this.GatewayComponent = res.json();
        console.log(this.GatewayComponent);
    });
}

  isDisabled = true;
  currentIPAddress:any;
  currentPort:any;
  showBacNetLoginPopUP:boolean;
  currentIPBacnet:any;
  currentPortBacnet:any;

  ngOnInit() {
    this.currentIPBacnet = localStorage.getItem('ipAddressBacNet');
    this.currentPortBacnet = localStorage.getItem('portBacNet');
    if (this.currentIPBacnet == null && this.currentPortBacnet == null) {
      console.log("if");
      this.showBacNetLoginPopUP = true;
    }

    // this.backnetHomeService.initLoadApp();
    this.backnetHomeService.getProjectsData();

    // this.currentIPAddress = localStorage.getItem('ipAddress');
    // this.currentPort = localStorage.getItem('port');
    setTimeout(() => {
      // this.initGateway();
      this.formGateway = this.fb.group({
        DeviceId: ['', [Validators.required]],
        MolexApiUrl: ['', [Validators.required]],
        MolexProjectId: ['', [Validators.required]],
        MonitoringDefaultPriority: ['', [Validators.required, Validators.minLength(5)]],
      });
    }, 1000);

    this.backnetHomeService.isConnected.subscribe( result => {
      if (result) {
        setTimeout(() => {
          this.isDataAvailable = true;
          this.getDataConfig();
          this.getGateway();
          this.subscribeInputFields();
        }, 1000);
      }
    // this.currentIPAddress = localStorage.getItem('ipAddress');
    // this.currentPort = localStorage.getItem('port');
  });

  }

  enaDisClicked(value){
    console.log("dsdsdsdsdsd", this._postJson.GatewayObject.DaylightSavingStatus);
    this.isDisabled = value;
    this._postJson.GatewayObject.DaylightSavingStatus = value;
  }

  molexProjectNameClick(value){
    console.log(value);
    this._postJson.Gateway.MonitoringDefaultPriority = value;
    // this.order.type=value;
  }

  MonitoringDefaultPriority(value){
    console.log(value);
    this._postJson.Gateway.MonitoringDefaultPriority = value;
    // this.order.type=value;
  }

  baseUrl:any;
  loginModelStatus:any
  loginModelStatusValue:boolean = true;
  isConnectedValue:boolean =true;
  localStorageValue:boolean;
  buildingConfiguration:boolean = false;

  initGateway(){
    this.backnetHomeService.initLoadApp(this.currentIPAddress,this.currentPort);
    this.backnetHomeService.isConnected.subscribe( result => {
      console.log("isConnected", result)
      this.isConnectedValue = result
      this.loginModelStatusValue = !result;
      if (result) {
        this.getDataConfig();
        this.getGateway();
        this.subscribeInputFields();
        // localStorage.setItem('dashBoardPopupDashboard', 'false');
        // localStorage.setItem('ipAddressDashboard', ipAddress);
        // localStorage.setItem('portDashboard', port);
      }
    });
  }

  getSmartCoreValue: any;
  SmartCore: any;
  buildingValue:any;
  optionsSmartCore:any;
  _postsArraySmartCore:any;
  _SmartCoreoptions:any;
  // systemValues:any;


    get repliesControl() {
      // console.log("this.feedbackForm", this.feedbackForm)
      return this.feedbackForm.get('buildings');
    }

  _postJson:any;
  getDataConfig(): void{
    // const resultJson = this.backnetHomeService.getData()
    // this._postJson = resultJson.JsonData;

    const resultJson = this.backnetHomeService.getJsonData();
    // this._postJson = resultJson.JsonData;
    this._postJson = resultJson.result.data[0];
    console.log("GateWay", this._postJson );
    console.log("this._postJson$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$", this._postJson );
}

optionsGateway:any;
getGateway(): void{
    // const resultArray = this.backnetHomeService.getGateway()
    this._postsArrayGateway = this._postJson.Gateway;
    var data = [];
    data.push(this._postsArrayGateway);
    this._postsArrayGateway = data;
    console.log("this._postsArrayGateway", this._postsArrayGateway);
    this.optionsGateway = this._postsArrayGateway[0];

    console.log("this.optionsGateway", this.optionsGateway)
    this.formGateway.get('DeviceId').setValue(this.optionsGateway.DeviceId);
    this.formGateway.get('MolexApiUrl').setValue(this.optionsGateway.MolexApiUrl);
    this.formGateway.get('MolexProjectId').setValue(this.optionsGateway.MolexProjectId);
    this.formGateway.get('MonitoringDefaultPriority').setValue(this.optionsGateway.MonitoringDefaultPriority);
   }


  get fGateway() { return this.formGateway.controls; }

submitGatewayComp():void{
  console.log("this.baseUrlSave", this.baseUrlSave);
  console.log("this._postJson", this._postJson);

  this.backnetHomeService.SaveBacNetConfiguration(this._postJson);
  this.getGateway();
  this.tostr.success('Success! BACnet Configuration is updated successfully.');
}

  subscribeInputFields():void {
    this.formGateway.valueChanges.subscribe(
      (data) => {
        this._postJson.Gateway = data;
        this._formDisabledState = this.formGateway.valid;
        }
      );
  }
  onSearchChange(searchValue : string ) {
    console.log(searchValue);
  }

}
