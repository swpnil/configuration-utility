import { BacknetHomeComponent } from './../backnet-home/backnet-home.component';
import { Component, OnInit, ViewChild, ElementRef, NgZone, SimpleChanges } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { ToastrService } from 'ngx-toastr';

import { NetworkInterfaceService } from './service/network-interface.service';
import {BacknetHomeService} from '../backnet-home/service/backnet-home.service';

import {} from '@agm/core/services/google-maps-types';
declare let google: any;


@Component({
  selector: 'app-network-interface',
  templateUrl: './network-interface.component.html',
  styleUrls: ['./network-interface.component.css']
})
export class NetworkInterfaceComponent implements OnInit {

  isDataAvailable:boolean=false;
  public formNetwork: FormGroup;
  _postsArrayNetwork:any;

  _formDisabledState:boolean = true;
  title = "Network Object Settings"

  ipPattern = "(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)";


  optionsApi:any;
  cancel() {
    this.formNetwork.get('UDPPort').setValue(this.optionsNetwork.UDPPort);
    this.formNetwork.get('VirtualNetworkNumber').setValue(this.optionsNetwork.VirtualNetworkNumber);
    this.formNetwork.get('LocalNetworkNumber').setValue(this.optionsNetwork.LocalNetworkNumber);
   }

  private NetworkComponentSer: NetworkInterfaceComponent[] = [];
  private NetworkComponentObservable : Observable<NetworkInterfaceComponent[]> ;

  feedbackForm: FormGroup;
  buildings = []

  constructor(private fb: FormBuilder, private networkInterfaceService: NetworkInterfaceService, private backnetHomeService: BacknetHomeService, private http: Http,private tostr: ToastrService) {
    this.NetworkComponentObservable = this.backnetHomeService.get_Network();
   }

  baseUrlJSON = "http://localhost:3000";
  baseUrlSave = "http://localhost:3000";

  private NetworkInterfaceComponent  = [];

  get_NetworkComponent(){
    this.http.get(this.baseUrlJSON + '/data').subscribe((res)=>{
        console.log(res.json());
        this.NetworkInterfaceComponent = res.json();
        console.log(this.NetworkInterfaceComponent);
    });
}

  isDisabled = true;
  currentIPAddress:any;
  currentPort:any;
  showBacNetLoginPopUP:boolean;
  currentIPBacnet:any;
  currentPortBacnet:any;

  ngOnInit() {
    this.currentIPBacnet = localStorage.getItem('ipAddressBacNet');
    this.currentPortBacnet = localStorage.getItem('portBacNet');
    if (this.currentIPBacnet == null && this.currentPortBacnet == null) {
      console.log("if");
      this.showBacNetLoginPopUP = true;
    }
    this.backnetHomeService.getProjectsData();

    setTimeout(() => {
      this.formNetwork = this.fb.group({
        UDPPort: ['', [Validators.required]],
        VirtualNetworkNumber: ['', [Validators.required]],
        LocalNetworkNumber: ['', [Validators.required]],
      });
    }, 1000);


    this.backnetHomeService.isConnected.subscribe( result => {
      if (result) {
        setTimeout(() => {
          this.isDataAvailable =true;
          this.getDataConfig();
          this.getNetwork();
          // this.subscribeInputFields();
        }, 1000);
      }
    // this.currentIPAddress = localStorage.getItem('ipAddress');
    // this.currentPort = localStorage.getItem('port');
  });
}

  selectStatus(selectStatus, selectedNum){
    console.log("this._postsArrayNetwork", this._postsArrayNetwork[0].IPAddressList);
    let array = this._postsArrayNetwork[0].IPAddressList;

    for (let i = 0; i < array.length; i++) {
      if(selectedNum == i){
        this._postJson.NetworkInterface.IPAddressList[i].IsSelected = selectStatus;
      }else{
        this._postJson.NetworkInterface.IPAddressList[i].IsSelected = false;
      }
    }
    // this._postsArrayNetwork.NetworkInterface.IPAddressList[i].IsSelected = selectStatus;
    // this.order.type=value;
  }

  baseUrl:any;
  loginModelStatus:any
  loginModelStatusValue:boolean = true;
  isConnectedValue:boolean =true;
  localStorageValue:boolean;
  buildingConfiguration:boolean = false;

  initNetwork(){
        this.getNetwork();
        this.subscribeInputFields();
    // this.networkInterfaceService.isConnected.subscribe( result => {
    //   console.log("isConnected", result)
    //   this.isConnectedValue = result
    //   this.loginModelStatusValue = !result;
    //   if (result) {
    //     this.getDataConfig();
    //     this.getNetwork();
    //     this.subscribeInputFields();
    //   }
    // });
  }

  getSmartCoreValue: any;
  SmartCore: any;
  buildingValue:any;
  optionsSmartCore:any;
  _postsArraySmartCore:any;
  _SmartCoreoptions:any;
  // systemValues:any;


    get repliesControl() {
      // console.log("this.feedbackForm", this.feedbackForm)
      return this.feedbackForm.get('buildings');
    }
    _postJson:any;
  getDataConfig(): void{
    const resultJson = this.backnetHomeService.getJsonData();
    // this._postJson = resultJson.JsonData;
    this._postJson = resultJson.result.data[0];
    console.log("this._postsArrayNetwork$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$", this._postJson );
}

optionsNetwork:any;
IPAddressList:any;
getNetwork(): void{
    // const resultArray = this.networkInterfaceService.getNetwork()
    // const resultArray = this.backnetHomeService.getNetwork()
    this._postsArrayNetwork = this._postJson.NetworkInterface;
    console.log("this._postsArrayNetwork", this._postsArrayNetwork)
    var data = [];
    data.push(this._postsArrayNetwork);
    this._postsArrayNetwork = data;
    console.log("this._postsArrayNetwork", this._postsArrayNetwork)
    this.optionsNetwork = this._postsArrayNetwork[0];

    this.IPAddressList = this._postsArrayNetwork[0].IPAddressList;
    console.log("this.optionsNetwork", this.optionsNetwork.UDPPort);
    this.formNetwork.get('UDPPort').setValue(this.optionsNetwork.UDPPort);
    this.formNetwork.get('VirtualNetworkNumber').setValue(this.optionsNetwork.VirtualNetworkNumber);
    this.formNetwork.get('LocalNetworkNumber').setValue(this.optionsNetwork.LocalNetworkNumber);
   }


  get fNetwork() { return this.formNetwork.controls; }

submitNetworkComp():void{
  console.log("this.baseUrlSave", this.baseUrlSave);
  console.log("this._postsArrayNetwork", this._postJson);
  this.backnetHomeService.SaveBacNetConfiguration(this._postJson);
  this.tostr.success('Success! BACnet Configuration is updated successfully.');
}

  subscribeInputFields():void {
    this.formNetwork.valueChanges.subscribe(
      (data) => {
        console.log("this._postsArrayNetwork", this._postsArrayNetwork)
        this._postJson.NetworkInterface = data;
        this._formDisabledState = this.formNetwork.valid;
        }
      );
  }

  onSearchChange(searchValue : string ) {
    console.log(searchValue);
  }


}
