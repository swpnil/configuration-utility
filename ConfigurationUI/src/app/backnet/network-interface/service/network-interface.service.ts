import { Injectable, EventEmitter } from '@angular/core';
import { Http, Response} from '@angular/http';
// import {Observable} from 'rxjs/Observable';
import 'rxjs/Rx';

@Injectable({
  providedIn: 'root'
})
export class NetworkInterfaceService {
  public isConnected: EventEmitter<any> = new EventEmitter();
  configData: any;

  baseUrl:any;
  initLoadApp(){
    this.baseUrl = "http://localhost:2000/test";
    console.log("this.baseUrl", this.baseUrl);
    // initLoadApp(ipAddress, port){
    // let baseUrl = this.getBaseUrlOnLoad(ipAddress, port)
    // let baseUrlSave = this.getBaseUrlOnSave(ipAddress, port)
    this.http
      .get(this.baseUrl)
      .map((response: Response) => {
        this.configData = response.json();
        console.log("response", this.configData);
      }).subscribe(
        response => {
          console.log("response192.168.80.93", response);
          console.log("response192.168.80.93", this.configData);
          this.isConnected.emit(true);
        },
        error => {
        console.log("error");
          this.isConnected.emit(false);
        }
      );

  }


  getData(): any{
    console.log("this.configData", this.configData)
    return (this.configData);
   }

   getNetwork(): any{
    return (this.configData.NetworkInterface);
   }


  get_Network(): any{
    // return (this.configData.deviceObject);
   }

  constructor(private http : Http) { }

}
