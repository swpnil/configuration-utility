import { Component, OnInit, ViewChild, ElementRef, NgZone, SimpleChanges } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { ToastrService } from 'ngx-toastr';

import {ApiService} from '../service/ApiService';
import { DeviceObjectService } from '../service/device-object.service';

import {BacknetHomeService} from '../backnet-home/service/backnet-home.service';
import {deviceComponent} from '../interface/device.comp.interface'

import { AgmCoreModule, MapsAPILoader, AgmMap, LatLngBounds, MouseEvent } from '@agm/core';
import {} from '@agm/core/services/google-maps-types';
declare let google: any;

@Component({
  selector: 'app-device',
  templateUrl: './device.component.html',
  styleUrls: ['./device.component.css']
})
export class DeviceComponent implements OnInit {
  public formDevice: FormGroup;
  _postsArrayDevice:any;

  isDataAvailable:boolean = false;
  _formDisabledState:boolean = true;
  title = "Device Object Settings"

  ipPattern = "(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)";


  optionsApi:any;
  cancel() {
    this.formDevice.get('VendorName').setValue(this.optionsDevice.VendorName);
    this.formDevice.get('ModelName').setValue(this.optionsDevice.ModelName);
    this.formDevice.get('Description').setValue(this.optionsDevice.Description);
    this.formDevice.get('APDUTimeout').setValue(this.optionsDevice.APDUTimeout);
    this.formDevice.get('APDURetries').setValue(this.optionsDevice.APDURetries);
    this.formDevice.get('APDUSegmentTimeout').setValue(this.optionsDevice.APDUSegmentTimeout);
    this.formDevice.get('UTCOffset').setValue(this.optionsDevice.UTCOffset);
  }

  private deviceComponentSer: deviceComponent[] = [];
  private deviceComponentObservable : Observable<deviceComponent[]> ;

  feedbackForm: FormGroup;
  buildings = []
  APDUTimeoutValue =["6000","7000","8000","9000", "10000", "11000", "12000", "13000", "14000", "15000", "16000","17000","18000","19000", "20000", "21000", "22000", "23000", "24000", "25000", "26000","27000","28000","29000", "30000", "31000", "32000", "33000", "34000", "35000", "36000","37000","38000","39000", "40000", "41000", "42000", "43000", "44000", "45000", "46000","47000","48000","49000", "50000", "51000", "52000", "53000", "54000", "55000", "56000","57000","58000","59000", "60000", "61000", "62000", "63000", "64000", "65000"]
  APDUSegmentTimeoutValue =["5000","6000","7000","8000","9000", "10000", "11000", "12000", "13000", "14000", "15000", "16000","17000","18000","19000", "20000", "21000", "22000", "23000", "24000", "25000", "26000","27000","28000","29000", "30000", "31000", "32000", "33000", "34000", "35000", "36000","37000","38000","39000", "40000", "41000", "42000", "43000", "44000", "45000", "46000","47000","48000","49000", "50000", "51000", "52000", "53000", "54000", "55000", "56000","57000","58000","59000", "60000", "61000", "62000", "63000", "64000", "65000"]


  constructor(private fb: FormBuilder, private apiService : ApiService, private deviceObjectService: DeviceObjectService,  private backnetHomeService: BacknetHomeService, private http: Http,private tostr: ToastrService) {
    this.deviceComponentObservable = this.backnetHomeService.get_Device();
   }

  baseUrlJSON = "http://localhost:3000";
  baseUrlSave = "http://localhost:3000"
  private deviceComponent  = [];

  get_deviceComponent(){
    this.http.get(this.baseUrlJSON + '/data').subscribe((res)=>{
        console.log(res.json());
        this.deviceComponent = res.json();
        console.log(this.deviceComponent);
    });
}

  isDisabled = true;
  currentIPAddress:any;
  currentPort:any;
  showBacNetLoginPopUP:boolean;
  currentIPBacnet:any;
  currentPortBacnet:any;

  ngOnInit() {
    this.currentIPBacnet = localStorage.getItem('ipAddressBacNet');
    this.currentPortBacnet = localStorage.getItem('portBacNet');
    if (this.currentIPBacnet == null && this.currentPortBacnet == null) {
      console.log("if");
      this.showBacNetLoginPopUP = true;
    }

    // this.backnetHomeService.initLoadApp();
    this.backnetHomeService.getProjectsData();

    // this.currentIPAddress = localStorage.getItem('ipAddress');
    // this.currentPort = localStorage.getItem('port');
    setTimeout(() => {
      // this.initDevices();
      this.formDevice = this.fb.group({
        VendorName: ['', [Validators.required]],
        ModelName: ['', [Validators.required]],
        Description: ['', [Validators.required]],
        APDUTimeout: ['', [Validators.required, Validators.minLength(5)]],
        APDURetries: ['', [Validators.required, Validators.minLength(5)]],
        APDUSegmentTimeout: ['', [Validators.required, Validators.minLength(5)]],
        UTCOffset: ['', [Validators.required, Validators.minLength(5)]],
      });
    }, 1000);

    this.backnetHomeService.isConnected.subscribe( result => {
      if (result) {
        setTimeout(() => {
          this.isDataAvailable = true;
          this.getDataConfig();
          this.getDevice();
          this.subscribeInputFields();
        }, 1000);
      }
    // this.currentIPAddress = localStorage.getItem('ipAddress');
    // this.currentPort = localStorage.getItem('port');
    });

  }

  enaDisClicked(value){
    console.log("dsdsdsdsdsd", this._postJson.DeviceObject.DaylightSavingStatus);
    console.log("dsdsdsdsdsd", value);
    this.isDisabled = value;
    this._postJson.DeviceObject.DaylightSavingStatus = value;
    console.log("dsdsdsdsdsd", this._postJson);
  }

  APDUTimeoutClick(value){
    console.log(value);
    this._postJson.DeviceObject.APDUTimeout = value;
    // this.order.type=value;
  }

  APDUSegmentTimeoutClick(value){
    console.log(value);
    this._postJson.DeviceObject.APDUSegmentTimeout = value;
    // this.order.type=value;
  }


  baseUrl:any;
  loginModelStatus:any
  loginModelStatusValue:boolean = true;
  isConnectedValue:boolean =true;
  localStorageValue:boolean;
  buildingConfiguration:boolean = false;

  initDevices(){
    this.deviceObjectService.initLoadApp();
    this.deviceObjectService.isConnected.subscribe( result => {
      console.log("isConnected", result)
      this.isConnectedValue = result
      this.loginModelStatusValue = !result;
      if (result) {
        this.getDataConfig();
        this.getDevice();
        this.subscribeInputFields();
        // localStorage.setItem('dashBoardPopupDashboard', 'false');
        // localStorage.setItem('ipAddressDashboard', ipAddress);
        // localStorage.setItem('portDashboard', port);
      }
    });
  }

  getSmartCoreValue: any;
  SmartCore: any;
  buildingValue:any;
  optionsSmartCore:any;
  _postsArraySmartCore:any;
  _SmartCoreoptions:any;
  // systemValues:any;


    get repliesControl() {
      // console.log("this.feedbackForm", this.feedbackForm)
      return this.feedbackForm.get('buildings');
    }

  _postJson:any;
  getDataConfig(): void{
    // const resultJson = this.backnetHomeService.getData()
    // this._postJson = resultJson.JsonData;
    const resultJson = this.backnetHomeService.getJsonData();
    // this._postJson = resultJson.JsonData;
    this._postJson = resultJson.result.data[0];
    console.log("Device", this._postJson );
    console.log("this._postJson$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$", this._postJson );
}

optionsDevice:any;
DaylightSavingVal:any;
getDevice(): void{
    // const resultArray = this.backnetHomeService.getDevice()
    this._postsArrayDevice = this._postJson.DeviceObject;
    var data = [];
    data.push(this._postsArrayDevice);
    this._postsArrayDevice = data;
    console.log("this._postsArrayDevice", this._postsArrayDevice)
    this.optionsDevice = this._postsArrayDevice[0];

    this.DaylightSavingVal = this.optionsDevice.DaylightSavingStatus;

    console.log("this.optionsDevice", this.DaylightSavingVal);


    this.formDevice.get('VendorName').setValue(this.optionsDevice.VendorName);
    this.formDevice.get('ModelName').setValue(this.optionsDevice.ModelName);
    this.formDevice.get('Description').setValue(this.optionsDevice.Description);
    this.formDevice.get('APDUTimeout').setValue(this.optionsDevice.APDUTimeout);
    this.formDevice.get('APDURetries').setValue(this.optionsDevice.APDURetries);
    this.formDevice.get('APDUSegmentTimeout').setValue(this.optionsDevice.APDUSegmentTimeout);
    this.formDevice.get('UTCOffset').setValue(this.optionsDevice.UTCOffset);
  }


  get fDevice() { return this.formDevice.controls; }

submitDeviceComp():void{
  console.log("this.baseUrlSave", this.baseUrlSave);
  console.log("this._postJson", this._postJson);
  this.backnetHomeService.SaveBacNetConfiguration(this._postJson);
  this.getDevice();
  this.tostr.success('Success! BACnet Configuration is updated successfully.');
}

  subscribeInputFields():void {
    this.formDevice.valueChanges.subscribe(
      (data) => {
        this._postJson.DeviceObject = data;
        this._formDisabledState = this.formDevice.valid;
        }
      );
  }
  onSearchChange(searchValue : string ) {
    console.log(searchValue);
  }

}




