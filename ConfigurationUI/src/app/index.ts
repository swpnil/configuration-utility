// start:ng42.barrel
export * from './app-routing.module';
export * from './app.component';
export * from './app.module';
// export * from './json-typings.d';
export * from './rest.service';
// end:ng42.barrel

