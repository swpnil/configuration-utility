import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
// import sampleData from './data.json';

import {ApiService} from '../service/api.service';
import {ConfigurationDashboard} from '../interface/configuration-dashboard'

@Component({
  selector: 'app-user-config',
  templateUrl: './user-config.component.html',
  styleUrls: ['./user-config.component.css']
})
export class UserConfigComponent implements OnInit {
  showHumidex: boolean = true;
  showHeatmap: boolean = false;

  firstLoad:boolean = true;

  // Users: any = sampleData;
  public formHumidex: FormGroup;
  public formHeatmap: FormGroup;

  // returns all form groups under contacts
  // get contactFormGroup() {
  //   return this.formSystem.get('contacts') as FormArray;
  // }

  allConfigList = ["Dummy Btn 1", "Dummy Btn 2"]

  _postsJson:any;
   _postsArray:any;
  _postsArrayHumidex:any;
  _postArrayHeatmap :any;

  datafield: any;
  enablefield: any;

  constructor(private fb: FormBuilder, private apiService : ApiService, private http: Http) {}

  // baseUrl:string = "http://localhost:5000/";
  isDataAvailable:boolean = false;

  getJsonConfig(): void{
    const resultJson = this.apiService.getJsonData()
    this._postsJson = resultJson;
    console.log("this._postsJson", this._postsJson );
    this.isDataAvailable = true;
    // var data = [];
    // data.push(resultJson);
    // this._postsJson = data;
    // console.log( "#####################################################################",this._postsJson[0].systemConfig)

}

  getHumidex(): void{
    // this.apiService.getHumidex()
    //   .subscribe(
    //     resultArray => {
      const resultArray = this.apiService.getHumidex()
      this._postsArrayHumidex = resultArray;
      var data = [];
      data.push(resultArray);
      this._postsArrayHumidex = data;
      console.log("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!",data);
      console.log("this._postsArrayHumidex", this._postsArrayHumidex)
      console.log("this._postsArrayHumidex", this._postsArrayHumidex.costFactorInDollar)
    }

  getTemperatureValue: any;
  selectedDay: string = '';

  getHeatmap (): void{
    // this.apiService.getHeatmap()
    //   .subscribe(
    //     resultArray => {
      const resultArray = this.apiService.getSensor()
      this._postArrayHeatmap = resultArray;
      var data = [];
      data.push(resultArray);
      this._postArrayHeatmap = data;

      if(this._postArrayHeatmap[0].temperature == "Fahrenheit"){
        this.getTemperatureValue ="Celcius"
        this.selectedDay = "Fahrenheit"
      }else{
        this.getTemperatureValue ="Fahrenheit"
        this.selectedDay = "Celcius"
      }
      console.log("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!",data);

      console.log("this._postArrayHeatmap ", this._postArrayHeatmap[0].temperature)
      console.log("getTemperatureValue", this.getTemperatureValue)
    }

  selectChangeHandler (event: any) {
    //update the ui
    this.selectedDay = event.target.value;
    console.log("selectedDay", this.selectedDay);
  }

  ngOnInit() {
    // this.apiService.getConfigData();
    setTimeout(() => {
      this.formHumidex = this.fb.group({
        goodMin: ['', [Validators.required, Validators.minLength(5)]],
        goodMax: ['', [Validators.required, Validators.minLength(5)]],
        goodColor: ['', [Validators.required, Validators.minLength(5)]],
        normalMin: ['', [Validators.required, Validators.minLength(5)]],
        normalMax: ['', [Validators.required, Validators.minLength(5)]],
        normalColor: ['', [Validators.required, Validators.minLength(5)]],
        notGoodMin: ['', [Validators.required, Validators.minLength(5)]],
        notGoodMax: ['', [Validators.required, Validators.minLength(5)]],
        notGoodColor: ['', [Validators.required, Validators.minLength(5)]],
        badMin: ['', [Validators.required, Validators.minLength(5)]],
        badMax: ['', [Validators.required, Validators.minLength(5)]],
        badColor: ['', [Validators.required, Validators.minLength(5)]]

      });

      this.formHeatmap = this.fb.group({
        temperature: ['', [Validators.required, Validators.minLength(5)]]
      });
      this.getJsonConfig();
      this.getHumidex();
      this.getHeatmap();
  },1000)

  }


  public isselected=0;

  // _getIndex: any;
  buttonClick(index){
    console.log("index", index)

    if(index==0){
      this.showHumidex = true
      this.showHeatmap = false
    }else if(index==1){
      this.showHumidex = false
      this.showHeatmap = true
    }

    // this._getIndex = index;
    this.isselected=index;
  };


  overlayClick(){
    console.log("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
    if(this.firstLoad == true){
      // this.firstLoad = false;
    }
  }

  submitted = false;
  submittedHumidex = false;
  submittedHeatmap = false;

  onSubmitHumidex= function (Humidex) {
    this.submittedHumidex = true;
    console.log("GGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGG", this.submittedHumidex);
    // if (this.formHumidex.invalid) {
    //     return;
    // }

    alert('SUCCESS!! :-)')
    console.log("GGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGG", Humidex);
    var body = "goodMin=" + Humidex.goodMin + "goodMax=" + Humidex.goodMax + "goodColor=" + Humidex.goodColor + "normalMin=" + Humidex.normalMin + "normalMax=" + Humidex.normalMax + "normalColor=" + Humidex.normalColor + "notGoodMin=" + Humidex.notGoodMin + "notGoodMax=" + Humidex.notGoodMax + "notGoodColor=" + Humidex.notGoodColor + "badMin=" + Humidex.badMin + "badMax=" + Humidex.badMax + "badColor=" + Humidex.badColor ;

    console.log("body", body);
    this.http.post('http://localhost:5000/humidexRangeConfig', Humidex).subscribe(data => console.log(JSON.stringify(data)));
  }


  onSubmitHeatmap= function (Heatmap) {
    this.submittedHeatmap = true;
    console.log("GGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGG", this.submittedHeatmap);
    console.log("qqqqqqqqq", Heatmap);
    // if (this.formHeatmap.invalid) {
    //     return;
    // }

    alert('SUCCESS!! :-)')
    console.log("GGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGG", Heatmap);
    var body = "temperature=" + Heatmap.temperature;

    console.log("body", body);
    this.http.post('http://localhost:5000/sensorUnitConfig', Heatmap).subscribe(data => console.log(JSON.stringify(data)));
  }

  // convenience getter for easy access to form fields
  get fhumidex() { return this.formHumidex.controls; }
  get fheatmap() { return this.formHeatmap.controls; }





  // contact formgroup
  createContact(): FormGroup {
    return this.fb.group({
      type: ['email', Validators.compose([Validators.required])], // i.e Email, Phone
      name: [null, Validators.compose([Validators.required])], // i.e. Home, Office
      value: [null, Validators.compose([Validators.required, Validators.email])]
    });
  }

}
