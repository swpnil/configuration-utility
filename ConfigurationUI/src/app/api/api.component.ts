import { Component, OnInit,HostListener } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';


// import sampleData from './data.json';

import {ApiConfigService} from '../service/api-config.service';
import {ConfigurationDashboard} from '../interface/configuration-dashboard'
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-api',
  templateUrl: './api.component.html',
  styleUrls: ['./api.component.css']
})
export class ApiComponent implements OnInit {
  // lightScenesUser = ['creative', 'meeting', 'orbit', 'speaker', 'daylight', 'video', 'personal', 'fastFlash', 'slowFlash', 'fastPulse', 'slowPulse'];
  // lightScenesBeacon = ['off','fastFlash', 'slowFlash', 'fastPulse', 'slowPulse', 'cycle', 'chase', 'oneColour', 'twoColour', 'threeColour'];
  // lightScenesGeneral = ['stairs', 'rr', 'elevator', 'lobby', 'exit', 'cafeteria', 'aisle', 'feature'];
  // lightScenesBuilding = ['workingHours', 'outOfHours'];
  // innerHeight:any;
  isDataAvailable:boolean = false;
  showDivContent:boolean = false;

  @HostListener('window:resize', ['$event'])
  onResize() {
    this.myInnerHeight = window.innerHeight;
    console.log("!@@@@@@!@!!!!!!!!!!!!!!!!", this.myInnerHeight)
  }

// GET API DATA
apiUrl = 'http://localhost:3000';


loginModelStatusValue:boolean;
isConnectedValue:boolean = true;
baseUrl: any;
baseUrlSave: any;
baseUrlMqtt: any;
baseUrlExport: any;

addApiAddress(ipAddress, port){
  // this.baseUrl = "http://" + ipAddress + ":" + port + "/" + "api/getConfig/";
  // this.baseUrlSave = "http://" + ipAddress + ":" + port + "/" + "api/saveConfig/";
  // this.baseUrlMqtt = "http://" + ipAddress + ":" + port + "/" + "loadMqtt";
  // this.baseUrlExport = "http://" + ipAddress + ":" + port + "/" + "transcend/api/v1/download/logger";
  // console.log("this.baseUrlExport", this.baseUrlExport);

  this.apiConfigService.getApiServerStarted(ipAddress, port);

  // this.apiConfigService.getApiConfigData(ipAddress, port);
  // this.apiConfigService.getApiProjectData(ipAddress, port);
  this.apiConfigService.getAPIConfig();

  this.apiConfigService.isConnectedAPI.subscribe( result => {
    console.log("isConnectedAPI", result)
    if (result) {
      console.log("true", result)
      this.loginModelStatusValue = false;
      this.showDivContent =false;
      this.apiConfigService.getProjectsData();
    } else {
      console.log("Fallllseeee")
      this.loginModelStatusValue = true
      this.isConnectedValue = false;
      this.showDivContent =false;
    }
  });




  
  // this.initaliseAPIData();

  this.apiConfigService.isConnected.subscribe( result => {
    console.log("isConnected", result)
    // this.isConnectedValue = result;

    // this.isDataAvailable = result;
    // this.loginModelStatusValue = !result;
    if (result) {
      // this.showDashboardConfig =true;
      // Will Load API 1st PAge
      // this.loginModelStatusValue = true;
      this.showDivContent =true;
      this.initaliseAPIData();
      // this.isDataAvailable = true;
      localStorage.setItem('APIPopup', 'false');
      localStorage.setItem('ipAddressAPI', ipAddress);
      localStorage.setItem('portAPI', port);
    } else {
      console.log("Fallllseeee")
      this.loginModelStatusValue = false
      this.isConnectedValue = false;
      this.showDivContent =false;
    }
  });
}

loginStatusBtn:boolean = false;
onChange(val, id){
  if((val.target.value).length != 0){
    this.loginStatusBtn = true;
  }
  console.log("val", val.target.value)
  console.log("val", (val.target.value).length)
}

loggerUrl:any;

exportLogger(){
  this.apiConfigService.exportAPILogger();

  this.apiConfigService.isConnectedLogger.subscribe( result => {
    console.log("isConnectedLogger", result)
    if (result) {
      this.loggerUrl = this.apiConfigService.getLoggerData();
      this.loggerUrl = this.loggerUrl.toString();
      console.log("this.loggerUrl",this.loggerUrl)
      window.open(this.loggerUrl, '_blank');
      // this.showDashboardConfig =true;
      // Will Load API 1st PAge
      // this.initaliseAPIData();
    }
  });

}


getUsers() {
  return new Promise(resolve => {
  this.http.get(this.apiUrl+'/users').subscribe(data => {
    console.log("hhhhhh", data)
  resolve(data);},
  err => {
  console.log("ggggggggggggggggggggggggggggggg",err);
  });
  });
  }

  showNewUser: boolean = false;
  inactiveTranscendServerVal:boolean =false;
  getDeletedIP:any;
  getDeletedID:any;

  inactiveTranscendServer(serverVal, deleteIp, deletePort, id){
    console.log("serverVal", serverVal)
    console.log("deleteIp", deleteIp)
    console.log("deletePort", deletePort)
    console.log("id", id)
    this.getDeletedIP = deleteIp + ":" + deletePort;
    this.inactiveTranscendServerVal = serverVal;
    this.getDeletedID = id;
  }

  deletedSCProjectId:any;
  inactiveSmartCore(showPopup){
    this.deletedSCProjectId = this.getDeletedID;
    console.log("this.deletedSCProjectId", this.deletedSCProjectId)
    this.apiConfigService.deleteSmartCore(this.deletedSCProjectId)
    this.apiConfigService.isConnectedDeleteSC.subscribe( result => {
      console.log("isConnectedValue", result)
      if (result) {
        this.inactiveTranscendServerVal =false;
        this.initaliseAPIData();
      }
    });

      // .subscribe(
      //   data => {
      //     console.log("this.getData", data);
      //     this.inactiveTranscendServerVal = false;
      //     this.initaliseAPIData();
      //   },
      //   error => {
      //     console.log("error", error.json())
      //   }
      // )
  }

  buttonModalClick(newUserVal){
    this.showNewUser =newUserVal;
  }
abc(){
  this.apiConfigService.getApiProjectSachin();
}

  showAPI: boolean = true;
  showMQTT: boolean = false;
  showTranscend: boolean = false;
  showTranscendEdit: boolean = false;


  firstLoad:boolean = true;

  // Users: any = sampleData;
  public formHumidex: FormGroup;
  public formHeatmap: FormGroup;

  // returns all form groups under contacts
  // get contactFormGroup() {
  //   return this.formSystem.get('contacts') as FormArray;
  // }

  allConfigList = ["API Server", "MQTT", "Transcend Server"]

  _postTranscendData:any;
  _postsJson:any;
  _postsArray:any;
  _postsArrayHumidex:any;
  _postArrayHeatmap :any;

  datafield: any;
  enablefield: any;


  masterSelectedUser:boolean;
  checklistUser:any;
  checkedListUser:any;
  checkedListValueUser:any;

  masterSelectedGeneral:boolean;
  checklistGeneral:any;
  checkedListGeneral:any;
  checkedListValueGeneral:any;

  masterSelectedBeacon:boolean;
  checklistBeacon:any;
  checkedListBeacon:any;
  checkedListValueBeacon:any;

  masterSelectedBuilding:boolean;
  checklistBuilding:any;
  checkedListBuilding:any;
  checkedListValueBuilding:any;
  boolValue:any

  constructor(private fb: FormBuilder, private apiConfigService : ApiConfigService, private http: Http) {

    this.onResize();
    // this.boolValue=localStorage.getItem('APIPopup')
    // this.boolValue = this.boolValue.toLowerCase() == 'true' ? true : false;
    console.log("localllllllllllllllllll",typeof this.boolValue);
    this.masterSelectedUser = false;
    this.masterSelectedGeneral = false;
    this.masterSelectedBeacon = false;
    this.masterSelectedBuilding = false;
    // this.lightScenesUser = ['creative', 'meeting', 'orbit', 'speaker', 'daylight', 'video', 'personal', 'fastFlash', 'slowFlash', 'fastPulse', 'slowPulse'];
    this.checklistUser = [
      {'lightScene': 'creative'},
      { 'lightScene': 'meeting' },
      { 'lightScene': 'orbit' },
      { 'lightScene': 'speaker' },
      { 'lightScene': 'daylight' },
      { 'lightScene': 'video' },
      { 'lightScene': 'personal' },
      { 'lightScene': 'fastFlash' },
      { 'lightScene': 'slowFlash' },
      { 'lightScene': 'fastPulse' },
      { 'lightScene': 'slowPulse' }
    ];
    this.checklistGeneral = [
      { 'lightScene' : 'stairs'},
      { 'lightScene': 'rr' },
      { 'lightScene': 'elevator' },
      { 'lightScene': 'lobby' },
      { 'lightScene': 'exit' },
      { 'lightScene': 'cafeteria' },
      { 'lightScene': 'aisle' },
      { 'lightScene': 'feature' }
    ];
    this.checklistBeacon = [
      {'lightScene': 'Off'},
      { 'lightScene': 'fastFlash' },
      { 'lightScene': 'slowFlash' },
      { 'lightScene': 'fastPulse' },
      { 'lightScene': 'slowPulse' },
      { 'lightScene': 'cycle' },
      { 'lightScene': 'chase' },
      { 'lightScene': 'oneColour' },
      { 'lightScene': 'twoColour' },
      { 'lightScene': 'threeColour' }
    ];
    this.checklistBuilding = [
      {'lightScene': 'workingHours'},
      { 'lightScene': 'outOfHours' }
    ];
      // this.getCheckedItemListUser();
      // this.checkUncheckAllOtherUser();
      // this.getCheckedItemListGeneral();
      // this.checkUncheckAllOtherGeneral();
      // this.getCheckedItemListBeacon();
      // this.checkUncheckAllOtherBeacon();
      // this.getCheckedItemListBuilding();
      // this.checkUncheckAllOtherBuilding();
  }

  // lightScenesUser = ['creative', 'meeting', 'orbit', 'speaker', 'daylight', 'video', 'personal', 'fastFlash', 'slowFlash', 'fastPulse', ];

  checkUncheckAllOtherUser() {
    for (var i = 0; i < this.checklistUser.length; i++) {
      if(this.lightScenesUser.includes(this.checklistUser[i].lightScene)){
        this.checklistUser[i].isSelectedUser =true;

      }
    }
  }

  checkUncheckAllUser() {
    for (var i = 0; i < this.checklistUser.length; i++) {
      this.checklistUser[i].isSelectedUser = this.masterSelectedUser;
    }
    this.getCheckedItemListUser();
  }
  isAllSelectedUser() {
    this.masterSelectedUser = this.checklistUser.every(function(item:any) {
      return item.isSelectedUser == true;
    })
    this.getCheckedItemListUser();
  }

  getCheckedItemListUser(){
    this.checkedListUser = [];
    this.checkedListValueUser = [];
    for (var i = 0; i < this.checklistUser.length; i++) {
      if(this.checklistUser[i].isSelectedUser){
        this.checkedListUser.push(this.checklistUser[i]);
        this.checkedListValueUser.push(this.checklistUser[i].lightScene);
      }
    }
    this.checkedListUser = JSON.stringify(this.checkedListUser);
    this.checkedListValueUser = JSON.stringify(this.checkedListValueUser);
  }

  // Check General zones
  // lightScenesGeneral = ['stairs', 'rr', 'elevator', 'lobby', 'exit', 'cafeteria', 'aisle', 'feature'];
  // lightScenesGeneral = ['stairs', 'rr', 'elevator', 'lobby', 'exit', 'aisle', 'feature'];
  checkUncheckAllOtherGeneral() {
    for (var i = 0; i < this.checklistGeneral.length; i++) {
      if(this.lightScenesGeneral.includes(this.checklistGeneral[i].lightScene)){
        this.checklistGeneral[i].isSelectedGeneral =true;

      }
    }
  }

  checkUncheckAllGeneral() {
    for (var i = 0; i < this.checklistGeneral.length; i++) {
      this.checklistGeneral[i].isSelectedGeneral = this.masterSelectedGeneral;
    }
    this.getCheckedItemListGeneral();
  }
  isAllSelectedGeneral() {
    this.masterSelectedGeneral = this.checklistGeneral.every(function(item:any) {
        return item.isSelectedGeneral == true;
      })
    this.getCheckedItemListGeneral();
  }

  getCheckedItemListGeneral(){
    this.checkedListGeneral = [];
    this.checkedListValueGeneral = [];
    for (var i = 0; i < this.checklistGeneral.length; i++) {
      if(this.checklistGeneral[i].isSelectedGeneral){
        this.checkedListGeneral.push(this.checklistGeneral[i]);
        this.checkedListValueGeneral.push(this.checklistGeneral[i].lightScene);
      }
    }
    this.checkedListGeneral = JSON.stringify(this.checkedListGeneral);
    this.checkedListValueGeneral = JSON.stringify(this.checkedListValueGeneral);
  }

  // GET BEACON ZONE

  // lightScenesBeacon = ['off','fastFlash', 'slowFlash', 'fastPulse', 'slowPulse', 'cycle', 'chase', 'oneColour', 'twoColour', 'threeColour'];

  checkUncheckAllOtherBeacon() {
    for (var i = 0; i < this.checklistBeacon.length; i++) {
      if(this.lightScenesBeacon.includes(this.checklistBeacon[i].lightScene)){
        this.checklistBeacon[i].isSelectedBeacon =true;

      }
    }
  }

  checkUncheckAllBeacon() {
    for (var i = 0; i < this.checklistBeacon.length; i++) {
      this.checklistBeacon[i].isSelectedBeacon = this.masterSelectedBeacon;
    }
    this.getCheckedItemListBeacon();
  }
  isAllSelectedBeacon() {
    this.masterSelectedBeacon = this.checklistBeacon.every(function(item:any) {
        return item.isSelectedBeacon == true;
      })
    this.getCheckedItemListBeacon();
  }

  getCheckedItemListBeacon(){
    this.checkedListBeacon = [];
    this.checkedListValueBeacon = [];
    for (var i = 0; i < this.checklistBeacon.length; i++) {
      if(this.checklistBeacon[i].isSelectedBeacon){
        this.checkedListBeacon.push(this.checklistBeacon[i]);
        this.checkedListValueBeacon.push(this.checklistBeacon[i].lightScene);
      }
    }
    this.checkedListBeacon = JSON.stringify(this.checkedListBeacon);
    this.checkedListValueBeacon = JSON.stringify(this.checkedListValueBeacon);
  }


  // GET Building ZONE
  // lightScenesBuilding = ['workingHours', 'outOfHours'];
  // lightScenesBuilding = ['workingHours'];

  checkUncheckAllOtherBuilding() {
    for (var i = 0; i < this.checklistBuilding.length; i++) {
      if(this.lightScenesBuilding.includes(this.checklistBuilding[i].lightScene)){
        this.checklistBuilding[i].isSelectedBuilding =true;

      }
    }
  }

  checkUncheckAllBuilding() {
    for (var i = 0; i < this.checklistBuilding.length; i++) {
      this.checklistBuilding[i].isSelectedBuilding = this.masterSelectedBuilding;
    }
    this.getCheckedItemListBuilding();
  }
  isAllSelectedBuilding() {
    this.masterSelectedBuilding = this.checklistBuilding.every(function(item:any) {
        return item.isSelectedBuilding == true;
      })
    this.getCheckedItemListBuilding();
  }

  getCheckedItemListBuilding(){
    this.checkedListBuilding = [];
    this.checkedListValueBuilding = [];
    for (var i = 0; i < this.checklistBuilding.length; i++) {
      if(this.checklistBuilding[i].isSelectedBuilding){
        this.checkedListBuilding.push(this.checklistBuilding[i]);
        this.checkedListValueBuilding.push(this.checklistBuilding[i].lightScene);
      }
    }
    this.checkedListBuilding = JSON.stringify(this.checkedListBuilding);
    this.checkedListValueBuilding = JSON.stringify(this.checkedListValueBuilding);
  }
  // baseUrl:string = "http://localhost:5000/";

  getJsonConfig(): void{
    const resultJson = this.apiConfigService.getJsonData();
    this._postsJson = resultJson;
    this.isDataAvailable = true;
}

_postsJsonAPI:any;

//   getAPIConfig(): void{
//     const resultJson = this.apiConfigService.getAPIData();
//     this._postsJsonAPI = resultJson.result.data[0].apiConfigurations;

//     var data = [];
//     data.push(this._postsJsonAPI);
//     this._postsJsonAPI = data;
//     console.log("this._postsJsonAPI", this._postsJsonAPI);
//     // this.isDataAvailable = true;
// }

// _smartCoreUrl:any;
// addSmartCore
_errorTranscendServer:boolean = false;

getData:any;
addSmartCore(ipAddress, port){
  console.log("ipAddress", ipAddress)
  console.log("port", port)
  this.apiConfigService.putsmartCore(ipAddress, port);
  this.apiConfigService.isConnectedNewSC.subscribe( result => {
    console.log("isConnectedValue", result)
    if (result) {
      this.showNewUser =false;
      this.initaliseAPIData();
    } else {
      this._errorTranscendServer = true;
    }
  });
      // .subscribe(
      //   data => {
      //     this.getData = data.json().status;
      //     console.log("this.getData", this.getData);
      //     console.log("this.getData",typeof this.getData);
      //     if(this.getData == false){
      //       console.log("False")
      //       this._errorTranscendServer = true;
      //     }else{
      //       console.log("true")
      //       this._errorTranscendServer = false;
      //       this.showNewUser =false;
      //       this.initaliseAPIData();
      //       // this.router.navigateByUrl('/user');

      //     }
      //     // this.router.navigate(ApiComponent));
      //     // console.log("hhhhhh", data.json().status)
      //   },
      //   error => {
      //     console.log("error", error.json())
      //   }
      // )

}

isBigEnough(element, index, array) {
  return (element >= 10);
}

passed = [12, 5, 8, 130, 44].filter(this.isBigEnough);



_getTranscendIp: any;
_getTranscendPort: any;
filterData:any={};
filterDataGeneral:any={};
filterDataBeacon:any={};
filterDataBuilding:any={};
allLightSceneName:any;
lightScene:any;
lightScenesUser:any = [];
lightScenesGeneral:any = [];
lightScenesBeacon:any = [];
lightScenesBuilding:any = [];

// getJsonConfigDummy(): void{
//     const resultJson = this.apiConfigService.getJsonDataDummy()
//     this._postTranscendData = resultJson;

//     this._getTranscendIp = this._postTranscendData[0].ip;
//     this._getTranscendPort = this._postTranscendData[0].port;
//     console.log("this._postTranscendData", this._postTranscendData );
//     console.log("############################", this._postTranscendData[0].controllers.lightScenes[0].type);
//     console.log("############################", this._postTranscendData[0].controllers.lightScenes);
//     this.lightScene = this._postTranscendData[0].controllers.lightScenes;

//     for (var i = 0; i < this.lightScene.length; i++) {
//       if(this.lightScene[i].type == "user"){
//         this.lightScenesUser.push(this.lightScene[i].key)
//       }else if(this.lightScene[i].type == "general"){
//         this.lightScenesGeneral.push(this.lightScene[i].key)
//       }else if(this.lightScene[i].type == "information"){
//         this.lightScenesBeacon.push(this.lightScene[i].key)
//       }else if(this.lightScene[i].type == "building"){
//         this.lightScenesBuilding.push(this.lightScene[i].key)
//       }
//       console.log("sdsdUSER VALUES", this.lightScenesUser)
//       console.log("sdsdUSER VALUESGENERAL", this.lightScenesGeneral)
//       console.log("sdsdUSER VALUESGENERAL", this.lightScenesBeacon)
//       console.log("sdsdUSER VALUESGENERAL", this.lightScenesBuilding)

//       this.checkUncheckAllOtherUser();
//       this.getCheckedItemListGeneral();
//       this.checkUncheckAllOtherGeneral();
//       this.getCheckedItemListBeacon();
//       this.checkUncheckAllOtherBeacon();
//       this.getCheckedItemListBuilding();
//       this.checkUncheckAllOtherBuilding();

//     }

//     this.filterData.type="user";
//     this.filterDataGeneral.type="general";
//     this.filterDataBeacon.type="information";
//     this.filterDataBuilding.type="building";
//     console.log("this._getTranscendIp", this._getTranscendIp );
//     console.log("this._getTranscendPort", this._getTranscendPort );

// }
getJsonConfigDummy(getApiJsonData): void{
    const resultJson = getApiJsonData;
    console.log("resultJson!!!!&&&&& ", resultJson)
    this._postTranscendData = resultJson.result.data;

    this._getTranscendIp = this._postTranscendData[0].ip;
    this._getTranscendPort = this._postTranscendData[0].port;
    console.log("this._postTranscendData", this._postTranscendData );
    console.log("############################", this._postTranscendData[0].controllers.lightScenes[0].type);
    console.log("############################", this._postTranscendData[0].controllers.lightScenes);
    this.lightScene = this._postTranscendData[0].controllers.lightScenes;

    for (var i = 0; i < this.lightScene.length; i++) {
      if(this.lightScene[i].type == "user"){
        this.lightScenesUser.push(this.lightScene[i].key)
      }else if(this.lightScene[i].type == "general"){
        this.lightScenesGeneral.push(this.lightScene[i].key)
      }else if(this.lightScene[i].type == "information"){
        this.lightScenesBeacon.push(this.lightScene[i].key)
      }else if(this.lightScene[i].type == "building"){
        this.lightScenesBuilding.push(this.lightScene[i].key)
      }
      console.log("sdsdUSER VALUES", this.lightScenesUser)
      console.log("sdsdUSER VALUESGENERAL", this.lightScenesGeneral)
      console.log("sdsdUSER VALUESGENERAL", this.lightScenesBeacon)
      console.log("sdsdUSER VALUESGENERAL", this.lightScenesBuilding)

      this.checkUncheckAllOtherUser();
      this.getCheckedItemListGeneral();
      this.checkUncheckAllOtherGeneral();
      this.getCheckedItemListBeacon();
      this.checkUncheckAllOtherBeacon();
      this.getCheckedItemListBuilding();
      this.checkUncheckAllOtherBuilding();

    }

    this.filterData.type="user";
    this.filterDataGeneral.type="general";
    this.filterDataBeacon.type="information";
    this.filterDataBuilding.type="building";
    // console.log("this._postTranscendData.controllers.lightScenes", this._postTranscendData.controllers[0].lightScenes[0].name);
    console.log("this._getTranscendIp", this._getTranscendIp );
    console.log("this._getTranscendPort", this._getTranscendPort );
    // var data = [];
    // data.push(resultJson);
    // this._postTranscendData = data;
    // console.log( "#####################################################################",this._postTranscendData[0].systemConfig)

}

_postsArrayApi:any;

public formApiServer: FormGroup;

cancel(){
  this.formApiServer.get('ip').setValue(this.optionsApi.ip);
  this.formApiServer.get('port').setValue(this.optionsApi.port);
}

apiVersion:any;
optionsApi:any;
  getApi(): void{
    // this.apiConfigService.getApi()
    //   .subscribe(
    //     resultArray => {
          // const resultArray = this.apiConfigService.getApi();
          const resultArray = this.apiConfigService.getAPIData();
          console.log("resultArray", resultArray);
          this.apiVersion = resultArray.apiVersion;
          this._postsArrayApi = resultArray;
          var data = [];
          data.push(resultArray.apiConfigurations);
          this._postsArrayApi = data;
          console.log("this._postsArrayApi", this._postsArrayApi);
          this.optionsApi = this._postsArrayApi[0];
          console.log("this.optionsApi", this.optionsApi);

          this.formApiServer.get('ip').setValue(this.optionsApi.ip);
          this.formApiServer.get('port').setValue(this.optionsApi.port);
          // this.formApiServer.get('IP').setValue(this.optionsApi.IP);
          // this.formApiServer.get('port').setValue(this.optionsApi.port);

        }


// apiVersion:any;
statusMessage:any;
  getStatusData(): void{

          const resultArrayStatus = this.apiConfigService.getStatusData();
          this.statusMessage = resultArrayStatus;
          console.log("resultArrayStatus", resultArrayStatus);
          // this.apiVersion = resultArray.apiVersion;
          // this._postsArrayApi = resultArray;
          // var data = [];
          // data.push(resultArray.apiConfigurations);
          // this._postsArrayApi = data;
          // console.log("this._postsArrayApi", this._postsArrayApi);
          // this.optionsApi = this._postsArrayApi[0];
          // console.log("this.optionsApi", this.optionsApi);

        }

getApiProject(): void{
    // this.apiConfigService.getHumidex()
    //   .subscribe(
    //     resultArray => {
      const resultArray = this.apiConfigService.getApiProject();
      this._postsArrayHumidex = resultArray;
      var data = [];
      data.push(resultArray);
      this._postsArrayHumidex = data;
      console.log("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!",data);
      // console.log("this._postsArrayHumidex", this._postsArrayHumidex)
      // console.log("this._postsArrayHumidex", this._postsArrayHumidex.costFactorInDollar)
    }

  getTemperatureValue: any;
  selectedDay: string = '';

  getHeatmap (): void{
    // this.apiConfigService.getHeatmap()
    //   .subscribe(
    //     resultArray => {
      const resultArray = this.apiConfigService.getSensor()
      this._postArrayHeatmap = resultArray;
      var data = [];
      data.push(resultArray);
      this._postArrayHeatmap = data;

      // if(this._postArrayHeatmap[0].temperature == "Fahrenheit"){
      //   this.getTemperatureValue ="Celcius"
      //   this.selectedDay = "Fahrenheit"
      // }else{
      //   this.getTemperatureValue ="Fahrenheit"
      //   this.selectedDay = "Celcius"
      // }
      // console.log("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!",data);

      // console.log("this._postArrayHeatmap ", this._postArrayHeatmap[0].temperature)
      // console.log("getTemperatureValue", this.getTemperatureValue)
    }

  selectChangeHandler (event: any) {
    //update the ui
    this.selectedDay = event.target.value;
    // console.log("selectedDay", this.selectedDay);
  }

ipPattern = "(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)";
_getApiJsonData:any;

currentIPAddress:any;
currentPort:any

public myInnerHeight: any;
disableGoBtn:any
ngOnInit() {
  console.log("*************************************************************************************************")
  this.currentIPAddress = localStorage.getItem('ipAddressAPI');
  this.currentPort = localStorage.getItem('portAPI');
  if(this.currentIPAddress==null && this.currentPort==null){
    this.loginModelStatusValue = true;
  }else{
    this.loginModelStatusValue = false;
    this.addApiAddress(this.currentIPAddress, this.currentPort);
  }
    console.log("LOCAL STORAG", this.currentIPAddress, this.currentPort);
    console.log("LOCAL STORAG", typeof this.currentIPAddress, typeof this.currentPort);
    console.log("Test Value : " + this.passed );
}

  closeForNewAPI:boolean = false;

  clicknewAPIBtn(){
    this.closeForNewAPI = true;
    this.loginModelStatusValue = true;
  }


  initaliseAPIData(){
    setTimeout(() => {
      // this.apiConfigService.getConfigDataDummy();
      // this.apiConfigService.getProjectsData();
      this.apiConfigService.getConfigDataProject().subscribe( resp => {
        console.log('got response@@@@@@@@@@@@@@@@@@@@@@@@@@', resp.json());
        this._getApiJsonData = resp.json();
        console.log("@@@@@@@@@@@@@@@@@@@@@@@@@@", this._getApiJsonData)
        this.getJsonConfigDummy(this._getApiJsonData);
        this.getJsonConfig();
        // this.getAPIConfig();
        this.getApiProject();
        this.getHeatmap();
        this.getApi();
        this.getCheckedItemListUser();

        // this.getStatusData();
      });

      this.formHumidex = this.fb.group({
        goodMin: ['', [Validators.required, Validators.minLength(5)]],
        goodMax: ['', [Validators.required, Validators.minLength(5)]],
        goodColor: ['', [Validators.required, Validators.minLength(5)]],
        normalMin: ['', [Validators.required, Validators.minLength(5)]],
        normalMax: ['', [Validators.required, Validators.minLength(5)]],
        normalColor: ['', [Validators.required, Validators.minLength(5)]],
        notGoodMin: ['', [Validators.required, Validators.minLength(5)]],
        notGoodMax: ['', [Validators.required, Validators.minLength(5)]],
        notGoodColor: ['', [Validators.required, Validators.minLength(5)]],
        badMin: ['', [Validators.required, Validators.minLength(5)]],
        badMax: ['', [Validators.required, Validators.minLength(5)]],
        badColor: ['', [Validators.required, Validators.minLength(5)]]

      });

      this.formHeatmap = this.fb.group({
        temperature: ['', [Validators.required, Validators.minLength(5)]]
      });

      this.formApiServer = this.fb.group({
        ip: ['', Validators.pattern(this.ipPattern)],
        port: ['', Validators.required]
      });

    },1000)
  }

  public isselected=0;

  // _getIndex: any;
  buttonClick(index){
    console.log("index", index)

    if(index==0){
      this.showAPI = true;
      this.showMQTT = false;
      this.showTranscend = false;
      this.showTranscendEdit = false;
    }else if(index==1){
      this.showAPI = false;
      this.showMQTT = true;
      this.showTranscend = false;
      this.showTranscendEdit = false;
    }else if(index==2){
      this.showAPI = false;
      this.showMQTT = false;
      this.showTranscend = true;
      this.showTranscendEdit = false;
    }

    // this._getIndex = index;
    this.isselected=index;
  };


  infoBtnClicked(){

    this.showTranscend = true;
    this.showTranscendEdit = false;
  }
  overlayClick(){
    console.log("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
    if(this.firstLoad == true){
      // this.firstLoad = false;
    }
  }


// EDIT OPTION
// sizes = [
//   {'lightScene': 'creative'},
//   { 'lightScene': 'meeting' },
//   { 'lightScene': 'orbit' },
//   { 'lightScene': 'speaker' },
//   { 'lightScene': 'daylight' },
//   { 'lightScene': 'video' },
//   { 'lightScene': 'personal' },
//   { 'lightScene': 'fastFlash' },
//   { 'lightScene': 'slowFlash' },
//   { 'lightScene': 'fastPulse' },
//   { 'lightScene': 'slowPulse' }
// ];

// lightScenesUser = ['creative', 'meeting', 'orbit', 'speaker', 'daylight', 'video', 'personal', 'fastFlash', 'slowFlash', 'fastPulse', 'slowPulse'];
  // lightScenesBeacon = ['off','fastFlash', 'slowFlash', 'fastPulse', 'slowPulse', 'cycle', 'chase', 'oneColour', 'twoColour', 'threeColour'];
  // lightScenesGeneral = ['stairs', 'rr', 'elevator', 'lobby', 'exit', 'cafeteria', 'aisle', 'feature'];
  // lightScenesBuilding = ['workingHours', 'outOfHours'];

checkAll(ev) {
  // this.sizes.forEach(x => x.state = ev.target.checked)
}

isAllChecked() {
  console.log('fired');
  // return this.sizes.every(_ => _.state);
}

options = ['OptionA', 'OptionB', 'OptionC'];
optionsMap = {
        OptionA: false,
        OptionB: false,
        OptionC: false,
};
optionsChecked = [];

initOptionsMap() {
  for (var x = 0; x<this.options.length; x++) {
      this.optionsMap[this.options[x]] = true;
  }
}

updateCheckedOptions(option, event) {
  this.optionsMap[option] = event.target.checked;
  console.log("option", option)
  console.log("this.optionsMap[option]", this.optionsMap[option])
  // this.updateOptions();
}

updateOptions() {
  for(var x in this.optionsMap) {
      if(this.optionsMap[x]) {
          this.optionsChecked.push(x);
          console.log("this.optionsChecked", this.optionsChecked)
      }
  }
  this.options = this.optionsChecked;
  console.log("this.optionsChecked", this.optionsChecked)
  this.optionsChecked = [];
}

  transcendEditBtn(){
    this.showTranscendEdit = true;
    console.log("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%", this.showTranscendEdit)
    this.showTranscend = false;
  }



  submitted = false;
  submittedHumidex = false;
  submittedHeatmap = false;
  submittedApi = false;

  onSubmitApi = function (api) {
    this.submittedApi = true;
    // if (this.formApi.invalid) {
    //     return;
    // }
    alert('SUCCESS!! :-)')
    var body = "ip=" + api.ip + "port=" + api.port ;
    this._postsJson.apiConfig = api;
    this.http.post("http://localhost:5200/api/saveConfig/", this._postsJson)
          .subscribe(
            data  => {
            //console.log("POST Request is successful ", data);
            },
            error  => {
            //console.log("Error", error);
            }
          );
    // this.http.post('http://localhost:5000/apiConfig', api).subscribe(data => console.log(JSON.stringify(data)));
  }


  onSubmitHumidex= function (Humidex) {
    this.submittedHumidex = true;
    console.log("GGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGG", this.submittedHumidex);
    // if (this.formHumidex.invalid) {
    //     return;
    // }

    alert('SUCCESS!! :-)')
    console.log("GGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGG", Humidex);
    var body = "goodMin=" + Humidex.goodMin + "goodMax=" + Humidex.goodMax + "goodColor=" + Humidex.goodColor + "normalMin=" + Humidex.normalMin + "normalMax=" + Humidex.normalMax + "normalColor=" + Humidex.normalColor + "notGoodMin=" + Humidex.notGoodMin + "notGoodMax=" + Humidex.notGoodMax + "notGoodColor=" + Humidex.notGoodColor + "badMin=" + Humidex.badMin + "badMax=" + Humidex.badMax + "badColor=" + Humidex.badColor ;

    console.log("body", body);
    this.http.post('http://localhost:5000/humidexRangeConfig', Humidex).subscribe(data => console.log(JSON.stringify(data)));
  }


  onSubmitHeatmap= function (Heatmap) {
    this.submittedHeatmap = true;
    console.log("GGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGG", this.submittedHeatmap);
    console.log("qqqqqqqqq", Heatmap);
    // if (this.formHeatmap.invalid) {
    //     return;
    // }

    alert('SUCCESS!! :-)')
    console.log("GGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGG", Heatmap);
    var body = "temperature=" + Heatmap.temperature;

    console.log("body", body);
    this.http.post('http://localhost:5000/sensorUnitConfig', Heatmap).subscribe(data => console.log(JSON.stringify(data)));
  }

  // convenience getter for easy access to form fields
  get fhumidex() { return this.formHumidex.controls; }
  get fheatmap() { return this.formHeatmap.controls; }
  get fApiServer() { return this.formApiServer.controls; }





  // contact formgroup
  createContact(): FormGroup {
    return this.fb.group({
      type: ['email', Validators.compose([Validators.required])], // i.e Email, Phone
      name: [null, Validators.compose([Validators.required])], // i.e. Home, Office
      value: [null, Validators.compose([Validators.required, Validators.email])]
    });
  }

  // MQTT CONFIGURATION
  _configStart:any;
  getMqttData:any;
  showMessage:any;
  startConfiguration(startVal){
    this.apiConfigService.loadMqttConfig(startVal);

    this.apiConfigService.isConnectedStatus.subscribe( result => {
      console.log("isConnectedValue", result)
      // this.isConnectedValue = result;
      this.loginModelStatusValue = !result;
      if (result) {
        this.getStatusData();
      }
    });
  }


  stopConfiguration(stopVal){
    this.apiConfigService.loadMqttConfig(stopVal);

    this.apiConfigService.isConnectedStatus.subscribe( result => {
      console.log("isConnectedValue", result)
      this.isConnectedValue = result;
      this.loginModelStatusValue = !result;
      if (result) {
        this.getStatusData();
      }
    });
  }

  statusConfiguration(pingVal){
    this.apiConfigService.loadMqttConfig(pingVal);

    this.apiConfigService.isConnectedStatus.subscribe( result => {
      console.log("isConnectedValue", result)
      this.isConnectedValue = result;
      this.loginModelStatusValue = !result;
      if (result) {
        this.getStatusData();
      }
    });
  }

}
