
import { Component, OnInit, ViewChild, ElementRef, NgZone, SimpleChanges, HostListener } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
// import { Observable } from 'rxjs/Observable';
import { Observable } from 'rxjs/Rx';
// import sampleData from './data.json';

import {ApiService} from '../service/api.service';
import {UserPortIp} from '../service/user-port-ip';
import {ConfigurationDashboard} from '../interface/configuration-dashboard';

import * as myGlobals from '../service/globalVariable';
import { AgmCoreModule, MapsAPILoader, AgmMap, LatLngBounds, MouseEvent } from '@agm/core';
import {} from '@agm/core/services/google-maps-types';
declare let google: any;

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  @HostListener('window:resize', ['$event'])
  onResize() {
    this.myInnerHeight = window.innerHeight;
    console.log("!@@@@@@!@!!!!!!!!!!!!!!!!", this.myInnerHeight)
  }


loginStatusBtn:boolean = false;
onChange(val, id){
  if((val.target.value).length != 0){
    this.loginStatusBtn = true;
  }
  console.log("val", val.target.value)
  console.log("val", (val.target.value).length)
}

  isselected: any = "Dashboard Configuration";
  show_dialog:boolean = true;

  // temperatureValue =["Fahrenheit", "Celcius"]

  addToList(selectedParameter){
    this.isselected = selectedParameter;
  }

  toggle() {
    this.show_dialog = !this.show_dialog;
  }

  latitude: number;
  longitude: number;
  zoom:number;
  address: string;
  private geoCoder;

  @ViewChild('search')
  public searchElementRef: ElementRef;



  showChart: boolean = true;
  showSensor: boolean = false;
  showApplication: boolean = false;
  showDb: boolean = false;
  showSystem: boolean = false;
  showJwt: boolean = false;
  showDms: boolean = false;
  showApi: boolean = false;
  showMqtt: boolean = false;
  showSmartCore: boolean = false;
  showCache: boolean = false;

  firstLoad:boolean = true;

  // Users: any = sampleData;
  public formBuilding: FormGroup;

  public formChart: FormGroup;
  public formHumidex: FormGroup;
  public formSensor: FormGroup;
  public formApplication: FormGroup;
  public formDB: FormGroup;
  public formSystem: FormGroup;
  public formJwt: FormGroup;
  public formDms: FormGroup;
  public formApi: FormGroup;
  public formMqtt: FormGroup;
  public formSmartCore: FormGroup;
  public formDetail: FormGroup;
  public contactList: FormArray;




  // returns all form groups under contacts
  // get contactFormGroup() {
  //   return this.formSystem.get('contacts') as FormArray;
  // }

  // allConfigList = ["chartConfig","sensorUnitConfig","applicationConfig", "dbConfig", "systemConfig", "jwtConfig", "apiConfig", "mqttConfig", "cacheData"]
  allConfigList = ["Dashboard Configuration"];
  _postsArrayBuilding:any;
  _postsBuilding:any;

  _postsJson:any;
  _postsArray:any;
  _postsArrayHumidex:any;
  _postsArraySensor:any;
  _postsArrayApplication:any;
  _postsArrayDb:any;
  _postsArraySystem:any;
  _postsArrayJwt:any;
  _postsArrayDms:any;
  _postsArrayApi:any;
  _postsArrayMqtt:any;
  _postsArraySmartCore:any;
  _mqttoptions:any;
  _SmartCoreoptions:any;
  // _systemTopics:any;
  topics:any;
  _postsArrayCache:any;

  getResetData: any;

  datafield: any;
  enablefield: any;
  boolValue:any;
  dashBoardPopup:any;
  _localIpAddress: any;
  _localPort: any;

  contacts: Array<UserPortIp>;


  feedbackForm: FormGroup;
  buildings = []

constructor(private fb: FormBuilder, private apiService : ApiService, private http: Http, private mapsAPILoader: MapsAPILoader, private ngZone: NgZone) {
    this.onResize();

    this.boolValue=localStorage.getItem('dashBoardPopupDashboard');
    if(this.boolValue != null){
      this.boolValue = this.boolValue.toLowerCase() == 'true' ? true : false;
    }
    //console.log("localllllllllllllllllll",typeof this.boolValue);
    this.contacts = [];
    this.dashBoardPopup = apiService.getPopupState;
    //console.log("localllllllllllllllllll",this.popUpState);


    if(this.dashBoardPopup == false){
      this._localIpAddress = localStorage.getItem('ipAddressDashboard')
      this._localPort = localStorage.getItem('portDashboard')
      //console.log("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@", this.popUpState, this._localIpAddress, this._localPort);
      this.addContactOnFalse(this._localIpAddress, this._localPort);
    //   this.apiService.getConfigData1(this._localIpAddress, this._localPort);
    //   this.apiService.isConnected.subscribe( result => {
    //     //console.log("isConnected", result)
    //     this.isConnectedValue = result
    //     this.loginModelStatusValue = !result;
    //     // this.buildingConfiguration = true;
    //     if (result) {
    //       // this.apiService.getConfigData();
    //       this.showDashboardConfig =true;
    //     }
    // });
  }

  }




  mapData:any;
//   ngAfterViewInit(): void {
//     this.agmMap.mapReady.subscribe(map => {
//     this.mapobj = map;
//   });
// }

// ngOnChanges(changes: SimpleChanges) {
//   const change = changes['mapData'];
//   // this.mapData = change.currentValue;
//   setTimeout(() => this.initMap(), 500);
// }

// center:any;
// // initMap();

//   initMap() {
//     //console.log('hope data comes',this.mapData);
//     const bounds: LatLngBounds = new google.maps.LatLngBounds();
//     for (const mm of this.mapData) {
//       bounds.extend(new google.maps.LatLng(mm.lat, mm.lng));
//     }

//     this.center = bounds.getCenter();  // still returns (55.04334815163844, -1.9917653831249726)

//     //console.log("^^^^^^^^^^^^^^^^^^^^", this.center)
//     var x = bounds.contains(this.center);
//     //console.log("^^^^^^^^^^^^^^^^^^^^", x)

//     this.mapobj.fitBounds(bounds);
//     this.setZoom();
//     // map.setZoom(map.getZoom() - 10);
//     this.agmMap.triggerResize();
// }


// setZoom() {
//   google.maps.event.addListenerOnce(this.mapobj, 'bounds_changed', function(event) {
//     if (this.getZoom() > 15) {
//       this.setZoom(5);
//     }
//   });
// }



  // get repliesControl() {
    //   return this.feedbackForm.get('replies');
    // }



  baseUrl:any;
  baseUrlSave:any;
  loginModelStatus:any
  loginModelStatusValue:boolean;
  isConnectedValue:boolean = true;
  localStorageValue:boolean;
  buildingConfiguration:boolean = false;
  showDivContent:boolean= false;

  buildingConfiguratioPop:boolean = false;

  addContact(ipAddress,port){
    console.log("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@", ipAddress, port);

    let contact = new UserPortIp(ipAddress,port);
    this.contacts.push(contact);

    // this.baseUrl = "http://" + ipAddress + ":" + port + "/" + "api/getConfig/";
    // this.baseUrlSave = "http://" + ipAddress + ":" + port + "/" + "api/saveConfig/";

    this.apiService.getDashboardServerStarted(ipAddress, port);

    // this.apiService.getConfigData1(ipAddress, port);
    this.apiService.isConnectedDashboard.subscribe( result => {
      if (result) {
        this.isConnectedValue = result;
        this.loginModelStatusValue = !result;
        this.showDashboardConfig =false;
        this.apiService.getProjectsData();
        this.apiService.getBuildingData(ipAddress, port);
        localStorage.setItem('dashBoardPopupDashboard', 'false');
        localStorage.setItem('ipAddressDashboard', ipAddress);
        localStorage.setItem('portDashboard', port);
      }
    });



    this.apiService.isConnectedProjectData.subscribe( result => {
      console.log("isConnectedProjectData", result)

      // this.buildingConfiguration = true;
      if (result) {
        // this.apiService.getConfigData();


        // //console.log("FFFFFFFFF")
        this.getJsonConfig();
        this.getSmartCoreConfig();
        this.getChartConfig();
        this.getHumidex();
        this.getSensor();
        this.getApplication();
        this.getDb();
        this.getSystem();
        this.getJwt();
        this.getDms();
        this.getApi();
        this.getMqtt();
        this.getSmartCore();
        this.getCache();
        this.subscribeInputFields();
         // this.apiService.getlatlng("pune")
        this.apiService.getlatlng("pune").subscribe( resp => {
          //console.log('MAP', resp.json());
        });


        // localStorage.setItem('dashBoardPopupDashboard', this.dashBoardPopupDashboard.length);
      } else {

      }
    });

    this.apiService.isConnectedBuilding.subscribe( result => {
      //console.log("isConnectedBuilding", result)
      this.isConnectedValue = result
      this.loginModelStatusValue = !result;
      if (result) {
        if(this.buildingConfiguratioPop == true){
          this.buildingConfiguration = false;
        } else {
          this.buildingConfiguration = true;
        }
        this.getSmartCoreConfig();
        this.getBuilding();
      }
    });
    // this.loginModelStatus = this.apiService.getJsonData()
    //console.log("this.loginModelStatus", this.loginModelStatus)

    // if(this.loginModelStatus == true){
    //   this.loginModelStatusValue =true
    // }else{
    //   this.loginModelStatusValue =false
    // }

  //console.log("this.loginModelStatusValue",this.loginModelStatusValue)
    // this.apiService.getJsonDataOnLoad(this.baseUrl)
  }


  addContactOnFalse(ipAddress,port){
    let contact = new UserPortIp(ipAddress,port);
    this.contacts.push(contact);

    this.baseUrl = "http://" + ipAddress + ":" + port + "/" + "api/getConfig/";
    this.baseUrlSave = "http://" + ipAddress + ":" + port + "/" + "api/saveConfig/";

    // this.apiService.getConfigData1(ipAddress, port);
    this.apiService.getDashboardServerStarted(ipAddress, port);
    this.apiService.isConnectedDashboard.subscribe( result => {
      console.log("dasssssss", result)
      if (result) {
        this.apiService.getProjectsData();
        this.apiService.getBuildingData(ipAddress, port);
      }
    });



    this.apiService.isConnectedProjectData.subscribe( result => {
      console.log("isConnectedFAAAAAALLLLLLSSSSEEEEEEEE", result)
      this.isConnectedValue = result;
      this.loginModelStatusValue = !result;
      // this.buildingConfiguration = true;
      if (result) {
        // this.showDashboardConfig =true;

        this.showDashboardConfig =false;
        this.buildingConfiguration = false;
        this.showDivContent = true;

        setTimeout(() => {
          //console.log("FFFFFFFFF")
          this.getJsonConfig();
          this.getSmartCoreConfig();
          this.getChartConfig();
          this.getHumidex();
          this.getSensor();
          this.getApplication();
          this.getDb();
          this.getSystem();
          this.getJwt();
          this.getDms();
          this.getApi();
          this.getMqtt();
          this.getSmartCore();
          this.getCache();
          this.subscribeInputFields();
           // this.apiService.getlatlng("pune")
          this.apiService.getlatlng("pune").subscribe( resp => {
            //console.log('MAP', resp.json());
          });
        },1000);
        // this.buildingConfiguration = false;
        // this.localStorageValue = "true";
        // this.localStorageValue = JSON.parse(this.localStorageValue);

        // localStorage.setItem('dashBoardPopupDashboard', 'false');
        // localStorage.setItem('ipAddressDashboard', ipAddress);
        // localStorage.setItem('portDashboard', port);

        // localStorage.setItem('dashBoardPopupDashboard', this.dashBoardPopupDashboard.length);
      }
    });

    // this.apiService.isConnectedBuilding.subscribe( result => {
    //   //console.log("isConnectedBuilding", result)
    //   this.isConnectedValue = result
    //   this.loginModelStatusValue = !result;
    //   if (result) {
    //     this.buildingConfiguration = true;
    //     this.getSmartCoreConfig();
    //     this.getBuilding();
    //   }
    // });
    // this.loginModelStatus = this.apiService.getJsonData()
    // //console.log("this.loginModelStatus", this.loginModelStatus)

    // if(this.loginModelStatus == true){
    //   this.loginModelStatusValue =true
    // }else{
    //   this.loginModelStatusValue =false
    // }

  // //console.log("this.loginModelStatusValue",this.loginModelStatusValue)
    // this.apiService.getJsonDataOnLoad(this.baseUrl)
  }

  saveBuilding(name,location,ipAddress,longitude,latitude){

    //console.log("name", name);
    //console.log("location", location);
    //console.log("ipAddress", ipAddress);
    //console.log("longitude", longitude);
    //console.log("latitude", latitude);

    this.buildingConfiguration = false;
    this.getJsonConfig();
    this.getSmartCoreConfig();
    this.getChartConfig();
    this.getHumidex();
    this.getSensor();
    this.getApplication();
    this.getDb();
    this.getSystem();
    this.getJwt();
    this.getDms();
    this.getApi();
    this.getMqtt();
    this.getSmartCore();
    this.getCache();
    this.subscribeInputFields();
    this.apiService.changStatus(false);

    // this.apiService.getlatlng("pune")
  this.apiService.getlatlng("pune").subscribe( resp => {
    //console.log('MAP', resp.json());
  });
   //console.log("Save");

  }




  removeContact(contact){
    let index = this.contacts.indexOf(contact);
    this.contacts.splice(index,1);
}

showDashboardConfig =false;
add2(){
  this.loginModelStatusValue = this.apiService.getJsonData()

  if(this.loginModelStatusValue == true){
    this.loginModelStatusValue = false;
    this.showDashboardConfig =true;
  }else{
    this.loginModelStatusValue = true;
  }

  //console.log("this.loginModelStatusValue",this.loginModelStatusValue)
}
  // baseUrl:string = "http://localhost:5000/";

  // baseUrl:any = "http://localhost:5200/api/getConfig/";


  // getChartConfig(): void{
  //   // //console.log("YEASSAS")
  //   this.apiService.getChartConfig()
  //     .subscribe(
  //       resultArray => {
  //        let temp=Object.keys(resultArray);
  //        temp.forEach(data=>{
  //         _postsArray.push(resultArray.data);
  //        });
  //     },
  //       error => { //console.log("Error ::" + error); }
  //     );
  // }

  // getResetData:any = this.apiService.getJsonData();
          // //console.log("getResetData", getResetData );

  getJsonReset(): void{
    // this._postsJson = this.getResetData;
    ////console.log("this._postsJson", this._postsJson );
    ////console.log("this.getResetData", this.getResetData );
  }

   DataPublishClick(value){
    console.log(value);
    this._postsJson.sensorUnitConfig.temperature = value;
    // this.order.type=value;
  }

enableValue:any;
  mqttEnableClick(value){
    console.log(value);
    // this._postsJson.mqttConfig.enable = value;
    this.enableValue = value
  }

  cancel() {
    this.formChart.get('costFactorInDollar').setValue(this.optionsChart.costFactorInDollar);
    this.formChart.get('heatMapRadius').setValue(this.optionsChart.heatMapRadius);
    this.formChart.get('PIRMAX').setValue(this.optionsChart.PIRMAX);
    this.formChart.get('PWMAX').setValue(this.optionsChart.PWMAX);
    this.formChart.get('CTMAX').setValue(this.optionsChart.CTMAX);
    this.formChart.get('AQMAX').setValue(this.optionsChart.AQMAX);
    this.formChart.get('ALMAX').setValue(this.optionsChart.ALMAX);
    this.formChart.get('TEMPMAX').setValue(this.optionsChart.TEMPMAX);
    this.formChart.get('HUMMAX').setValue(this.optionsChart.HUMMAX);

    this.formHumidex.get('goodMin').setValue(this.optionsHumidex.goodMin);
    this.formHumidex.get('goodMax').setValue(this.optionsHumidex.goodMax);
    this.formHumidex.get('goodColor').setValue(this.optionsHumidex.goodColor);
    this.formHumidex.get('normalMin').setValue(this.optionsHumidex.normalMin);
    this.formHumidex.get('normalMax').setValue(this.optionsHumidex.normalMax);
    this.formHumidex.get('normalColor').setValue(this.optionsHumidex.normalColor);
    this.formHumidex.get('notGoodMin').setValue(this.optionsHumidex.notGoodMin);
    this.formHumidex.get('notGoodMax').setValue(this.optionsHumidex.notGoodMax);
    this.formHumidex.get('notGoodColor').setValue(this.optionsHumidex.notGoodColor);
    this.formHumidex.get('badMin').setValue(this.optionsHumidex.badMin);
    this.formHumidex.get('badMax').setValue(this.optionsHumidex.badMax);
    this.formHumidex.get('badColor').setValue(this.optionsHumidex.badColor);

    // this.formSensor.get('temperature').setValue(this.optionsSensor.temperature);

    this.formApplication.get('name').setValue(this.optionsApplication.name);
    this.formApplication.get('location').setValue(this.optionsApplication.location);
    this.formApplication.get('longitude').setValue(this.optionsApplication.longitude);
    this.formApplication.get('latitude').setValue(this.optionsApplication.latitude);
    // this.formApplication.get('timeZone').setValue(this.optionsApplication.timeZone);
    this.formDB.get('database').setValue(this.optionsDb.database);
    this.formDB.get('host').setValue(this.optionsDb.host);
    this.formDB.get('password').setValue(this.optionsDb.password);
    this.formDB.get('user').setValue(this.optionsDb.user);
    this.formDB.get('multipleStatements').setValue(this.optionsDb.multipleStatements);
    this.formSystem.get('port').setValue(this.optionsSystem.port);
    this.formJwt.get('expiry').setValue(this.optionsJwt.expiry);
    this.formJwt.get('secretKey').setValue(this.optionsJwt.secretKey);
    this.formDms.get('IP').setValue(this.optionsDms.IP);
    this.formDms.get('DMSport').setValue(this.optionsDms.DMSport);
    this.formDms.get('firmwarePort').setValue(this.optionsDms.firmwarePort);
    this.formApi.get('IP').setValue(this.optionsApi.IP);
    this.formApi.get('port').setValue(this.optionsApi.port);
    // Simply navigate back to reminders view
    // window.location.reload();
  }

  getJsonConfig(): void{
          const resultJson = this.apiService.getJsonData()
          this._postsJson = resultJson;
          //console.log("this._postsJson$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$", this._postsJson );
          this.isDataAvailable = true;

          // var data = [];
          // data.push(resultJson);
          // this._postsJson = data;
          // //console.log( "#####################################################################",this._postsJson[0].systemConfig)
  }

  getSmartCoreConfig(): void{
    const resultJson = this.apiService.getSmartCoreData()
    this._postsBuilding = resultJson;
    //console.log("this._postsJson$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$", this._postsBuilding );
    // this.isDataAvailable = true;

    // var data = [];
    // data.push(resultJson);
    // this._postsJson = data;
    // //console.log( "#####################################################################",this._postsJson[0].systemConfig)

}

  optionsBuilding:any;
  getBuilding(): void{
    // this.apiService.getBuilding()
    //   .subscribe(
    //     resultArray => {
      const resultArray = this.apiService.getBuilding();
      this._postsArrayBuilding = resultArray;
      // var data = [];
      // data.push(resultArray);
      // this._postsArrayBuilding = data;
      //console.log("this._postsArrayBuilding", this._postsArrayBuilding)
      ////console.log("this._postsArrayBuilding", this._postsArrayBuilding.costFactorInDollar)

      this.optionsBuilding = this._postsArrayBuilding[0];
      // this.formBuilding.get('goodMin').setValue(this.optionsBuilding.goodMin);
      // this.formBuilding.get('goodMax').setValue(this.optionsBuilding.goodMax);
      // this.formBuilding.get('goodColor').setValue(this.optionsBuilding.goodColor);
      // this.formBuilding.get('normalMin').setValue(this.optionsBuilding.normalMin);
      // this.formBuilding.get('normalMax').setValue(this.optionsBuilding.normalMax);
      // this.formBuilding.get('normalColor').setValue(this.optionsBuilding.normalColor);
      // this.formBuilding.get('notGoodMin').setValue(this.optionsBuilding.notGoodMin);
      // this.formBuilding.get('notGoodMax').setValue(this.optionsBuilding.notGoodMax);
      // this.formBuilding.get('notGoodColor').setValue(this.optionsBuilding.notGoodColor);
      // this.formBuilding.get('badMin').setValue(this.optionsBuilding.badMin);
      // this.formBuilding.get('badMax').setValue(this.optionsBuilding.badMax);
      // this.formBuilding.get('badColor').setValue(this.optionsBuilding.badColor);
    }



  optionsChart:any;
  getChartConfig(): void{
    // //console.log("YEASSAS")
          const resultArray = this.apiService.getChartConfig()

          this._postsArray = resultArray;
          var data = [];
          data.push(resultArray);
          this._postsArray = data;
          this.optionsChart = this._postsArray[0];
          this.formChart.get('costFactorInDollar').setValue(this.optionsChart.costFactorInDollar);
          this.formChart.get('heatMapRadius').setValue(this.optionsChart.heatMapRadius);
          this.formChart.get('PIRMAX').setValue(this.optionsChart.PIRMAX);
          this.formChart.get('PWMAX').setValue(this.optionsChart.PWMAX);
          this.formChart.get('CTMAX').setValue(this.optionsChart.CTMAX);
          this.formChart.get('AQMAX').setValue(this.optionsChart.AQMAX);
          this.formChart.get('ALMAX').setValue(this.optionsChart.ALMAX);
          this.formChart.get('TEMPMAX').setValue(this.optionsChart.TEMPMAX);
          this.formChart.get('HUMMAX').setValue(this.optionsChart.HUMMAX);
          //console.log("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!",data);
          // //console.log("#########################",result);
          // //console.log("this._postsArray", this._postsArray)
          // //console.log("this._postsArray", this._postsArray.costFactorInDollar)

  }

  optionsHumidex:any;
  getHumidex(): void{
    // this.apiService.getHumidex()
    //   .subscribe(
    //     resultArray => {
      const resultArray = this.apiService.getHumidex()
      this._postsArrayHumidex = resultArray;
      var data = [];
      data.push(resultArray);
      this._postsArrayHumidex = data;
      ////console.log("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!",data);
      ////console.log("this._postsArrayHumidex", this._postsArrayHumidex)
      ////console.log("this._postsArrayHumidex", this._postsArrayHumidex.costFactorInDollar)

      this.optionsHumidex = this._postsArrayHumidex[0];
      this.formHumidex.get('goodMin').setValue(this.optionsHumidex.goodMin);
      this.formHumidex.get('goodMax').setValue(this.optionsHumidex.goodMax);
      this.formHumidex.get('goodColor').setValue(this.optionsHumidex.goodColor);
      this.formHumidex.get('normalMin').setValue(this.optionsHumidex.normalMin);
      this.formHumidex.get('normalMax').setValue(this.optionsHumidex.normalMax);
      this.formHumidex.get('normalColor').setValue(this.optionsHumidex.normalColor);
      this.formHumidex.get('notGoodMin').setValue(this.optionsHumidex.notGoodMin);
      this.formHumidex.get('notGoodMax').setValue(this.optionsHumidex.notGoodMax);
      this.formHumidex.get('notGoodColor').setValue(this.optionsHumidex.notGoodColor);
      this.formHumidex.get('badMin').setValue(this.optionsHumidex.badMin);
      this.formHumidex.get('badMax').setValue(this.optionsHumidex.badMax);
      this.formHumidex.get('badColor').setValue(this.optionsHumidex.badColor);
    }

  getTemperatureValue: any;
  selectedDay: string = '';
  optionsSensor:any;
  temperatureValue:any = [];
  getSensor(): void{
    // this.apiService.getSensor()
    //   .subscribe(
    //     resultArray => {
      const resultArray = this.apiService.getSensor()
      this._postsArraySensor = resultArray;
      var data = [];
      data.push(resultArray);
      this._postsArraySensor = data;
    this.selectedDay = this._postsArraySensor[0].temperature;
      if(this._postsArraySensor[0].temperature == "Fahrenheit"){
        this.temperatureValue =["Fahrenheit", "Celcius"]
      }else{
        this.temperatureValue =["Celcius", "Fahrenheit"]
      }
      this.optionsSensor = this._postsArraySensor[0];
      this.formSensor.get('temperature').setValue(this.optionsSensor.temperature);
    }

  selectChangeHandler (event: any) {
    //update the ui
    this.selectedDay = event.target.value;
    console.log("selectedDay", this.selectedDay);
  }
  optionsApplication:any;
  getApplication(): void{
    // this.apiService.getApplication()
    //   .subscribe(
    //     resultArray => {
          const resultArray = this.apiService.getApplication()
          this._postsArrayApplication = resultArray;
          var data = [];
          data.push(resultArray);
          this._postsArrayApplication = data;
          ////console.log("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!",data);
          ////console.log("this._postsArrayApplication", this._postsArrayApplication)
          this.optionsApplication = this._postsArrayApplication[0];
          this.formApplication.get('name').setValue(this.optionsApplication.name);
          this.formApplication.get('location').setValue(this.optionsApplication.location);
          this.formApplication.get('longitude').setValue(this.optionsApplication.longitude);
          this.formApplication.get('latitude').setValue(this.optionsApplication.latitude);
          // this.formApplication.get('timeZone').setValue(this.optionsApplication.timeZone);
        }

  getStatementValue: any;
  Statement: any;
  optionsDb: any;
  getDb(): void{
    // this.apiService.getDb()
    //   .subscribe(
    //     resultArray => {
          const resultArray = this.apiService.getDb()
          this._postsArrayDb = resultArray;
          var data = [];
          data.push(resultArray);
          this._postsArrayDb = data;

          if(this._postsArrayDb[0].multipleStatements == true){
            this.getStatementValue =false;
            this.Statement = true;
          }else{
            this.getStatementValue =true;
            this.Statement = false;
          }
          this.optionsDb = this._postsArrayDb[0];
          this.formDB.get('database').setValue(this.optionsDb.database);
          this.formDB.get('host').setValue(this.optionsDb.host);
          this.formDB.get('password').setValue(this.optionsDb.password);
          this.formDB.get('user').setValue(this.optionsDb.user);
          this.formDB.get('multipleStatements').setValue(this.optionsDb.multipleStatements);

        }

  selectStatementHandler (event: any) {
    this.Statement = event.target.value;
  }

  optionsSystem:any;
  getSystem(): void{
    // this.apiService.getSystem()
    //   .subscribe(
    //     resultArray => {
          const resultArray = this.apiService.getSystem()
          this._postsArraySystem = resultArray;
          var data = [];
          data.push(resultArray);
          this._postsArraySystem = data;
          ////console.log("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!",data);
          ////console.log("this._postsArraySystem", this._postsArraySystem)
          this.optionsSystem = this._postsArraySystem[0];
          this.formSystem.get('port').setValue(this.optionsSystem.port);
        }

  optionsJwt:any;
  getJwt(): void{
    // this.apiService.getJwt()
    //   .subscribe(
    //     resultArray => {
      const resultArray = this.apiService.getJwt()
      this._postsArrayJwt = resultArray;
      var data = [];
      data.push(resultArray);
      this._postsArrayJwt = data;
      ////console.log("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!",data);
      ////console.log("this._postsArrayJwt", this._postsArrayJwt)
      this.optionsJwt = this._postsArrayJwt[0];
      this.formJwt.get('expiry').setValue(this.optionsJwt.expiry);
      this.formJwt.get('secretKey').setValue(this.optionsJwt.secretKey);
    }

  optionsDms:any;
  getDms(): void{
    // this.apiService.getJwt()
    //   .subscribe(
    //     resultArray => {
      const resultArray = this.apiService.getDms();
      this._postsArrayDms = resultArray;
      var data = [];
      data.push(resultArray);
      this._postsArrayDms = data;
      ////console.log("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!",data);
      //console.log("this._postsArrayDms", this._postsArrayDms);
      this.optionsDms = this._postsArrayDms[0];
      this.formDms.get('IP').setValue(this.optionsDms.IP);
      this.formDms.get('DMSport').setValue(this.optionsDms.DMSport);
      this.formDms.get('firmwarePort').setValue(this.optionsDms.firmwarePort);
    }

  optionsApi:any;
  getApi(): void{
    // this.apiService.getApi()
    //   .subscribe(
    //     resultArray => {
          const resultArray = this.apiService.getApi();
          this._postsArrayApi = resultArray;
          var data = [];
          data.push(resultArray);
          this._postsArrayApi = data;
          //console.log("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!",data);
          //console.log("this._postsArrayApi", this._postsArrayApi)
          this.optionsApi = this._postsArrayApi[0];
          this.formApi.get('IP').setValue(this.optionsApi.IP);
          this.formApi.get('port').setValue(this.optionsApi.port);


        }


_formDisabledState:boolean = true;

// getInputStatus():void{

//   //console.log("this.formApi.valid", this.formApi.valid)
//   //console.log("this.formSystem.valid", this.formSystem.valid)
//   if(this.formApi.valid != true){
//     this._formDisabledState = false;
//     //console.log("this._formDisabledState", this._formDisabledState)
//   }else if(this.formSystem.valid != true){
//     this._formDisabledState = false;
//     //console.log("this._formDisabledState", this._formDisabledState)
//   }else{
//     this._formDisabledState = true;
//   }

// }

subscribeInputFields():void {
  this.formApi.valueChanges.subscribe(
    (data) => {
      // this.getInputStatus();
      this._formDisabledState = this.formApi.valid;
      this._postsJson.apiConfig = data;
    }
    );

  this.formSystem.valueChanges.subscribe(
      (data) => {
      // this.getInputStatus();
      this._formDisabledState = this.formSystem.valid;
      this._postsJson.systemConfig = data;
    }
  );

  this.formJwt.valueChanges.subscribe(
    (data) => {
      ////console.log("this.formJwt.valid", this.formJwt.valid)
      this._formDisabledState = this.formJwt.valid;
      this._postsJson.jwtConfig = data;
    }
  );
  this.formDms.valueChanges.subscribe(
    (data) => {
      ////console.log("this.formDms.valid", this.formDms.valid)
      this._formDisabledState = this.formDms.valid;
      this._postsJson.dmsConfig = data;
    }
  );

  this.formMqtt.valueChanges.subscribe(
    (data) => {
      this._formDisabledState = this.formMqtt.valid;
      this._postsJson.mqttConfig = data;
    }
    );

  this.feedbackForm.valueChanges.subscribe(
    (data) => {
      this._formDisabledState = this.feedbackForm.valid;
      //console.log("this._formDisabledState", this._formDisabledState)
      // this._postsJson.smartcoreConfig = data;
      this._postsJson.smartcoreConfig = data;
      //console.log("_postsJson.smartcoreConfig", this._postsJson.smartcoreConfig)
    }
    );

  this.formChart.valueChanges.subscribe(
      (data) => {
        this._formDisabledState = this.formChart.valid;
        this._postsJson.chartConfig = data;
      }
      );

  this.formSensor.valueChanges.subscribe(
    (data) => {
      // //this._formDisabledState = this.formSensor.valid;
      this._postsJson.sensorUnitConfig = data;
    }
    );

  this.formDB.valueChanges.subscribe(
    (data) => {
      this._postsJson.dbConfig = data;
      this._formDisabledState = this.formDB.valid;
    }
    );

  this.formApplication.valueChanges.subscribe(
      (data) => {
        this._postsJson.applicationConfig = data;
        this._formDisabledState = this.formApplication.valid;
      }
      );

  this.formHumidex.valueChanges.subscribe(
        (data) => {
          this._postsJson.humidexRangeConfig = data;
          this._formDisabledState = this.formHumidex.valid;
    }
  );

  // this.formBuilding.valueChanges.subscribe(
  //       (data) => {
  //         this._postsJson.smartcoreConfig = data;
  //         this._formDisabledState = this.formBuilding.valid;
  //   }
  // );

}

updatedBuildingPopup():void{
  // loginModelStatusValue
  this.buildingConfiguration= false
  this.buildingConfiguratioPop = true;
  this.loginModelStatusValue = false;
  this.showDivContent = true;
  //console.log("this.baseUrlSave1111111", this.baseUrlSave);
  //console.log("this.isDataAvailable", this.isDataAvailable);
  //console.log("this.showDivContent", this.showDivContent);
  //console.log("this._postsJson11111", this._postsJson);
  this.http.post(this.baseUrlSave, this._postsJson)
          .subscribe(
            data  => {
            //console.log("POST Request is successful!!!!!!!!!!!! ", data);
            this.currentIPAddress = localStorage.getItem('ipAddressDashboard');
            this.currentPort = localStorage.getItem('portDashboard');
            //console.log("POST Request is successfulcurrentIPPPPPP ", this.currentIPAddress);
            //console.log("POST Request is successfulcurrentPort ", this.currentPort);
            this.addContactOnFalse(this.currentIPAddress, this.currentPort);
            this.buildingConfiguration = false;
            },
            error  => {
            //console.log("Error", error);
            }
          );
}

savedDataStatus:boolean = false;
errorDataStatus:boolean = false;

errorPopupMsg(){
  setTimeout(() => {
    console.log('hide');
    this.savedDataStatus = false;
    this.errorDataStatus = false;
  }, 2000);
}


mainSubmit():void{
  //console.log("this.baseUrlSave", this.baseUrlSave);
  console.log("this._postsJson*******************************", this._postsJson);
  this.apiService.SaveBacNetConfiguration(this._postsJson);

  this.http.post(this.baseUrlSave, this._postsJson)
          .subscribe(
            data  => {
            //console.log("POST Request is successful ", data);
            this.savedDataStatus = true;
            this.errorPopupMsg();
            this.addContactOnFalse(this.currentIPAddress, this.currentPort);
            console.log("POST Request is successful ",  this.savedDataStatus);
            },
            error  => {
              this.errorDataStatus = true;
              this.errorPopupMsg();
            //console.log("Error", error);
            }
          );
}

  getMqttValue: any;
  Mqtt: any;
  optionsValue:any;
  optionsMqtt:any;
  systemValues:any;
  getMqtt(): void{
    // this.apiService.getMqtt()
    //   .subscribe(
    //     resultArray => {
      const resultArray = this.apiService.getMqtt()
      this._postsArrayMqtt = resultArray;
      var data = [];
      data.push(resultArray);
      this._postsArrayMqtt = data;

      if(this._postsArrayMqtt[0].enable == true){
        this.enableValue =["true", "false"]
      }else{
        this.enableValue =["false", "true"]
      }

      this._mqttoptions =  [];
      this._mqttoptions.push(this._postsArrayMqtt[0].options);
      // this._systemTopics =  [];
      // this._systemTopics.push(this._postsArrayMqtt[0].systemTopics);
      this.topics = []
      this.topics.push(this._postsArrayMqtt[0].topics);

      // if(this._postsArrayMqtt[0].getMqttValue == true){
      //   this.getMqttValue = false;
      //   this.Mqtt = true;
      //   console.log("this.getMqttValue",this.getMqttValue)
      //   console.log("this.Mqtt",this.Mqtt)
      // }else{
      //   this.getMqttValue = true;
      //   this.Mqtt = false;
      //   console.log("this.getMqttValue",this.getMqttValue)
      //   console.log("this.Mqtt",this.Mqtt)
      // }


      // const resultArray = this.apiService.getSensor()
      // this._postsArraySensor = resultArray;
      // var data = [];
      // data.push(resultArray);
      // this._postsArraySensor = data;

      // if(this._postsArraySensor[0].temperature == "Fahrenheit"){
      //   this.getTemperatureValue ="Celcius"
      //   this.selectedDay = "Fahrenheit"
      // }else{
      //   this.getTemperatureValue ="Fahrenheit"
      //   this.selectedDay = "Celcius"
      // }
      // this.optionsSensor = this._postsArraySensor[0];
      // this.formSensor.get('temperature').setValue(this.optionsSensor.temperature);






      this.optionsMqtt = this._postsArrayMqtt[0];
      this.optionsValue = this._postsArrayMqtt[0].options;
      // this.systemValues = this._postsArrayMqtt[0].systemTopics;

      this.formMqtt.get('brokerUrl').setValue(this.optionsMqtt.brokerUrl);
          // this.formMqtt.controls['options'].controls['port'].setValue('50');

      this.formMqtt.get(['options','port']).setValue(this.optionsValue.port);
      this.formMqtt.get(['options','username']).setValue(this.optionsValue.username);
      this.formMqtt.get(['options','password']).setValue(this.optionsValue.password);
      this.formMqtt.get(['options','reconnectPeriod']).setValue(this.optionsValue.reconnectPeriod);
      this.formMqtt.get(['options','keepalive']).setValue(this.optionsValue.keepalive);


      // this.formMqtt.get(['systemTopics','subscribe']).setValue(this.systemValues.subscribe);
      // this.formMqtt.get(['systemTopics','unsubscribe']).setValue(this.systemValues.unsubscribe);
    }

  getSmartCoreValue: any;
  SmartCore: any;
  buildingValue:any;
  optionsSmartCore:any;
  // systemValues:any;
  getSmartCore(): void{
    // this.apiService.getSmartCore()
    //   .subscribe(
    //     resultArray => {

      const resultArray = this.apiService.getSmartCore();
      this._postsArraySmartCore = resultArray.buildings;
      // this.mapData = this._postsArraySmartCore;
      // this.initMap();
      //console.log("this._postsArraySmartCore11111", this._postsArraySmartCore)
      // var data = [];
      // data.push(resultArray);
      // this._postsArraySmartCore = data;
      // //console.log("this._postsArraySmartCore", this._postsArraySmartCore)

      this._SmartCoreoptions =  [];
      this._SmartCoreoptions.push(this._postsArraySmartCore[0].building);
      // this._systemTopics =  [];
      // this._systemTopics.push(this._postsArraySmartCore[0].systemTopics);
      // this.topics = []
      // this.topics.push(this._postsArraySmartCore[0].topics);

      if(this._postsArraySmartCore[0].getSmartCoreValue == true){
        this.getSmartCoreValue = false;
        this.SmartCore = true;
      }else{
        this.getSmartCoreValue =true;
        this.SmartCore = false;
      }




      this.optionsSmartCore = this._postsArraySmartCore[0];
      this.buildingValue = this._postsArraySmartCore[0].building;


      const controls = this._postsArraySmartCore.map((c => this.fb.group({
        name: c.name,
        location: c.location,
        ip: c.ip,
        longitude: c.longitude,
        latitude: c.latitude
      })))

      this.feedbackForm = this.fb.group({
        buildings: this.fb.array(controls)
      })




      // this.formSmartCore.get('IP').setValue(this.optionsApi.IP);
      //     this.formApi.get('port').setValue(this.optionsApi.port);

      // this.formSmartCore.get('brokerUrl').setValue(this.optionsSmartCore.brokerUrl);
          // this.formSmartCore.controls['options'].controls['port'].setValue('50');
      // tslint:disable-next-line:prefer-for-of
      // for (let i = 0; i < this._postsArraySmartCore.length; i++ ) {
      //   this.formSmartCore.get('name').setValue(this._postsArraySmartCore[i].name);
      //   this.formSmartCore.get('location').setValue(this._postsArraySmartCore[i].location);
      //   this.formSmartCore.get('ip').setValue(this._postsArraySmartCore[i].ip);
      //   this.formSmartCore.get('longitude').setValue(this._postsArraySmartCore[i].longitude);
      //   this.formSmartCore.get('latitude').setValue(this._postsArraySmartCore[i].latitude);
      // };

    }

    get repliesControl() {
      return this.feedbackForm.get('buildings');
    }

  selectMqttHandler (event: any) {
    if(event.target.value == true){
      this.Mqtt = true;
      this.getMqttValue = false;
    }else{
      this.Mqtt = false;
      this.getMqttValue = true;
    }

  //   this.getMqttValue = false;
  //   this.Mqtt = true;
  //   console.log("this.getMqttValue",this.getMqttValue)
  //   console.log("this.Mqtt",this.Mqtt)
  // }else{
  //   this.getMqttValue = true;
  //   this.Mqtt = false;
  //   console.log("this.getMqttValue",this.getMqttValue)
  //   console.log("this.Mqtt",this.Mqtt)
  // }


  }

    optionsCache:any;
  getCache():void{
    // this.apiService.getCache()
    //   .subscribe(
    //     resultArray => {
      const resultArray = this.apiService.getCache();
      this._postsArrayCache = resultArray;
      var data = [];
      data.push(resultArray);
      this._postsArrayCache = data;

      this.optionsCache = this._postsArrayCache[0];

      // this.formDetail.get('projectDetail').setValue(this.optionsCache.projectDetail);
      // this.formDetail.get('floorDetail').setValue(this.optionsCache.floorDetail);
      // this.formDetail.get('projectStat').setValue(this.optionsCache.projectStat);

      ////console.log("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!",data);
      ////console.log("this._postsArrayCache", this._postsArrayApi)
      ////console.log("this._postsArrayCache", this._postsArrayCache)
  }

  ipPattern = "(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)";
  // latitudePattern = "^[-+]?([1-8]?\d(\.\d+)?|90(\.0+)?),\s*[-+]?(180(\.0+)?|((1[0-7]\d)|([1-9]?\d))(\.\d+)?)$";
  // latitudePattern = "/^[-+]?[0-9]{1,7}(\.[0-9]+)?$/";
  hexaPattern = "^#+([a-fA-F0-9]{6}|[a-fA-F0-9]{3})$";
  isDataAvailable:boolean = false;


  currentIPAddress:any;
  currentPort:any
  public myInnerHeight: any;
  ngOnInit() {
    this.myInnerHeight= window.innerHeight;
    console.log("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&", this.myInnerHeight)

    this.currentIPAddress = localStorage.getItem('ipAddressDashboard');
    this.currentPort = localStorage.getItem('portDashboard');
    if(this.currentIPAddress==null && this.currentPort==null){
      this.loginModelStatusValue = true;
    }else{
      this.loginModelStatusValue = false;
      this.addContactOnFalse(this.currentIPAddress, this.currentPort);
    };

    this.mapsAPILoader.load().then(() => {
      //console.log("Yaha");
      // this.setCurrentLocation();
      this.geoCoder = new google.maps.Geocoder;

      let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
        types: ["address"]
      });
      autocomplete.addListener("place_changed", () => {
        this.ngZone.run(() => {
          //get the place result
          let place: google.maps.places.PlaceResult = autocomplete.getPlace();

          //verify result
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }

          //set latitude, longitude and zoom
          this.latitude = place.geometry.location.lat();
          this.longitude = place.geometry.location.lng();
          this.zoom = 12;
          //console.log("$$$$$$$$$$$$$$$$$", this.latitude, this.longitude, this.zoom)
        });
      });
    });

    setTimeout(() => {
      this.formChart = this.fb.group({
        costFactorInDollar: ['', [Validators.required, Validators.minLength(5)]],
        heatMapRadius: ['', [Validators.required, Validators.minLength(5)]],
        PIRMAX: ['', [Validators.required, Validators.minLength(5)]],
        PWMAX: ['', [Validators.required, Validators.minLength(5)]],
        CTMAX: ['', [Validators.required, Validators.minLength(5)]],
        AQMAX: ['', [Validators.required, Validators.minLength(5)]],
        ALMAX: ['', [Validators.required, Validators.minLength(5)]],
        TEMPMAX: ['', [Validators.required, Validators.minLength(5)]],
        HUMMAX: ['', [Validators.required, Validators.minLength(5)]]
      });

      this.formHumidex = this.fb.group({
        goodMin: ['', [Validators.required, Validators.minLength(5)]],
        goodMax: ['', [Validators.required, Validators.minLength(5)]],
        goodColor: ['', [Validators.required, Validators.minLength(5), Validators.pattern(this.hexaPattern)]],
        normalMin: ['', [Validators.required, Validators.minLength(5)]],
        normalMax: ['', [Validators.required, Validators.minLength(5)]],
        normalColor: ['', [Validators.required, Validators.minLength(5), Validators.pattern(this.hexaPattern)]],
        notGoodMin: ['', [Validators.required, Validators.minLength(5)]],
        notGoodMax: ['', [Validators.required, Validators.minLength(5)]],
        notGoodColor: ['', [Validators.required, Validators.minLength(5), Validators.pattern(this.hexaPattern)]],
        badMin: ['', [Validators.required, Validators.minLength(5)]],
        badMax: ['', [Validators.required, Validators.minLength(5)]],
        badColor: ['', [Validators.required, Validators.minLength(5), Validators.pattern(this.hexaPattern)]]

      });

      this.formSensor = this.fb.group({
        temperature: ['', [Validators.required, Validators.minLength(5)]]
      });

      this.formApplication = this.fb.group({
        name: ['', [Validators.required, Validators.minLength(5)]],
        location: ['', [Validators.required, Validators.minLength(5)]],
        // longitude: ['', [Validators.required, Validators.pattern(this.latitudePattern)]],
        // latitude: ['', [Validators.required, Validators.pattern(this.latitudePattern)]],
        longitude: ['', Validators.required],
        latitude: ['', Validators.required],
        // timeZone: ['', [Validators.required, Validators.minLength(5)]],
      });

      this.formDB = this.fb.group({
        database: ['', [Validators.required, Validators.minLength(5)]],
        host: ['', [Validators.required, Validators.minLength(5)]],
        password: ['', [Validators.required, Validators.minLength(5)]],
        user: ['', [Validators.required, Validators.minLength(5)]],
        multipleStatements: [ false, Validators.required],
      });

      this.formSystem = this.fb.group({
        // port: [null, Validators.compose([Validators.required])],
        port: ['', [Validators.required, Validators.minLength(5)]]
        // organization: [null],
        // contacts: this.fb.array([this.createContact()])
      });
      this.formJwt = this.fb.group({
        // port: [null, Validators.compose([Validators.required])],
        expiry: ['', [Validators.required, Validators.minLength(5)]],
        secretKey: ['', [Validators.required, Validators.minLength(5)]]
        // organization: [null],
        // contacts: this.fb.array([this.createContact()])
      });
      this.formDms = this.fb.group({
        // port: [null, Validators.compose([Validators.required])],
        IP: ['', [Validators.required, Validators.minLength(5)]],
        DMSport: ['', [Validators.required, Validators.minLength(5)]],
        firmwarePort: ['', [Validators.required, Validators.minLength(5)]]
        // organization: [null],
        // contacts: this.fb.array([this.createContact()])
      });
      this.formApi = this.fb.group({
        // port: [null, Validators.compose([Validators.required])],
        IP: ['', [Validators.pattern(this.ipPattern), Validators.minLength(5)]],
        // inp: ['', Validators.pattern(this.ipPattern)],
        port: ['', [Validators.required, Validators.minLength(5)]]
        // organization: [null],
        // contacts: this.fb.array([this.createContact()])
      });
      this.formMqtt = this.fb.group({
        // port: [null, Validators.compose([Validators.required])],
        enable: ['', [Validators.required, Validators.minLength(5)]],
        brokerUrl: ['', [Validators.required, Validators.minLength(5)]],
        // port: ['', [Validators.required, Validators.minLength(5)]],
        options: this.fb.group({
          port: ['', [Validators.required, Validators.minLength(5)]],
          username: ['', [Validators.required, Validators.minLength(5)]],
          password: ['', [Validators.required, Validators.minLength(5)]],
          reconnectPeriod: ['', [Validators.required, Validators.minLength(5)]],
          keepalive: ['', [Validators.required, Validators.minLength(5)]]
        }),
        // systemTopics: this.fb.group({
        //   subscribe: ['', [Validators.required, Validators.minLength(5)]],
        //   unsubscribe: ['', [Validators.required, Validators.minLength(5)]],
        // })
        // organization: [null],

        // contacts: this.fb.array([this.createContact()])
      });

      this.formSmartCore = this.fb.group({
        // port: [null, Validators.compose([Validators.required])],
        // enable: ['', [Validators.required, Validators.minLength(5)]],
        // brokerUrl: ['', [Validators.required, Validators.minLength(5)]],
        // port: ['', [Validators.required, Validators.minLength(5)]],
        // options: this.fb.group({
          name: ['', [Validators.required, Validators.minLength(5)]],
          location: ['', [Validators.required, Validators.minLength(5)]],
          ip: ['', [Validators.required, Validators.minLength(5)]],
          longitude: ['', [Validators.required, Validators.minLength(5)]],
          latitude: ['', [Validators.required, Validators.minLength(5)]]
        // }),

      });

    // this.userForm = this.fb.group({
    //   type: this.fb.group({
    //     options: this.fb.array([]) // create empty form array
    //   })
    // });

      this.formDetail = this.fb.group({
        // port: [null, Validators.compose([Validators.required])],
        projectDetail: ['', [Validators.required, Validators.minLength(5)]],
        floorDetail: ['', [Validators.required, Validators.minLength(5)]],
        projectStat: ['', [Validators.required, Validators.minLength(5)]]
        // organization: [null],
        // contacts: this.fb.array([this.createContact()])
      });

      // this.getJsonConfig();


      //   this.getChartConfig();
      //   this.getHumidex();
      //   this.getSensor();
      //   this.getApplication();
      //   this.getDb();
      //   this.getSystem();
      //   this.getJwt();
      //   this.getApi();
      //   this.getMqtt();
      //   this.getCache();



      // this.getInputStatus();
      this.getResetData = this.apiService.getJsonData();
      //console.log("this.getResetData", this.getResetData)

    },1000);
  }

  showMapConfig: boolean = false;
  showBuildingData: any;
//  open Map Window
  getLocation(e, index){
    //console.log("e",e);
    //console.log("e",e.value);
    //console.log("loc",location);
    //console.log("index",index);
    this.showBuildingData = e.value;
    this.showMapConfig = true;
    this.setCurrentLocation(e.value, index);

  }

  setMapValue1(mapIndex){
    //console.log("mapIndex1111", mapIndex)
  }


  setMapValue(mapIndex,address,latitude,longitude){
    this._postsArraySmartCore[mapIndex].location =address;
    this._postsArraySmartCore[mapIndex].latitude = latitude;
    this._postsArraySmartCore[mapIndex].longitude = longitude;
    //console.log("this._postsJson", this._postsJson);
    this.http.post(this.baseUrlSave, this._postsJson)
            .subscribe(
              data  => {
              //console.log("POST Request is successful ", data);
              this.showMapConfig = false;
              this.addContactOnFalse(this.currentIPAddress, this.currentPort);
              },
              error  => {
              //console.log("Error", error);
              }
            );

    //console.log("this.mapIndex", this._postsArraySmartCore[mapIndex].location);
    //console.log("this.mapIndex", this._postsArraySmartCore[mapIndex].latitude);
    //console.log("this.mapIndex", this._postsArraySmartCore[mapIndex].longitude);
    //console.log("this.mapIndex", this._postsArraySmartCore);
    //console.log("this.mapIndex", mapIndex);
    //console.log("this.address", address);
    //console.log("this.latitude", latitude);
    //console.log("this.longitude", longitude);

  }
  // Get Current Location Coordinates
  returnIndexVal: any;

  private setCurrentLocation(value, indexVal) {
    //console.log("this.showBuildingData", value);
    //console.log("this.showBuildingData99999999999999999999999999", indexVal);
    this.latitude = value.latitude;
    this.longitude = value.longitude;
        // this.longitude = position.coords.longitude;
    this.zoom = 8;
    this.returnIndexVal = indexVal;
    // if ('geolocation' in navigator) {
    //   navigator.geolocation.getCurrentPosition((position) => {
    //     this.latitude = position.coords.latitude;
    //     this.longitude = position.coords.longitude;
    //     this.zoom = 8;
    //     this.getAddress(this.latitude, this.longitude);
    //   });
    // }
  }


  markerDragEnd($event: MouseEvent) {
    //console.log($event);
    this.latitude = $event.coords.lat;
    this.longitude = $event.coords.lng;
    this.getAddress(this.latitude, this.longitude);
  }

  getAddress(latitude, longitude) {
    this.geoCoder.geocode({ 'location': { lat: latitude, lng: longitude } }, (results, status) => {
      //console.log(results);
      //console.log(status);
      if (status === 'OK') {
        if (results[0]) {
          this.zoom = 12;
          this.address = results[0].formatted_address;
        } else {
          window.alert('No results found');
        }
      } else {
        window.alert('Geocoder failed due to: ' + status);
      }

    });
  }




  // public isselected=0;

  closeForNewDashboard:boolean = false;

  clicknewDashBoardBtn(){
    this.buildingConfiguratioPop = false;
    this.closeForNewDashboard = true;
    this.loginModelStatusValue = true;
    this.dashBoardPopup = true;
    this.isDataAvailable = false;
    this.showDivContent = false;

  }

  closenewDashBoardBtn(){
    this.closeForNewDashboard=false;
    this.loginModelStatusValue = false;
    this.isDataAvailable = true;
    this.showDivContent = true

  }

  // _getIndex: any;
  buttonClick(index){
    // this.showChart = false;
    ////console.log("index", index)

    if(index==0){
      this.showChart = true
      this.showSensor = false
      this.showApplication = false
      this.showDb = false
      this.showSystem = false
      this.showJwt = false
      this.showApi = false
      this.showMqtt = false
      this.showCache = false
    }else if(index==1){
      this.showChart = false
      this.showSensor = true
      this.showApplication = false
      this.showDb = false
      this.showSystem = false
      this.showJwt = false
      this.showApi = false
      this.showMqtt = false
      this.showCache = false
    }else if(index==2){
      this.showChart = false
      this.showSensor = false
      this.showApplication = true
      this.showDb = false
      this.showSystem = false
      this.showJwt = false
      this.showApi = false
      this.showMqtt = false
      this.showCache = false
    }else if(index==3){
      this.showChart = false
      this.showSensor = false
      this.showApplication = false
      this.showDb = true
      this.showSystem = false
      this.showJwt = false
      this.showApi = false
      this.showMqtt = false
      this.showCache = false
    }else if(index==4){
      this.showChart = false
      this.showSensor = false
      this.showApplication = false
      this.showDb = false
      this.showSystem = true
      this.showJwt = false
      this.showApi = false;
      this.showMqtt = false
      this.showCache = false
    }else if(index==5){
      this.showChart = false
      this.showSensor = false
      this.showApplication = false;
      this.showDb = false;
      this.showSystem = false;
      this.showJwt = true;
      this.showApi = false;
      this.showMqtt = false;
      this.showCache = false;
    } else if(index===6) {
      this.showChart = false;
      this.showSensor = false;
      this.showApplication = false;
      this.showDb = false;
      this.showSystem = false;
      this.showJwt = false;
      this.showApi = true;
      this.showMqtt = false;
      this.showCache = false;
    } else if(index === 7) {
      this.showChart = false;
      this.showSensor = false;
      this.showApplication = false;
      this.showDb = false;
      this.showSystem = false;
      this.showJwt = false;
      this.showApi = false;
      this.showMqtt = true;
      this.showCache = false;
    } else if(index === 8) {
      this.showChart = false;
      this.showSensor = false;
      this.showApplication = false;
      this.showDb = false;
      this.showSystem = false;
      this.showJwt = false;
      this.showApi = false;
      this.showMqtt = false;
      this.showCache = true;
    }

    // this._getIndex = index;
    this.isselected = index;
  }


  overlayClick() {
    // //console.log("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
    if (this.firstLoad === true) {
      // this.showChart = true;
      // this.firstLoad = false;
    }
  }

  submitted = false;
  submittedChart = false;
  submittedSensor = false;
  submittedApplication = false;
  submittedDB = false;
  submittedJwt = false;
  submittedDms = false;
  submittedApi = false;
  submittedDetail = false;


  // this.formMqtt.get('port').setValue('VIKAS');

  onSubmitChart = function(Chart) {
    this.submittedChart = true;
    // if (this.formChart.invalid) {
    //     return;
    // }
    alert('SUCCESS!! :-)');
    const body = 'costFactorInDollar=' + Chart.costFactorInDollar + 'heatMapRadius=' + Chart.heatMapRadius + 'PIRMAX=' + Chart.PIRMAX + 'PWMAX=' + Chart.PWMAX + 'CTMAX=' + Chart.CTMAX + 'AQMAX=' + Chart.AQMAX + 'ALMAX=' + Chart.ALMAX + 'TEMPMAX=' + Chart.TEMPMAX + 'HUMMAX=' + Chart.HUMMAX;

    this._postsJson.chartConfig = Chart;
    this.http.post('http://localhost:888/api/saveConfig/', this._postsJson)
          .subscribe(
            data  => {
            // //console.log("POST Request is successful ", data);
            },
            error  => {
            // //console.log("Error", error);
            }
          );

    // ////console.log("body", body);
    // this.http.post('http://localhost:5000/chartConfig', Chart).subscribe(data => ////console.log(JSON.stringify(data)));
  };

  onSubmitSensor = function(Sensor) {
    this.submittedSensor = true;
    // if (this.formSensor.invalid) {
    //     return;
    // }
    alert('SUCCESS!! :-)');
    const body = 'temperature=' + Sensor.temperature;
    this._postsJson.sensorUnitConfig = Sensor;
    this.http.post('http://localhost:888/api/saveConfig/', this._postsJson)
          .subscribe(
            data  => {
            // //console.log("POST Request is successful ", data);
            },
            error  => {
            // //console.log("Error", error);
            }
          );
    // ////console.log("body", body);
    // this.http.post('http://localhost:5000/sensorUnitConfig', Sensor).subscribe(data => //console.log(JSON.stringify(data)));
  };


  onSubmitApplication = function(Application) {
    this.submittedApplication = true;
    // if (this.formApplication.invalid) {
    //     return;
    // }
    alert('SUCCESS!! :-)');
    const body = 'name=' + Application.name + 'location=' + Application.location + 'longitude=' + Application.longitude + 'latitude=' + Application.latitude;
    this._postsJson.applicationConfig = Application;
    this.http.post('http://localhost:888/api/saveConfig/', this._postsJson)
          .subscribe(
            data  => {
            // //console.log("POST Request is successful ", data);
            },
            error  => {
            // //console.log("Error", error);
            }
          );

    // //console.log("body", body);
    // this.http.post('http://localhost:5000/applicationConfig', Application).subscribe(data => //console.log(JSON.stringify(data)));
  };

  onSubmitDb = function(DB) {
    this.submittedDB = true;
    // if (this.formDB.invalid) {
    //     return;
    // }
    this.datafield = [];
    this.datafield.push(DB);
    this.datafield.map((e) => {
      e.multipleStatements === 'true' ? e.multipleStatements = true : e.multipleStatements = false;
    });
    alert('SUCCESS!! :-)');
    const body = 'database=' + DB.database + 'host=' + DB.host + 'password=' + DB.password + 'user=' + DB.user + 'multipleStatements=' + this.multipleStatements;
    this._postsJson.dbConfig = DB;
    this.http.post('http://localhost:888/api/saveConfig/', this._postsJson)
          .subscribe(
            data  => {
            // //console.log("POST Request is successful ", data);
            },
            error  => {
            // //console.log("Error", error);
            }
          );

    // //console.log("body", body);
    // this.http.post('http://localhost:5000/dbConfig', DB).subscribe(data => //console.log(JSON.stringify(data)));
  };


  // convenience getter for easy access to form fields
  get fchart() { return this.formChart.controls; }
  get fhumidex() { return this.formHumidex.controls; }
  get fsensor() { return this.formSensor.controls; }
  get fapplication() { return this.formApplication.controls; }
  get fdb() { return this.formDB.controls; }
  get fsystem() { return this.formSystem.controls; }
  get fjwt() { return this.formJwt.controls; }
  get fdms() { return this.formDms.controls; }
  get fapi() { return this.formApi.controls; }
  get fmqtt() { return this.formMqtt.controls; }
  get fdetail() { return this.formDetail.controls; }

  // getSystemConfig: any;

  onSubmitSystem = function(user) {
    this.submitted = true;
    // if (this.formSystem.invalid) {
    //     return;
    // }
    this._postsJson.systemConfig = user;
    alert('SUCCESS!! :-)');
    const body = 'port=' + user.port;
    this.http.post('http://localhost:888/api/saveConfig/', this._postsJson)
          .subscribe(
            data  => {
            // //console.log("POST Request is successful ", data);
            },
            error  => {
            // //console.log("Error", error);
            }
          );
    // this.http.post('http://localhost:5200/api/saveConfig/', user).subscribe(data => //console.log(JSON.stringify(data)));
  };

  onSubmitJwt = function(jwt) {
    this.submittedJwt = true;
    // if (this.formJwt.invalid) {
    //     return;
    // }
    alert('SUCCESS!! :-)');
    const body = 'expiry=' + jwt.expiry + 'secretKey=' + jwt.secretKey ;
    this._postsJson.jwtConfig = jwt;
    this.http.post('http://localhost:888/api/saveConfig/', this._postsJson)
          .subscribe(
            data  => {
            // //console.log("POST Request is successful ", data);
            },
            error  => {
            // //console.log("Error", error);
            }
          );
    // this.http.post('http://localhost:5000/jwtConfig', jwt).subscribe(data => //console.log(JSON.stringify(data)));
    // this.http.post("http://www.testtttt.com", body).subscribe((data) => {});
  };

  onSubmitDms = function(dms) {
    this.submittedDms = true;
    // if (this.formJwt.invalid) {
    //     return;
    // }
    alert('SUCCESS!! :-)');
    const body = 'expiry=' + dms.expiry + 'secretKey=' + dms.secretKey ;
    this._postsJson.dmsConfig = dms;
    this.http.post('http://localhost:888/api/saveConfig/', this._postsJson)
          .subscribe(
            data  => {
            // //console.log("POST Request is successful ", data);
            },
            error  => {
            // //console.log("Error", error);
            }
          );
    // this.http.post('http://localhost:5000/jwtConfig', jwt).subscribe(data => //console.log(JSON.stringify(data)));
    // this.http.post("http://www.testtttt.com", body).subscribe((data) => {});
  };

  onSubmitApi = function(api) {
    this.submittedApi = true;
    // if (this.formApi.invalid) {
    //     return;
    // }
    alert('SUCCESS!! :-)');
    const body = 'IP=' + api.IP + 'port=' + api.port ;
    this._postsJson.apiConfig = api;
    this.http.post('http://localhost:888/api/saveConfig/', this._postsJson)
          .subscribe(
            data  => {
            // //console.log("POST Request is successful ", data);
            },
            error  => {
            // //console.log("Error", error);
            }
          );
    // this.http.post('http://localhost:5000/apiConfig', api).subscribe(data => //console.log(JSON.stringify(data)));
  };



  onSubmitMqtt = function(mqtt) {
    this.submittedMqtt = true;
    // if (this.formMqtt.invalid) {
    //     return;
    // }
    this.enablefield = [];
    this.enablefield.push(mqtt);
    this.enablefield.map((e) => {
      e.enable === 'true' ? e.enable = true : e.enable = false;
    });
    alert('SUCCESS!! :-)');
    this._postsJson.mqttConfig  = mqtt;
    this.http.post('http://localhost:888/api/saveConfig/', this._postsJson)
          .subscribe(
            data  => {
            // //console.log("POST Request is successful ", data);
            },
            error  => {
            // //console.log("Error", error);
            }
          );
    // this.http.post('http://localhost:5000/mqttConfig', mqtt).subscribe(data => //console.log(JSON.stringify(data)));
  };

  onSubmitSmartCore = function(SmartCore) {
    this.submittedSmartCore = true;
    // if (this.formSmartCore.invalid) {
    //     return;
    // }
    this.enablefield = [];
    this.enablefield.push(SmartCore);
    this.enablefield.map((e) => {
      e.enable === 'true' ? e.enable = true : e.enable = false;
    });
    alert('SUCCESS!! :-)');
    this._postsJson.smartcoreConfig  = SmartCore;
    this.http.post('http://localhost:5200/api/saveConfig/', this._postsJson)
          .subscribe(
            data  => {
            // //console.log("POST Request is successful ", data);
            },
            error  => {
            // //console.log("Error", error);
            }
          );
    // this.http.post('http://localhost:5000/SmartCoreConfig', SmartCore).subscribe(data => //console.log(JSON.stringify(data)));
  };

  onSubmitDetail = function(Detail) {
    this.submittedDetail = true;
    // if (this.formDetail.invalid) {
    //     return;
    // }
    alert('SUCCESS!! :-)');
    const body = 'projectDetail=' + Detail.projectDetail + 'floorDetail=' + Detail.floorDetail + 'projectStat=' + Detail.projectStat ;
    this._postsJson.cacheData  = Detail;
    this.http.post('http://localhost:888/api/saveConfig/', this._postsJson)
          .subscribe(
            data  => {
            // //console.log("POST Request is successful ", data);
            },
            error  => {
            // //console.log("Error", error);
            }
          );
    // this.http.post('http://localhost:5000/cacheData', Detail).subscribe(data => //console.log(JSON.stringify(data)));
  };


  // contact formgroup
  createContact(): FormGroup {
    return this.fb.group({
      type: ['email', Validators.compose([Validators.required])], // i.e Email, Phone
      name: [null, Validators.compose([Validators.required])], // i.e. Home, Office
      value: [null, Validators.compose([Validators.required, Validators.email])]
    });
  }

  // triggered to change validation of value field type
  changedFieldType(index) {
    let validators = null;

    if (this.getContactsFormGroup(index).controls.type.value === 'email') {
      validators = Validators.compose([Validators.required, Validators.email]);
    } else {
      validators = Validators.compose([
        Validators.required,
        Validators.pattern(new RegExp('^\\+[0-9]?()[0-9](\\d[0-9]{9})$')) // pattern for validating international phone number
      ]);
    }

    this.getContactsFormGroup(index).controls.value.setValidators(
      validators
    );

    this.getContactsFormGroup(index).controls.value.updateValueAndValidity();
  }

  // get the formgroup under contacts form array
  getContactsFormGroup(index): FormGroup {
    // this.contactList = this.form.get('contacts') as FormArray;
    const formGroup = this.contactList.controls[index] as FormGroup;
    return formGroup;
  }

  // method triggered when form is submitted
  submit() {
    // //console.log(this.formSystem.value);
  }

}
