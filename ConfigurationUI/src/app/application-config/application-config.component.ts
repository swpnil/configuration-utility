import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
// import sampleData from './data.json';

import {ApiService} from '../service/api.service';
import {ConfigurationDashboard} from '../interface/configuration-dashboard'

@Component({
  selector: 'app-application-config',
  templateUrl: './application-config.component.html',
  styleUrls: ['./application-config.component.css']
})
export class ApplicationConfigComponent implements OnInit {

  showHumidex: boolean = true;
  showHeatmap: boolean = false;

  firstLoad:boolean = true;

  // Users: any = sampleData;
  public formHumidex: FormGroup;
  public formHeatmap: FormGroup;

  // returns all form groups under contacts
  // get contactFormGroup() {
  //   return this.formSystem.get('contacts') as FormArray;
  // }

  allConfigList = ["HumidexRangeConfig", "Heatmap Config"]

   _postsArray:any;
   _postsJson:any;
  _postsArrayHumidex:any;
  _postArrayHeatmap :any;

  datafield: any;
  enablefield: any;

  constructor(private fb: FormBuilder, private apiService : ApiService, private http: Http) {}

  // baseUrl:string = "http://localhost:5000/";

  getJsonConfig(): void{
    const resultJson = this.apiService.getJsonData()
    this._postsJson = resultJson;
    console.log("this._postsJson", this._postsJson );
    this.isDataAvailable = true;
    // var data = [];
    // data.push(resultJson);
    // this._postsJson = data;
    // console.log( "#####################################################################",this._postsJson[0].systemConfig)

}

optionsHumidex:any;
  getHumidex(): void{
    // this.apiService.getHumidex()
    //   .subscribe(
    //     resultArray => {
      const resultArray = this.apiService.getHumidex()
      this._postsArrayHumidex = resultArray;
      var data = [];
      data.push(resultArray);
      this._postsArrayHumidex = data;
      console.log("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!",data);
      console.log("this._postsArrayHumidex", this._postsArrayHumidex)
      console.log("this._postsArrayHumidex", this._postsArrayHumidex.costFactorInDollar)

      this.optionsHumidex = this._postsArrayHumidex[0];
      this.formHumidex.get('goodMin').setValue(this.optionsHumidex.goodMin);
      this.formHumidex.get('goodMax').setValue(this.optionsHumidex.goodMax);
      this.formHumidex.get('goodColor').setValue(this.optionsHumidex.goodColor);
      this.formHumidex.get('normalMin').setValue(this.optionsHumidex.normalMin);
      this.formHumidex.get('normalMax').setValue(this.optionsHumidex.normalMax);
      this.formHumidex.get('normalColor').setValue(this.optionsHumidex.normalColor);
      this.formHumidex.get('notGoodMin').setValue(this.optionsHumidex.notGoodMin);
      this.formHumidex.get('notGoodMax').setValue(this.optionsHumidex.notGoodMax);
      this.formHumidex.get('notGoodColor').setValue(this.optionsHumidex.notGoodColor);
      this.formHumidex.get('badMin').setValue(this.optionsHumidex.badMin);
      this.formHumidex.get('badMax').setValue(this.optionsHumidex.badMax);
      this.formHumidex.get('badColor').setValue(this.optionsHumidex.badColor);
    }

  getTemperatureValue: any;
  selectedDay: string = '';

  getHeatmap (): void{
    // this.apiService.getHeatmap()
    //   .subscribe(
    //     resultArray => {
      const resultArray = this.apiService.getSensor()
      this._postArrayHeatmap = resultArray;
      var data = [];
      data.push(resultArray);
      this._postArrayHeatmap = data;

      if(this._postArrayHeatmap[0].temperature == "Fahrenheit"){
        this.getTemperatureValue ="Celcius"
        this.selectedDay = "Fahrenheit"
      }else{
        this.getTemperatureValue ="Fahrenheit"
        this.selectedDay = "Celcius"
      }
      console.log("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!",data);

      console.log("this._postArrayHeatmap ", this._postArrayHeatmap[0].temperature)
      console.log("getTemperatureValue", this.getTemperatureValue)
    }

  selectChangeHandler (event: any) {
    //update the ui
    this.selectedDay = event.target.value;
    console.log("selectedDay", this.selectedDay);
  }

  hexaPattern = "^#+([a-fA-F0-9]{6}|[a-fA-F0-9]{3})$";
  isDataAvailable:boolean = false;

  ngOnInit() {
    // this.apiService.getConfigData();
    setTimeout(() => {
      this.formHumidex = this.fb.group({
        goodMin: ['', [Validators.required, Validators.minLength(5)]],
        goodMax: ['', [Validators.required, Validators.minLength(5)]],
        goodColor: ['', [Validators.required, Validators.minLength(5), Validators.pattern(this.hexaPattern)]],
        normalMin: ['', [Validators.required, Validators.minLength(5)]],
        normalMax: ['', [Validators.required, Validators.minLength(5)]],
        normalColor: ['', [Validators.required, Validators.minLength(5), Validators.pattern(this.hexaPattern)]],
        notGoodMin: ['', [Validators.required, Validators.minLength(5)]],
        notGoodMax: ['', [Validators.required, Validators.minLength(5)]],
        notGoodColor: ['', [Validators.required, Validators.minLength(5), Validators.pattern(this.hexaPattern)]],
        badMin: ['', [Validators.required, Validators.minLength(5)]],
        badMax: ['', [Validators.required, Validators.minLength(5)]],
        badColor: ['', [Validators.required, Validators.minLength(5), Validators.pattern(this.hexaPattern)]]

      });

      this.formHeatmap = this.fb.group({
        temperature: ['', [Validators.required, Validators.minLength(5)]]
      });
      this.getJsonConfig();
      this.getHumidex();
      this.getHeatmap();
  },1000)

  }


  public isselected=0;

  // _getIndex: any;
  buttonClick(index){
    console.log("index", index)

    if(index==0){
      this.showHumidex = true
      this.showHeatmap = false
    }else if(index==1){
      this.showHumidex = false
      this.showHeatmap = true
    }

    // this._getIndex = index;
    this.isselected=index;
  };


  overlayClick(){
    console.log("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
    if(this.firstLoad == true){
      // this.firstLoad = false;
    }
  }

  submitted = false;
  submittedHumidex = false;
  submittedHeatmap = false;

  onSubmitHumidex= function (Humidex) {
    this.submittedHumidex = true;
    // if (this.formHumidex.invalid) {
    //     return;
    // }
    alert('SUCCESS!! :-)')
    var body = "goodMin=" + Humidex.goodMin + "goodMax=" + Humidex.goodMax + "goodColor=" + Humidex.goodColor + "normalMin=" + Humidex.normalMin + "normalMax=" + Humidex.normalMax + "normalColor=" + Humidex.normalColor + "notGoodMin=" + Humidex.notGoodMin + "notGoodMax=" + Humidex.notGoodMax + "notGoodColor=" + Humidex.notGoodColor + "badMin=" + Humidex.badMin + "badMax=" + Humidex.badMax + "badColor=" + Humidex.badColor ;

    // this.http.post('http://localhost:5000/humidexRangeConfig', Humidex).subscribe(data => console.log(JSON.stringify(data)));
    this._postsJson.humidexRangeConfig = Humidex;
    this.http.post("http://localhost:5200/api/saveConfig/", this._postsJson)
          .subscribe(
            data  => {
            console.log("POST Request is successful ", data);
            },
            error  => {
            console.log("Error", error);
            }
          );
  }


  onSubmitHeatmap= function (Heatmap) {
    this.submittedHeatmap = true;
    // if (this.formHeatmap.invalid) {
    //     return;
    // }
    alert('SUCCESS!! :-)')
    var body = "temperature=" + Heatmap.temperature;
    // this.http.post('http://localhost:5000/sensorUnitConfig', Heatmap).subscribe(data => console.log(JSON.stringify(data)));
    this._postsJson.humidexRangeConfig = Heatmap;
    this.http.post("http://localhost:5200/api/saveConfig/", this._postsJson)
          .subscribe(
            data  => {
            console.log("POST Request is successful ", data);
            },
            error  => {
            console.log("Error", error);
            }
          );
  }

  // convenience getter for easy access to form fields
  get fhumidex() { return this.formHumidex.controls; }
  get fheatmap() { return this.formHeatmap.controls; }





  // contact formgroup
  createContact(): FormGroup {
    return this.fb.group({
      type: ['email', Validators.compose([Validators.required])], // i.e Email, Phone
      name: [null, Validators.compose([Validators.required])], // i.e. Home, Office
      value: [null, Validators.compose([Validators.required, Validators.email])]
    });
  }




}
