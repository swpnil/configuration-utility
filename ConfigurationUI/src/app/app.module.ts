import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Pipe } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';

import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import {TooltipModule} from "ngx-tooltip";

import { HttpClientModule } from '@angular/common/http';

import {FilterPipe} from './pipe/filter.pipe';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ApiComponent } from './api/api.component';
// import { BacknetComponent } from './backnet/backnet.component';
import { BacknetModule } from './backnet/backnet.module';
// import { ApiModule } from './api/api.module';


import { SidebarDirective } from './sidebar.directive';
import { ExpandMenu } from './directive/sideNav.directive';
import { ApplicationConfigComponent } from './application-config/application-config.component';
import { UserConfigComponent } from './user-config/user-config.component';

import { AgmCoreModule, GoogleMapsAPIWrapper } from '@agm/core';
import { AgmOverlays } from "agm-overlays";

import { Ng4GeoautocompleteModule } from 'ng4-geoautocomplete';

import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
 
import { ToastrModule } from 'ngx-toastr';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    DashboardComponent,
    ApiComponent,
    // BacknetComponent,
    SidebarDirective,
    ApplicationConfigComponent,
    UserConfigComponent,
    FilterPipe,
    ExpandMenu
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,

    FormsModule,
    HttpModule,
    AgmOverlays,
    AgmCoreModule.forRoot({
      libraries: ["places"]
    }),
    Ng4GeoautocompleteModule.forRoot(),
    BacknetModule,
    // ApiModule,
    TooltipModule,
    CommonModule,
    BrowserAnimationsModule, // required animations module
    ToastrModule.forRoot() // ToastrModule added
    // AgmCoreModule.forRoot({
    //   apiKey: 'AIzaSyA6xc2JUYuXQ9CmJjR6LPwe6KuP7GF7U6w'
    // })
  ],
  providers: [ GoogleMapsAPIWrapper ],
  bootstrap: [AppComponent]
})
export class AppModule { }
