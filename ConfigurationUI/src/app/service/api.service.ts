import { Injectable, EventEmitter } from '@angular/core';
import { Http, Response, Headers} from '@angular/http';
// import {Observable} from 'rxjs/Observable';
import { HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/Rx';
import {ConfigurationDashboard} from '../interface/configuration-dashboard'

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Authorization': 'my-auth-token'
  })
};

@Injectable({
  providedIn: 'root'
})
export class ApiService {
   public isConnected: EventEmitter<any> = new EventEmitter();
   public isConnectedBuilding: EventEmitter<any> = new EventEmitter();
   public isConnectedDashboard: EventEmitter<any> = new EventEmitter();
   public isConnectedProjectData: EventEmitter<any> = new EventEmitter();
   public isConnectedDashboardPost: EventEmitter<any> = new EventEmitter();
   constructor(private http: Http){

   }

  //  baseUrl:string = "http://localhost:5000/";
  //  baseUrl:any = "http://localhost:5200/api/getConfig/";

  ipAddress:any;
    port:any;
    getDashboardData:any;
    getDashboardServerStarted(ipAddressDashboard, portDashboard) {
      console.log("Server Called");
      this.ipAddress = ipAddressDashboard;
      this.port = portDashboard;
      const headers = new Headers();
      headers.append('Content-Type', 'application/x-www-form-urlencoded');

      let postParams = {
        ip: this.ipAddress,
        port: this.port,
        setModuleName: "Dashboard"
      }
            // .set('Authorization', 'my-auth-token')
            // .set('Content-Type', 'application/json');

            console.log("postParamsof SetIPDashboard----------- ", postParams)

      this.http.put('http://localhost:888/setIpDashboard', JSON.stringify(postParams), { headers: headers })
      .subscribe(data => {
        this.getDashboardData = data.json();
        if ( this.getDashboardData.status == true) {
        //  console.log("Server DATA11111111111", data.json());
          this.isConnectedDashboard.emit(true);
        } else {
          this.isConnectedDashboard.emit(false);
          console.log("Server ERROR");// Error getting the data
        }

      });
    }


    configData: any;
    configDataAll: any;
  getProject: any;
  getProjectsData() {
    this.getProject = 'http://localhost:888' + '/' + 'getConfig';

    console.log('this.baseUrl', this.baseUrl);
    this.http
    .get(this.getProject)
    .map((response: Response) => {
      // this.configData = response.json();
      this.configDataAll = response.json();
      this.configData = this.configDataAll.result.data[0];

      //  this.configData = response;
      console.log('response@@@@@@@@@@@@@@@@@@@@@@@@@@@@', response);
      console.log('!!!!!!!!!!!!!!!!!!!!this.configData@@@@@@@@@@@@@@@@@@@@@@', this.configData);
      }).subscribe(
        response => {
          // return this._getSuccesPortIP = true;
          // this.configData = true;
          console.log('response192.168.80.93', response);
          console.log('response192.168.80.93', this.configData);
          this.isConnectedProjectData.emit(true);

        },
        error => {
          // this.error = error
          console.log('error###################################', error);
          console.log('error###################################', this.configData);
          // this.configData = false;
          this.isConnectedProjectData.emit(false);
        }
      );
  }



  getlatlng(address){
    return this.http.get('https://maps.googleapis.com/maps/api/geocode/json?address=' + address)
  }

  getPopupState:boolean = true;

  changStatus(value){
    this.getPopupState = value;
    console.log("dsdjdsdjdjsd", this.getPopupState);
  }


   baseUrl:any;
   baseUrlSave:any;
   baseUrlBuilding:any;

  //  configData: any;
   configDataBuilding: any;
  //  baseUrl = JSON.parse(this.baseUrl1);

  getConfigData(baseUrl) {
    console.log("baseUrl",baseUrl)
    this.http
      .get(this.baseUrl)
      .map((response: Response) => {
        this.configData = response.json();
        console.log("response", response);
      }).subscribe();
  }



  _getSuccesPortIP: Boolean;

  getConfigData1(ipAddress, port){
    // console.log("baseUrl",baseUrl)
    // console.log("type of baseUrl",typeof baseUrl)
    let baseUrl = this.getJsonDataOnLoad(ipAddress, port)
    let baseUrlSave = this.getJsonDataOnSave(ipAddress, port)

    this.http
      .get(baseUrl)
      .map((response: Response) => {
        this.configData = response.json();
        console.log("response@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@", response);
      }).subscribe(
        response => {
          // return this._getSuccesPortIP = true;
          // this.configData = true;
          console.log("response192.168.80.93", response)
          console.log("response192.168.80.93", this.configData)
          this.isConnected.emit(true);

        },
        error => {
          // this.error = error
          console.log("error###################################", error)
          console.log("error###################################", this.configData)
          // this.configData = false;
          this.isConnected.emit(false);
        }
      );

  }

  getBuildingData(ipAddress, port){
    let baseUrlBuilding = this.getJsonDataOnBuilding(ipAddress, port)

    this.http
      .get(baseUrlBuilding)
      .map((response: Response) => {
        this.configDataBuilding = response.json();
        console.log("responseBUILDING", response);
      }).subscribe(
        response => {
          // return this._getSuccesPortIP = true;
          // this.configData = true;
          // console.log("response", response)
          console.log("responseBUILDING", this.configDataBuilding)
          this.isConnectedBuilding.emit(true);
        },
        error => {
          // this.error = error
          console.log("error###################################", error)
          console.log("error###################################", this.configDataBuilding)
          // this.configData = false;
          this.isConnectedBuilding.emit(false);
        }
      );
  }

  getBuilding(): any{
    console.log("this.configDataBuilding.smartcoreConfig", this.configDataBuilding.smartcoreConfig);
    return (this.configDataBuilding.smartcoreConfig);
    // ////console.log("Mqtt", this._postsCache)
    // return this.http
    //  .get(this.baseUrl + 'cacheData')
    //  .map((response: Response) => {
    //    return <ConfigurationDashboard[]>response.json();
    //  })
    //  .catch(this.handleError)
  }


  getProjectMetaData(ipAddress, port){
    // console.log("baseUrl",baseUrl)
    // console.log("type of baseUrl",typeof baseUrl)
    let baseUrl = this.getJsonDataOnLoad(ipAddress, port)
    let baseUrlSave = this.getJsonDataOnSave(ipAddress, port)

    this.http
      .get(baseUrl)
      .map((response: Response) => {
        this.configData = response.json();
        // console.log("response", response);
      }).subscribe(
        response => {
          // return this._getSuccesPortIP = true;
          // this.configData = true;
          console.log("response", response)
          console.log("response", this.configData)
          this.isConnected.emit(true);

        },
        error => {
          // this.error = error
          console.log("error###################################", error)
          console.log("error###################################", this.configData)
          // this.configData = false;
          this.isConnected.emit(false);
        }
      );
  }

  getPostData:any;
  getPostDataSuccess:any;
  SaveBacNetConfiguration(getData) {
    console.log("Server Called", getData);
    this.getPostData = getData;
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');

    let postParams = {
      JsonData: this.getPostData
    }
    // .set('Authorization', 'my-auth-token')
    // .set('Content-Type', 'application/json');
    console.log("Server Called", postParams);
    console.log("Server Called", JSON.stringify(postParams));
    // console.log("Server Called", JSON.parse(postParams));

    // this.http.post('http://localhost:5200/SaveBacNetConfiguration', JSON.stringify(postParams), { headers: headers })
    this.http.post('http://localhost:5200/saveConfig', JSON.stringify(postParams) , { headers: headers })
    .subscribe(data => {
      this.getPostDataSuccess = data.json();
      console.log("this.getPostDataSuccess", this.getPostDataSuccess)
      if ( this.getPostDataSuccess.status == true) {
        console.log("Server DATA11111111111", data.json());
        this.isConnectedDashboardPost.emit(true);
      } else {
        this.isConnectedDashboardPost.emit(false);
        console.log("Server ERROR");// Error getting the data
      }

    });
  }



  getJsonDataOnLoad(ipAddress, port): any{

    console.log("getJsonData.ipAddress", typeof ipAddress, ipAddress)
    console.log("getJsonData.port", typeof port, port)
    this.baseUrl = "http://" + ipAddress + ":" + port + "/" + "api/getConfig/";

    console.log("this.baseUrl", this.baseUrl)
    // this.getConfigData1(this.baseUrl)
    // this.baseUrl = "http://localhost:5200/api/getConfig/";
    return (this.baseUrl);
   }

  getJsonDataOnSave(ipAddress, port): any{
    console.log("getJsonData.ipAddress", ipAddress)
    console.log("getJsonData.port", port)
    this.baseUrlSave = "http://" + ipAddress + ":" + port + "/" + "api/saveConfig/";

    console.log("this.baseUrlSave", this.baseUrlSave)
    return (this.baseUrlSave);
   }

  //  http://localhost:5200/api/getProjectMetaData
   getJsonDataOnBuilding(ipAddress, port): any{
    console.log("getJsonData.ipAddress", ipAddress)
    console.log("getJsonData.port", port)
    this.baseUrlBuilding = "http://" + ipAddress + ":" + port + "/" + "api/getProjectMetaData";

    console.log("this.baseUrlBuilding", this.baseUrlBuilding)
    return (this.baseUrlBuilding);
   }


// GET Full JSON
  getJsonData(): any{
    return (this.configData);
   }

   getSmartCoreData():any {
    return (this.configDataBuilding);
   }


// GET ALL DASHBOARD DATA
   getChartConfig(): any{
    //  console.log("GET BASE URL chartConfig", this.baseUrl + 'chartConfig')
    //  return this.http
    //   .get(this.baseUrl)
    //   .map((response: Response) => {
    //     console.log("response", this.baseUrl)
    //     return <ConfigurationDashboard[]>response.json();
    //   })
    //   .catch(this.handleError)
    return (this.configData.chartConfig);
   }

  //  getHumidex(): Observable<ConfigurationDashboard[]>{
  //    return this.http
  //     .get(this.baseUrl + 'humidexRangeConfig')
  //     .map((response: Response) => {
  //       return <ConfigurationDashboard[]>response.json();
  //     })
  //     .catch(this.handleError)
  //  }

  getHumidex(): any{
    //  console.log("humidexRangeConfig", this.baseUrl + 'humidexRangeConfig')
    return (this.configData.humidexRangeConfig);
   }

   getSensor(): any{

    return (this.configData.sensorUnitConfig);
    //  console.log("Hello", this.baseUrl + 'sensorUnitConfig')
    //  return this.http
    //   .get(this.baseUrl + 'sensorUnitConfig')
    //   .map((response: Response) => {
    //     return <ConfigurationDashboard[]>response.json();
    //   })
    //   .catch(this.handleError)
   }

   getApplication(): any{
    //console.log("this.configData.applicationConfig", this.configData.applicationConfig)
    return (this.configData.applicationConfig);
    // console.log("Hello", this.baseUrl + 'applicationConfig')
    // return this.http
    //  .get(this.baseUrl + 'applicationConfig')
    //  .map((response: Response) => {
    //    return <ConfigurationDashboard[]>response.json();
    //  })
    //  .catch(this.handleError)
  }

  getDb(): any{
    // console.log("Hello", this._postsDb)
    return (this.configData.dbConfig);
    // return this.http
    //  .get(this.baseUrl + 'dbConfig')
    //  .map((response: Response) => {
    //    return <ConfigurationDashboard[]>response.json();
    //  })
    //  .catch(this.handleError)
  }

  getSystem(): any{
    //console.log("this.configData.systemConfig", this.configData.systemConfig);
    return (this.configData.systemConfig);
    // console.log("Hello", this._postsSystem)
    // return this.http
    //  .get(this.baseUrl + 'systemConfig')
    //  .map((response: Response) => {
    //    return <ConfigurationDashboard[]>response.json();
    //  })
    //  .catch(this.handleError)
  }

  getJwt(): any{
    //console.log("this.configData.jwtConfig", this.configData.jwtConfig);
    return (this.configData.jwtConfig);
    // console.log("Hello", this._postsJwt)
    // return this.http
    //  .get(this.baseUrl + 'jwtConfig')
    //  .map((response: Response) => {
    //    return <ConfigurationDashboard[]>response.json();
    //  })
    //  .catch(this.handleError)
  }
  getDms(): any{
    //console.log("this.configData.jwtConfig", this.configData.jwtConfig);
    return (this.configData.dmsConfig);
    // console.log("Hello", this._postsJwt)
    // return this.http
    //  .get(this.baseUrl + 'jwtConfig')
    //  .map((response: Response) => {
    //    return <ConfigurationDashboard[]>response.json();
    //  })
    //  .catch(this.handleError)
  }

  getApi(): any{
    //console.log("this.configData.apiConfig", this.configData.apiConfig);
    return (this.configData.apiConfig);
    // console.log("Hello", this._postsApi)
    // return this.http
    //  .get(this.baseUrl + 'apiConfig')
    //  .map((response: Response) => {
    //    return <ConfigurationDashboard[]>response.json();
    //  })
    //  .catch(this.handleError)
  }

  getMqtt(): any{
    // console.log("this.configData.mqttConfig", this.configData.mqttConfig);
    return (this.configData.mqttConfig);
    // //console.log("Mqtt", this._postsMqtt)
    // return this.http
    //  .get(this.baseUrl + 'mqttConfig')
    //  .map((response: Response) => {
    //    return <ConfigurationDashboard[]>response.json();
    //  })
    //  .catch(this.handleError)
  }

  getSmartCore(): any{
    console.log("this.configData.smartcoreConfig", this.configData.smartcoreConfig);
    return (this.configData.smartcoreConfig);
    // //console.log("Mqtt", this._postsMqtt)
    // return this.http
    //  .get(this.baseUrl + 'mqttConfig')
    //  .map((response: Response) => {
    //    return <ConfigurationDashboard[]>response.json();
    //  })
    //  .catch(this.handleError)
  }



  getCache(): any{
    // console.log("this.configData.cacheData", this.configData.cacheData);
    return (this.configData.cacheData);
    // ////console.log("Mqtt", this._postsCache)
    // return this.http
    //  .get(this.baseUrl + 'cacheData')
    //  .map((response: Response) => {
    //    return <ConfigurationDashboard[]>response.json();
    //  })
    //  .catch(this.handleError)
  }

  //APPLICATION CONFIG
  getHeatmap(): Observable<ConfigurationDashboard[]>{
    //  //console.log("Hello", this.baseUrl + 'sensorUnitConfig')
     return this.http
      .get(this.baseUrl + 'sensorUnitConfig')
      .map((response: Response) => {
        return <ConfigurationDashboard[]>response.json();
      })
      .catch(this.handleError)
   }

   private handleError(error:Response){
     return Observable.throwError(error.statusText)
    //  Observable.throwError()
   }

  //  GET DASHBOARD DATA CLOSED

  // START POST DASHBOARD DATA
  //  addHero (hero: Hero): Observable<Hero> {
  //   return this.http.post<Hero>(this.heroesUrl, hero, httpOptions)
  //     .pipe(
  //       catchError(this.handleError('addHero', hero))
  //     );
  // }

  public portNumber: string = "Ravi";

  // CLOSE POST DASHBOARD DATA

}
