import { Injectable, EventEmitter } from '@angular/core';
import { Http, Response, Headers} from '@angular/http';
// import {Observable} from 'rxjs/Observable';
import { HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/Rx';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Authorization': 'my-auth-token'
  })
};

@Injectable({
  providedIn: 'root'
})
export class ApiConfigService {
  public isConnected: EventEmitter<any> = new EventEmitter();
  public isConnectedStatus: EventEmitter<any> = new EventEmitter();
  public isConnectedNewSC: EventEmitter<any> = new EventEmitter();
  public isConnectedDeleteSC: EventEmitter<any> = new EventEmitter();
  public isConnectedLogger: EventEmitter<any> = new EventEmitter();
  public isConnectedAPI: EventEmitter<any> = new EventEmitter();

  currentIPAddress = localStorage.getItem('ipAddressAPI');
  currentPort = localStorage.getItem('portAPI');

  rspo:any;

  constructor(private http: Http) {
    console.log("respppppppppppppppppppppppppppppppppppppp",this.rspo)
  }

  ipAddress:any;
  port:any;
  getPutStatus:any;
  setModuleName:any;
  getApiServerStarted(ipAddressAPI, portAPI) {
    console.log("Server Called");
    this.ipAddress = ipAddressAPI;
    this.port = portAPI;
    const headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');

    let postParams = {
      ip: this.ipAddress,
      port: this.port,
      setModuleName: "API"
    }
          // .set('Authorization', 'my-auth-token')
          // .set('Content-Type', 'application/json');

    this.http.put('http://localhost:888/setIp', JSON.stringify(postParams), { headers: headers })
    .subscribe(data => {
      this.getPutStatus = data.json();
      if ( this.getPutStatus.status == true) {
        console.log("Server DATA11111111111", data.json());
      } else {
        console.log("Server ERROR");// Error getting the data
      }

    });
  }

  ipAddressLogger:any;
  portLogger:any;
  getLogger:any;
  configDataLogger:any;

  exportAPILogger() {
    this.getProject = 'http://localhost:888' + '/' + 'getDownloadLogger';

    console.log('this.baseUrl', this.baseUrl);
    this.http
    .get(this.getProject)
    .map((response: Response) => {
      this.configDataLogger = response.json();
      //  this.configDataLogger = response;
      console.log('response@@@@@@@@@@@@@@@@@@@@@@@@@@@@', response);
      console.log('response@@@@@@@@@@@@@@@@@@@@@@@@@@@@', response.json());
      console.log('response@@@@@@@@@@@@@@@@@@@@@@@@@@@@', JSON.stringify(response));
      // console.log('this.configDataLogger@@@@@@@@@@@@@@@@@@@@@@', this.configDataLogger);
      }).subscribe(
        response => {
          // return this._getSuccesPortIP = true;
          // this.configDataLogger = true;
          console.log('response192.168.80.93', response);
          // console.log('response192.168.80.93', this.configDataLogger);
          this.isConnectedLogger.emit(true);

        },
        error => {
          // this.error = error
          console.log('error###################################', error);
          // console.log('error###################################', this.configDataLogger);
          // this.configDataLogger = false;
          this.isConnectedLogger.emit(false);
        }
      );
  }

  statusPassed:any;

  mqttStatus:any;
  loadMqttConfig(status) {
    this.statusPassed = status;
    const headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');

    let postParams1 = {
      statusData: this.statusPassed
    }
          // .set('Authorization', 'my-auth-token')
          // .set('Content-Type', 'application/json');

    console.log("JSON.stringify(postParams)", postParams1);

    this.http.put('http://localhost:888/loadMqtt', JSON.stringify(postParams1), { headers: headers })
    .subscribe(data => {
      this.mqttStatus = data.json();
      console.log("Server DATA11111111111", data);
      console.log("Server DATA11111111111", this.mqttStatus);
      if ( this.mqttStatus.status == true) {
        this.isConnectedStatus.emit(true);
        console.log("Server DATA11111111111", data.json());
      } else {
        this.isConnectedStatus.emit(false);
        console.log("Server ERROR");// Error getting the data
      }

    });
  }


  getStatusData(): any {
    return (this.mqttStatus.result.data).toString();
  }


  createAuthorizationHeader(headers: Headers) : Headers{
    headers.append('Authorization', 'Basic ' +
      btoa('admin:admin'));
      return headers;
    }

//   var headers_object = new HttpHeaders();
// headers_object.append('Content-Type', 'application/json');
// headers_object.append("Authorization", "Basic " + btoa('admin' + ":" + 'admin'));

// const httpOptions = {
//   headers: headers_object
// };

  // baseUrl:any;
  configDataDummy: any;
  configData: any;
  configDataProject: any;
 //  baseUrl = JSON.parse(this.baseUrl1);

  baseUrlDummy:any = "http://localhost:3000/data";
  getConfigDataDummy() {
      console.log("this.baseUrlDummy",this.baseUrlDummy)
      this.http
    .get(this.baseUrlDummy)
    .map((response: Response) => {
      this.configDataDummy = response.json();
      //  this.configData = response;
      console.log("responseDummy", response);
      }).subscribe();
    }


  baseUrl:any;
  getProject:any;

  getApiConfigData(ipAddress, port): any{
    this.baseUrl = "http://" + ipAddress + ':' + port + '/' + 'transcend/api/v1/apiconfig';

    console.log('this.baseUrl', this.baseUrl);
    return (this.baseUrl);
  }

  // baseUrl:any = "http://192.168.80.93:81/transcend/api/v1/apiconfig";
  // getConfigData() {
  //     console.log('this.baseUrl', this.baseUrl);
  //     this.callServer();
  //     this.http
  //   .get(this.baseUrl)
  //   .map((response: Response) => {
  //     this.configData = response.json();
  //     console.log('response@@@@@@@@@@@@@@@@@@@@@@@@@@@@', response);
  //     console.log('this.configData@@@@@@@@@@@@@@@@@@@@@@', this.configData);
  //     }).subscribe(
  //       response => {
  //         console.log('response192.168.80.93', response);
  //         console.log('response192.168.80.93', this.configData);
  //         this.isConnected.emit(true);

  //       },
  //       error => {
  //         console.log('error###################################', error);
  //         console.log('error###################################', this.configData);
  //         this.isConnected.emit(false);
  //       }
  //     );
  //   }

  getProjectsData() {
    this.getProject = 'http://localhost:888' + '/' + 'projects';

    console.log('this.baseUrl', this.baseUrl);
    this.http
    .get(this.getProject)
    .map((response: Response) => {
      this.configData = response.json();
      //  this.configData = response;
      console.log('response@@@@@@@@@@@@@@@@@@@@@@@@@@@@', response);
      console.log('this.configData@@@@@@@@@@@@@@@@@@@@@@', this.configData);
      }).subscribe(
        response => {
          // return this._getSuccesPortIP = true;
          // this.configData = true;
          console.log('response192.168.80.93', response);
          console.log('response192.168.80.93', this.configData);
          this.isConnected.emit(true);

        },
        error => {
          // this.error = error
          console.log('error###################################', error);
          console.log('error###################################', this.configData);
          // this.configData = false;
          this.isConnected.emit(false);
        }
      );
  }

APIConfigData:any;
getAPIConfigPath:any;

  getAPIConfig() {
    this.getAPIConfigPath = 'http://localhost:888' + '/' + 'getAPIConfig';

    console.log('this.baseUrl', this.baseUrl);
    this.http
    .get(this.getAPIConfigPath)
    .map((response: Response) => {
      this.APIConfigData = response.json();
      //  this.configData = response;
      console.log('/*********************************************************************************************', response.json());
      console.log('/*********************************************************************************************', this.APIConfigData);

      if(this.APIConfigData.status == true){
        this.isConnectedAPI.emit(true);
      }else{
        this.isConnectedAPI.emit(false);
      }
      }).subscribe(
        response => {
          // return this._getSuccesPortIP = true;
          // this.configData = true;
          console.log('response192.168.80.93', response);
          console.log('response192.168.80.93', this.APIConfigData);
          // this.isConnectedAPI.emit(true);

        },
        error => {
          // this.error = error
          console.log('error###################################ERRRRORORORRORORO', error);
          // console.log('error###################################', this.configData);
          // this.configData = false;
          // this.isConnectedAPI.emit(false);
        }
      );
  }

  // GET Full JSON
  getJsonData(): any {
    return (this.configData);
  }

  getAPIData(): any {
    return (this.APIConfigData.result.data[0]);
  }

  getLoggerData(): any {
    return (this.configDataLogger);
  }


  getJsonDataDummy(): any {
    return (this.configDataDummy);
  }

  getApiProjectSachin(){
    this.getProject = 'http://localhost:888' + '/' + 'getTsConfig ';

    console.log('this.baseUrl', this.baseUrl);
    this.http
    .get(this.getProject)
    .map((response: Response) => {
      this.configData = response.json();
      //  this.configData = response;
      console.log('response@@@@@@@@@@@@@@@@@@@@@@@@@@@@', response);
      console.log('this.configData@@@@@@@@@@@@@@@@@@@@@@', this.configData);
      }).subscribe(
        response => {
          // return this._getSuccesPortIP = true;
          // this.configData = true;
          console.log('response192.168.80.93', response);
          console.log('response192.168.80.93', this.configData);
          this.isConnected.emit(true);

        },
        error => {
          // this.error = error
          console.log('error###################################', error);
          console.log('error###################################', this.configData);
          // this.configData = false;
          this.isConnected.emit(false);
        }
      );


    // return (this.getProject);
  }

  getApiProjectData(ipAddress, port): any {
    console.log('getJsonData.ipAddress', typeof ipAddress, ipAddress);
    console.log('getJsonData.port', typeof port, port);
    this.getProject = 'http://' + ipAddress + ':' + port + '/' + 'transcend/api/v1/projects';

    console.log('this.baseUrl', this.baseUrl);
    // this.getConfigData1(this.baseUrl)
    // this.baseUrl = "http://localhost:5200/api/getConfig/";
    return (this.getProject);
  }

// getProject:any = "http://192.168.80.93:81/transcend/api/v1/projects"

//  getConfigDataProject() {
//    let headers = new Headers();
//    this.createAuthorizationHeader(headers);
//   //  headers: headers;
//   console.log("this.getProjectDATAAAAAAAAAAAAAAAAAAAAA",this.getProject)
//   this.http
//   .get(this.getProject)
//   .map((response: Response) => {
//     this.configDataProject = response.json();
//     //  this.configDataProject = response;
//       console.log("response", response);
//     }).subscribe();
// }



getConfigDataProject() {
const headers = new Headers();
return this.http.get(this.getProject, {
      headers: this.createAuthorizationHeader(headers)
    });
  }

  newSCIP: any;
  newSCPort: any;
newSCStatus: any;
  putsmartCore(ip, port) {

    this.newSCIP = ip;
    this.newSCPort = port
    const headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');

    let postParamsSC = {
      newSmartIP: this.newSCIP,
      newSmartPort: this.newSCPort,
      setModuleName: "API"
  }
          // .set('Authorization', 'my-auth-token')
          // .set('Content-Type', 'application/json');

    console.log("JSON.stringify(postParams)", postParamsSC);

    this.http.put('http://localhost:888/setTsConfig', JSON.stringify(postParamsSC), { headers: headers })
    .subscribe(data => {
      this.newSCStatus = data.json();
      console.log("Server newSCStatus", data);
      console.log("Server newSCStatus", this.newSCStatus);
      if ( this.newSCStatus.status == true) {
        this.isConnectedNewSC.emit(true);
        console.log("Server isConnectedNewSC", data.json());
      } else {
        this.isConnectedNewSC.emit(false);
        console.log("Server ERROR");// Error getting the data
      }

    });



    // this._smartCoreUrl = 'http://' + this.currentIPAddress + ':' + this.currentPort + '/transcend/api/v1/tsConfig';
    // // this._smartCoreUrl = "http://192.168.80.96:81/transcend/api/v1/tsConfig"
    // console.log('this._smartCoreUrl', this._smartCoreUrl);
    // const headers = new Headers();
    // return this.http.put(this._smartCoreUrl, { ip, port}, {
    //       headers: this.createAuthorizationHeader(headers)
    //     });
      }

deleteSCProjectID:any;
deleteSCStatus: any;

deleteSmartCore(deletedID) {
  this.deleteSCProjectID = deletedID;
  console.log('deleteSCProjectID', this.deleteSCProjectID);

  const headers = new Headers();
  headers.append('Content-Type', 'application/x-www-form-urlencoded');

  let postParamsDelete = {
    deleteSCProjID: this.deleteSCProjectID
  }
        // .set('Authorization', 'my-auth-token')
        // .set('Content-Type', 'application/json');

  console.log("JSON.stringify(postParams)", postParamsDelete);

  this.http.put('http://localhost:888/setDeactiveTS', JSON.stringify(postParamsDelete), { headers: headers })
  .subscribe(data => {
    this.deleteSCStatus = data.json();
    console.log("Server deleteSCStatus", data);
    console.log("Server deleteSCStatus", this.deleteSCStatus);
    if ( this.deleteSCStatus.status == true) {
      this.isConnectedDeleteSC.emit(true);
      console.log("Server isConnectedDeleteSC", data.json());
    } else {
      this.isConnectedDeleteSC.emit(false);
      console.log("Server ERROR");// Error getting the data
    }

  });
}

getJsonDataProject(): any {
  return (this.configDataProject);
}

 getApi(): any {
  // this.configData = this.configData.result.data[0].apiConfigurations;
  // console.log('this.configData.resullt.apiConfigurations:', this.configData.result.data[0]);
  console.log('this.configData.resullt.apiConfigurations:', this.configData);
  // console.log("this.configData.resullt.apiConfigurations:", this.configData.result.data.apiConfigurations);
  return (this.configData.result.data[0]);
  // console.log("Hello", this._postsApi)
  // return this.http
  //  .get(this.baseUrl + 'apiConfig')
  //  .map((response: Response) => {
  //    return <ConfigurationDashboard[]>response.json();
  //  })
  //  .catch(this.handleError)
}

getApiProject(): any {
  return (this.configDataProject);
 }

 getSensor(): any {

  // return (this.configData.sensorUnitConfig);
 }
}
