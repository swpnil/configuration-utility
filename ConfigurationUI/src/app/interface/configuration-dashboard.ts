export interface ConfigurationDashboard {
    userId : Number;
    id : Number;
    title : string;
    body : string
}
