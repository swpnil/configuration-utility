import {AuthenticationValidator} from "./AuthenticationValidator";
import {UserManagementValidator} from "./UserManagementValidator";
import {WebPortalConfigurationValidator} from "./WebPortalConfigurationValidator";

class Validator {
    public AuthenticationValidator: AuthenticationValidator;
    public UserManagementValidator: UserManagementValidator;
    public WebPortalConfigurationValidator: WebPortalConfigurationValidator;
    constructor() {
        // constructor
        this.AuthenticationValidator = new AuthenticationValidator();
        this.UserManagementValidator = new UserManagementValidator();
        this.WebPortalConfigurationValidator = new WebPortalConfigurationValidator();
    }
}

export default new Validator();
