import ResponseFormatter from "../lib/helpers/ResponseFormatter";
import {ForgotPasswordReqModel} from "../models/requestModels/ForgotPasswordReqModel";
import {LoginReqModel} from "../models/requestModels/LoginReqModel";

export class AuthenticationValidator {
    constructor() {
        // constructor
    }
    public validateForgotPasswordRequestParams(req): any {
        let responeObj: any = {};
        const requestObj: any = (req.body) ? (req.body) : ({}); // move this to get login data from request object
        if (!req.body.username) {
            responeObj = {
                isSuccess: false,
                result: ResponseFormatter.getErrorObject("emptyUsername", requestObj),
            };
        } else {
            const forgotPasswordModelObj = new ForgotPasswordReqModel(req.body);
            responeObj = {
                isSuccess: true,
                result: forgotPasswordModelObj,
            };
        }

        return responeObj;
    }

    public validateLoginRequestParams(req): any {
        let responeObj: any = {};
        const requestObj: any = (req.body) ? (req.body) : ({}); // move this to get login data from request object
        if (!req.body.username) {
            responeObj = {
                isSuccess: false,
                result: ResponseFormatter.getErrorObject("emptyUsername", requestObj),
            };
        } else if (!req.body.password) {
            responeObj = {
                isSuccess: false,
                result: ResponseFormatter.getErrorObject("emptyPassword", requestObj),
            };
        } else {
            const loginModelObj = new LoginReqModel(req.body);
            responeObj = {
                isSuccess: true,
                result: loginModelObj,
            };
        }

        return responeObj;
    }
}
