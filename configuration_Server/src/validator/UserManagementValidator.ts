import * as passwordGenerator from "password-generator";
import ResponseFormatter from "../lib/helpers/ResponseFormatter";
import {AddUserReqModel} from "../models/requestModels/AddUserReqModel";
import {DeleteUserReqModel} from "../models/requestModels/DeleteUserReqModel";
import {ResetPasswordReqModel} from "../models/requestModels/ResetPasswordReqModel";
import {UpdatePasswordReqModel} from "../models/requestModels/UpdatePasswordReqModel";
import {UpdateUserReqModel} from "../models/requestModels/UpdateUserReqModel";

export class UserManagementValidator {
    constructor() {
        // constructor
    }
    public validateUpdatePasswordRequest(req, tokenDetails): any {
        let responeObj: any = {};
        const requestObj: any = (req.body) ? (req.body) : ({}); // move this to get login data from request object
        if (tokenDetails.role === "user" && req.body.userid !== tokenDetails.userId ) {
            responeObj = {
                isSuccess: false,
                result: ResponseFormatter.getErrorObject("forbidden", tokenDetails),
            };
        } else if (!req.body.userid) {
            responeObj = {
                isSuccess: false,
                result: ResponseFormatter.getErrorObject("emptyUserid", requestObj),
            };
        } else if (!req.body.currpassword) {
            responeObj = {
                isSuccess: false,
                result: ResponseFormatter.getErrorObject("emptyCurrentPassword", requestObj),
            };
        } else if (!req.body.newpassword) {
            responeObj = {
                isSuccess: false,
                result: ResponseFormatter.getErrorObject("emptyNewPassword", requestObj),
            };
        } else if (req.body.newpassword.length < 6) {
            responeObj = {
                isSuccess: false,
                result: ResponseFormatter.getErrorObject("passwordMinInvalid", requestObj),
            };
        } else {
            const updatePasswordModelObj = new UpdatePasswordReqModel(req.body);
            responeObj = {
                isSuccess: true,
                result: updatePasswordModelObj,
            };
        }

        return responeObj;
    }

    public validateResetPasswordRequest(req): any {
        let responeObj: any = {};
        const requestObj: any = (req.body) ? (req.body) : ({}); // move this to get login data from request object
        if (!req.body.username) {
            responeObj = {
                isSuccess: false,
                result: ResponseFormatter.getErrorObject("emptyUsername", requestObj),
            };
        } else {
            const resetPasswordModelObj = new ResetPasswordReqModel(req.body);
            responeObj = {
                isSuccess: true,
                result: resetPasswordModelObj,
            };
        }

        return responeObj;
    }

    public validateAddUserRequest(req): any {
        let responeObj: any = {};
        const requestObj: any = (req.body) ? (req.body) : ({}); // move this to get login data from request object
        if (!req.body.username) {
            responeObj = {
                isSuccess: false,
                result: ResponseFormatter.getErrorObject("emptyUsername", requestObj),
            };
        } else if (!req.body.role) {
            responeObj = {
                isSuccess: false,
                result: ResponseFormatter.getErrorObject("emptyRole", requestObj),
            };
        } else if (!req.body.name) {
            responeObj = {
                isSuccess: false,
                result: ResponseFormatter.getErrorObject("emptyName", requestObj),
            };
        } else if (!(req.body.role === "admin") && !(req.body.role === "user")) {
            responeObj = {
                isSuccess: false,
                result: ResponseFormatter.getErrorObject("invalidRole", requestObj),
            };
        } else if (!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(req.body.username)) {
            responeObj = {
                isSuccess: false,
                result: ResponseFormatter.getErrorObject("invalidUsername", requestObj),
            };
        } else if (req.body.phone && !/^\d+$/.test(req.body.phone)) {
            responeObj = {
                isSuccess: false,
                result: ResponseFormatter.getErrorObject("invalidPhone", requestObj),
            };
        } else if (req.body.phone && req.body.phone.length > 16) {
            responeObj = {
                isSuccess: false,
                result: ResponseFormatter.getErrorObject("phoneMaxInvalid", requestObj),
            };
        } else {
            const addUserModelObj = new AddUserReqModel(req.body);
            responeObj = {
                isSuccess: true,
                result: addUserModelObj,
            };
        }

        return responeObj;
    }

    public validateUpdateUserRequest(req, tokenDetails): any {
        let responeObj: any = {};
        const requestObj: any = (req.body) ? (req.body) : ({}); // move this to get login data from request object
        if (tokenDetails.role === "user" && req.body.userid !== tokenDetails.userId ) {
            responeObj = {
                isSuccess: false,
                result: ResponseFormatter.getErrorObject("forbidden", tokenDetails),
            };
        } else if (tokenDetails.role === "user" && req.body.role === "admin" ) {
            responeObj = {
                isSuccess: false,
                result: ResponseFormatter.getErrorObject("invalidRole", requestObj),
            };
        } else if (!req.body.userid) {
            responeObj = {
                isSuccess: false,
                result: ResponseFormatter.getErrorObject("emptyUserid", requestObj),
            };
        } else if (!req.body.role) {
            responeObj = {
                isSuccess: false,
                result: ResponseFormatter.getErrorObject("emptyRole", requestObj),
            };
        } else if (!req.body.name) {
            responeObj = {
                isSuccess: false,
                result: ResponseFormatter.getErrorObject("emptyName", requestObj),
            };
        } else if (!(req.body.role === "admin") && !(req.body.role === "user")) {
            responeObj = {
                isSuccess: false,
                result: ResponseFormatter.getErrorObject("invalidRole", requestObj),
            };
        }  else if (req.body.phone && !/^\d+$/.test(req.body.phone)) {
            responeObj = {
                isSuccess: false,
                result: ResponseFormatter.getErrorObject("invalidPhone", requestObj),
            };
        } else if (req.body.phone && req.body.phone.length > 16) {
            responeObj = {
                isSuccess: false,
                result: ResponseFormatter.getErrorObject("phoneMaxInvalid", requestObj),
            };
        } else {
            const updateUserModelObj = new UpdateUserReqModel(req.body);
            responeObj = {
                isSuccess: true,
                result: updateUserModelObj,
            };
        }

        return responeObj;
    }

    public validateDeleteUserRequest(req): any {
        let responeObj: any = {};
        const requestObj: any = (req.body) ? (req.body) : ({}); // move this to get login data from request object
        if (!req.body.userid) {
            responeObj = {
                isSuccess: false,
                result: ResponseFormatter.getErrorObject("emptyUserid", requestObj),
            };
        } else {
            const deleteUserModelObj = new DeleteUserReqModel(req.body);
            responeObj = {
                isSuccess: true,
                result: deleteUserModelObj,
            };
        }

        return responeObj;
    }
}
