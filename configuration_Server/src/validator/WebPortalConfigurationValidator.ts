import * as moment from "moment";
import * as passwordGenerator from "password-generator";
import ResponseFormatter from "../lib/helpers/ResponseFormatter";
import {AddEmailConfigReqModel} from "../models/requestModels/AddEmailConfigReqModel";
// import {SetSystemtimeReqModel} from "../models/requestModels/SetSystemtimeReqModel";

export class WebPortalConfigurationValidator {
    constructor() {
        // constructor
    }

    public validateAddEmailConfigurationRequest(req): any {
        let responeObj: any = {};
        const requestObj: any = (req.body) ? (req.body) : ({}); // move this to get login data from request object
        if (!req.body.email) {
            responeObj = {
                isSuccess: false,
                result: ResponseFormatter.getErrorObject("emptyEmail", requestObj),
            };
        } else if (!req.body.password) {
            responeObj = {
                isSuccess: false,
                result: ResponseFormatter.getErrorObject("emptyPassword", requestObj),
            };
        } else if (!req.body.host) {
            responeObj = {
                isSuccess: false,
                result: ResponseFormatter.getErrorObject("emptyHost", requestObj),
            };
        } else if (!req.body.port) {
            responeObj = {
                isSuccess: false,
                result: ResponseFormatter.getErrorObject("emptyPort", requestObj),
            };
        } else if (!/^\d+$/.test(req.body.port)) {
            responeObj = {
                isSuccess: false,
                result: ResponseFormatter.getErrorObject("invalidPort", requestObj),
            };
        } else if (!req.body.issecure) {
            responeObj = {
                isSuccess: false,
                result: ResponseFormatter.getErrorObject("emptyIsSecure", requestObj),
            };
        } else if (req.body.issecure !== "0" && req.body.issecure !== "1") {
            responeObj = {
                isSuccess: false,
                result: ResponseFormatter.getErrorObject("invalidIsSecure", requestObj),
            };
        } else if (!req.body.sendername) {
            responeObj = {
                isSuccess: false,
                result: ResponseFormatter.getErrorObject("emptySenderName", requestObj),
            };
        } else if (!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(req.body.email)) {
            responeObj = {
                isSuccess: false,
                result: ResponseFormatter.getErrorObject("invalidEmail", requestObj),
            };
        } else {
            const addEmailConfigModelObj = new AddEmailConfigReqModel(req.body);
            responeObj = {
                isSuccess: true,
                result: addEmailConfigModelObj,
            };
        }

        return responeObj;
    }

    // public validatSetSystemtimeRequest(req): any {
    //     let responeObj: any = {};
    //     const requestObj: any = (req.body) ? (req.body) : ({}); // move this to get login data from request object
    //     if (!req.body.systemtime) {
    //         responeObj = {
    //             isSuccess: false,
    //             result: ResponseFormatter.getErrorObject("emptySystemTime", requestObj),
    //         };
    //     } else if (!moment(req.body.systemtime, "MM-DD-YYYY HH:mm:ss").isValid()) {
    //         responeObj = {
    //             isSuccess: false,
    //             result: ResponseFormatter.getErrorObject("invalidDateTimeFormat", requestObj),
    //         };
    //     } else {
    //         const setSystemtimeModelObj = new SetSystemtimeReqModel(req.body);
    //         responeObj = {
    //             isSuccess: true,
    //             result: setSystemtimeModelObj,
    //         };
    //     }

    //     return responeObj;
    // }
}
