import {CommunicationManager} from "../lib/communicationManager";
import {Config} from "../config";
import ResponseFormatter from "../lib/helpers/ResponseFormatter";
import Logger from "../lib/logger/Logger";
import {CreateAuditLogModel} from "../models/databaseModels/CreateAuditLogModel";
import {GetAuditLogResModel} from "../models/responseModels/GetAuditLogResModel";

export class AuditLogger {
    public static getInstance(): AuditLogger {
        return AuditLogger.instance;
    }
    private static instance: AuditLogger = new AuditLogger();
    private CommunicationManager: CommunicationManager;

    constructor() {
        if (AuditLogger.instance) {
            Logger.log("error", "Instantion failed! use getInstance() instead.");
        }
        AuditLogger.instance = this;
    }

    public init(): void {
        this.CommunicationManager = CommunicationManager.getInstance();
    }

    public getLogs(calback): any {
        let responeObj: any = {};
        this.CommunicationManager.DbManager.dbQuery.execProc("sp_ReadAllAuditLog",  (err, result, fields) => {
            if (err) {
                Logger.log("error", err.message);
                responeObj = {
                    isSuccess: false,
                    result: ResponseFormatter.getErrorObject("failedDbQuery", {}),
                };
            } else {
                if (Array.isArray(result) && result[0].length >= 0) {
                    const resultSet = result[0];
                    const auditLogResObj = new GetAuditLogResModel(resultSet);
                    responeObj = {
                        isSuccess: true,
                        result: auditLogResObj.data,
                    };
                } else {
                    responeObj = {
                        isSuccess: false,
                        result: ResponseFormatter.getErrorObject("failedDbQuery", {}),
                    };
                }
            }
            calback(responeObj);
        });
    }

    public log(userName, level, logAction, logAdditionalInfo, alType) {
        const auditLogObj = new CreateAuditLogModel(userName, logAction, logAdditionalInfo, alType, level);
        this.CommunicationManager.DbManager.dbQuery.execProc("sp_CreateAuditLog", (err, result, fields) => {
            if (err) {
                Logger.log("error", err.message);
            }
        }, auditLogObj.data);
    }
}
