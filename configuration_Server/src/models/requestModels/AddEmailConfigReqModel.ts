export class AddEmailConfigReqModel {
    public email: string;
    public host: string;
    public issecure: string;
    public password: string;
    public port: string;
    public sendername: string;
    constructor(apiReqParam) {
        this.email = apiReqParam.email.trim();
        this.host = apiReqParam.host.trim();
        this.issecure = apiReqParam.issecure.trim();
        this.password = apiReqParam.password.trim();
        this.port = apiReqParam.port.trim();
        this.sendername = apiReqParam.sendername.trim();
    }
}
