export class LoginReqModel {
    public username;
    public password;
    constructor(apiReqParam) {
        this.username = apiReqParam.username.trim();
        this.password = apiReqParam.password.trim();
    }
}
