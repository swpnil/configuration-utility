export class UpdatePasswordReqModel {
    public currpassword;
    public newpassword;
    public userid;
    constructor(apiReqParam) {
        this.currpassword = apiReqParam.currpassword.trim();
        this.newpassword = apiReqParam.newpassword.trim();
        this.userid = apiReqParam.userid.trim();
    }
}
