export class DeleteUserReqModel {
    public userid: string;
    constructor(apiReqParam) {
        this.userid = apiReqParam.userid.trim();
    }
}
