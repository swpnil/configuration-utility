import * as passwordGenerator from "password-generator";
interface IOptions {
    name?: string;
    password?: string;
    role?: string;
    username?: string;
    phone?: string;
    address?: string;
}

export class AddUserReqModel {
    public name: string;
    public password: string;
    public role: string;
    public username: string;
    public phone: string;
    public address: string;
    constructor(apiReqParam) {
        this.name = apiReqParam.name.trim();
        this.password = passwordGenerator();
        this.role = apiReqParam.role.trim();
        this.username = apiReqParam.username.trim();

        if (apiReqParam.phone) {
            this.phone = apiReqParam.phone.trim();
        } else {
            this.phone = "";
        }

        if (apiReqParam.address) {
            this.address = apiReqParam.address.trim();
        } else {
            this.address = "";
        }
    }
}
