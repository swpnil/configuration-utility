interface IOptions {
    name?: string;
    role?: string;
    userid?: string;
    phone?: string;
    address?: string;
}

export class UpdateUserReqModel {
    public name: string;
    public role: string;
    public phone: string;
    public address: string;
    public userid: string;
    constructor(apiReqParam) {
        this.name = apiReqParam.name.trim();
        this.role = apiReqParam.role.trim();
        this.userid = apiReqParam.userid.trim();

        if (apiReqParam.phone) {
            this.phone = apiReqParam.phone.trim();
        } else {
            this.phone = "";
        }

        if (apiReqParam.address) {
            this.address = apiReqParam.address.trim();
        } else {
            this.address = "";
        }
    }
}
