export enum Datatype {
    SensorData,
    Properties,
    Configurations,
    Status,
    Notifications
}
