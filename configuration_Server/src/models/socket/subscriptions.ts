export class MqttSubscription {
    topic: string;
    clientIds: string[];
    constructor() {
        this.topic = undefined;
        this.clientIds = [];
    }
}
