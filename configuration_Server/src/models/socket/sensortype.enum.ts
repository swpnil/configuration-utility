export enum SensorType {
    TEMP,
    HUM,
    PW,
    PIR,
    AL,
    AQ,
    CO2
}
