import * as moment from "moment";
export class CreateAuditLogModel {
    public data = [];

    constructor(alUserName, logAction, logAdditionalInfo, alType = "information", level = "system") {
        const logTime = moment(new Date()).format("MM/DD/YYYY HH:mm:ss");
        this.data = [alUserName, alType, logAction, logAdditionalInfo, level, logTime];
    }
}
