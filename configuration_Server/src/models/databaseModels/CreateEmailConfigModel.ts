export class CreateEmailConfigModel {
    public data = [];

    constructor(email, password, host, port, isSecure, senderName) {
        this.data = [email, password, host, port, isSecure, senderName];
    }
}
