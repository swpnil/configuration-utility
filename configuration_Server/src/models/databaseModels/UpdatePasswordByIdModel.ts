export class UpdatePasswordByIdModel {
    public data = [];

    constructor(userId, currPassword, newPassword) {
        this.data = [userId, currPassword, newPassword];
    }
}
