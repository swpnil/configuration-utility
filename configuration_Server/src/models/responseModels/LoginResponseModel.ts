export class LoginResponseModel {
    public username: string;
    public token: string;
    public role: string;

    constructor(loginResponseObj, token) {
        this.username = loginResponseObj.username;
        this.token = token;
        this.role = loginResponseObj.role;
    }
}
