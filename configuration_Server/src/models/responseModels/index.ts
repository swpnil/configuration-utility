import { SiteResModel } from "./SiteResModel";
import { BuildingResModel } from "./BuildingResModel";
import { FloorResModel } from "./FloorResModel";
import { ZoneResModel } from "./ZoneResModel";

export class ResponseModels {
    public SiteResModel: SiteResModel;
    public BuildingResModel: BuildingResModel;
    public FloorResModel: FloorResModel;
    public ZoneResModel: ZoneResModel;

    constructor() {
        this.SiteResModel = new SiteResModel();
        this.BuildingResModel = new BuildingResModel();
        this.FloorResModel = new FloorResModel();
        this.ZoneResModel = new ZoneResModel();
    }
}