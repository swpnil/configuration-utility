export class AddUserResModel {
    public name: string = "";
    public role: string = "";
    public username: string = "";
    public phone: string = "";
    public address: string = "";
    public userId: string = "";
    constructor(requestParam, addUserDbRes) {
        this.name = requestParam.name;
        this.role = requestParam.role;
        this.username = requestParam.username;
        this.phone = requestParam.phone;
        this.address = requestParam.address;
        this.userId = addUserDbRes._returnValue;
    }
}
