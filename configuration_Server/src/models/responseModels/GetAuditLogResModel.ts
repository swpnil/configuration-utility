/* tslint:disable: object-literal-sort-keys */
export class GetAuditLogResModel {
    public data = [];
    constructor(auditLogDbRes) {
        this.data = auditLogDbRes.map((a) => ({
            id: a.id,
            logLevel: a.log_level,
            username: a.username,
            alTimestamp: a.al_timestamp,
            action: a.action,
            additionalInfo: a.additional_info,
            logType: a.log_type,
        }));
    }
}
