export class GetEmailConfigurationResModel {
    public authEmail: string = "";
    public authPassword: string = "";
    public host: string = "";
    public port: string = "";
    public isSecure: string = "";
    public senderName: string = "";
    constructor(emailConfigDbRes) {
        this.authEmail = emailConfigDbRes[0].auth_email;
        this.authPassword = emailConfigDbRes[0].auth_password;
        this.host = emailConfigDbRes[0].host;
        this.port = emailConfigDbRes[0].port;
        this.isSecure = emailConfigDbRes[0].is_secure;
        this.senderName = emailConfigDbRes[0].sender_name;
    }
}
