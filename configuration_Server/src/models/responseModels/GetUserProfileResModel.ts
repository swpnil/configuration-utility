export class GetUserProfileResModel {
    public address: string;
    public name: string;
    public phone: string;
    public role: string;
    public userId: string;
    public username: string;

    constructor(userProfileResObj) {
        this.address = userProfileResObj.address;
        this.name = userProfileResObj.name;
        this.phone = userProfileResObj.phone;
        this.role = userProfileResObj.role;
        this.userId = userProfileResObj.user_id;
        this.username = userProfileResObj.username;
    }
}
