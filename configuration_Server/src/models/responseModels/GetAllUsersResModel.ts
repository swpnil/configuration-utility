export class GetAllUsersResModel {
    public data = [];
    constructor(allUsersDbResponse) {
        this.data = allUsersDbResponse.map((a) => ({
            address: a.address,
            name: a.name,
            phone: a.phone,
            role: a.role,
            userId: a.user_id,
            username: a.username,
        }));
    }
}
