import { BuildingResModel } from "./BuildingResModel";

export class SiteResModel {
    
    public id: string = "";
    public siteName: string = "";
    public location: object = {};
    public buildings: BuildingResModel[];
    
    constructor() {
    }
}
