interface IOptions {
    userid?: string;
    token?: string;
}

export class UpdatePasswordResModel implements IOptions {
    public userid;
    public token;
    constructor(apiReqParam: IOptions) {
        this.userid = apiReqParam.userid;
    }
    public updateToken(token) {
        this.token = token;
    }
}
