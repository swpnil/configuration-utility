import { FloorResModel } from "./FloorResModel";

export class BuildingResModel {
    
    public projectId: string = "";
    public projectName: string = "";
    public id: string = "";
    public name: string = "";
    public location: object= {};
    public floors: FloorResModel[];
    
    constructor() {
    }
}
