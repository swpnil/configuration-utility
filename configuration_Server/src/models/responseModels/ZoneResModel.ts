export class ZoneResModel {

    public id: string = "";
    public name: string = "";
    public layer: string = "";
    public type: string = "";
    public location: object = {};
    public height: number = 0;
    public width: number = 0;
    
    constructor() {
    }
}
