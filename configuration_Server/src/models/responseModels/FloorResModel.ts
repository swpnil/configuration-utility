import { ZoneResModel } from "./ZoneResModel";

export class FloorResModel {
    
    public id: string = "";
    public name: string = "";
    public level: number = 0;
    public zones: ZoneResModel[];
    
    constructor() {
    }
}
