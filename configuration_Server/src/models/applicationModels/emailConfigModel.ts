// setting this object in email collection
export class EmailConfigModel {
    public senderOptions: any = {
        email: "",
        name: "",
    };
    public serverOptions: any = {
        auth: {
            pass: "",
            user: "",
        },
        host: "",
        port: "",
        secure: "",
    };

    constructor(authEmail, senderName, authPassword, mailHost, mailPort, isSecure) {
        this.senderOptions.email = authEmail;
        this.senderOptions.name = senderName;
        this.serverOptions.auth.pass = authPassword;
        this.serverOptions.auth.user = authEmail;
        this.serverOptions.host = mailHost;
        this.serverOptions.port = mailPort;
        this.serverOptions.secure = (isSecure.toString() === "1") ? (true) : (false);
    }
}
