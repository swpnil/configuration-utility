import * as async from "async";
import * as path from "path";
import {FileHelper} from "../lib/helpers/FileHelper";

export class Config {
    public static getInstance(): Config {
        return Config.instance;
    }
    private static instance: Config = new Config();
    private fileHelper: FileHelper;
    private configFilePath: string;
    private configuration: any = {}; // object doesn't have property is thrown by compiler hence declared as any

    constructor() {
        if (Config.instance) {
            throw new Error("Instantion failed! use getInstance() instead.");
        }
        Config.instance = this;
    }

    public init(configFilePath, callback): void {
        this.configFilePath = configFilePath;
        this.fileHelper = new FileHelper();
        // this.readConfigurationSettings();
        async.waterfall([async.apply(this.readConfigurationSettings, this), async.apply(this.setConfigurationSettings, this)], (err, results) => {
            callback();
        });
    }

    public getConfig(key): any {
        if (this.configuration.hasOwnProperty(key)) {
            // console.log('configuration',this.configuration)
            return this.configuration[key];
        }
        return false;
    }
    public setConfigByKey(key, configValue): void {
        this.configuration[key] = configValue;
    
    }
    public setConfigKey(key, configValue): void {
       this.configuration[key] = configValue;
       return this.configuration;
   }

    private readConfigurationSettings(selfObj, callback): void {
        selfObj.fileHelper.readFile(path.resolve(selfObj.configFilePath), (err, fileData) => {
            if (err) {
                throw err;
            }
            callback(null, fileData);
        });
    }

    private setConfigurationSettings(selfObj, fileData, callback): void {
        try {
            const configurationSettings = JSON.parse(fileData);
            if (configurationSettings.hasOwnProperty("dbConfig")) {
                selfObj.configuration.dbConfig = configurationSettings.dbConfig;
            }
            if (configurationSettings.hasOwnProperty("applicationConfig")) {
                selfObj.configuration.applicationConfig = configurationSettings.applicationConfig;
            }
            if (configurationSettings.hasOwnProperty("chartConfig")) {
                selfObj.configuration.chartConfig = configurationSettings.chartConfig;
            }
            if (configurationSettings.hasOwnProperty("humidexRangeConfig")) {
                selfObj.configuration.humidexRangeConfig = configurationSettings.humidexRangeConfig;
            }
            if (configurationSettings.hasOwnProperty("sensorUnitConfig")) {
                selfObj.configuration.sensorUnitConfig = configurationSettings.sensorUnitConfig;
            }
            if (configurationSettings.hasOwnProperty("themeConfig")) {
                selfObj.configuration.themeConfig = configurationSettings.themeConfig;
            }
            if (configurationSettings.hasOwnProperty("systemConfig")) {
                selfObj.configuration.systemConfig = configurationSettings.systemConfig;
            }
            if (configurationSettings.hasOwnProperty("jwtConfig")) {
                selfObj.configuration.jwtConfig = configurationSettings.jwtConfig;
            }
            if (configurationSettings.hasOwnProperty("apiConfig")) {
                selfObj.configuration.apiConfig = configurationSettings.apiConfig;
            }
            if (configurationSettings.hasOwnProperty("dmsConfig")) {
                selfObj.configuration.dmsConfig = configurationSettings.dmsConfig;
            }
            if (configurationSettings.hasOwnProperty("mqttConfig")) {
                selfObj.configuration.mqttConfig = configurationSettings.mqttConfig;
            }
            if (configurationSettings.hasOwnProperty("cacheData")) {
                selfObj.configuration.cacheData = configurationSettings.cacheData;
            }
            if (configurationSettings.hasOwnProperty("smartcoreConfig")) {
                selfObj.configuration.smartcoreConfig = configurationSettings.smartcoreConfig;
            }
        } catch (err) {
            throw err;
        }
        callback();
    }
}
