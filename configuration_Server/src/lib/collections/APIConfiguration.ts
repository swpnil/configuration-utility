export class APIConfiguration {
    public static getInstance(): APIConfiguration {
        return APIConfiguration.instance;
    }
    private static instance: APIConfiguration = new APIConfiguration();
    private data: any = {};
    public authentication: object = { 'user': 'admin', 'pass': 'admin'};
    public urlDir: any = {
        stats: '',
        projects: '',
        sensorLastValue: '',
        sensorHistoryData: '',
        deviceData: ''
    };
    public startupUrl: any = {
        projectStat: '',
        projectDetail: '',
        floorPlan: '',
        projects: ''
    };
    public dmsUrlDir :any ={
        ping :''
    };
    constructor() {
        if (APIConfiguration.instance) {
            throw new Error("Instantion failed! use getInstance() instead.");
        }
        APIConfiguration.instance = this;
    }

    public setAPI(init): any {
        // console.log('Connected to Molex API',apiConfig)
        this.data["apiBaseUrl"] = this.setBaseUrl(init);
        // this.data["apiStartupUrl"] = this.setStartupUrl(init);
    }

    public getAPIBaseUrl(): any {
        if (this.data.hasOwnProperty("apiBaseUrl")) {
            return this.data["apiBaseUrl"];
        }
        return false;
    }

    private setBaseUrl(config): string {
        console.log("setBaseUrl-*************--config",config)
        // set base url
if(config.setModuleName == 'Dashboard'){
    var Dashboard_BASE_URL = 'http://'+ config.ip + ':' + config.port;

} 
if(config.setModuleName == 'API'){
   var API_BASE_URL = 'http://'+ config.ip + ':' + config.port;

} 
if(config.setModuleName == 'Bacnet'){
   var Bacnet_BASE_URL = 'http://'+ config.ip + ':' + config.port;

} 
console.log("AAAAAAAAAAAAAAAAAAAAAAAA",Dashboard_BASE_URL)

        // let Bacnet_BASE_URL = 'http://'+ config.bacnetIP + ':' + config.backnetPort,
        // Bacnet_BASE_URL = 'http://'+ config.dashbordIp + ':' + config.dashbordPort;  
        // let FIRMWARE_BASE_URL = 'http://'+ config.dmsIp + ':' + config.firmwarePort; 
        // let DMS_BASE_URL = 'http://'+ config.dmsIp + ':' + config.dmsPort; 
        // console.log("FIRMWARE_BASE_URL   ",FIRMWARE_BASE_URL)
        // console.log("DMS_BASE_URL  ",DMS_BASE_URL)
        // modify url dir on API's ip modify
        this.urlDir = {

            //bacNetData:  'http://192.168.80.58:7004/GetBacNetConfiguration',
            bacNetData:  Bacnet_BASE_URL + '/GetBacNetConfiguration',
            saveBacNetData:  Bacnet_BASE_URL + '/SaveBacNetConfiguration',
            getGWServiceStatusData:  Bacnet_BASE_URL + '/GetGWServiceStatus',
             restoreDefaultConfigurationsData:  Bacnet_BASE_URL + '/RestoreDefaultConfigurations', 
             setDefaultConfigurationsData:  Bacnet_BASE_URL + '/SetDefaultConfigurations',
             setToggleGWServiceStatusdata :  Bacnet_BASE_URL + '/ToggleGWServiceStatus ',
            // downloadObjectMappingData:  Bacnet_BASE_URL + '/DownloadObjectMapping',
 
            getProjects:    API_BASE_URL + '/transcend/api/v1/projects', 
            tsConfigData:   API_BASE_URL +'/transcend/api/v1/tsConfig',
            apiConfigData:  API_BASE_URL + "/transcend/api/v1/apiconfig",
            auditData:      API_BASE_URL + '/transcend/api/v1/audits',
            removeAuditLog: API_BASE_URL + '/transcend/api/v1/audits',
           downloadLogger: API_BASE_URL + '/transcend/api/v1/download/logger',
            //downloadLogger   :'http://192.168.80.96:81/transcend/api/v1/download/logger',
            lightsceneList: API_BASE_URL + '/transcend/api/v1/project/lightscenes/list',
            setTsConfig:    API_BASE_URL + '/transcend/api/v1/tsConfig',
            setDeactive:    API_BASE_URL + '/transcend/api/v1/project/inactive',
            //setmqtt:'http://192.168.80.103:82/transcend/api/v1/mqtt',
            setmqtt: API_BASE_URL + '/transcend/api/v1/mqtt',


           getConfigDataURL :Dashboard_BASE_URL + '/api/getConfig',
            saveConfigData :Dashboard_BASE_URL + '/api/saveConfig',
            //getDasConfigData :Dashboard_BASE_URL + '/api/getConfig'

        }

        // this.dmsUrlDir = {
        //     ping: FIRMWARE_BASE_URL + '/api/v1/ping',
        //     getFirmwareList :  FIRMWARE_BASE_URL + '/api/firmware/list',
        //     uploadFirmwareFile :  FIRMWARE_BASE_URL + '/api/firmware/upload',
        //     firmwareUpdate: FIRMWARE_BASE_URL + '/api/firmware/update',
        //     getVersionList:  FIRMWARE_BASE_URL + '/api/firmware/version',
        //     getFirmwareStatus:  FIRMWARE_BASE_URL + '/api/firmware/fwStatus',
        //     dmsDeviceList:  DMS_BASE_URL + '/api/dms/device/list',
        // }
        // return base url
        return API_BASE_URL + '/transcend/api/v1/';
    }


    private setStartupUrl(apiConfig): string {
        
        // set base url
        let BASE_URL = 'http://' + apiConfig.ip + ':' + apiConfig.port;
        // modify url dir on API's ip modify
        this.startupUrl = {
            projectDetail: BASE_URL + '/transcend/api/v1/projects?info=detail',
            projectStat: BASE_URL + '/transcend/api/v1/stats',
            getFloorPlan: BASE_URL + '/transcend/api/v1/floor/plan',
            projects: BASE_URL + '/transcend/api/v1/projects'
        }
        return BASE_URL + '/transcend/api/v1/';
    }
}
