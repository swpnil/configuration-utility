import {UserCollection} from "./UserCollection";
import {APIConfiguration} from "./APIConfiguration";

export class Collections {
    public static getInstance(): Collections {
        return Collections.instance;
    }
    private static instance: Collections = new Collections();
    public UserCollection: UserCollection;
    public APIConfiguration: APIConfiguration;
    constructor() {
        if (Collections.instance) {
            throw new Error("Instantion failed! use getInstance() instead.");
        }
        Collections.instance = this;
    }

    public init(): void {
        this.UserCollection = UserCollection.getInstance();
        this.APIConfiguration = APIConfiguration.getInstance();
    }
}
