export class UserCollection {
    public static getInstance(): UserCollection {
        return UserCollection.instance;
    }
    private static instance: UserCollection = new UserCollection();
    private data: any = {};
    constructor() {
        if (UserCollection.instance) {
            throw new Error("Instantion failed! use getInstance() instead.");
        }
        UserCollection.instance = this;
    }

    public setUser(key, value): void {
        this.data[key] = value;
    }

    public getUserById(userId): any {
        if (this.data.hasOwnProperty(userId)) {
            return this.data[userId];
        }

        return false;
    }

    public deleteUserById(userId): void {
        delete this.data[userId];
    }
}
