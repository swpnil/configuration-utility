import * as async from "async";
import * as nodemailer from "nodemailer";
// import {AuditLogger} from "../auditLogger";
import {CommunicationManager} from "../communicationManager";
import {Config} from "../../config";
import Logger from "../logger/Logger";
import {EmailConfigModel} from "../../models/applicationModels/emailConfigModel";
import {Template} from "./template";

export class MailingService {
    public static getInstance(): MailingService {
        return MailingService.instance;
    }
    private static instance: MailingService = new MailingService();
    private emailConfig: any; // declaring type as object throws errors
    private transporter: any;
    private Config: Config;
    private CommunicationManager: CommunicationManager;
    // private AuditLogger: AuditLogger;

    constructor() {
        if (MailingService.instance) {
            throw new Error("Instantion failed! use getInstance() instead.");
        }
        MailingService.instance = this;
    }

    public init(callback): void {
        this.Config = Config.getInstance();
        this.CommunicationManager = CommunicationManager.getInstance();
        // this.AuditLogger = AuditLogger.getInstance();
        async.waterfall([async.apply(this.checkEmailConfigExists, this), async.apply(this.setEmailConfigInCollection, this)], (err, results) => {
            this.createTransport();
            callback();
        });
    }

    public sendMail(auditLogUsername, receiverEmail, templateKey, params, callback): void {
        const templateObj = new Template();
        let cbResponse = {};
        const templateDetails = templateObj.getTemplate(templateKey, {password: params.password, username: params.username});
        const mailOptions = {
            from: this.emailConfig.senderOptions.name + " <" + this.emailConfig.senderOptions.email + ">",
            html: templateDetails.html,
            subject: templateDetails.subject,
            to: receiverEmail,
        };

        this.transporter.sendMail(mailOptions, (error, info) => {
            if (error) {
                // check email configured
                Logger.log("error", "Error sending email:" + error.message);
                // this.AuditLogger.log(auditLogUsername, "system", "Send Mail", "Error sending email: " + JSON.stringify(mailOptions), "error");
                cbResponse = {
                    isSuccess: false,
                };
            } else {
                cbResponse = {
                    isSuccess: true,
                };
                // this.AuditLogger.log(auditLogUsername, "system", "Send Mail", "Email sent successfully: " + JSON.stringify(mailOptions), "information");
                Logger.log("info", "Message sent:" + info.messageId);
            }
            callback(cbResponse);
        });
    }

    public updateEmailConfig(email, senderName, password, host, port, isSecure): void {
        this.emailConfig = new EmailConfigModel(email, senderName, password, host, port, isSecure);
        this.Config.setConfigByKey("emailConfig", this.emailConfig);
        this.createTransport();
    }

    private createTransport(): void {
        this.transporter = nodemailer.createTransport(this.Config.getConfig("emailConfig").serverOptions);
    }

    private checkEmailConfigExists(selfObj, callback): void {
        selfObj.CommunicationManager.DbManager.dbQuery.execProc("sp_ReadAllEmailConfig", (err, result, fields) => {
            callback(null, result[0]);
        });
    }

    private setEmailConfigInCollection(selfObj, emailConfigRes, callback): void {
        const emailConfig = emailConfigRes[0]; // since there shall be only one row in email_config, we are specifying 0 index in result set
        selfObj.emailConfig = new EmailConfigModel(emailConfig.auth_email, emailConfig.sender_name, emailConfig.auth_password, emailConfig.host, emailConfig.port, emailConfig.is_secure);

        selfObj.Config.setConfigByKey("emailConfig", selfObj.emailConfig);
        callback();
    }
}
