import Logger from "../logger/Logger";

export class Template {
    private templateObj: any;
    constructor() {
        this.templateObj = {
            forgotPassword: {
                html: "<b>Your password has been reset </b> <p>Your new password is <b><i> _#_PASSWORD_#_ </i></b></p>",
                subject: "Molex Web Portal - Password Reset",
            },
            userCreated: {
                html: "<b>New user has been created </b> <p> with username <b><i> _#_USERNAME_#_ </i></b> and password <b><i> _#_PASSWORD_#_ </i></b> </p>",
                subject: "Molex Web Portal - Password Reset",
            },
        };
    }

    public getTemplate(key, params) {
        const responseObj = this.templateObj[key];
        responseObj.html = responseObj.html.replace("_#_PASSWORD_#_", params.password).replace("_#_USERNAME_#_", params.username);
        return responseObj;
    }
}
// export default new Template();
