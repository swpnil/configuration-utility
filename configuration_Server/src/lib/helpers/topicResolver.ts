export class TopicResolver {
    public static instance: TopicResolver = new TopicResolver();
    
    public static getInstance(): TopicResolver {
        return TopicResolver.instance;
    }

    private topics = [['energy', 'occupancy','sensorData'], ['consumption','saving','PW'], ['minute','hour','#']];

    constructor() {
        TopicResolver.instance = this;
    }

    getTopic(topic: any): string {
        let resolvedTopic='';
        if (topic.projectId) {
            resolvedTopic += topic.projectId + '/';
        }
        if (topic.buildingId) {
            resolvedTopic += topic.buildingId + '/';
        }
        if (topic.floorId) {
            resolvedTopic += topic.floorId + '/';
        }
        if (topic.zoneId) {
            resolvedTopic += topic.zoneId + '/';
        }
        const indexes: string[] = topic.topic.split('/');
        for (let i = 0; i < indexes.length; i++) {
            if (indexes[i]) {
                resolvedTopic += this.topics[i][parseInt(indexes[i])];
                if ( !(i ==  indexes.length - 1))  {
                    resolvedTopic+='/';
                }
            }
           
        }
        return resolvedTopic;
    }
}