import * as fs from "fs";
import * as path from "path";

export class FileHelper {
    public readFile(filename, callback): void {
        fs.readFile(filename, "utf8", callback);
    }
}
