import moment = require("moment");
import momentTz = require("moment-timezone");
import { Config } from "../../config";

export class DateConverter {

  
  public static getInstance(): DateConverter {
    return DateConverter.instance;
  }
  private Config: Config;
  public static instance: DateConverter = new DateConverter();
  constructor() {
    if (DateConverter.instance) {
      // Logger.log("error", "Instantion failed! use getInstance() instead.");
    }
    DateConverter.instance = this;
    this.Config = Config.getInstance();
  }

  public convertToLocalMili(UTCMILI) {
    let apiConfig = this.Config.getConfig("applicationConfig");
    // console.log("UTCMILI",UTCMILI)  
    // const milli = moment.utc(UTCMILI).tz(apiConfig.timeZone).format();
    // const milli = momentTz.tz(UTCMILI,apiConfig.timeZone).format();
    // console.log("milli",milli)
    // var date = new Date(milli); // some mock date
    // var milliseconds = date.getTime();
    // console.log("miliseconds",milliseconds )
    return UTCMILI;
  }

  public convertToUTC(dateToStore){
    let apiConfig = this.Config.getConfig("applicationConfig");
    // console.log("bEFORE ", dateToStore)
    // console.log("After ", momentTz.tz(dateToStore,apiConfig.timeZone).utc().format())
    return momentTz.tz(dateToStore,apiConfig.timeZone).utc().format();
  }

  public getMinValue(dataArray) {
    return Math.min.apply(null, dataArray);
  }


  public getMaxValue(dataArray) {
    return Math.max.apply(null, dataArray);
  }

}