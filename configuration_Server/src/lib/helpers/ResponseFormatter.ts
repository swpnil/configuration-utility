import { ResponseModels } from "../../models/responseModels/index";

/* tslint:disable: object-literal-sort-keys */
class ResponseFormatter {
    private static errorObj = {};
    private static ResponseModels: ResponseModels;

    private static instance: ResponseFormatter = new ResponseFormatter();
    constructor() {
        ResponseFormatter.ResponseModels = new ResponseModels();
        ResponseFormatter.errorObj = {
            emptyAddress: {message: "Empty address field", code: "1001", type: "unauthorized"},
            emptyCurrentPassword: {message: "Empty currpassword field", code: "1002", type: "unauthorized"},
            emptyEmail: {message: "Empty email field", code: "1003", type: "unauthorized"},
            emptyHost: {message: "Empty host field", code: "1004", type: "unauthorized"},
            emptyIsSecure: {message: "Empty issecure field", code: "1005", type: "unauthorized"},
            emptyName: {message: "Empty name field", code: "1006", type: "unauthorized"},
            emptyNewPassword: {message: "Empty newpassword field", code: "1007", type: "unauthorized"},
            emptyPassword: {message: "Empty password field", code: "1008", type: "unauthorized"},
            emptyPhone: {message: "Empty phone field", code: "1009", type: "unauthorized"},
            emptyPort: {message: "Empty port field", code: "1010", type: "unauthorized"},
            emptyRole: {message: "Empty role field", code: "1011", type: "unauthorized"},
            emptySenderEmail: {message: "Empty senderemail field", code: "1012", type: "unauthorized"},
            emptySenderName: {message: "Empty sendername field", code: "1013", type: "unauthorized"},
            emptySystemTime: {message: "Empty systemtime field", code: "1014", type: "unauthorized"},
            emptyUserid: {message: "Empty userid field", code: "1015", type: "unauthorized"},
            emptyUsername: {message: "Empty username field", code: "1016", type: "unauthorized"},
            failedDbQuery: {message: "Failed database query", code: "1017", type: "failed_db_query"},
            forbidden: {message: "Access forbidden", code: "1018", type: "forbidden"},
            internalServerError: {message: "Internal server error", code: "1019", type: "unauthorized"},
            invalidDateTimeFormat: {message: "Invalid datetime format", code: "1020", type: "unauthorized"},
            invalidEmail: {message: "Invalid email", code: "1021", type: "unauthorized"},
            invalidIsSecure: {message: "Invalid issecure", code: "1022", type: "unauthorized"},
            invalidPassword: {message: "Invalid password", code: "1023", type: "unauthorized"},
            invalidPhone: {message: "Invalid phone", code: "1024", type: "unauthorized"},
            invalidPort: {message: "Invalid port", code: "1025", type: "unauthorized"},
            invalidRole: {message: "Invalid role", code: "1026", type: "unauthorized"},
            invalidSenderEmail: {message: "Internal senderemail error", code: "1027", type: "unauthorized"},
            invalidToken: {message: "Invalid token", code: "1028", type: "unauthorized"},
            invalidUsername: {message: "Invalid username", code: "1029", type: "unauthorized"},
            passwordMinInvalid: {message: "Password should be minimum 6 characters", code: "1030", type: "unauthorized"},
            phoneMaxInvalid: {message: "Phone number should be less than 15 digits", code: "1031", type: "unauthorized"},
            emailError: {message: "Error sending email", code: "1032", type: "email_error"},
            minOneAdmin: {message: "Minimum one admin is needed in the system", code: "1033", type: "min_one_admin"},
            noAuditLog: {message: "No audit logs created", code: "1034", type: "no_audit_logs"},
            noResponse: {message: "No response from database", code: "1035", type: "no_audit_logs"},
            scriptError: {message: "Error executing script", code: "1036", type: "script_error"},
            userExists: {message: "User already exists", code: "1037", type: "user_exists"},
            userNotFound: {message: "User not found", code: "1038", type: "not_found"},
            projectNotFound:{message: "Project not found", code: "1039", type: "not_found"},
            invalidParams:{message: "Params are invalid", code: "1040", type: "not_found"}
        };
    }

    public getErrorObject(errorKey, propertyInfoVal): any {
        let errorResponseObj: object;
        const errorObjByKey = ResponseFormatter.errorObj[errorKey];
        errorResponseObj = {
            errorMsg: errorObjByKey.message,
            propertyInfo: propertyInfoVal,
            statusCode: errorObjByKey.code,
            type: errorObjByKey.type,
        };
        return errorResponseObj;
    }

    public getResponseObject(responseStatus, responseMessage, result): any {
        let responseObj: any;
        responseObj = {
            message: responseMessage,
            result: {
                data: [],
                error: [],
            },
            status: responseStatus,
        };

        if (responseStatus === true) {
            if (Array.isArray(result)) {
                responseObj.result.data = result;
            } else {
                responseObj.result.data.push(result);
            }
        } else {
            if (Array.isArray(result)) {
                responseObj.result.error = result;
            } else {
                responseObj.result.error.push(result);
            }
        }

        return responseObj;

    }


    public getBuildingResponseObject(responseStatus, responseMessage, result): any {
        let responseObj: any;
        responseObj = {
            message: responseMessage,
            result: {
                data: [],
                error: [],
            },
            status: responseStatus,
        };

        if (responseStatus === true) {
            if (Array.isArray(result)) {
                
                // Object.keys(result).map(function(elementKey){
                    
                    let building = []
                    for (let i = 0; i < result.length; i++) {
                        
                        // console.log('main ', i )
                        building.push(ResponseFormatter.instance.getBuildingObject(result[i]));
                        // console.log(i)
                        // console.log('type of ', i+' ' + typeof result[i])
                        // console.log(i, ' ====> '+result[i].id)
                        
                    }
                    ResponseFormatter.ResponseModels.SiteResModel.buildings = building;
                    responseObj.result.data.push(ResponseFormatter.ResponseModels);

                // });
                responseObj.result.data = result;
            } else {
                responseObj.result.data.push(result);
            }
        } else {
            if (Array.isArray(result)) {
                responseObj.result.error = result;
            } else {
                responseObj.result.error.push(result);
            }
        }
        // console.log('ResponseFormatter.ResponseModels ', ResponseFormatter.ResponseModels)
// console.log('responseObj ', responseObj.result.data[0].floors[0].zones)
        return responseObj;
    }


    public getBuildingObject(result) {
        let floors = []
        
        Object.keys(result).forEach(function(elementKey) {
            ResponseFormatter.ResponseModels.BuildingResModel.projectId = result['id'];
            ResponseFormatter.ResponseModels.BuildingResModel.projectName = result['name'];
            ResponseFormatter.ResponseModels.BuildingResModel.id = result['buildingID'];
            ResponseFormatter.ResponseModels.BuildingResModel.name = result['buildingName'];
            // console.log('type of ', elementKey+' ' + typeof result[elementKey])
            if(elementKey=='floors')
            floors = ResponseFormatter.instance.getFloorObject(result['floors'])
            // console.log(JSON.stringify(floors))
            
            // console.log('floors ', JSON.parse(ResponseFormatter.instance.getFloorObject(result['floors']).toString()))
            ResponseFormatter.ResponseModels.BuildingResModel.floors = floors
        })
        
        
        return ResponseFormatter.ResponseModels.BuildingResModel
    }


    public getFloorObject(floors) {
        // ResponseFormatter.instance.iterateObject('floors', floors);
        let floorsArr = []
        // floors.forEach(function(key, value){
        //     console.log('key => ', key)
        //     console.log('value => ', value)
        // });
        for (let i = 0; i < floors.length; i++) {
            // console.log('floors[i] ', i +' => '+floors[i])
            let temp = {}
            temp = {
                'id': floors[i]['id'],
                'name': floors[i]['name'],
                'zones': ResponseFormatter.instance.getZoneObject(floors[i]['zones'])
            }
            // temp.id = floors[i]['id']
            // temp.name = floors[i]['name']
            // temp.zones = ResponseFormatter.instance.getZoneObject(floors[i]['zones'])
            // console.log('zones ', temp)
            floorsArr.push(temp)
            
        }
        // console.log(JSON.parse(floorsArr))
        return floorsArr;

    }


    public getZoneObject(zones) {
        // console.log("in getZoneObj")
        // console.log('zones type of ', typeof zones)
        // console.log('zones ', zones)
        let zonesObj = [];
        for (let i = 0; i < zones.length; i++) {
            let temp = {}
            let locX = zones[i]['locX']
            let locY = zones[i]['locY']
            temp = {
                'id': zones[i]['id'],
                'name': zones[i]['name'],
                'type': zones[i]['type'],
                'location': {locX, locY},
                'height': zones[i]['height'],
                'width': zones[i]['width']
            }

            zonesObj.push(temp)
        }
        return zonesObj;
    }

}

export default new ResponseFormatter();
