
export class TemperatureConverter {
    
  public static getInstance(): TemperatureConverter {
    return TemperatureConverter.instance;
  }
  public static instance: TemperatureConverter = new TemperatureConverter();

  constructor() {
      TemperatureConverter.instance = this;
  }

  public convertToCelcius(temp){
    var celcius =(temp-32)/1.8;
    return Math.round(celcius);
  }

  public convertToFahrenheit(temp){
    var fahrenheit=(temp*1.8)+32;
    return Math.round(fahrenheit);
  }
}
