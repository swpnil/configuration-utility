import { DataCollectionManager } from './../processManager/dataCollectionManager';
import { Socket } from './../communicationManager/socketManager/Socket';
import {Config} from "../../config";
import Logger from "../logger/Logger";
import { BuildingService } from "../../services/BuildingService";
import { connect, Client } from "mqtt";
const uuidv1 = require('uuid/v1');

export class MqttManager {

    public static getInstance(): MqttManager {
        return MqttManager.instance;
    }
    private static instance: MqttManager = new MqttManager();
    private client: Client;
    private Configmqtt;
    private config;
    private Topic;
    private socket: any;
    public mqttConnected: boolean;
    private dataCollectionManager: DataCollectionManager;
    constructor() {
       
        if (MqttManager.instance) {
            Logger.log("error", "Instantion failed! use getInstance() instead.");
        }
        MqttManager.instance = this;
        this.Configmqtt = Config.getInstance();

    }

    public init(): void {
        this.config =  this.Configmqtt.getConfig("mqttConfig");
        // console.log('uuid',uuidv1());
        this.config.options.clientId = uuidv1();
        // this.config.options.clientId = this.socket.
        // console.log('mqtt settngs', this.config);
        // const mqttSystemConfig = this.Configmqtt.getConfig("mqttConfig");
        this.connect();
        // 6525cf7a-0c54-4554-b0ef-7c22f118decd/a1c2eb66-219e-48f4-9a51-211c9fa27d96/floors/energy/consumption/minute
        // this.subscribe('6525cf7a-0c54-4554-b0ef-7c22f118decd/a1c2eb66-219e-48f4-9a51-211c9fa27d96/floors/energy/consumption/minute');
        // this.unsubscribe('6525cf7a-0c54-4554-b0ef-7c22f118decd/a1c2eb66-219e-48f4-9a51-211c9fa27d96/floors/energy/consumption/minute');
        // this.subscribe('testtopic/4');
    }

    public getMqttClient(): any{
        
        /* const mqttClient = connect('mqtt://broker.mqttdashboard.com', {
            reconnectPeriod: 1000
        }); */
        // console.log('mqtt settngs', this.config);
        
        this.client  = connect(this.config.brokerUrl,this.config.options);
        return this.client;
    }

    updateSocketConnection(socketio) {
        if (socketio) {
            this.socket = socketio;
        }
    }

    public connect (): void {
        this.client  = this.getMqttClient();
        // console.log("connected client", this.client);
        var self = this;
        this.client.on('connect', self.mqtt_connect.bind(self));
        this.client.on('reconnect', self.mqtt_reconnect.bind(self));
        this.client.on('error', self.mqtt_error.bind(self));
        this.client.on('message', self.mqtt_messsageReceived.bind(self));
        this.client.on('close', self.mqtt_close.bind(self));
    }

    mqtt_messsageReceived(topic, data, packet) {
        let message = JSON.parse(data);
        let response = {
            topic: topic,
            data: message
        }
        // console.log('got data', message, topic);
        if (this.socket) {
            this.socket.emit('message', response);
            if (response.topic.includes('configuration')) {
                this.dataCollectionManager = DataCollectionManager.getInstance();
                this.dataCollectionManager.init(true);
            }
        }
     }
     
    mqtt_connect() {
        this.mqttConnected = true;
        console.log("MQTT Connected");
    }

    // mqtt_subscribe(err, granted) {
    //     console.log("Subscribed to " + this.Topic);
    //     if (err) { console.log(err); }
    // }

    mqtt_reconnect(err) {
        if (err) { console.log(err); }
        this.client = this.getMqttClient()
        this.mqttConnected = true;
        console.log("MQTT ReConnected");
    }

    mqtt_error(err) {
        // console.log(err);
        if (err) {
            //console.log('conn error', err); 
        }
        this.mqttConnected = false;
    }
    
    mqtt_close() {
        console.log("MQTT Disconnected");
        this.client.end();
        this.mqttConnected = false;
        this.client.emit('connect');
    }

    public subscribe(subscriptionTopic): void{
        // console.log('subscribe', subscriptionTopic);
        const client  = this.client;
        client.subscribe(subscriptionTopic, function(err){
            if (err) {
                client.publish('Subscribe Error:' + subscriptionTopic, err.message)
            }
        });
    }

    public unsubscribe(subscriptionTopic): void{
        const client  = this.client;
        client.unsubscribe(subscriptionTopic, function(err){
            if (err) {
                client.publish('Unsubscribe Error:' + subscriptionTopic, err.message)
            }
        });
    }

}
