import * as url from "url";
import ResponseFormatter from "../../helpers/ResponseFormatter";
import Logger from "../../logger/Logger";

export class RoleAndPrivilegesWrapper {
    public static getInstance(): RoleAndPrivilegesWrapper {
        return RoleAndPrivilegesWrapper.instance;
    }

    public static RoleAndPrivilegesWrapper(): RoleAndPrivilegesWrapper {
        return RoleAndPrivilegesWrapper.instance;
    }

    private static instance: RoleAndPrivilegesWrapper = new RoleAndPrivilegesWrapper();

    constructor() {
        if (RoleAndPrivilegesWrapper.instance) {
            throw new Error("Instantion failed! use getInstance() instead.");
        }
        RoleAndPrivilegesWrapper.instance = this;
    }

    public init(roleType, req, res, next): void {
        // if token is parsed properly and we get role in response, check if user is privileged to access it
        if (res.locals.response.role) {
            if (roleType.admin === true && res.locals.response.role === "admin") {
                next();
            } else {
                const errorObj = ResponseFormatter.getErrorObject("forbidden", res.locals.response);
                const responseObj = ResponseFormatter.getResponseObject(false, "Unable to access page", errorObj);
                res.status(403).send(responseObj);
            }
        } else {
            // else continue the execution to validate token in controllers
            next();
        }
    }
}
