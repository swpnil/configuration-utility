import * as jwt from "jsonwebtoken";
import * as url from "url";
import {Collections} from "../../collections";
import {Config} from "../../../config";
import ResponseFormatter from "../../helpers/ResponseFormatter";
import Logger from "../../logger/Logger";

export class JwtWrapper {
    public static getInstance(): JwtWrapper {
        return JwtWrapper.instance;
    }

    public static JwtWrapper(): JwtWrapper {
        return JwtWrapper.instance;
    }

    private static instance: JwtWrapper = new JwtWrapper();
    private excludeUrl = ["/api", "/api/setIp", "/api/login", "/api/signup", "/api/forgotPassword"]; // Token will not be verified for these urls
    private Config: Config;
    private jwtConfig: any; // object to which config is assigned
    private Collections: Collections;

    constructor() {
        if (JwtWrapper.instance) {
            throw new Error("Instantion failed! use getInstance() instead.");
        }
        JwtWrapper.instance = this;
    }

    public init(req, res, next): void {
        this.Config = Config.getInstance();
        this.jwtConfig = this.Config.getConfig("jwtConfig");
        this.Collections = Collections.getInstance();
        this.validateRequestToken(req, res, next);
    }

    public generateToken(payload): string {
        const token = jwt.sign(payload, this.jwtConfig.secretKey, { expiresIn: this.jwtConfig.expiry });

        return token;
    }

    private validateRequestToken(req, res, next): void {
        let tokenVal: string = "";
        let decodedToken: any = {};
        let tokenObj: any = {};
        let userDetailFromCollection: any = {};
        if (this.excludeUrl.indexOf(url.parse(req.url).pathname) === -1) {
            tokenVal = this.getTokenFromRequest(req);
            decodedToken = this.extractPaylodFromToken(tokenVal);
            userDetailFromCollection = (decodedToken) ? (this.Collections.UserCollection.getUserById(decodedToken.id)) : (null);

            tokenObj = {
                token: tokenVal,
            };
            if (userDetailFromCollection) {
                tokenObj.userId = decodedToken.id;
                tokenObj.username = decodedToken.username;
                tokenObj.role = userDetailFromCollection.role;
                jwt.verify(tokenVal, this.jwtConfig.secretKey, (err, decoded) => {
                    if (err) { // res.send error moved to controller for different high level error message as per controller
                        Logger.log("error", err.message);
                        res.locals.err = true;
                        res.locals.response = ResponseFormatter.getErrorObject("invalidToken", tokenObj);
                        next();
                    } else {
                        res.locals.err = false;
                        res.locals.response = tokenObj;
                        next();
                    }
                });
            } else {
                //Logger.log("error", "Invalid Token " + url.parse(req.url).pathname);
                res.locals.err = true;
                res.locals.response = ResponseFormatter.getErrorObject("invalidToken", tokenObj);
                next();
            }
        } else {
            res.locals.err = false;
            next();
        }
    }

    private extractPaylodFromToken(token): any {
        return jwt.decode(token);
    }

    private getTokenFromRequest(req): string {
        let token: string = "";

        if (req.headers.authorization && req.headers.authorization.split(" ")[0] === "Bearer" || req.headers.authorization && req.headers.authorization.split(" ")[0] === "JWT") {
            token = req.headers.authorization.split(" ")[1];
        } else if (req.headers["x-access-token"]) {
            token = req.headers["x-access-token"];
        } else if (req.body && req.body.token) {
            token = req.body.token;
        } else if (req.query && req.query.token) {
            token = req.query.token;
        }

        return token;
    }
}
