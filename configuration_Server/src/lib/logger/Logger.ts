import * as winston from "winston";

class Logger {
    private static logFile = "debug.log";
    private NODE_ENV = process.env.NODE_ENV || "development";
    private logger: any;
    constructor() {
        this.logger = new (winston.Logger)({
            level: "info",
            transports: [
                new (winston.transports.Console)({ level: "debug", colorize: true }),
                new (winston.transports.File)({ level: "info", filename: Logger.logFile }),
            ],
        });
    }

    public log(type, message): void {
        this.logger.log(type, message);
    }
}

export default new Logger();
