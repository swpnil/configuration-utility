// import * as mysql from "mysql";

export class DbQuery {
    private dbConnObj: any;

    constructor(dbConnObj) {
        this.dbConnObj = dbConnObj;
    }

    public execQuery(query, callback): void {
        this.dbConnObj.query(query, callback);
    }

    public execProc(procedureName, callback, params, outparams): void {
        let query = "";
        let outParamsString = "";
        let escapedParams = [];
        if (params && params.constructor === Array) {
            // append paranthesis to parameters
            escapedParams = params.map((x) => "'" + x + "'");
            if (outparams && outparams.constructor === Array) {
                outParamsString = outparams.join(",");
            }
            query = "call " + procedureName + " ( " + escapedParams.join(",") + ((outparams) ? (", " + outParamsString) : ("")) + " ) ";
        } else {
            query = "call " + procedureName + "()";
        }
        this.dbConnObj.query(query, callback);
    }
}
