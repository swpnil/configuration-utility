// import * as mysql from "mysql";
import Logger from "../../logger/Logger";
import {DbQuery} from "./DbQuery";

export class DbManager {
    public static getInstance(): DbManager {
        return DbManager.instance;
    }
    private static instance: DbManager = new DbManager();
    private static credentials: object;
    private static connection: any;
    public isConnected: boolean = false;
    public dbQuery: any;
    private dbConfig: object;

    constructor() {
        if (DbManager.instance) {
            Logger.log("error", "Instantion failed! use getInstance() instead.");
        }
        DbManager.instance = this;
    }

    public init(dbConfig): void {
        this.dbConfig = dbConfig;
        this.connectDb();
    }

    private connectDb(): void {
        // DbManager.connection = mysql.createConnection(this.dbConfig);

        // DbManager.connection.connect((err) => {
        //     if (err) {
        //         Logger.log("error", "Unable to connect to database " + err.message);
        //         this.isConnected = false;
        //     } else {
        //         Logger.log("info", "Connected successfully to database");
        //         this.isConnected = true;
        //     }
        // });

        DbManager.connection.on("error", () => {
            Logger.log("error", "Connection lost with database");
        });

        this.dbQuery = new DbQuery(DbManager.connection);
    }
}
