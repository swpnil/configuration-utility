import { SocketClients } from './../../../models/socket/socketclients';
import * as socketIo from "socket.io";
import { TopicResolver } from "../../helpers/topicResolver";
import { MqttManager } from "../../mqttManager";
import { DataCollectionManager } from "../../processManager/dataCollectionManager";
import { MqttSubscription } from "../../../models/socket/subscriptions";
export class Socket {
    
    private io: any;
    private serverObj: any;
    private static topicResolver: TopicResolver;
    private static mqtt: MqttManager;
    private static datacollectionmanager: DataCollectionManager;
    private static subscriptionList: MqttSubscription[];
    private static scoketClient: SocketClients;
    private static updateTopicList(operation: string, clientId: string, topic?: string) {        
        switch (operation) {
            case 'add': 
                let found = false;
                Socket.subscriptionList.forEach( sub => {
                    if (sub.topic == topic) {
                        found = true;
                        sub.clientIds.push(clientId);
                    }
                });
                if (!found) {
                    let mqttSubscription = new MqttSubscription();
                    mqttSubscription.topic = topic;
                    mqttSubscription.clientIds.push(clientId)
                    Socket.subscriptionList.push(mqttSubscription);
                }
            break;
            
            case 'remove':
                    Socket.subscriptionList.forEach( sub => {
                        if (!topic) {
                            sub.clientIds = sub.clientIds.filter(function(id) {
                                return id !== clientId;
                            })
                        } else {
                            if (sub.topic == topic) {
                                sub.clientIds = sub.clientIds.filter(function(id) {
                                    return id !== clientId;
                                })
                                if (sub.clientIds.length < 1) {
                                    Socket.mqtt.unsubscribe(topic);
                                    console.log('unsubscribe', topic);
                                }
                            }
                        }
                    });
            break;
        }
        // console.log('subscription list ', Socket.subscriptionList)
    }
    constructor(serverObj) {
        Socket.topicResolver = TopicResolver.getInstance();
        Socket.mqtt = MqttManager.getInstance();
        Socket.datacollectionmanager = DataCollectionManager.getInstance();
        this.serverObj = serverObj;
        this.init();
        Socket.scoketClient = new SocketClients();
        Socket.subscriptionList = [];
    }

    private init(): void {
        this.io = socketIo(this.serverObj);
        Socket.mqtt.updateSocketConnection(this.io);
        // console.log('socket manager connect', this.io);
        this.io.on("connect", (socket: any) => {
            // console.log('Client connected', socket.client.id);
            
                socket.on('subscribe', (m: any) => {
                    if (!Socket.mqtt.mqttConnected) {
                        Socket.mqtt.connect();
                    }
                   
                    // const topic = Socket.topicResolver.getTopic(m);
                    const topic = m;
                    
                    console.log('subscribe', topic);
                    Socket.mqtt.subscribe(topic);
                    Socket.updateTopicList('add', socket.client.id, topic);
                });
                socket.on('unsubscribe', (m: any) => {
                    if (!Socket.mqtt.mqttConnected) {
                        Socket.mqtt.connect();
                    }
                    // const topic = Socket.topicResolver.getTopic(m);
                    const topic = m;
                    
                    Socket.updateTopicList('remove', socket.client.id, topic);
                 });
                 socket.on('message', (m: any) => {
                    // console.log('message', m);
                 });
                socket.on('disconnect', () => {
                    // console.log('Client disconnected', socket.client.id);
                    Socket.updateTopicList('remove', socket.client.id);
                });
        });

        // this.io.on('connect', (socket: any) => {
        //     socket.on('message', (m: any) => {
        //         // console.log('[server](message): %s', JSON.stringify(m));
        //         this.io.emit('message', m);
        //     });

        //     socket.on('disconnect', () => {
        //         // console.log('Client disconnected');
        //     });
        // });

    }

    public subscribeToNotifications(projects: any) {
        if (projects.hasOwnProperty("projectObj")) {
            for (const key of Object.keys(projects.projectObj)) {
                Socket.mqtt.subscribe(key+'/notification/#');
            }
        }
    }
}
