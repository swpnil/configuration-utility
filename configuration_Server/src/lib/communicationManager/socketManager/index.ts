import Logger from "../../logger/Logger";
import {Socket} from "./Socket";

export class SocketManager {
    public static getInstance(): SocketManager {
        return SocketManager.instance;
    }
    private static instance: SocketManager = new SocketManager();
    private socketObj: Socket;

    constructor() {
        if (SocketManager.instance) {
            Logger.log("error", "Instantion failed! use getInstance() instead.");
        }
        SocketManager.instance = this;
    }

    public init(serverObj): void {
        this.socketObj = new Socket(serverObj);
    }

    public subscrieToNotification(projects) {
        this.socketObj.subscribeToNotifications(projects);
    }
}
