import * as http from "http";
import { Controllers } from "../../../controllers";
import { RoleAndPrivilegesWrapper } from "../../middleware/roleAndPrivilegesWrapper";
import { forEach } from "async";
import { Config } from "../../../config";
const fs = require('fs');
var path = require('path')
var multer = require('multer');
var storage = multer.diskStorage({
    destination: function(req, file, callback) {
        callback(null, './uploads')
    },
    filename: function(req, file, callback) {
        // console.log("FILEE ",file)
        callback(null,file.originalname)
    }
})

var upload = multer({ storage: storage })

export class RestApi {
    private Controllers: Controllers;
    private RoleAndPrivilegesWrapper;
    private appServer: any;
    private Config: Config

    public static getInstance(): RestApi {
        return RestApi.instance;
    }
    private static instance: RestApi = new RestApi();

    constructor() {
        if (RestApi.instance) {
            // Logger.log("error", "Instantion failed! use getInstance() instead.");
        }
        this.Config = Config.getInstance();
        RestApi.instance = this;
    }

    public intializeServerRoutes(app) {
        this.Controllers = new Controllers();
        this.RoleAndPrivilegesWrapper = RoleAndPrivilegesWrapper.getInstance();
        this.appServer = app;
        this.initiateApplicationRoutes();
        this.initiateBuildingRoutes();
    }

    public initializeAPIRoutes(projectIds) {
        this.initiateProjectRoutes(projectIds);
        this.initiateAuthenticationRoutes();
        // this.initiateUserManagementRoutes();
        this.initiateWebPortalConfigurationRoutes();
        this.initiateAuditLogRoutes();
        this.initiateBuildingRoutes();
    }

    private initiateApplicationRoutes() {
        //this.appServer.get("/api", this.Controllers.ApplicationController.getHomePage);
        //this.appServer.post("/api/setIp", this.Controllers.ApplicationController.setAPIip);
    //    this.appServer.get("/api/getConfig", this.Controllers.ApplicationController.getConfig);
    //     this.appServer.post("/api/saveConfig", this.Controllers.ApplicationController.setConfig);
    }

    private initiateAuthenticationRoutes() {
        this.appServer.post("/api/login", this.Controllers.AuthenticationController.login);
        this.appServer.post("/api/logout", this.Controllers.AuthenticationController.logout);
        this.appServer.post("/api/forgotPassword", this.Controllers.AuthenticationController.forgotPassword);
    }

    /* private initiateUserManagementRoutes(app) {
        app.get("/api/getUserProfile", this.Controllers.UserManagementController.getUserProfile);
        app.get("/api/getAllUsers", this.RoleAndPrivilegesWrapper.init.bind(null, {admin: true}), this.Controllers.UserManagementController.getAllUsers);
        app.post("/api/addUser", this.RoleAndPrivilegesWrapper.init.bind(null, {admin: true}), this.Controllers.UserManagementController.addUser);
        app.post("/api/editUser", this.Controllers.UserManagementController.editUser);
        app.post("/api/deleteUser", this.RoleAndPrivilegesWrapper.init.bind(null, {admin: true}), this.Controllers.UserManagementController.deleteUser);
        app.post("/api/updatePassword", this.Controllers.UserManagementController.updatePasword);
        app.post("/api/resetPassword", this.Controllers.UserManagementController.resetPassword);
    } */

    private initiateWebPortalConfigurationRoutes() {
        this.appServer.post("/api/addEmailConfiguration", this.RoleAndPrivilegesWrapper.init.bind(null, { admin: true }), this.Controllers.WebPortalConfigurationController.addEmailConfiguration);
        this.appServer.get("/api/getEmailConfiguration", this.RoleAndPrivilegesWrapper.init.bind(null, { admin: true }), this.Controllers.WebPortalConfigurationController.getEmailConfiguration);
        // app.post("/api/setSystemTime", this.RoleAndPrivilegesWrapper.init.bind(null, {admin: true}), this.Controllers.WebPortalConfigurationController.setSystemTime);
    }

    private initiateAuditLogRoutes() {
        this.appServer.get("/api/getAuditLog", this.Controllers.AuditLogController.getAuditLog);
    }

    private initiateBuildingRoutes() {
        /* let cacheObj = this.Config.getConfig('cacheData')
        this.appServer.get("/api/getProject", function (req, res) {
            let projectDetail = (fs.existsSync(cacheObj.projectDetail)) ? JSON.parse(fs.readFileSync(cacheObj.projectDetail)) : 'Project details not cached, Restart application server!!!';
            let floorDetail = (fs.existsSync(cacheObj.floorDetail)) ? JSON.parse(fs.readFileSync(cacheObj.floorDetail)) : 'Floor details not cached, Restart application server!!!';
            res.send({ 'projectDetail': projectDetail, 'floorDetail': floorDetail });
        });
        this.appServer.get("/api/getStats", function (req, res) {
            let projectStat = (fs.existsSync(cacheObj.projectStat)) ? JSON.parse(fs.readFileSync(cacheObj.projectStat)) : 'Project stat not cached, Restart application server!!!';
            res.send({ 'projectStat': projectStat });
        }); */
        this.appServer.put("/setIp", this.Controllers.BacNetDataController.setAPIPortData); 
        this.appServer.put("/setIpBacnet", this.Controllers.BacNetDataController.checkBacNetData);
        this.appServer.put("/setIpDashboard", this.Controllers.BacNetDataController.checkDashboardData);



        this.appServer.get("/getBacnetData", this.Controllers.BacNetDataController.getBacNetData);
        this.appServer.post("/SaveBacNetConfiguration", this.Controllers.BacNetDataController.saveBacNetConfiguration);
        this.appServer.get("/GetGWServiceStatus", this.Controllers.BacNetDataController.getGWServiceStatus);
        this.appServer.get("/downloadObjectMapping", this.Controllers.BacNetDataController.downloadObjectMapping);
        this.appServer.get("/RestoreDefaultConfigurations", this.Controllers.BacNetDataController.restoreDefaultConfigurations);
        this.appServer.get("/SetDefaultConfigurations", this.Controllers.BacNetDataController.SetDefaultConfigurations);
        this.appServer.post("/ToggleGWServiceStatus", this.Controllers.BacNetDataController.ToggleGWServiceStatus);
        //this.appServer.get("/getBacnetData", this.Controllers.BacNetDataController.getBacNetData);

        
        this.appServer.get("/projects", this.Controllers.BacNetDataController.getProjectData);
        this.appServer.get("/getTsConfig", this.Controllers.BacNetDataController.getTsConfigData);
        this.appServer.get("/getAPIConfig", this.Controllers.BacNetDataController.getAPIConfigData);
        this.appServer.get("/getAudit", this.Controllers.BacNetDataController.getAuditData);
        this.appServer.get("/getDownloadLogger", this.Controllers.BacNetDataController.getdownloadLoggerData);
        this.appServer.get("/getLightscene", this.Controllers.BacNetDataController.getLightsceneData);
         this.appServer.put("/setTsConfig", this.Controllers.BacNetDataController.setTsConfigData); 
         this.appServer.put("/setDeactiveTS", this.Controllers.BacNetDataController.setDeactiveTS); 
         this.appServer.put("/loadMqtt", this.Controllers.BacNetDataController.loadMqtt);
         this.appServer.put("/removeAuditLog", this.Controllers.BacNetDataController.loadMqtt);


        // this.appServer.get("/getConfig", this.Controllers.BacNetDataController.getConfigData); 
         this.appServer.post("/saveConfig", this.Controllers.BacNetDataController.saveDashboardConfigData);
         this.appServer.get("/getConfig", this.Controllers.BacNetDataController.getDashboardConfigData);

    }

    private initiateProjectRoutes(projectIds) {
        let cacheObj = this.Config.getConfig('cacheData')
        projectIds.forEach(element => {
           
        });

    }
}
