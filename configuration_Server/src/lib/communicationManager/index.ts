import {Config} from "../../config";
import Logger from "../logger/Logger";
import {DbManager} from "./dbManager";
import {RestApi} from "./restApi";
import {SocketManager} from "./socketManager";
import { BuildingService } from "../../services/BuildingService";


export class CommunicationManager {

    private static BuildingService: BuildingService;
    
    public static getInstance(): CommunicationManager {
        return CommunicationManager.instance;
    }
    private static instance: CommunicationManager = new CommunicationManager();
    public DbManager: DbManager; // TODO: remove objects and make variable same as class
    // private RestApi: RestApi;
    private SocketManager: SocketManager;
    public ProjectIds: string []=[];


    constructor() {
        if (CommunicationManager.instance) {
            Logger.log("error", "Instantion failed! use getInstance() instead.");
        }
        CommunicationManager.instance = this;
    }

    public init(serverObj, appObj): void {
        RestApi.getInstance().intializeServerRoutes(appObj)
      SocketManager.getInstance().init(serverObj);
    }

    public initSocket(serverObj) {
       
    }
}
