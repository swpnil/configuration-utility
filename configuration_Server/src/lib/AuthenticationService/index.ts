import * as async from "async";
import * as passwordGenerator from "password-generator";
// import {AuditLogger} from "../auditLogger";
import {Collections} from "../collections";
import {CommunicationManager} from "../communicationManager";
import {Config} from "../../config";
import ResponseFormatter from "../helpers/ResponseFormatter";
import Logger from "../logger/Logger";
import {MailingService} from "../mailingService";
import {JwtWrapper} from "../middleware/jwtWrapper";
import {ReadUserByUsernameModel} from "../../models/databaseModels/ReadUserByUsernameModel";
import {ResetPasswordByUsernameModel} from "../../models/databaseModels/ResetPasswordByUsernameModel";
import {LoginResponseModel} from "../../models/responseModels/LoginResponseModel";

export class AuthenticationService {
    public static getInstance(): AuthenticationService {
        return AuthenticationService.instance;
    }
    private static instance: AuthenticationService = new AuthenticationService();
    private CommunicationManager: any;
    // private AuditLogger: AuditLogger;
    private JwtWrapper: JwtWrapper;
    private MailingService: MailingService;
    private Collections: Collections;
    private userList: any;
    constructor() {
        if (AuthenticationService.instance) {
            throw new Error("Instantion failed! use getInstance() instead.");
        }
        AuthenticationService.instance = this;
        this.userList = {"admin": { "username": "admin", "password": "admin", "user_id": "ap987yh6", "role": "admin" },
            "user": { "username": "user", "password": "user", "user_id": "ap987yh6", "role": "user" }}
    }

    /**
     * validateLoginRequest() - receives request parameter from authentication controller and checks if it is valid in database
     *
     * @param <Object> loginCredentials - request parameter formed in object - username, password
     * @return <Function> callback - method executed when database read operation is done
     */
    public validateLoginRequest(loginCredentials, callback): any {
        let cbResponse = {};
        let result = {};
        const readUserObj = new ReadUserByUsernameModel(loginCredentials.username);
        // this.CommunicationManager.DbManager.dbQuery.execProc("sp_ReadUserByUsername", (err, result, fields) => {
        //     if (err) {
        //         Logger.log("error", err.message);
        //         cbResponse = {
        //             isSuccess: false,
        //             result: ResponseFormatter.getErrorObject("failedDbQuery", loginCredentials),
        //         };
        //     }

            // if (Array.isArray(result) && result[0].length > 0) { // user exists
        if (loginCredentials.username == "admin" || loginCredentials.username == "user") {
                // result[0] = {"username": "admin", "password": "admin", "user_id": "ap987yh6"}
                // const resultSet = {"username": "admin", "password": "admin", "user_id": "ap987yh6", "role": "admin"};
                const resultSet = this.userList[loginCredentials.username]
                if (loginCredentials.password == resultSet.password) {
                // if (resultSet[0].password === loginCredentials.password) {
                    const jwtToken = this.JwtWrapper.generateToken({id: resultSet.user_id, username: resultSet.username});
                    const loginApiResponse = new LoginResponseModel(resultSet, jwtToken);
                    cbResponse = {
                        isSuccess: true,
                        result: loginApiResponse,
                    };
                    this.Collections.UserCollection.setUser(resultSet.user_id, {role: resultSet.role});
                } else {
                    cbResponse = {
                        isSuccess: false,
                        result: ResponseFormatter.getErrorObject("invalidPassword", loginCredentials),
                    };
                }
            } else {
                cbResponse = {
                    isSuccess: false,
                    result: ResponseFormatter.getErrorObject("userNotFound", loginCredentials),
                };
            }
            callback(cbResponse);
        //}, readUserObj.data);
    }

    public validateForgotPasswordRequest(forgotPasswordCredentials, callback, adminEmail = null): any {
        let cbResponse = {};
        const newPassword = (forgotPasswordCredentials.password) ? (forgotPasswordCredentials.password) : (passwordGenerator());
        const resetPasswordObj = new ResetPasswordByUsernameModel(forgotPasswordCredentials.username, forgotPasswordCredentials.user_id, newPassword);
        this.CommunicationManager.DbManager.dbQuery.execProc("sp_ResetPasswordByUsername", (updateReqErr, updateReqResponse, updateReqFields) => {
            if (updateReqErr) {
                Logger.log("error", updateReqErr.message);
                // this.AuditLogger.log(forgotPasswordCredentials.username, "application", "Forgot Password", "Error updating password", "error");
                cbResponse = {
                    isSuccess: false,
                    result: ResponseFormatter.getErrorObject("failedDbQuery", forgotPasswordCredentials),
                };
            }

            if (Array.isArray(updateReqResponse) && updateReqResponse[0].length > 0) {
                const updateReqResultSet = updateReqResponse[0];

                if (updateReqResultSet[0]._returnValue === 0) {
                    cbResponse = {
                        isSuccess: false,
                        result: ResponseFormatter.getErrorObject("userNotFound", forgotPasswordCredentials),
                    };
                    callback(cbResponse);
                } else {
                    this.MailingService.sendMail(forgotPasswordCredentials.username, forgotPasswordCredentials.username, "forgotPassword", {password: newPassword}, (sendMailRes) => {
                        if (sendMailRes.isSuccess) {
                            cbResponse = {
                                isSuccess: true,
                                result: forgotPasswordCredentials,
                            };
                        } else {
                            cbResponse = {
                                isSuccess: false,
                                result: ResponseFormatter.getErrorObject("emailError", forgotPasswordCredentials),
                            };
                        }
                        callback(cbResponse);
                    });
                    // if (adminEmail) {
                    //     this.AuditLogger.log(adminEmail, "application", "Reset Password", "User password reset successfully " + forgotPasswordCredentials.username, "information");
                    // } else {
                    //     this.AuditLogger.log(forgotPasswordCredentials.username, "application", "Forgot Password", "User password set successfully", "information");
                    // }
                }
            }
        }, resetPasswordObj.data, ["@returnValue"]);
    }

    public destroyToken(userId): void {
        this.Collections.UserCollection.deleteUserById(userId);
    }

    public refreshToken(tokenUserid, userId, uName): any {
        let newToken = null;
        const userDetailFromCollection = this.Collections.UserCollection.getUserById(userId);
        if (userDetailFromCollection && tokenUserid === userId) {
            newToken = this.JwtWrapper.generateToken({id: userId, username: uName});
        }
        return newToken;
    }

    public init(callback): void {
        this.CommunicationManager = CommunicationManager.getInstance();
        // this.AuditLogger = AuditLogger.getInstance();
        this.JwtWrapper = JwtWrapper.getInstance();
        this.Collections = Collections.getInstance();
        this.MailingService = MailingService.getInstance();
    }
}
