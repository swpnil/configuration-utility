import * as childProcess from "child_process";
import * as winston from "winston";

class ProcessManager {
    private exec;
    // private scriptObj: object = {
    //     setSystemTime: "./scripts/setSystemTime.sh",
    // };
    constructor() {
        // code will come here
        this.exec = childProcess.exec;
    }

    // public executeScript(scriptName, callback, param = "") {
    //     const yourscript = this.exec("sh " + this.scriptObj[scriptName] + " " + param, callback);
    // }
}

export default new ProcessManager();
