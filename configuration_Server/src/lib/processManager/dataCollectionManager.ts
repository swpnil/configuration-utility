import { BuildingService } from './../../services/BuildingService';
import Logger from "../logger/Logger";
import { HttpRequest } from "../../HttpRequest";
import { APIConfiguration } from "../collections/APIConfiguration";
import { FileHelper } from "../helpers/FileHelper";
import * as async from "async";
import { Config } from "../../config";
import { SocketManager } from "../communicationManager/socketManager";

var path = require("path");
var fs = require("fs");
var base64Img = require('base64-img');
const fileType = require('file-type');

export class DataCollectionManager {

  public static getInstance(): DataCollectionManager {
    return DataCollectionManager.instance;
  }
  private SocketManager: SocketManager;
  private static instance: DataCollectionManager = new DataCollectionManager();
  private HttpRequest: HttpRequest;
  private APIConfiguration: APIConfiguration;
  private fileHelper: FileHelper;
  private Config: Config;
  public dataObject: any = {}
  private buildingService: BuildingService;

  constructor() {
    if (DataCollectionManager.instance) {
      Logger.log("error", "Instantion failed! use getInstance() instead.");
    }
    DataCollectionManager.instance = this;
    this.HttpRequest = new HttpRequest();
    this.APIConfiguration = APIConfiguration.getInstance();
    this.Config = Config.getInstance();
    this.dataObject = {};
    this.SocketManager = SocketManager.getInstance();
  }

  public init(recalled?): void { 
    async.waterfall([
      async.apply(this.initializeProcess, this),
      //this.cachedInitializeProcessData,
      this.projectStat
    ], (err, results) => {
      if (!recalled) {
        let apiConfig = this.Config.getConfig("apiConfig");
        const systemConfig = this.Config.getConfig("systemConfig");
        Logger.log("info", "Configuration Server running at port:888");
        console.log('Connected to Molex API', apiConfig)
        // console.log('dataObject ', this.dataObject)
      }
    })
  }

public getChartConfig(){
  return this.Config.getConfig("chartConfig");
}
  /**
   * Method use to get project details and set data into object
   */
  public initializeProcess(selfObj, callback: any) {
    // console.log("initialize");
   // selfObj.clearFloorPlanDirectory();
    let appConfig = selfObj.Config.getConfig("applicationConfig");
    let smartCoreConfig = selfObj.Config.getConfig("smartcoreConfig");
    // console.log('smart core config',smartCoreConfig);
    
    let projectMapping: any[] = [];
    let options = {
      method: "GET",
      url: selfObj.APIConfiguration.startupUrl.projectDetail,
      json: true,
      auth: selfObj.APIConfiguration.authentication
    };
    let optionsProject = {
      method: "GET",
      url: selfObj.APIConfiguration.startupUrl.projects,
      json: true,
      auth: selfObj.APIConfiguration.authentication
    };
    selfObj.HttpRequest.makeHttpCall(options, function (result) {
    // console.log('initializeProcess ', options)
    selfObj.HttpRequest.makeHttpCall(optionsProject, function (r) {
     if ( r.status) {
      r.result.data.forEach( data => {
        let obj = {
          "ip": data.ip,
          "projectid": data.id
        }
        // console.log('big data', data)
        projectMapping.push(obj);
      });
     } 
     
        // init object for project and floor
      // console.log('project ', result)
      var projectObj: any = {};
      var floorObj: any = {};
      let locations = {
        Goldleaf: { lat: "36.71829033", long: "-119.70289029" },
        Cecile: { lat: "36.69682155", long: "-119.70598019" },
        "Loan start": { lat: "36.7004001", long: "-119.66718472" },
        Malaga: { lat: "36.68828742", long: "-119.73035611" },
        "Loan start 1": { lat: "36.69461928", long: "-119.68469418" },
        "Cecile 1": { lat: "36.70618047", long: "-119.70220364" },
        "Goldleaf 1": { lat: "36.71966633", long: "-119.70392025" },
        "Malaga 1": { lat: "36.68993926", long: "-119.74923886" }
      };
      if (result.status) {
        result.result.data.forEach(project => {
          // console.log('project ', project)
          let projectId = project.id;
          let buildingId = project.buildingID;
          // set projectId into data object
          projectObj[projectId] = {}
          floorObj[projectId] = { [buildingId]: {}}
          let keys = Object.keys(locations);
          let randomLocation = locations[keys[(keys.length * Math.random()) << 0]];
          let location = keys.find(key => locations[key] === randomLocation);
          // console.log('location ', location)
          // console.log('randomLocation ', randomLocation)
          let latlang = {
            name: undefined,
            lat: undefined,
            lang: undefined,
            loc: undefined
          };
          projectMapping.forEach(map => {
            if (map.projectid == project.id) {
              // console.log();
              smartCoreConfig.buildings.forEach( b => {
                // console.log('buildingsssssssss',map.ip, b.ip);
                if (map.ip == b.ip) {
                  latlang.name = b.name
                  latlang.lang = b.longitude;
                  latlang.lat = b.latitude;
                  latlang.loc = b.location;
                }
              });
            }
          });
          // console.log('latlang', projectObj[projectId][buildingId]);
          
          projectObj[projectId][buildingId] = {
            'buildingInfo': {
              'name': latlang.name? latlang.name : project.buildingName,
              'location': latlang.loc? latlang.loc : location,
              'longitude': latlang.lang? latlang.lang: randomLocation.long,
              'latitude': latlang.lat? latlang.lat: randomLocation.lat,
            },
            'floorCount': Object.keys(project.floors).length,
            'zoneCount': 0
          }
          // console.log('latlang', projectObj[projectId][buildingId]);
          let floorCounter = 1;
          project.floors.forEach(floor => {
            // zone count for building
            projectObj[projectId][buildingId]['zoneCount'] = projectObj[projectId][buildingId]['zoneCount'] + floor.zones.length

            // zone count for floor
            floorObj[projectId][buildingId][floor.id] = {
              'floorInfo': {
                'name': floor.name,
                'displayName': 'Floor ' + floorCounter,
                'floorGridH': '100',
                'floorGridW': '100'
              },
              'floorStat': {
                'zoneCount': floor.zones.length
              },
              'zones': floor.zones,
              'floorMap': ''
            }
            selfObj.getFloorPlan(selfObj, projectId, floor.id);
            floorCounter++
          });
        });
        selfObj.dataObject['projectObj'] = projectObj;
        selfObj.dataObject['floorObj'] = floorObj;
        // selfObj.updateFloorPlanDimension(result.result.data);
        
        
        // final callback
        // callback(null, selfObj, { 'projectObj': projectObj, 'floorObj': floorObj });
      }
      selfObj.SocketManager.subscrieToNotification(selfObj.dataObject);
      callback(null, selfObj, projectObj);
    });
    
    });

  }

  public getFloorPlan(selfObj, pid,fid) {
    var options = {
      method: "GET",
      url: selfObj.APIConfiguration.startupUrl.getFloorPlan,
      qs: { projectid: pid, id: fid},
      json: true,
      auth: selfObj.APIConfiguration.authentication
    };
    this.buildingService = BuildingService.getInstance();
    this.HttpRequest.makeHttpCall(options, function(result) {
      let responseObj = {};
      if (!result.status) {
        // Logger.log("error", err.message);
        // const errorObj = ResponseFormatter.getErrorObject("projectNotFound", result.locals);
        // responseObj = ResponseFormatter.getResponseObject(false, "Unable to get Floor Paln", errorObj);
        // callback(responseObj);
      } else {
        if (result && Object.keys(result).length > 0) {
          var buf = Buffer.from(result.result.data[0].plan,'base64');
          console.log('floor dimension', result.result.data[0].gridH, result.result.data[0].gridW);
          if (fileType(buf).ext !== 'pdf') {

           selfObj.buildingService.updateGridDimentsions(fid, result.result.data[0].gridW, result.result.data[0].gridH);
            fs.writeFile(path.join('floorplans/',fid+'.'+fileType(buf).ext), buf, 'base64', function(error){
              if(error){
                return false;
              }else{
                // console.log('File created from base64 string!');
                return true;
              }
            });
          }
        } else {
          // const errorObj = ResponseFormatter.getErrorObject("projectNotFound", result.locals);
          // responseObj = ResponseFormatter.getResponseObject(false, "Unable to get Floor Plan", errorObj);
          // callback(responseObj);
        }
      }
    });
  }

  public updateFloorPlanDimension(resp) {
    let floorObj =  this.dataObject['floorObj'];
    resp.forEach(project => {
      // console.log('project ', project)
      let projectId = project.id;
      let buildingId = project.buildingID;
      Object.keys(floorObj[projectId][buildingId]).forEach( floorid => {
        const dim =  this.buildingService.getFloorDimensions(floorid);
        if (dim) {
          floorObj[projectId][buildingId][floorid].floorInfo.floorGridH = dim.gridH;
          floorObj[projectId][buildingId][floorid].floorInfo.floorGridW = dim.gridW;
        } else {
          floorObj[projectId][buildingId][floorid].floorInfo.floorGridH = '100';
          floorObj[projectId][buildingId][floorid].floorInfo.floorGridW = '100';
        }
        
      });

    });
  }

  // public clearFloorPlanDirectory() {
  //   fs.readdir('floorplans/', (err, files) => {
  //     if (err) console.log(err)
    
  //     for (const file of files) {
  //       // console.log(file);
  //       if (!file.includes('defaultfloormap')) {
  //         fs.unlink(path.join('floorplans/', file), err => {
  //           if (err) console.log(err)
  //         });
  //       }
  //     }
  //   });
  // }
  /**
   * cachedInitializeProcessData
   * Method to cache data
   */
  public cachedInitializeProcessData(selfObj, data, callback: any) {
    let appConfig = selfObj.Config.getConfig("cacheData");
    // console.log('appConfig ', appConfig)
    // make tasks to execute in sequense
    const tasks = [function projectDetails(cb) {
      // update project detail cache and call inner callback

      let filePath = appConfig.projectDetail;
      DataCollectionManager.writeCacheFile(filePath, data.projectObj, (err) => {
        if (err) {
          console.log("Project details cached!!!")
          return cb(null);
        }
      });
    },
    function floorDetails(cb) {
      // update floor detail cache and call inner callback
      let floorFilePath = appConfig.floorDetail;
      DataCollectionManager.writeCacheFile(floorFilePath, data.floorObj, (err) => {
        if (err) {
          console.log("Floor details cached!!!")
          return cb(null);
        }
      });
    }];
    // complete task in a series and call final callback
    async.series(tasks, (err, results) => {
      if (err) {
        callback(err);
      }
      callback(null, selfObj, data.projectObj);
    })
  }


  /* *
   * Method use to get project stat data and cache it.
   */
  public projectStat(selfObj, projectObj, callback: any) {
    // init project stat object
    var projectStatObj: any = {};
    let options = {
      method: "GET",
      url: selfObj.APIConfiguration.startupUrl.projectStat,
      qs: {},
      json: true,
      auth: selfObj.APIConfiguration.authentication
    };
    // Push HTTP request into promises array, once all request completed then update cache.
    var promises = [];
    Object.keys(projectObj).forEach((projectId) => {
      // modify options qs param with projectId
      options.qs = { projectid: projectId }
      // push each HTTP request into promises array
      promises.push(new Promise(function (resolve, reject) {
        selfObj.HttpRequest.makeHttpCall(options, function (result) {
          let data = result.result.data[0];
          projectStatObj[projectId] = { data };
          // return HTTP request response
          resolve(projectStatObj);
        });
      }));
    })
    
    // selfObj.dataObject = { 'projectStat':  }
    // when all promises completed update cache.
    Promise.all(promises).then(function (value) {
      let appConfig = selfObj.Config.getConfig("cacheData");
      let projectStatFilePath = appConfig.projectStat;
      selfObj.dataObject['projectStat'] = projectStatObj
      selfObj.dataObject['chartConfig'] = selfObj.Config.getConfig("chartConfig");
      selfObj.dataObject['humidexRangeConfig'] = selfObj.Config.getConfig("humidexRangeConfig");
      selfObj.dataObject['sensorUnitConfig'] = selfObj.Config.getConfig("sensorUnitConfig");
      selfObj.dataObject['applicationConfig'] = selfObj.Config.getConfig("applicationConfig");
      
      /* DataCollectionManager.writeCacheFile(projectStatFilePath, projectStatObj, (err) => {
        if (err) {
          console.log("Project stat cached!!!")
          callback(null);
        }
      }); */
      callback(null);
    })
  }

  /*
   *  Method use to write file
   *  @param filePath string path of file
   *  @param data Object data which need to be write
   */
  private static writeCacheFile(filePath, data, callback: any) {
    fs.writeFile(filePath, JSON.stringify(data, null, 2), (err) => {
      if (err) {
        callback(err);
        console.error(err);
      };
      callback(true);
    })
  }


  public getProjects(): any {
    return this.dataObject.projectObj;
  }


  public getFloors(): any {
    return this.dataObject.floorObj;
  }


  public getProjectStat(): any {
    return this.dataObject.projectStat;
  }


  public getAllProject(): any {
    // console.log('this.dataObject ', this.dataObject)
    return this.dataObject;
  }

  public getFloorsByProjectId(pid: string) {
    return this.dataObject.floorObj[pid];
  }

}