import * as jwt from "jsonwebtoken";
// import {AuditLogger} from "../auditLogger";
import {AuthenticationService} from "../lib/AuthenticationService";
import ResponseFormatter from "../lib/helpers/ResponseFormatter";
import Validator from "../validator";

export class AuthenticationController {
    // this object is undefined hence using static private members
    private static AuthenticationService: AuthenticationService;
    // private static AuditLogger: AuditLogger;

    constructor() {
        AuthenticationController.AuthenticationService = AuthenticationService.getInstance();
        // AuthenticationController.AuditLogger = AuditLogger.getInstance();
    }

    public login(req, res): void {
        let responseObj = {};
        const reqParamsRes = Validator.AuthenticationValidator.validateLoginRequestParams(req);
        const reqParams = reqParamsRes.result;
        if (reqParamsRes.isSuccess) {
            AuthenticationController.AuthenticationService.validateLoginRequest(reqParams, (response) => {
                responseObj = ResponseFormatter.getResponseObject(response.isSuccess, (response.isSuccess) ? ("Logged in successfully") : ("Error logging in"), response.result);
                if (response.isSuccess === false) {
                    res.status(401);
                } else {
                    // AuthenticationController.AuditLogger.log(reqParams.username, "application", "Sign in", "Signed in successfully", "information");
                }
                res.send(responseObj);
            });
        } else {
            responseObj = ResponseFormatter.getResponseObject(false, "Error logging in", reqParams);
            res.status(401).send(responseObj);
        }
    }

    public logout(req, res): void {
        let responseObj = {};
        if (!res.locals.err) {
            AuthenticationController.AuthenticationService.destroyToken(res.locals.response.userId);
            // AuthenticationController.AuditLogger.log(res.locals.response.username, "application", "Sign out", "Signed out successfully", "information");
            responseObj = ResponseFormatter.getResponseObject(true, "Logged out successfully", []);
        } else {
            responseObj = ResponseFormatter.getResponseObject(false, "Unable to log out", res.locals.response);
        }
        res.send(responseObj);
    }

    public forgotPassword(req, res): void {
        let responseObj = {};
        const reqParamsRes = Validator.AuthenticationValidator.validateForgotPasswordRequestParams(req);
        const reqParams = reqParamsRes.result;
        if (reqParamsRes.isSuccess) {
            AuthenticationController.AuthenticationService.validateForgotPasswordRequest(reqParams, (response) => {
                responseObj = ResponseFormatter.getResponseObject(response.isSuccess, (response.isSuccess) ? ("Password reset successfully") : ("Error resetting password"), (response.isSuccess) ? ([]) : (response.result));
                res.send(responseObj);
            });
        } else {
            responseObj = ResponseFormatter.getResponseObject(false, "Error resetting password", reqParams);
            res.send(responseObj);
        }
    }
}
