import {AuditLogger} from "../auditLogger";
import {CommunicationManager} from "../lib/communicationManager";
import ResponseFormatter from "../lib/helpers/ResponseFormatter";

export class AuditLogController {
    // this object is undefined hence using static private members
    private static CommunicationManager: CommunicationManager;
    private static AuditLogger: AuditLogger;
    constructor() {
        AuditLogController.CommunicationManager = CommunicationManager.getInstance();
        AuditLogController.AuditLogger = AuditLogger.getInstance();
    }

    public getAuditLog(req, res): void {
        let responseObj = {};
        if (!res.locals.err) {
            AuditLogController.AuditLogger.getLogs((response) => {
                if (response.isSuccess) {
                    responseObj = ResponseFormatter.getResponseObject(true, (response.result.length > 0) ? ("Audit log received successfully") : ("No audit logs created"), response.result);
                } else {
                    responseObj = ResponseFormatter.getResponseObject(false, "Unable to receive audit log", response.result);
                }
                res.send(responseObj);
            });
        } else {
            responseObj = ResponseFormatter.getResponseObject(false, "Unable to receive audit log", res.locals.response);
            res.status(401).send(responseObj);
        }
    }
}
