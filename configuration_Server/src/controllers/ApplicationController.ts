import * as path from "path";
import { Collections } from "../lib/collections";
import { CommunicationManager } from "../lib/communicationManager";
import { RestApi } from "../lib/communicationManager/restApi";
import { BuildingService } from "../services/BuildingService";
const fs = require('fs');
export class ApplicationController {
    private BuildingService: BuildingService;
    constructor() {
        // constructor code will come here
    }

    public static getInstance(): ApplicationController {
        return ApplicationController.instance;
    }
    private static instance: ApplicationController = new ApplicationController();


    public initApi(req, res): void {
        BuildingService.getInstance();
    }

    public getHomePage(req, res): void {
        res.sendFile(path.join(__dirname + "/ui/index.html"));
    }

    public setAPIip(req, res): any {
        // console.log('Set API called', req.body);
        let collection = Collections.getInstance();
        let init = {
            ip: req.body.ip,
            port: req.body.port,
            dmsIp :req.body.dmsIp,
            firmwarePort : req.body.firmwarePort,
            dmsPort : req.body.dmsPort
        }
        let ProjectIds: string []=[];
        collection.APIConfiguration.setAPI(init)
        BuildingService.getInstance().getProject({}, function(response){
            response.result.data.forEach(element => {
                ProjectIds.push(element.id)
            });
        
        RestApi.getInstance().initializeAPIRoutes(ProjectIds);
        // RestApi = new RestApi(appObj, ProjectIds);
            
        });
        res.send(true);
    }

    public getConfig(req, res): any {
        // console.log('dir', __dirname);
        fs.readFile("./appConfig.json", (err, data) => {  
            if (err) {
                res.send(err);
            }
            let config = JSON.parse(data);
            res.send(config);
        });
        
    }

    public setConfig(req, res): any {
        // console.log('data', req.body, JSON.stringify(req.body))
        if (req.body) {
            fs.writeFile('./appConfig.json', JSON.stringify(req.body, null, 4), (err) => {  
                if (err) {
                    res.send(err);
                }
                res.send(true);
                // console.log('Data written to file');
            });
        }
        
    }

}
