import { ApplicationController } from './ApplicationController';
import { forEach } from 'async';
import { log } from 'util';
import { DataCollectionManager } from './../lib/processManager/dataCollectionManager';
import { DateConverter } from './../lib/helpers/DateConverter';
import * as bodyParser from "body-parser";
import { BuildingService } from "../services/BuildingService";
import { AuthenticationService } from "../lib/AuthenticationService";
import ResponseFormatter from "../lib/helpers/ResponseFormatter";
import Logger from "../lib/logger/Logger";
import { GetUserProfileResModel } from "../models/responseModels/GetUserProfileResModel";
import { Config } from '../config';
import { TemperatureConverter } from '../lib/helpers/TemperatureConverter';
var lodashSortBy = require('lodash');
var fs = require("fs");
export class BuildingController {

    private static BuildingService: BuildingService;
    private static DataCollectionManager: DataCollectionManager;
    private static dateConverter: DateConverter;
    private static tempConverter: TemperatureConverter;
    private static appController : ApplicationController;
    private static Config: Config;
    constructor() {
        // constructor code will come here
        BuildingController.dateConverter = DateConverter.getInstance();
        BuildingController.tempConverter = TemperatureConverter.getInstance();
        BuildingController.Config = Config.getInstance();
    }

    public getProject(req, res): void {
        BuildingController.BuildingService = BuildingService.getInstance();
        let responseObj = {};
        /* if (cache.getProject) {
            res.send(cache.getProject)
        } else { */
        BuildingController.BuildingService.getProject(req, (result) => {
            // cache.getProject = result;
            // return response
            res.send(result);
        });
        // }
    }

    public getStats(req, res): void {
        BuildingController.BuildingService = BuildingService.getInstance();
        let responseObj = {};

        // var id = req.query.id;
        // var type = req.query.type;
        // var projectId = req.query.projectid;

        // if (id && type) {
            /* if (cache.getStats) {
                res.send(cache.getStats)
            } else { */
            BuildingController.BuildingService.getStats((result) => {
                // cache.getStats = result;
                res.send(result);
            });
            // }
        // } else {
        //     responseObj = ResponseFormatter.getResponseObject(false, "Unable to get stats", req.query);
        //     // cache.getStats = '';
        //     res.send(responseObj);
        // }
    }


    getProjectMetaData(req, res): void {
        BuildingController.BuildingService = BuildingService.getInstance();
        BuildingController.BuildingService.getProjectMetaData((result) => {
            // cache.getSensorLiveData = result;
            res.send(result);
        });
    }

    /**
     *  Method getSensorLiveData use to get sensor last value
     *  @param projectid project id
     *  @param type type of area of which sensor data require (building, zone, floor)
     *  @param id type id (building id, zone id)
     *  @param starttime start time of sensor data (Format 2018-11-14T09:57:00Z)
     *  @param endtime end time of sensor data (Format 2018-11-14T09:57:00Z)
     *  @param unit unit of time (hours, min)
     * 
     **/
    public getSensorLiveData(req, res): void {
        BuildingController.BuildingService = BuildingService.getInstance();
        let responseObj = {};
        var projectId = req.query.projectid;
        var id = req.query.id;
        var type = req.query.type;
        var sensor = req.query.sensor;
        var starttime = req.query.starttime;
        var endtime = req.query.endtime;
        var unit = req.query.unit;
        // param require
        if (projectId && id && type && sensor && starttime && endtime && unit) {
            // if sensor data cached then return cached data, else call service
            /* if (cache.getSensorLiveData) {
                res.send(cache.getSensorLiveData)
            } else { */
            BuildingController.BuildingService.getSensorLiveData(projectId, id, type, sensor, starttime, endtime, unit, (result) => {
                // cache.getSensorLiveData = result;
                res.send(result);
            });
            // }
        } else {
            responseObj = ResponseFormatter.getResponseObject(false, "Unable to get Sensor data", req);
            // cache.getSensorLiveData = '';
            res.send(responseObj);
        }
    } // end getSensorLiveData method

    public getOccupancy(req, res): void {
        BuildingController.BuildingService = BuildingService.getInstance();
        let responseObj = {},
            requestObj = {},
            id = req.query.id,
            type = req.query.type,
            projectId = req.query.projectid,
            sTime = req.query.starttime,
            eTime = req.query.endtime,
            unit = req.query.unit;
            // unit = 'hour';
        if (id && type && projectId && sTime && eTime && unit) {
            /* if (cache.getOccupancy) {
                res.send(cache.getOccupancy)
            } else { */
            var starttime=  BuildingController.dateConverter.convertToUTC(sTime);
            var endtime=  BuildingController.dateConverter.convertToUTC(eTime);
            requestObj = { id, type, projectId, starttime, endtime, unit }
            // console.log("requestObj",requestObj)
            BuildingController.BuildingService.getOccupancy(requestObj, (result) => {
                if (result.status && result.result.data[0]['e'].length > 0) {
                    let dataValueArray = [],
                        filterdataArray = [],
                        timeArray = [],
                        chartData = [];
                    // result.result.data[0]['e'].sort((a, b) => parseFloat(a.t) - parseFloat(b.t));
                    filterdataArray = lodashSortBy.sortBy(result.result.data[0]['e'], ['t']);
                    filterdataArray.forEach(e => {
                        dataValueArray.push(Math.round(parseFloat(e.v)));
                        let timeConverted = BuildingController.dateConverter.convertToLocalMili(parseInt(e.t));
                        timeArray.push(timeConverted);
                        const temp = [timeConverted, Math.round(parseFloat(e.v))];
                        chartData.push(temp);
                    });
                    let minValue = BuildingController.dateConverter.getMinValue(dataValueArray);
                    let maxValue = BuildingController.dateConverter.getMaxValue(dataValueArray);
                    if (minValue == 0 && maxValue == 0) {
                        minValue = 0
                        maxValue = 100
                    } else if (minValue == maxValue && maxValue > 0) {
                        minValue = 0
                    }
                    /* console.log('timeArray ', timeArray)
                    console.log('maxTime ', maxTime)
                    console.log('minTime ', minTime) */
                    result.result.data[0].minValue = minValue;
                    result.result.data[0].maxValue = maxValue;
                    result.result.data[0].minTime = BuildingController.dateConverter.getMinValue(timeArray);
                    result.result.data[0].maxTime = BuildingController.dateConverter.getMaxValue(timeArray);
                    result.result.data[0].chartData = chartData;
                    responseObj = result;
                }
                res.send(responseObj);
               /*  let chartData = [];
                let categories = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
                let returnData = {};
                if (result.status) {
                    // console.log('unit ', unit)    
                    // console.log('unit type', typeof unit)
                    if (unit == 'min') {
                        
                        let counter = 1;
                        let length = result.result.data[0].e.length - 1;
                        let sumArr = [];
                        let chunk = 60;
                        let temparray = []
                        for (let i = 0, j = result.result.data[0].e.length; i < j; i += chunk) {
                            // console.log(i)
                            temparray.push(result.result.data[0].e.slice(i, i + chunk + 1));
                        }
    
                        temparray.forEach((element, index) => {
    
                            temparray[index].forEach(e => {
    
                                sumArr.push(parseFloat(e.v));
    
                            });
                            let sum = sumArr.reduce(function (a, b) { return a + b; });
                            // console.log('sum ', sum)
                            
                            let sumLength = (sumArr.length > 60) ? sumArr.length - 1 : sumArr.length;
                            // console.log('sumArr.length ', sumLength)
                            let avg = sum / sumLength;
                            // console.log('avg ', avg)
                            if (avg < 10) {
                                avg = Math.round(avg) / 10;
                            } else if (avg < 100) {
                                avg = Math.round(avg) / 100;
                            } else {
                                avg = 1.0;
                            }
                            let day = new Date(parseInt(temparray[index][temparray[index].length - 1].t)).getDay();
                            sumArr = []
                            chartData.push({
                                "x": temparray[index][0].t,
                                "x2": temparray[index][temparray[index].length - 1].t,
                                "y": day,
                                "partialFill": avg,
                                "color": '#FDA660'
                            });
                        })
                    } else { */
                        // console.log('unit 1', unit)
                 /*        result.result.data[0].e.forEach((element, index) => {
                            let avg = element.v;
                            if (avg < 10) {
                                avg = Math.round(avg) / 10;
                            } else if (avg < 100) {
                                avg = Math.round(avg) / 100;
                            } else {
                                avg = 1.0;
                            }
                            let day = new Date(parseInt(element.t)).getDay();
                            let xHour = parseInt(element.t);
                            let x2Hour = parseInt(element.t)+3600000;
                            chartData.push({
                                "x": xHour,
                                "x2": x2Hour,
                                "y": day,
                                "partialFill": avg,
                                "color": '#FDA660'
                            });
                        });
                    } */
                    // console.log("chartData ", chartData)
                    
                    // ==================== 15 min sampling =========================
                    /* result.result.data[0].e.forEach((element, index) => {
                        
                        if (counter <= 15 && ((index + 1) < length)) {
    
                            
                            sumArr.push(Math.round(parseFloat(element.v)));
                            if (counter === 15) {
                                
                                let sum = sumArr.reduce(function (a, b) { return a + b; });
                                // console.log('sum ', sum)
                                // console.log('sumArr.length ', sumArr.length)
                                let avg = sum / sumArr.length;
                                // console.log('avg ', avg)
                                if (avg < 10) {
                                    avg = Math.round(avg)/10;
                                } else if (avg < 100) {
                                    avg = Math.round(avg) / 100;
                                } else {
                                    avg = 1.0;
                                }
                                let day = new Date(parseInt(element.t)).getDay();
                                sumArr = []
                                // console.log('avg=========', avg)
                                chartData.push({
                                    "x": parseInt(result.result.data[0].e[index - 14].t),
                                    "x2": parseInt(result.result.data[0].e[index + 1].t),
                                    "y": day,
                                    "partialFill": avg,
                                    "color": '#FDA660'
                                });
                                avg = 0;
                                counter = 1;
                            } else {
                                counter++;
                            }
                        }
                    }); */
                /*     returnData = { 'chartData': chartData, 'categories': categories, 'status': true }
                } else {
                    returnData = { 'chartData': chartData, 'categories': categories, result: result };
                }
                res.send(returnData); */
            });
            // }
        } else {
            responseObj = ResponseFormatter.getErrorObject("invalidParams", req.query);
            res.send(responseObj);
        }
    }


    public getZoneOccupancy(req, res): void {
        BuildingController.BuildingService = BuildingService.getInstance();
        let responseObj = {},
            requestObj = {},
            id = req.query.id,
            type = req.query.type,
            projectId = req.query.projectid,
            starttime = req.query.starttime,
            endtime = req.query.endtime,
            unit = req.query.unit;
        if (id && type && projectId) {
            /* if (cache.getOccupancy) {
                res.send(cache.getOccupancy)
            } else { */
            requestObj = { id, type, projectId, starttime, endtime, unit }
            BuildingController.BuildingService.getOccupancy(requestObj, (result) => {
                // console.log('result ', result)
                let chartData = [];
                let categories = ["0%", "20%", "40%", "60%", "80%", "100%"];
                let returnData = {};
                if (result.status) {

                    let counter = 1;
                    let length = result.result.data[0].e.length - 1;
                    let sumArr = [];
                    result.result.data[0].e.forEach((element, index) => {

                        if (counter <= 15 && ((index + 1) < length)) {

                            /* if (categories.indexOf(weekday[day])===-1) {
                                categories.push(weekday[day]);
                            } */
                            sumArr.push(Math.round(parseFloat(element.v)));
                            if (counter === 15) {

                                let sum = sumArr.reduce(function (a, b) { return a + b; });
                                // console.log('sum ', sum)
                                // console.log('sumArr.length ', sumArr.length)
                                let avg = sum / sumArr.length;
                                // console.log('avg ', avg)
                                if (avg < 10) {
                                    avg = Math.round(avg) / 10;
                                } else if (avg < 100) {
                                    avg = Math.round(avg) / 100;
                                } else {
                                    avg = 1.0;
                                }
                                let day = new Date(parseInt(element.t)).getDay();
                                sumArr = []
                                /* console.log('avg=========', avg)
                                console.log('index ==> ', result.result.data[0].e[index])
                                console.log('index value ==> ', result.result.data[0].e[index].t) */
                                chartData.push({
                                    "x": parseInt(result.result.data[0].e[index - 14].t),
                                    "x2": parseInt(result.result.data[0].e[index + 1].t),
                                    "y": day,
                                    "partialFill": avg,
                                    "color": '#FDA660'
                                });
                                avg = 0;
                                counter = 1;
                            } else {
                                counter++;
                            }
                        }
                    });
                    returnData = { 'chartData': chartData, 'categories': categories, 'status': true }
                } else {
                    returnData = { 'chartData': chartData, 'categories': categories, result: result };
                }
                res.send(returnData);
            });
            // }
        } else {
            responseObj = ResponseFormatter.getErrorObject("invalidParams", req.query);
            res.send(responseObj);
        }
    }


    public getDeviceData(req, res): void {
        BuildingController.BuildingService = BuildingService.getInstance();
        let responseObj = {};
        var id = req.query.id;
        var type = req.query.type;
        var deviceType = req.query.device;
        var projectId = req.query.projectid;
        if (id && type && deviceType && projectId) {
            /* if (cache.getDeviceData) {
                res.send(cache.getDeviceData)
            } else { */
            BuildingController.BuildingService.getDeviceData(projectId, id, type, deviceType, (result) => {
                // cache.getDeviceData = result;
                res.send(result);
            });
            // }
        } else {
            responseObj = ResponseFormatter.getResponseObject(false, "Unable to get Device data", req);
            // cache.getDeviceData = '';
            res.send(responseObj);
        }
    }

    public getPowerUsage(req, res): void {
        BuildingController.BuildingService = BuildingService.getInstance();
        let responseObj = {};
        var pid = req.query.pid;
        var id = req.query.bid;
        var type = req.query.type;
        if (pid && id && type) {
            var reqObj = { pid, id, type }
            BuildingController.BuildingService.getPowerUsage(reqObj, (result) => {
                res.send(result);
            });
        } else {
            responseObj = ResponseFormatter.getResponseObject(false, "Unable to get Power usage data", req);
            res.send(responseObj);
        }
    }
    public getProjectStatus(req, res): void {
        BuildingController.BuildingService = BuildingService.getInstance();
        let responseObj = {};
        var pid = req.query.pid;
        if (pid) {
            var reqObj = {pid}
            BuildingController.BuildingService.getProjectStatus(reqObj, (result) => {
                
                res.send(result);
            });
        } else {
            responseObj = ResponseFormatter.getResponseObject(false, "Unable to get Project status data", req);
            res.send(responseObj);
        }
    }

    public getEnergySavings(req, res): void {
        BuildingController.BuildingService = BuildingService.getInstance();
        let responseObj = {};
        var pid = req.query.projectid;
        var id = req.query.id;
        var type = req.query.type;
        var sTime=req.query.starttime;
        var eTime=req.query.endtime;
        var unit=req.query.unit;
        var querystring=req.query.source;
        let qstring = req.query.qstring;
        if (pid && id && type && sTime && eTime && unit && querystring){
            var starttime=  BuildingController.dateConverter.convertToUTC(sTime);
            var endtime=  BuildingController.dateConverter.convertToUTC(eTime);
            var reqObj = { pid, id, type, starttime, endtime, unit, querystring}
            BuildingController.BuildingService.getEnergySavings(reqObj,(result) => {
                if (result.status && result.result.data[0]['e'].length > 0) {
                    let costConst=0.12;
                try{
                   BuildingController.DataCollectionManager = DataCollectionManager.getInstance();
                   costConst= BuildingController.DataCollectionManager.getChartConfig().costFactorInDollar;
                }catch(e){
                   costConst=0.12
                }
                    let dataValueArray = [],
                        filterdataArray = [],
                        timeArray = [],
                        chartData = [];
                        // console.log('result.result.data[0]', result.result.data[0]['e'])
                    filterdataArray = lodashSortBy.sortBy(result.result.data[0]['e'], ['t']);
                    filterdataArray.forEach(e => {
                        let timeConverted = BuildingController.dateConverter.convertToLocalMili(parseInt(e.t));
                        timeArray.push(timeConverted);
                        let val = parseFloat(e.v);
                        if (qstring === 'costsaving') {
                            // val = parseFloat((val * costConst).toFixed(4));
                            dataValueArray.push(val);
                        } else {
                            dataValueArray.push(parseFloat(e.v));
                        }
                        const temp = [timeConverted, val];
                        chartData.push(temp);
                    });
                    // console.log('chartData ', chartData)
                    let minValue = BuildingController.dateConverter.getMinValue(dataValueArray);
                    let maxValue = BuildingController.dateConverter.getMaxValue(dataValueArray);
                    if (minValue == 0 && maxValue == 0) {
                        minValue = 0
                        maxValue = 1
                    } else if (minValue == maxValue && maxValue > 0) {
                        minValue = 0
                    }
                    result.result.data[0].minValue = minValue;
                    result.result.data[0].maxValue = maxValue;
                    result.result.data[0].minTime = BuildingController.dateConverter.getMinValue(timeArray);
                    result.result.data[0].maxTime = BuildingController.dateConverter.getMaxValue(timeArray);
                    result.result.data[0].chartData = chartData;
                    responseObj = result;
                }

                res.send(responseObj);
            });
        } else {
            responseObj = ResponseFormatter.getResponseObject(false, "Unable to get power sensor data", req);
            res.send(responseObj);
        }
    }

    public getCO2Emmision(req, res): void {
        BuildingController.BuildingService = BuildingService.getInstance();
        let responseObj = {};
        var pid = req.query.pid;
        var bid = req.query.bid;
        if (pid && bid) {
            BuildingController.BuildingService.calculateCO2Emmision(pid, bid, (result) => {
                res.send(result);
            });
        } else {
            responseObj = ResponseFormatter.getResponseObject(false, "Unable to get Power saving data", req);
            res.send(responseObj);
        }
    }

    /**
     *  Method getSensorHistoryData use to get sensor last value
     *  @param projectid project id
     *  @param type type of area of which sensor data require (building, zone, floor)
     *  @param id type id (building id, zone id)
     *  @param starttime start time of sensor data (Format 2018-11-14T09:57:00Z)
     *  @param endtime end time of sensor data (Format 2018-11-14T09:57:00Z)
     * 
     **/
    public getSensorHistoryData(req, res): void {
        BuildingController.BuildingService = BuildingService.getInstance();
        let responseObj = {};
        let projectId = req.query.projectid;
        let sTime = req.query.starttime;
        let eTime = req.query.endtime;
        let id = req.query.id;
        let type = req.query.type;
        let sensor = req.query.sensor;
        let unit = "";
        if(req.query.unit){
            unit=req.query.unit;
        }

        if (projectId && id && type && sensor && sTime && eTime ) {
            var starttime=  BuildingController.dateConverter.convertToUTC(sTime);
           var endtime=  BuildingController.dateConverter.convertToUTC(eTime);
            /* if (cache.getSensorHistoryData) {
                res.send(cache.getSensorHistoryData)
            } else { */
            BuildingController.BuildingService.getSensorHistoryData(projectId, id, type, sensor, starttime, endtime, unit, (result) => {
                // cache.getSensorHistoryData = result;
               // console.log(JSON.stringify(result));
                res.send(result);
            });
            // }
        } else {
            responseObj = ResponseFormatter.getResponseObject(false, "Unable to get sensor data history", req);
            // cache.getSensorHistoryData = '';
            res.send(responseObj);
        }
    } // end getSensorHistoryData method

    public getSensorChartData(req, res): void {
        BuildingController.BuildingService = BuildingService.getInstance();
        let responseObj = {};
        let projectId = req.query.projectid;
        let sTime = req.query.starttime;
        let eTime = req.query.endtime;
        let id = req.query.id;
        let type = req.query.type;
        let sensor = req.query.sensor;
        let unit = req.query.unit;
        let chartType = req.query.chartType;

        if (projectId && id && type && sensor && sTime && eTime && unit && chartType) {
            var starttime=  BuildingController.dateConverter.convertToUTC(sTime);
           var endtime=  BuildingController.dateConverter.convertToUTC(eTime);
            BuildingController.BuildingService.getSensorChartData(projectId, id, type, sensor, starttime, endtime, unit, (result) => {
                if (result.status && result.result.data.length > 0 &&result.result.data[0]['e'].length > 0) {
                    let dataValueArray = [],
                        filterdataArray = [],
                        timeArray = [],
                        chartData = [];
                        let sensorUnitConfig =  BuildingController.Config.getConfig("sensorUnitConfig");
                        if(sensorUnitConfig.temperature == 'Fahrenheit'  && sensor=='TEMP' ){
                            //console.log("Temp is in Fahrenheit.....");
                            filterdataArray = lodashSortBy.sortBy(result.result.data[0]['e'], ['t']);
                                            filterdataArray.forEach(e => {
                                            let timeConverted = BuildingController.dateConverter.convertToLocalMili(parseInt(e.t));
                                            timeArray.push(timeConverted);
                                            let tempval=BuildingController.tempConverter.convertToFahrenheit(e.v);
                                            let val = tempval;
                                            dataValueArray.push(tempval);
                                            const temp = [timeConverted, val];
                                            chartData.push(temp);
                                        });
                                        let minValue = BuildingController.dateConverter.getMinValue(dataValueArray);
                                        let maxValue = BuildingController.dateConverter.getMaxValue(dataValueArray);
                                        if (minValue == 0 && maxValue == 0) {
                                            minValue = 0
                                            maxValue = 1
                                        } else if (minValue == maxValue && maxValue > 0) {
                                            minValue = 0
                                        }
                                        result.result.data[0].minValue = minValue;
                                        result.result.data[0].maxValue = maxValue;
                                        result.result.data[0].minTime = BuildingController.dateConverter.getMinValue(timeArray);
                                        result.result.data[0].maxTime = BuildingController.dateConverter.getMaxValue(timeArray);
                                        result.result.data[0].chartData = chartData;
                                        result.result.data[0].bu ="°F";
                                        responseObj = result;


                        }else{
                                            filterdataArray = lodashSortBy.sortBy(result.result.data[0]['e'], ['t']);
                                            filterdataArray.forEach(e => {
                                            let timeConverted = BuildingController.dateConverter.convertToLocalMili(parseInt(e.t));
                                            timeArray.push(timeConverted);
                                            let val = parseFloat(e.v);
                                            dataValueArray.push(parseFloat(e.v));
                                            const temp = [timeConverted, val];
                                            chartData.push(temp);
                                        });
                                        let minValue = BuildingController.dateConverter.getMinValue(dataValueArray);
                                        let maxValue = BuildingController.dateConverter.getMaxValue(dataValueArray);
                                        if (minValue == 0 && maxValue == 0) {
                                            minValue = 0
                                            maxValue = 1
                                        } else if (minValue == maxValue && maxValue > 0) {
                                            minValue = 0
                                        }
                                        result.result.data[0].minValue = minValue;
                                        result.result.data[0].maxValue = maxValue;
                                        result.result.data[0].minTime = BuildingController.dateConverter.getMinValue(timeArray);
                                        result.result.data[0].maxTime = BuildingController.dateConverter.getMaxValue(timeArray);
                                        result.result.data[0].chartData = chartData;
                                        responseObj = result;
                        }
                    
                }
                res.send(responseObj);
            });
        } else {
            responseObj = ResponseFormatter.getResponseObject(false, "Unable to get sensor data history", req);
            res.send(responseObj);
        }
    }
    public setBrightness(req, res): void {
        BuildingController.BuildingService = BuildingService.getInstance();
        let responseObj = {};
        var id = req.query.id;
        var type = req.query.type;
        var brightnesslevel = req.query.brightness;
        var zonetype = req.query.zonetype;
        var property = req.query.property;
        //    var brightnesslevel  = 10 ;
        var projectId = req.query.projectid;
        if (id && type && brightnesslevel && projectId && zonetype && property) {
            BuildingController.BuildingService.setBrightnessData(projectId, id, type, brightnesslevel, property, zonetype, (result) => {
                res.send(result);
            });
        } else {
            responseObj = ResponseFormatter.getResponseObject(false, "Unable to set Brightness data", req);
            res.send(responseObj);
        }
    }

    public setPaletteColor(req, res): void {
        BuildingController.BuildingService = BuildingService.getInstance();
        let responseObj = {};
        var id = req.query.id;
        var type = req.query.type;
        var zonetype = req.query.zonetype;
        var projectId = req.query.projectid;
        var dataparam = req.query.colorArray;
        if (id && type && projectId && zonetype && dataparam) {
            BuildingController.BuildingService.setPaletteColor(projectId, id, type, zonetype,dataparam, (result) => {
                res.send(result);
            });
        } else {
            responseObj = ResponseFormatter.getResponseObject(false, "Unable to set Palette data", req);
            res.send(responseObj);
        }
    }

    public getHumidexData(req, res): void {
        BuildingController.BuildingService = BuildingService.getInstance();
        let responseObj = {};
        var count=1;
        var id = req.query.id;
        var type = req.query.type;
        var projectId = req.query.projectid;
        var BuildingId = req.query.buildingId;
        let tempUnit;
        let goodMin;
        let goodMax;
        let normalMin;
        let normalMax;
        let notGoodMin;
        let notGoodMax;
        let badMin;
        let badMax;
            let humidexRangeConfig =  BuildingController.Config.getConfig("humidexRangeConfig");
            let sensorUnitConfig =  BuildingController.Config.getConfig("sensorUnitConfig");
            if(sensorUnitConfig.temperature == 'Fahrenheit'){
                goodMin = BuildingController.tempConverter.convertToFahrenheit(humidexRangeConfig.goodMin);
                goodMax = BuildingController.tempConverter.convertToFahrenheit(humidexRangeConfig.goodMax);
                normalMin = BuildingController.tempConverter.convertToFahrenheit(humidexRangeConfig.normalMin);
                normalMax = BuildingController.tempConverter.convertToFahrenheit(humidexRangeConfig.normalMax);
                notGoodMin = BuildingController.tempConverter.convertToFahrenheit(humidexRangeConfig.notGoodMin);
                notGoodMax = BuildingController.tempConverter.convertToFahrenheit(humidexRangeConfig.notGoodMax);
                badMin = BuildingController.tempConverter.convertToFahrenheit(humidexRangeConfig.badMin);
                badMax = BuildingController.tempConverter.convertToFahrenheit(humidexRangeConfig.badMax);
                tempUnit = '\u00b0'+'F';
              }else{
                goodMin = humidexRangeConfig.goodMin;
                goodMax = humidexRangeConfig.goodMax;
                normalMin = humidexRangeConfig.normalMin;
                normalMax = humidexRangeConfig.normalMax;
                notGoodMin = humidexRangeConfig.notGoodMin;
                notGoodMax =humidexRangeConfig.notGoodMax;
                badMin = humidexRangeConfig.badMin;
                badMax =humidexRangeConfig.badMax;
                tempUnit = '\u00b0'+'C';
              }
            //   console.log("humidexRangeConfig",humidexRangeConfig)
        if (id && type && projectId && BuildingId ) {
            let floors = [];
            let value;
            let val;
            BuildingController.BuildingService.getHumidexData(projectId, id, type, (result) => {
                BuildingController.BuildingService.getFloorsByProjectId(projectId, (resultData) => {
                    let floorData = resultData.result.data[0][BuildingId][id];
                    if (Object.keys(result.result.data).length > 0) {
                        result.result.data[0]['e'].forEach(floor => {
                                if(sensorUnitConfig.temperature == 'Fahrenheit'){
                                      value =  BuildingController.tempConverter.convertToFahrenheit(floor.v);
                                }else{
                                      value = floor.v;
                                }
                                // console.log("value" , value)
                                 floorData.zones.forEach(zone =>{
                                    //  console.log("zone.id",zone.id)
                                    if ((floor.v !== null && floor.v != 'N/A') && (floor.h !== null && floor.h != 'N/A') && (floor.t !== null && floor.t != 'N/A' && zone.id == floor.zid)) {
                                       if(value>=goodMin && value <=goodMax){
                                             floors.push({ name: 'z'+count , y: parseFloat(value), z: 10 ,unit: tempUnit, fullName:zone.name,color: humidexRangeConfig.goodColor});
                                             count++;
                                       }else if(value>=normalMin && value <= normalMax){
                                             floors.push({ name: 'z'+count , y:  parseFloat(value), z: 10 ,unit: tempUnit, fullName:zone.name,color: humidexRangeConfig.normalColor});
                                             count++;
                                       }else if(value>=notGoodMin && value <=notGoodMax){
                                             floors.push({ name: 'z'+count , y:  parseFloat(value), z: 10 , unit: tempUnit, fullName:zone.name,color: humidexRangeConfig.notGoodColor});
                                             count++;
                                       }else if(value>=badMin && value <= badMax){
                                            floors.push({ name: 'z'+count , y:  parseFloat(value), z: 10 ,unit: tempUnit, fullName:zone.name,color: humidexRangeConfig.badColor});
                                            count++;
                                       }
                                    }
                                 });
                            
                        });
                        result.result.data[0]['chartData'] = floors;
                    } 
                });
                res.send(result);
            });
        } else {
            responseObj = ResponseFormatter.getResponseObject(false, "Unable to get Humidex data", req);
            res.send(responseObj);
        }
    }

    public getBrightness(req, res): void {
        BuildingController.BuildingService = BuildingService.getInstance();
        let responseObj = {};
        var id = req.query.id;
        var type = req.query.type;
        var zonetype = req.query.zonetype;
        var property = req.query.property;
        //    var brightnesslevel  = 10 ;
        var projectId = req.query.projectid;
        if (id && type && projectId && zonetype && property) {
            BuildingController.BuildingService.getBrightnessData(projectId, id, type, property, zonetype, (result) => {
                res.send(result);
            });
        } else {
            responseObj = ResponseFormatter.getResponseObject(false, "Unable to get Brightness data", req);
            res.send(responseObj);
        }
    }

    public setOnOff(req, res): void {
        BuildingController.BuildingService = BuildingService.getInstance();
        let responseObj = {};
        var id = req.query.id;
        var type = req.query.type;
        var state = req.query.state;
        //   var state  = 'on' ;
        var projectId = req.query.projectid;
        if (id && type && state && projectId) {

            BuildingController.BuildingService.setOnOffData(projectId, id, type, state, (result) => {
                res.send(result);
            });
        } else {
            responseObj = ResponseFormatter.getResponseObject(false, "Unable to set ON/OFF data", req);
            res.send(responseObj);

        }
    }
    public getOnOff(req, res): void {
        BuildingController.BuildingService = BuildingService.getInstance();
        let responseObj = {};
        var id = req.query.id;
        var type = req.query.type;
        //   var state  = 'on' ;
        var projectId = req.query.projectid;
        if (id && type && projectId) {

            BuildingController.BuildingService.getOnOffData(projectId, id, type, (result) => {
                res.send(result);
            });
        } else {
            responseObj = ResponseFormatter.getResponseObject(false, "Unable to get ON/OFF data", req);
            res.send(responseObj);

        }
    }

    public setControl(req, res): void {
        BuildingController.BuildingService = BuildingService.getInstance();
        let responseObj = {};
        var id = req.query.id;
        var type = req.query.type;
        var value = req.query.value;
        var property = req.query.property;
        // var value  = 23 ;
        // var property  = 'immersion' ;
        var projectId = req.query.projectid;
        if (id && type && value && property && projectId) {

            BuildingController.BuildingService.setControlData(projectId, id, type, value, property, (result) => {
                res.send(result);
            });
        } else {
            responseObj = ResponseFormatter.getResponseObject(false, "Unable to set Control data", req);
            res.send(responseObj);

        }
    }
    public setRGB(req, res): void {
        BuildingController.BuildingService = BuildingService.getInstance();
        let responseObj = {};
        var zoneid = req.query.id;
        var red = req.query.red;
        var green = req.query.green;
        var blue = req.query.blue;
        //   var red  = 255 ;
        //    var green  = 23 ;
        //   var blue  = 25 ;
        var projectId = req.query.projectid;
        if (zoneid && red && green && blue && projectId) {

            BuildingController.BuildingService.setRGBData(projectId, zoneid, red, green, blue, (result) => {
                res.send(result);
            });
        } else {
            responseObj = ResponseFormatter.getResponseObject(false, "Unable to set RGB data", req);
            res.send(responseObj);

        }
    }

    public setBlindLevel(req, res): void {
        BuildingController.BuildingService = BuildingService.getInstance();
        let responseObj = {};
        var id = req.query.id;
        var type = req.query.type;
        var projectId = req.query.projectid;
        var level = req.query.level;
        if (id && type && projectId) {
            BuildingController.BuildingService.setBlindLevelData(projectId, id, type, level, (result) => {
                res.send(result);
            });
        } else {
            responseObj = ResponseFormatter.getResponseObject(false, "Unable to set Blind level", req);
            res.send(responseObj);

        }
    }
    public configSensorEnableDisable(req, res): void {

        BuildingController.BuildingService = BuildingService.getInstance();
        let responseObj = {};
        var pid = req.query.pid;
        var id = req.query.id;
        var type = req.query.type;
        var zonetype = req.query.zonetype;
        var property = req.query.property;
        var sensors = req.query.sensors;
        var object = {
            pid, id, type, zonetype, sensors, property
        }
        if (pid && id && type && zonetype && sensors && property) {
            BuildingController.BuildingService.setSensor(object, (result) => {
                res.send(result);
            });
        } else {
            responseObj = ResponseFormatter.getResponseObject(false, "Unable to set Sensor State", req);
            res.send(responseObj);

        }
    }
    public setMood(req, res): void {
        
        BuildingController.BuildingService = BuildingService.getInstance();
        let responseObj = {};
        var pid = req.query.pid;
        var bid = req.query.bid;
        var mood = req.query.mood;
        var fid = req.query.fid;
        var zid = req.query.zid;
        var object = {
            pid, bid, mood, fid, zid
        }
        if (pid && mood) {
            BuildingController.BuildingService.setMood(object, (result) => {
                res.send(result);
            });
        } else {
            responseObj = ResponseFormatter.getResponseObject(false, "Unable to set Mood", req);
            res.send(responseObj);

        }
    }
    public getMood(req, res): void {
        BuildingController.BuildingService = BuildingService.getInstance();
        let responseObj = {};
        var pid = req.query.pid;
        var id = req.query.id;
        var object = {
            pid, id
        }
        if (pid && id) {
            BuildingController.BuildingService.getMood(object, (result) => {
                res.send(result);
            });
        } else {
            responseObj = ResponseFormatter.getResponseObject(false, "Unable to set Mood", req);
            res.send(responseObj);
        }
    }
    public sensorsEnabledDisable(req, res): void {
        BuildingController.BuildingService = BuildingService.getInstance();
        let responseObj = {};
        var pid = req.query.pid;
        var id = req.query.id;
        var type = req.query.type;
        var property = req.query.property;
        var zonetype = req.query.zonetype;
        var object = {
            pid, id, type, property, zonetype
        }
        if (pid && id && type && property && zonetype) {
            BuildingController.BuildingService.getSensorsEnabledDisable(object, (result) => {
                res.send(result);
            });
        } else {
            responseObj = ResponseFormatter.getResponseObject(false, "Unable to set Mood", req);
            res.send(responseObj);
        }
    }
    public setLightScene(req, res): void {
        BuildingController.BuildingService = BuildingService.getInstance();
        let responseObj = {};
        var pid = req.query.pid;
        var id = req.query.id;
        var lightscene = req.query.lightscene;
        var type = req.query.type;
        var object = {
            pid, id, lightscene, type
        }
        if (pid && id && lightscene && type) {
            BuildingController.BuildingService.setLightScene(object, (result) => {
                res.send(result);
            });
        } else {
            responseObj = ResponseFormatter.getResponseObject(false, "Unable to set LightScene", req);
            res.send(responseObj);

        }
    }

    public currentData(req, res): void {
        BuildingController.BuildingService = BuildingService.getInstance();
        
        let responseObj = {};
        let pid = req.query.projectid;
        let type = req.query.type;
        let id = req.query.id;
        let object = {
            pid, type, id
        }
        if (pid && type && id) {
            BuildingController.BuildingService.getCurrentData(object, (result) => {
                res.send(result);
            });
        } else {
            responseObj = ResponseFormatter.getResponseObject(false, "Unable to get current data", req);
            res.send(responseObj);
        }
    }



    public static calculateProjectCost(data: any): any {
        var hour=0;
        if(data.duration == '6M'){
            hour=4380;
        }else if(data.duration == '1Y'){
            hour=8760;
        }else if(data.duration == '2Y'){
            hour=17520;
        }else if(data.duration == '3Y'){
            hour=26280;
        }else if(data.duration == '5Y'){
            hour=43800;
            
        }
        return Math.round(hour*data.perHour*data.cost);
    }


    /**
     * static calculatePercentageIncrease
     */
    public static calculatePercentageIncrease(calculatedCost, totalCost) {
        return (calculatedCost / totalCost) * 100;
    }
    public getProjectedCost(req, res): void {
        BuildingController.BuildingService = BuildingService.getInstance();
        let responseObj = {};
        let pid = req.query.projectid;
        let type = req.query.type;
        let id = req.query.id;
        let object = {
            pid, type, id
        }
        if (pid && type && id) {
            
            BuildingController.BuildingService.getCurrentData(object, (result) => {
                //console.log("result ", result.result.data);
                if (result.status && result.result.data[0]["Energy"].hasOwnProperty('consumption')) {
                  let hoursInMilli = 1122445566;
                  let totalPower = parseFloat(result.result.data[0]["Energy"].consumption.total.value);
                  let hours = result.result.data[0]["Energy"].consumption.total.duration; //moment.duration(hoursInMilli).hours();
                  let costConst=0.12;
                 try{
                    BuildingController.DataCollectionManager = DataCollectionManager.getInstance();
                    costConst= BuildingController.DataCollectionManager.getChartConfig().costFactorInDollar;
                 }catch(e){
                    costConst=0.12
                 }
                  let perHour = totalPower / hours;
                  let resultObj = { sixMonth: { cost: BuildingController.calculateProjectCost(
                        {
                          duration:"6M",
                          perHour: perHour,
                          cost:costConst
                        }
                      ) }, oneYear: { cost: BuildingController.calculateProjectCost(
                        {
                          duration:"1Y",
                          perHour: perHour,
                          cost:costConst
                        }
                      ) }, twoYear: { cost: BuildingController.calculateProjectCost(
                        {
                            duration:"2Y",
                            perHour: perHour,
                            cost:costConst
                        }
                      ) }, threeYear: { cost: BuildingController.calculateProjectCost(
                        {
                            duration:"3Y",
                            perHour: perHour,
                            cost:costConst
                        }
                      ) }, fiveYear: { cost: BuildingController.calculateProjectCost(
                        {
                            duration:"5Y",
                            perHour: perHour,
                            cost:costConst
                        }
                      )
                    }};
                    // console.log('resultObj ', resultObj)
                    res.send(resultObj);
                }
            });
        } else {
            responseObj = ResponseFormatter.getResponseObject(false, "Unable to get current data", req);
            res.send(responseObj);
        }
    }

    /**
    *  Method getPowerConsumption use to get sensor last value
    *  @param projectid project id
    *  @param type type of area of which sensor data require (building, zone, floor)
    *  @param id type id (building id, zone id)
    *  @param starttime start time of sensor data (Format 2018-11-14T09:57:00Z)
    *  @param endtime end time of sensor data (Format 2018-11-14T09:57:00Z)
    *  @param unit unit of time (hours, min)
    * 
    **/
    public getPowerConsumption(req, res): void {
        BuildingController.BuildingService = BuildingService.getInstance();
        let responseObj: any = {};
        let projectId = req.query.projectid;
        let sTime = req.query.starttime;
        let eTime = req.query.endtime;
        let id = req.query.id;
        let type = req.query.type;
        let unit = req.query.unit;
        let qstring = req.query.qstring;
        let chartConfig =  BuildingController.Config.getConfig("chartConfig");
        if (projectId && id && type && sTime && eTime && unit) {
           var starttime=  BuildingController.dateConverter.convertToUTC(sTime);
           var endtime=  BuildingController.dateConverter.convertToUTC(eTime);
        //    console.log(" time starttime ",starttime)
        //    console.log(" time endtime ",endtime)
            //  console.log(" time starttime ",starttime)
            /* if (cache.getSensorHistoryData) {
                res.send(cache.getSensorHistoryData)
            } else { */
            BuildingController.BuildingService.getPowerConsumption(projectId, id, type, starttime, endtime, unit, (result) => {
                // cache.getSensorHistoryData = result;
                let costConst=0.12;
                try{
                   BuildingController.DataCollectionManager = DataCollectionManager.getInstance();
                   costConst= BuildingController.DataCollectionManager.getChartConfig().costFactorInDollar;
                }catch(e){
                   costConst=0.12
                }
                if (result.status && result.result.data[0]['e'].length > 0) {
                    let dataValueArray = [],
                        filterdataArray = [],
                        timeArray = [],
                        chartData = [];
                        // result.result.data[0]['e'].sort((a, b) => a.t - b.t);
                    filterdataArray = lodashSortBy.sortBy(result.result.data[0]['e'], ['t']);
                    filterdataArray.forEach(e => {
                        let timeConverted = BuildingController.dateConverter.convertToLocalMili(parseInt(e.t));
                        timeArray.push(timeConverted);
                        let val = parseFloat(e.v);
                        if (qstring ==='costsaving') {
                            // val = parseFloat((val * costConst).toFixed(4));
                            val =val*(chartConfig.costFactorInDollar);
                            dataValueArray.push(val);
                        } else {
                            dataValueArray.push(parseFloat(e.v));
                        }
                        const temp = [timeConverted, val];
                        // console.log('temp ', temp)
                        chartData.push(temp);
                    });
                    let minValue = BuildingController.dateConverter.getMinValue(dataValueArray);
                    let maxValue = BuildingController.dateConverter.getMaxValue(dataValueArray);
                    if (minValue == 0 && maxValue == 0) {
                        minValue = 0
                        maxValue = 1
                    } else if (minValue == maxValue && maxValue > 0) {
                        minValue = 0
                    }
                    /* console.log('timeArray ', timeArray)
                    console.log('maxTime ', maxTime)
                    console.log('minTime ', minTime) */
                    result.result.data[0].minValue = minValue;
                    result.result.data[0].maxValue = maxValue;
                    result.result.data[0].minTime = BuildingController.dateConverter.getMinValue(timeArray);
                    result.result.data[0].maxTime = BuildingController.dateConverter.getMaxValue(timeArray);
                    result.result.data[0].chartData = chartData;
                    responseObj = result;
                }

                res.send(responseObj);
            });
            // }
        } else {
            responseObj = ResponseFormatter.getResponseObject(false, "Unable to get sensor data history", req);
            // cache.getSensorHistoryData = '';
            res.send(responseObj);
        }
    } // end getPowerConsumption method



    

    /**
       *  Method getZoneSensorData use to get sensor last value
       *  @param projectid project id
       *  @param type type of area of which sensor data require (building, zone, floor)
       *  @param id type id (building id, zone id)
       * 
       **/
    
       public getZoneSensorData(req, res): void {
       BuildingController.BuildingService = BuildingService.getInstance();
        let responseObj = {};
        let projectId = req.query.projectid;
         let id = req.query.id;
        let type = req.query.type;

        if (projectId && id && type) {

             BuildingController.BuildingService.getZoneSensorData(projectId, id, type, (result) => {
                 res.send(result);
            });

         } else {
         responseObj = ResponseFormatter.getResponseObject(false, "Unable to get sensor data history", req);
            res.send(responseObj);
        }
     } // end getZoneSensorData method


    /**
   *  Method getFloorDevicesStat use to get sensor last value
   *  @param projectid project id
   *  @param type type of area of which sensor data require (building, zone, floor)
   *  @param id type id (building id, floor id)
   *  @param device device 
   * 
   **/
    public getFixtureData(req, res): void {
        BuildingController.BuildingService = BuildingService.getInstance();
        let responseObj = {};
        let projectId = req.query.projectid;
        let id = req.query.id;
        let type = req.query.type;
        let device = req.query.device;

        if (projectId && id && type && device) {

            BuildingController.BuildingService.getDevicesStat(projectId, id, type, device, (result) => {
                res.send(result);
            });
        } else {
            responseObj = ResponseFormatter.getResponseObject(false, "Unable to get device data history", req);

            res.send(responseObj);
        }
    } // end getFixtureData method


    
 /**
   *  Method getScenesPalette use to get Light Scenes and RGB values for Palette 
   *  @param projectid project id
   *  @param type type of area of which sensor data require (zone)
   *  @param id type id (building id, floor id, zone id)
   *  @param zonetype zonetype (beacon) 
   * 
   **/
  public getScenesPalette(req, res): void {
    BuildingController.BuildingService = BuildingService.getInstance();
    let responseObj = {};
    let projectid = req.query.projectid;
    let id = req.query.id;
    let type = req.query.type;
    let zonetype = req.query.zonetype;

    
    if (projectid && id && type && zonetype) {

        BuildingController.BuildingService.getScenesPalette(projectid, id, type, zonetype, (result) => {
            res.send(result);
        });
    } else {
        responseObj = ResponseFormatter.getResponseObject(false, "Unable to get device data history", req);

        res.send(responseObj);
    }
} // end getScenesPalette method





    /**
    *  Method getFloorDevicesStat use to get sensor last value
    *  @param projectid project id
    *  @param type type of area of which sensor data require (building, zone, floor)
    *  @param id type id (building id, floor id)
    *  @param device device 
    * 
    **/
    public getFloorDevicesStat(req, res): void {
        BuildingController.BuildingService = BuildingService.getInstance();
        let responseObj = {};
        let projectId = req.query.projectid;
        let floorId = req.query.id;
        let type = 'floor';
        let device = 'all';

        if (projectId && floorId && type && device) {
            /* if (cache.getSensorHistoryData) {
                res.send(cache.getSensorHistoryData)
            } else { */
            var deviceData = {};
            BuildingController.BuildingService.getDevicesStat(projectId, floorId, type, device, (result) => {
                // var totalDevices = { 'fixtures': 0, 'beacons': 0, 'blinds': 0, 'sensors': 0}
                let floorDeviceStat = {
                    'Building': { 'fixtures': 0, 'beacons': 0, 'blinds': 0, 'sensors': 0 },
                    'User': { 'fixtures': 0, 'beacons': 0, 'blinds': 0, 'sensors': 0 },
                    'Information': { 'fixtures': 0, 'beacons': 0, 'blinds': 0, 'sensors': 0 },
                    'General': { 'fixtures': 0, 'beacons': 0, 'blinds': 0, 'sensors': 0 },
                    'Shading': { 'fixtures': 0, 'beacons': 0, 'blinds': 0, 'sensors': 0 }
                };
                //console.log('result status ', result.status)
                if (result.status) {
                    deviceData = result.result.data[0];
                    /* totalDevices.fixtures = result.result.data[0]['fixtures'].length;
                    totalDevices.beacons = result.result.data[0]['beacons'].length;
                    totalDevices.blinds = result.result.data[0]['blinds'].length;
                    totalDevices.sensors = result.result.data[0]['sensors'].length; */
                    BuildingController.BuildingService.getProject(req, (result) => {
                        // console.log("project id ", projectId);
                        // console.log('result status ', result.status)
                        if (result.status) {
                            // console.log("result.result.data[0][floors] ", result.result.data[0]["floors"]);
                            let projectData = BuildingController.getProjectById(result.result.data, projectId);
                            // console.log("projectData ", projectData);
                            let floor = BuildingController.getFloorById(projectData[0]["floors"], floorId);
                            // console.log("floorId ", floorId);
                            // console.log("floor ", floor);
                            // res.send(result);


                            let floorZones = { 'Building': [], 'User': [], 'Information': [], 'General': [], 'Shading': [] };
                            floor[0]['zones'].filter(function (zone) {
                               
                                if (Array.isArray(floorZones[zone.layer])) {
                                    floorZones[zone.layer].push(zone.id);
                                }

                            });
                            // let devices = { 'fixtures': 0, 'beacons': 0, 'blinds': 0, 'sensors': 0};
                            

                            Object.keys(deviceData).filter(function (floor) {
                                //  console.log('floor ', floor)
                                deviceData[floor].forEach(flr => {
                                     //console.log('flr=====', flr);
                                    Object.keys(floorZones).forEach(function (zones) {
                                        if (floorZones[zones].includes(flr.zoneID)) {
                                            floorDeviceStat[zones][floor]++;
                                        }
                                    })


                                });

                            });
                             //console.log('floorDeviceStat ', floorDeviceStat)
                            result.result.data = floorDeviceStat
                            res.send(result);
                        }

                    });

                } else {
                    responseObj = ResponseFormatter.getResponseObject(false, "Unable to get device data history", {});
                    // cache.getSensorHistoryData = '';
                    res.send(responseObj);
                }

            });


            // }
        } else {
            responseObj = ResponseFormatter.getResponseObject(false, "Unable to get device data history", req);
            // cache.getSensorHistoryData = '';
            res.send(responseObj);
        }
    } // end getFloorDevicesStat method



    public static getFloorById(floorArray, floorId): any {
        return floorArray.filter(function (floor) {
            return floor.id === floorId;
        });
    }

    public static getProjectById(projectArray, projectId): any {
        return projectArray.filter(function (project) {
            return project.id === projectId;
        });
    }


    public getAllProjects(req, res): void {
        BuildingController.BuildingService = BuildingService.getInstance();
        BuildingController.BuildingService.getAllProjects((result) => {
        res.send(result);
        });
    }


    public getFloorPlan(req, res): void {
        BuildingController.BuildingService = BuildingService.getInstance();
        let responseObj = {};
        let pid = req.query.projectid;
        let id = req.query.id;
        let object = {
            pid, id
        }
        BuildingController.BuildingService.getFloorPlan(object, (result, mime) => {
            var img = result;
// console.log('got the mime',mime);
            res.writeHead(200, {'Content-Type': mime });
            res.end(img, 'binary');
            // console.log('got the floor plan', result);
        });
    }

    public getVersionNumber(req, res): void {
            let applicationConfig =  BuildingController.Config.getConfig("applicationConfig");
            res.send(applicationConfig);
    }  
    //DMS ping API
    public pingDMS(req, res): void {
        BuildingController.BuildingService = BuildingService.getInstance();
            BuildingController.BuildingService.pingDMS(req, (result) => {
                res.send(result);
            });
    }

    //DMS Firmware List
    public getFirmwareList(req, res): void {
        BuildingController.BuildingService = BuildingService.getInstance();
            BuildingController.BuildingService.getFirmwareList(req, (result) => {
                res.send(result);
            });
    }

    //DMS file upload
    public uploadFirmwareFile(req, res): void {
        const gateway = req.body.gateway;
        const file = req.file;
        BuildingController.BuildingService = BuildingService.getInstance();
            BuildingController.BuildingService.uploadFirmwareFile(gateway,file, (result) => {
                res.send(result);
         });   
    }

     //DMS firmware Update
     public updateFirmware(req, res): void {
        let responseObj = {};
        var body =req.body;
        BuildingController.BuildingService = BuildingService.getInstance();

        if (body) {
            BuildingController.BuildingService.updateFirmware(body, (result) => {
                res.send(result);
            });
        } else {
            responseObj = ResponseFormatter.getResponseObject(false, "Unable to update firmware", req);
           res.send(responseObj);
        }     
    }

    public DMSdeviceList(req, res): void {
        BuildingController.BuildingService = BuildingService.getInstance();
            BuildingController.BuildingService.getDeviceList(req, (result) => {
                res.send(result);
            });
    }
    public getVersionList(req, res): void {
        BuildingController.BuildingService = BuildingService.getInstance();
            BuildingController.BuildingService.getVersionList(req, (result) => {
                res.send(result);
            });
    }
    public getFirmwareStatus(req, res): void {
        BuildingController.BuildingService = BuildingService.getInstance();
            BuildingController.BuildingService.getFirmwareStatus(req, (result) => {
                res.send(result);
            });
    }
}
