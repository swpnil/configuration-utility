import {ApplicationController} from "./ApplicationController";
import {AuditLogController} from "./AuditLogController";
import {AuthenticationController} from "./AuthenticationController";
import {UserManagementController} from "./UserManagementController";
import {WebPortalConfigurationController} from "./WebPortalConfigurationController";
import {BuildingController} from "./BuildingController";
import {BacNetDataController} from "./BacNetDataController";

export class Controllers {
    public UserManagementController: UserManagementController;
    public AuthenticationController: AuthenticationController;
    public WebPortalConfigurationController: WebPortalConfigurationController;
    public AuditLogController: AuditLogController;
    public ApplicationController: ApplicationController;
    public BuildingController :BuildingController;
    public BacNetDataController :BacNetDataController;
    constructor() {
        // todo: move this to function
        this.UserManagementController = new UserManagementController();
        this.AuthenticationController = new AuthenticationController();
        this.WebPortalConfigurationController = new WebPortalConfigurationController();
        this.AuditLogController = new AuditLogController();
        this.ApplicationController = new ApplicationController();
        this.BuildingController = new BuildingController();
        this.BacNetDataController = new BacNetDataController();
    }
}

// export = new Controllers();
