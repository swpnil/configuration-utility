// import {AuditLogger} from "../auditLogger";
import {CommunicationManager} from "../lib/communicationManager";
import {Config} from "../config";
import ResponseFormatter from "../lib/helpers/ResponseFormatter";
import Logger from "../lib/logger/Logger";
import {MailingService} from "../lib/mailingService";
import {CreateEmailConfigModel} from "../models/databaseModels/CreateEmailConfigModel";
import {GetEmailConfigurationResModel} from "../models/responseModels/GetEmailConfigurationResModel";
import ProcessManager from "../lib/processManager";
import Validator from "../validator";

export class WebPortalConfigurationController {
    private static CommunicationManager: CommunicationManager;
    // private static AuditLogger: AuditLogger;
    private static MailingService: MailingService;
    private static Config: Config;

    constructor() {
        WebPortalConfigurationController.CommunicationManager = CommunicationManager.getInstance();
        // WebPortalConfigurationController.AuditLogger = AuditLogger.getInstance();
        WebPortalConfigurationController.Config = Config.getInstance();
        WebPortalConfigurationController.MailingService = MailingService.getInstance();
    }

    public addEmailConfiguration(req, res): void {
        let responseObj = {};
        if (!res.locals.err) {
            const reqParamsRes = Validator.WebPortalConfigurationValidator.validateAddEmailConfigurationRequest(req);
            const reqParams = reqParamsRes.result;
            if (reqParamsRes.isSuccess) {
                const createEmailConfigObj = new CreateEmailConfigModel(reqParams.email, reqParams.password, reqParams.host, reqParams.port, reqParams.issecure, reqParams.sendername);
                WebPortalConfigurationController.CommunicationManager.DbManager.dbQuery.execProc("sp_CreateEmailConfig", (err, result, fields) => {
                    if (err) {
                        Logger.log("error", err.message);
                        const errorObj = ResponseFormatter.getErrorObject("failedDbQuery", {});
                        // WebPortalConfigurationController.AuditLogger.log(res.locals.response.username, "application", "Add email configuration", "Error adding email configuration" + JSON.stringify(reqParams), "error");
                        responseObj = ResponseFormatter.getResponseObject(false, "Unable to set email configuration", errorObj);
                        res.send(responseObj);
                    } else {
                        const resultSet = result[0];
                        // WebPortalConfigurationController.AuditLogger.log(res.locals.response.username, "application", "Add email configuration", "Email configuration added successfully" + JSON.stringify(reqParams), "information");
                        WebPortalConfigurationController.MailingService.updateEmailConfig(reqParams.email, reqParams.sendername, reqParams.password, reqParams.host, reqParams.port, reqParams.issecure);
                        responseObj = ResponseFormatter.getResponseObject(true, "Email configuration set successfully", []);
                        res.send(responseObj);
                    }
                }, createEmailConfigObj.data);
            } else {
                responseObj = ResponseFormatter.getResponseObject(false, "Unable to set email configuration", reqParams);
                res.send(responseObj);
            }
        } else {
            responseObj = ResponseFormatter.getResponseObject(false, "Unable to set email configurationr", res.locals.response);
            res.status(401).send(responseObj);
        }
    }

     public getEmailConfiguration(req, res): void {
        let responseObj = {};
        if (!res.locals.err) {
            WebPortalConfigurationController.CommunicationManager.DbManager.dbQuery.execProc("sp_ReadAllEmailConfig", (err, result, fields) => {
                if (err) {
                    Logger.log("error", err.message);
                    const errorObj = ResponseFormatter.getErrorObject("failedDbQuery", {});
                    responseObj = ResponseFormatter.getResponseObject(false, "Unable to receive email configuration", errorObj);
                    res.send(responseObj);
                } else {
                    if (Array.isArray(result) && result[0].length > 0) {
                        const resultSet = result[0];
                        const emailConfigResObj = new GetEmailConfigurationResModel(resultSet);
                        responseObj = ResponseFormatter.getResponseObject(true, "Email configuration received successfully", emailConfigResObj);
                        res.send(responseObj);
                    } else {
                        const errorObj = ResponseFormatter.getErrorObject("noResponse", {});
                        responseObj = ResponseFormatter.getResponseObject(false, "Unable to receive email configuration", errorObj);
                        res.send(responseObj);
                    }
                }
            });
        } else {
            responseObj = ResponseFormatter.getResponseObject(false, "Unable to receive email configuration", res.locals.response);
            res.status(401).send(responseObj);
        }
    }

    // public setSystemTime(req, res): void {
    //     let responseObj = {};
    //     if (!res.locals.err) {
    //         const reqParamsRes = Validator.WebPortalConfigurationValidator.validatSetSystemtimeRequest(req);
    //         const reqParams = reqParamsRes.result;
    //         if (reqParamsRes.isSuccess) {
    //             ProcessManager.executeScript("setSystemTime", (error, stdout, stderr) => {
    //                 if (error !== null) {
    //                     Logger.log("error", error.message);
    //                     // WebPortalConfigurationController.AuditLogger.log(res.locals.response.username, "system", "Set system time", "Error setting system time: " + JSON.stringify(reqParams), "error");
    //                     const errorObj = ResponseFormatter.getErrorObject("scriptError", reqParams);
    //                     responseObj = ResponseFormatter.getResponseObject(false, "Unable to set system time", errorObj);
    //                     res.send(responseObj);
    //                 } else {
    //                     // WebPortalConfigurationController.AuditLogger.log(res.locals.response.username, "system", "Set system time", "System time set successfully:  " + JSON.stringify(reqParams), "information");
    //                     Logger.log("info", "Script Executed: " + stdout);
    //                     responseObj = ResponseFormatter.getResponseObject(true, "System time set successfully", []);
    //                     res.send(responseObj);
    //                 }
    //             }, reqParams.systemtime);
    //         } else {
    //             responseObj = ResponseFormatter.getResponseObject(false, "Unable to set system time", reqParams);
    //             res.send(responseObj);
    //         }
    //     } else {
    //         responseObj = ResponseFormatter.getResponseObject(false, "Unable to set system time", res.locals.response);
    //         res.status(401).send(responseObj);
    //     }
    // }
}
