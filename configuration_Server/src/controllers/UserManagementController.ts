// import {AuditLogger} from "../auditLogger";
import {AuthenticationService} from "../lib/AuthenticationService";
import {CommunicationManager} from "../lib/communicationManager";
import {FileHelper} from "../lib/helpers/FileHelper";
import ResponseFormatter from "../lib/helpers/ResponseFormatter";
import Logger from "../lib/logger/Logger";
import {MailingService} from "../lib/mailingService";
import {CreateUserModel} from "../models/databaseModels/CreateUserModel";
import {DeleteUserByIdModel} from "../models/databaseModels/DeleteUserByIdModel";
import {ReadUserByIdModel} from "../models/databaseModels/ReadUserByIdModel";
import {UpdatePasswordByIdModel} from "../models/databaseModels/UpdatePasswordByIdModel";
import {UpdateUserByIdModel} from "../models/databaseModels/UpdateUserByIdModel";
import {AddUserResModel} from "../models/responseModels/AddUserResModel";
import {GetAllUsersResModel} from "../models/responseModels/GetAllUsersResModel";
import {GetUserProfileResModel} from "../models/responseModels/GetUserProfileResModel";
import {UpdatePasswordResModel} from "../models/responseModels/UpdatePasswordResModel";
import Validator from "../validator";

export class UserManagementController {
    // this object is undefined hence using static private members
    private static CommunicationManager: CommunicationManager;
    private static fileHelper: FileHelper;
    private static MailingService: MailingService;
    // private static AuditLogger: AuditLogger;
    private static AuthenticationService: AuthenticationService;

    constructor() {
        UserManagementController.AuthenticationService = AuthenticationService.getInstance();
        UserManagementController.CommunicationManager = CommunicationManager.getInstance();
        // UserManagementController.AuditLogger = AuditLogger.getInstance();
        UserManagementController.fileHelper = new FileHelper();
        UserManagementController.MailingService = MailingService.getInstance();
    }

    public getUserProfile(req, res): void {
        let responseObj = {};
        if (!res.locals.err) {
            const readUserByIdObj = new ReadUserByIdModel([res.locals.response.userId]);
            UserManagementController.CommunicationManager.DbManager.dbQuery.execProc("sp_ReadUserById", (err, result, fields) => {
                if (err) {
                    Logger.log("error", err.message);
                    const errorObj = ResponseFormatter.getErrorObject("failedDbQuery", res.locals);
                    responseObj = ResponseFormatter.getResponseObject(false, "Unable to receive user profile", errorObj);
                    res.send(responseObj);
                } else {
                    if (Array.isArray(result) && result[0].length > 0) { // user exists
                        const resultSet = result[0];
                        const userProfileApiRes = new GetUserProfileResModel(resultSet[0]);
                        responseObj = ResponseFormatter.getResponseObject(true, "User profile successfully received", userProfileApiRes);
                        res.send(responseObj);
                    } else {
                        const errorObj = ResponseFormatter.getErrorObject("userNotFound", res.locals);
                        responseObj = ResponseFormatter.getResponseObject(false, "Unable to receive user profile", errorObj);
                        res.send(responseObj);
                    }
                }
            }, readUserByIdObj.data);
        } else {
            responseObj = ResponseFormatter.getResponseObject(false, "Unable to receive user profile", res.locals.response);
            res.send(responseObj);
        }
    }

    public getAllUsers(req, res): void {
        let responseObj = {};
        if (!res.locals.err) {
            UserManagementController.CommunicationManager.DbManager.dbQuery.execProc("sp_ReadAllUser", (err, result, fields) => {
                if (err) {
                    Logger.log("error", err.message);
                    const errorObj = ResponseFormatter.getErrorObject("failedDbQuery", {});
                    responseObj = ResponseFormatter.getResponseObject(false, "Unable to receive all users", errorObj);
                    res.send(responseObj);
                } else {
                    if (Array.isArray(result) && result[0].length > 0) { // user exists
                        const resultSet = result[0];
                        const allUsersResObj = new GetAllUsersResModel(resultSet);
                        responseObj = ResponseFormatter.getResponseObject(true, "All users received successfully", allUsersResObj.data);
                        res.send(responseObj);
                    } else {
                        const errorObj = ResponseFormatter.getErrorObject("userNotFound", {});
                        responseObj = ResponseFormatter.getResponseObject(false, "Unable to receive all users", errorObj);
                        res.send(responseObj);
                    }
                }
            });
        } else {
            responseObj = ResponseFormatter.getResponseObject(false, "Unable to receive all users", res.locals.response);
            res.status(401).send(responseObj);
        }
    }

    public addUser(req, res): void {
        let responseObj = {};
        if (!res.locals.err) {
            const reqParamsRes = Validator.UserManagementValidator.validateAddUserRequest(req);
            const reqParams = reqParamsRes.result;
            if (reqParamsRes.isSuccess) {
                const createUserObj = new CreateUserModel(reqParams.username, reqParams.password, reqParams.role, reqParams.name, reqParams.phone, reqParams.address);
                UserManagementController.CommunicationManager.DbManager.dbQuery.execProc("sp_CreateUser", (err, result, fields) => {
                    if (err) {
                        Logger.log("error", err.message);
                        // UserManagementController.AuditLogger.log(res.locals.response.username, "application", "Add user", "Error adding new user" + JSON.stringify(reqParams), "error");
                        const errorObj = ResponseFormatter.getErrorObject("failedDbQuery", {});
                        responseObj = ResponseFormatter.getResponseObject(false, "Unable to add user", errorObj);
                        res.send(responseObj);
                    } else {
                        const resultSet = result[0];
                        const addUserResObj = new AddUserResModel(reqParams, resultSet[0]);
                        if (resultSet[0]._returnValue === "0") {
                            const errorObj = ResponseFormatter.getErrorObject("userExists", addUserResObj);
                            responseObj = ResponseFormatter.getResponseObject(false, "Unable to add user", errorObj);
                            res.send(responseObj);
                        } else {
                            // UserManagementController.AuditLogger.log(res.locals.response.username, "application", "Add user", "User added successfully" + JSON.stringify(reqParams), "information");

                            UserManagementController.MailingService.sendMail(res.locals.response.username, reqParams.username, "userCreated", {username: reqParams.username, password: reqParams.password}, (sendEmailRes) => {
                                if (sendEmailRes.isSuccess) {
                                    responseObj = ResponseFormatter.getResponseObject(true, "User added successfully", addUserResObj);
                                    res.send(responseObj);
                                } else {
                                    const errorObj = ResponseFormatter.getErrorObject("emailError", addUserResObj);
                                    responseObj = ResponseFormatter.getResponseObject(false, "Unable to add user", errorObj);
                                    res.send(responseObj);
                                }
                            });
                        }
                    }
                }, createUserObj.data, ["@returnValue"]);
            } else {
                responseObj = ResponseFormatter.getResponseObject(false, "Unable to add user", reqParams);
                res.send(responseObj);
            }
        } else {
            responseObj = ResponseFormatter.getResponseObject(false, "Unable to add user", res.locals.response);
            res.status(401).send(responseObj);
        }
    }

    public editUser(req, res): void {
        let responseObj = {};
        if (!res.locals.err) {
            const reqParamsRes = Validator.UserManagementValidator.validateUpdateUserRequest(req, res.locals.response);
            const reqParams = reqParamsRes.result;
            if (reqParamsRes.isSuccess) {
                const updateUserObj = new UpdateUserByIdModel(reqParams.userid, reqParams.role, reqParams.name, reqParams.phone, reqParams.address);
                UserManagementController.CommunicationManager.DbManager.dbQuery.execProc("sp_UpdateUserById", (err, result, fields) => {
                    if (err) {
                        // UserManagementController.AuditLogger.log(res.locals.response.username, "application", "Edit user", "Error editting user" + JSON.stringify(reqParams), "error");
                        Logger.log("error", err.message);
                        const errorObj = ResponseFormatter.getErrorObject("failedDbQuery", reqParams);
                        responseObj = ResponseFormatter.getResponseObject(false, "Unable to update user details", errorObj);
                        res.send(responseObj);
                    } else {
                        if (Array.isArray(result) && result[0].length >= 0) {
                            const resultSet = result[0];
                            if (resultSet[0]._returnValue === 0) {
                                const errorObj = ResponseFormatter.getErrorObject("userNotFound", reqParams);
                                responseObj = ResponseFormatter.getResponseObject(false, "Unable to update user details", errorObj);
                                res.send(responseObj);
                            } else if (resultSet[0]._returnValue === 1) {
                                const errorObj = ResponseFormatter.getErrorObject("minOneAdmin", reqParams);
                                responseObj = ResponseFormatter.getResponseObject(false, "Unable to update user details", errorObj);
                                res.send(responseObj);
                            } else {
                                responseObj = ResponseFormatter.getResponseObject(true, "User details updated successfully", []);
                                // UserManagementController.AuditLogger.log(res.locals.response.username, "application", "Edit user", "User edited successfully" + JSON.stringify(reqParams), "information");
                                res.send(responseObj);
                            }
                        } else {
                            // UserManagementController.AuditLogger.log(res.locals.response.username, "application", "Edit user", "Error editting user" + JSON.stringify(reqParams), "error");
                            const errorObj = ResponseFormatter.getErrorObject("noResponse", reqParams);
                            Logger.log("error", errorObj);
                            responseObj = ResponseFormatter.getResponseObject(false, "Unable to update user details", errorObj);
                            res.send(responseObj);
                        }
                    }
                }, updateUserObj.data, ["@returnValue"]);
            } else {
                responseObj = ResponseFormatter.getResponseObject(false, "Unable to update user details", reqParams);
                res.send(responseObj);
            }
        } else {
            responseObj = ResponseFormatter.getResponseObject(false, "Unable to update user details", res.locals.response);
            res.status(401).send(responseObj);
        }
    }

    public deleteUser(req, res): void {
        let responseObj = {};
        if (!res.locals.err) {
            const reqParamsRes = Validator.UserManagementValidator.validateDeleteUserRequest(req);
            const reqParams = reqParamsRes.result;
            if (reqParamsRes.isSuccess) {
                const deleteUserObj = new DeleteUserByIdModel(reqParams.userid);
                UserManagementController.CommunicationManager.DbManager.dbQuery.execProc("sp_DeleteUserById", (err, result, fields) => {
                    if (err) {
                        // UserManagementController.AuditLogger.log(res.locals.response.username, "application", "Delete user", "Error deleting user" + JSON.stringify(reqParams), "error");
                        Logger.log("error", err.message);
                        const errorObj = ResponseFormatter.getErrorObject("failedDbQuery", {});
                        responseObj = ResponseFormatter.getResponseObject(false, "Unable to delete user", errorObj);
                        res.send(responseObj);
                    } else {
                        if (Array.isArray(result) && result[0].length > 0) { // user exists
                            const resultSet = result[0];
                            if (resultSet[0]._returnValue === 0) {
                                // user not found
                                const errorObj = ResponseFormatter.getErrorObject("userNotFound", reqParams);
                                responseObj = ResponseFormatter.getResponseObject(false, "Unable to delete user", errorObj);
                                // UserManagementController.AuditLogger.log(res.locals.response.username, "application", "Delete user", "Error deleting user" + JSON.stringify(reqParams), "error");
                                res.send(responseObj);
                            } else if (resultSet[0]._returnValue === 1) {
                                // minimum one admin
                                const errorObj = ResponseFormatter.getErrorObject("minOneAdmin", reqParams);
                                responseObj = ResponseFormatter.getResponseObject(false, "Unable to delete user", errorObj);
                                // UserManagementController.AuditLogger.log(res.locals.response.username, "application", "Delete user", "Error deleting user" + JSON.stringify(reqParams), "error");
                                res.send(responseObj);
                            } else {
                                // UserManagementController.AuditLogger.log(res.locals.username, "application", "Delete user", "User deleted successfully" + JSON.stringify(reqParams), "information");
                                responseObj = ResponseFormatter.getResponseObject(true, "User deleted successfully", []);
                                res.send(responseObj);
                            }
                        } else {
                            const errorObj = ResponseFormatter.getErrorObject("noResponse", reqParams);
                            responseObj = ResponseFormatter.getResponseObject(false, "Unable to delete user", errorObj);
                            // UserManagementController.AuditLogger.log(res.locals.response.username, "application", "Delete user", "Error deleting user" + JSON.stringify(reqParams), "error");
                            res.send(responseObj);
                        }
                    }
                }, deleteUserObj.data, ["@returnValue"]);
            } else {
                responseObj = ResponseFormatter.getResponseObject(false, "Unable to delete user", reqParams);
                res.send(responseObj);
            }
        } else {
            responseObj = ResponseFormatter.getResponseObject(false, "Unable to delete user", res.locals.response);
            res.status(401).send(responseObj);
        }
    }

    public updatePasword(req, res): void {
        let responseObj = {};
        if (!res.locals.err) {
            const reqParamsRes = Validator.UserManagementValidator.validateUpdatePasswordRequest(req, res.locals.response);
            const reqParams = reqParamsRes.result;
            if (reqParamsRes.isSuccess) {
                const updatePasswordObj = new UpdatePasswordByIdModel(reqParams.userid, reqParams.currpassword, reqParams.newpassword);
                UserManagementController.CommunicationManager.DbManager.dbQuery.execProc("sp_UpdatePasswordById", (err, result, fields) => {
                    if (err) {
                        Logger.log("error", err.message);
                        const errorObj = ResponseFormatter.getErrorObject("failedDbQuery", res.locals);
                        responseObj = ResponseFormatter.getResponseObject(false, "Unable to update password", errorObj);
                        res.send(responseObj);
                    } else {
                        const updatePassResObj = new UpdatePasswordResModel(reqParams);
                        if (Array.isArray(result) && result[0].length > 0) {
                            const resultSet = result[0];
                            if (resultSet[0]._returnValue === 0) {
                                const errorObj = ResponseFormatter.getErrorObject("userNotFound", updatePassResObj);
                                responseObj = ResponseFormatter.getResponseObject(false, "Unable to update password", errorObj);
                                // UserManagementController.AuditLogger.log(res.locals.response.username, "application", "Update password", "Unable to update password" + JSON.stringify(reqParams), "error");
                                res.send(responseObj);
                            } else if (resultSet[0]._returnValue === 1) {
                                const errorObj = ResponseFormatter.getErrorObject("invalidPassword", updatePassResObj);
                                responseObj = ResponseFormatter.getResponseObject(false, "Unable to update password", errorObj);
                                // UserManagementController.AuditLogger.log(res.locals.response.username, "application", "Update password", "Unable to update password" + JSON.stringify(reqParams), "error");
                                res.send(responseObj);
                            } else {
                                const newToken = UserManagementController.AuthenticationService.refreshToken(res.locals.response.userId, reqParams.userid, res.locals.response.username);
                                updatePassResObj.updateToken(newToken);
                                // UserManagementController.AuditLogger.log(res.locals.response.username, "application", "Update password", "Password updated successfully" + JSON.stringify(reqParams), "information");
                                responseObj = ResponseFormatter.getResponseObject(true, "Password updated successfully", updatePassResObj);
                                res.send(responseObj);
                            }
                        } else {
                            // UserManagementController.AuditLogger.log(res.locals.response.username, "application", "Update password", "Unable to update password" + JSON.stringify(reqParams), "error");
                            const errorObj = ResponseFormatter.getErrorObject("noResponse", updatePassResObj);
                            responseObj = ResponseFormatter.getResponseObject(false, "Unable to update password", errorObj);
                            res.send(responseObj);
                        }
                    }
                }, updatePasswordObj.data, ["@returnValue"]);
            } else {
                responseObj = ResponseFormatter.getResponseObject(false, "Unable to update password", reqParams);
                res.send(responseObj);
            }
        } else {
            responseObj = ResponseFormatter.getResponseObject(false, "Unable to update password", res.locals.response);
            res.send(responseObj);
        }
    }

    public resetPassword(req, res): void {
        let responseObj = {};
        if (!res.locals.err) {
            const reqParamsRes = Validator.UserManagementValidator.validateResetPasswordRequest(req);
            const reqParams = reqParamsRes.result;
            if (reqParamsRes.isSuccess) {
                UserManagementController.AuthenticationService.validateForgotPasswordRequest(reqParams, (response) => {
                    responseObj = ResponseFormatter.getResponseObject(response.isSuccess, (response.isSuccess) ? ("Password reset successfully") : ("Error resetting password"), (response.isSuccess) ? ([]) : (response.result));
                    res.send(responseObj);
                }, res.locals.response.username);
            } else {
                responseObj = ResponseFormatter.getResponseObject(false, "Unable to reset password", reqParams);
                res.send(responseObj);
            }
        } else {
            responseObj = ResponseFormatter.getResponseObject(false, "Unable to reset password", res.locals.response);
            res.send(responseObj);
        }
    }
}
