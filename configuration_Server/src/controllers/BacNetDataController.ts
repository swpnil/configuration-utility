import { ApplicationController } from './ApplicationController';
import { BacNetDataService } from "../services/BacNetDataService";
import { Config } from '../config';
var fs = require("fs");
import { Collections } from "../lib/collections";
import { GetEmailConfigurationResModel } from '../models/responseModels/GetEmailConfigurationResModel';
export class BacNetDataController {

    private static BacNetDataService: BacNetDataService;
    private static appController : ApplicationController;
    private static Config: Config;
    constructor() {
        // constructor code will come here
        BacNetDataController.Config = Config.getInstance();
    }

    /**
     *  Method getSensorLiveData use to get sensor last value
     *  @param projectid project id
     *  @param type type of area of which sensor data require (building, zone, floor)
     *  @param id type id (building id, zone id)
     *  @param starttime start time of sensor data (Format 2018-11-14T09:57:00Z)
     *  @param endtime end time of sensor data (Format 2018-11-14T09:57:00Z)
     *  @param unit unit of time (hours, min)
     * 
     **/
 public setAPIPortData(req,res): void {
     for(let key in req.body)
     {
        let obj = JSON.parse(key);
         var init = {
            ip: obj.ip,
            port: obj.port,
            setModuleName :obj.setModuleName
         }
         console.log("INITTTTTT",init)
     }
    let collection = Collections.getInstance();
    collection.APIConfiguration.setAPI(init)
    BacNetDataController.BacNetDataService = BacNetDataService.getInstance();
    // param require
    BacNetDataController.BacNetDataService.setAPIPortData(req, (result) => {

        //write code for any
        
        res.send(result);
    });
 }

 public checkBacNetData(req,res): void {
    for(let key in req.body)
    {
       let obj = JSON.parse(key);
        var init = {
           ip: obj.ip,
           port: obj.port,
           setModuleName :obj.setModuleName
        }
    }
   let collection = Collections.getInstance();
   collection.APIConfiguration.setAPI(init)
   BacNetDataController.BacNetDataService = BacNetDataService.getInstance();
   // param require
   BacNetDataController.BacNetDataService.checkBacNetData(req, (result) => {

       //write code for any
       
       res.send(result);
   });
}

public checkDashboardData(req,res): void {
    for(let key in req.body)
    {
    //    let obj = JSON.parse(key);
       let obj = JSON.parse(key);
        var init = {
           ip: obj.ip,
           port: obj.port,
           setModuleName :obj.setModuleName
        }
    }
   let collection = Collections.getInstance();
   collection.APIConfiguration.setAPI(init)
   BacNetDataController.BacNetDataService = BacNetDataService.getInstance();
   // param require
   BacNetDataController.BacNetDataService.checkDashboardData(req, (result) => {

       //write code for any
       
       res.send(result);
   });
}
    public saveDashboardConfigData(req, res): void {
        BacNetDataController.BacNetDataService = BacNetDataService.getInstance();
        // param require
        BacNetDataController.BacNetDataService.saveDashboardConfigData(req.body, (result) => {
            res.send(result);
        });
    }
    public getDashboardConfigData(req, res): void {
        BacNetDataController.BacNetDataService = BacNetDataService.getInstance();
        // param require
        BacNetDataController.BacNetDataService.getDashboardConfigData(req, (result) => {
            console.log("+++++++++++++++++++++++++++++++",result)
            res.send(result);
        });
    }
 
    public saveBacNetConfiguration(req, res): void {
        BacNetDataController.BacNetDataService = BacNetDataService.getInstance();

        BacNetDataController.BacNetDataService.saveBacNetConfiguration(req.body, (result) => {
            res.send(result);
        });
    } 
    public getBacNetData(req, res): void {
        BacNetDataController.BacNetDataService = BacNetDataService.getInstance();
        // param require
        BacNetDataController.BacNetDataService.getBacNetData(req, (result) => {
            res.send(result);
        });
    }
    public getGWServiceStatus(req, res): void {
        BacNetDataController.BacNetDataService = BacNetDataService.getInstance();
        // param require
        BacNetDataController.BacNetDataService.getGWServiceStatus(req, (result) => { 
            res.send(result);
        });
    }
    public downloadObjectMapping(req, res): void {
        BacNetDataController.BacNetDataService = BacNetDataService.getInstance();
        // param require
        BacNetDataController.BacNetDataService.downloadObjectMapping(req, (result) => {
            res.send(result);
        });
    }
    public restoreDefaultConfigurations(req, res): void {
        BacNetDataController.BacNetDataService = BacNetDataService.getInstance();
        // param require
        BacNetDataController.BacNetDataService.restoreDefaultConfigurations(req, (result) => {
            res.send(result);
        });
    }

    public SetDefaultConfigurations(req, res): void {
        BacNetDataController.BacNetDataService = BacNetDataService.getInstance();
        // param require
        BacNetDataController.BacNetDataService.SetDefaultConfigurations(req, (result) => {
            res.send(result);
        });
    }

    public ToggleGWServiceStatus (req, res): void {
        BacNetDataController.BacNetDataService = BacNetDataService.getInstance();
        BacNetDataController.BacNetDataService.ToggleGWServiceStatus (req.body, (result) => {
            res.send(result);
        });
    }
    public getProjectData(req, res): void {
        BacNetDataController.BacNetDataService = BacNetDataService.getInstance();
        // param require
        BacNetDataController.BacNetDataService.getProjectData(req, (result) => {
            res.send(result);
        });
    }
    public getTsConfigData(req, res): void {
        BacNetDataController.BacNetDataService = BacNetDataService.getInstance();
        // param require
        BacNetDataController.BacNetDataService.getTsConfigData(req, (result) => {
            res.send(result);
        });
    } 

    public getAPIConfigData(req, res): void {
        BacNetDataController.BacNetDataService = BacNetDataService.getInstance();
        // param require
        BacNetDataController.BacNetDataService.getAPIConfigData(req, (result) => {
            res.send(result);
        });
    } 
    public getAuditData(req, res): void {
        BacNetDataController.BacNetDataService = BacNetDataService.getInstance();
        // param require
        BacNetDataController.BacNetDataService.getAuditData(req, (result) => {
            res.send(result);
        });
    } 
    public removeAuditLogData(req, res): void {
        BacNetDataController.BacNetDataService = BacNetDataService.getInstance();
        // param require
        BacNetDataController.BacNetDataService.removeAuditLogData(req, (result) => {
            res.send(result);
        });
    }
    public getdownloadLoggerData(req, res): void {
        BacNetDataController.BacNetDataService = BacNetDataService.getInstance();
        // param require
        BacNetDataController.BacNetDataService.getdownloadLoggerData(req, (result) => {
            res.send(result);
        });
    } 
    public getLightsceneData(req, res): void {
        BacNetDataController.BacNetDataService = BacNetDataService.getInstance();
        // param require
        BacNetDataController.BacNetDataService.getLightsceneData(req, (result) => {
            res.send(result);
        });
    }
    public setTsConfigData(req, res): void {
        BacNetDataController.BacNetDataService = BacNetDataService.getInstance();
        // param require
        BacNetDataController.BacNetDataService.setTsConfigData(req, (result) => {
            res.send(result);
        });
    }
    public setDeactiveTS(req, res): void {
        BacNetDataController.BacNetDataService = BacNetDataService.getInstance();
        // param require
        BacNetDataController.BacNetDataService.setDeactiveTS(req, (result) => {
            res.send(result);
        });
    }
    public loadMqtt(req, res): void {
        BacNetDataController.BacNetDataService = BacNetDataService.getInstance();
        // param require
        BacNetDataController.BacNetDataService.loadMqtt(req, (result) => {
            res.send(result);
        });
    }  

}
