import * as http from "http";
import * as request from "request";

export class HttpRequest {
    constructor() {
        // constructor code will come here
    }

    public makeHttpCall(options, callback) {
        request(options, function (error, response, body) {
            if (error) {
                callback(error);
            } else {
                callback(body);
            }
        });
    }


    public makeHttpCallPowerUsage(options) {
        var responseJSONPromise = function (fulfill, reject) {
            

            request(options, function (error, res, body) {
                if (body) {
                    fulfill(body);
                } else {
                    reject(error);
                }
            });
        }
        return new Promise(responseJSONPromise);
    }

    public makeHttpCallEnergySavings(options) {
        var responseJSONPromise = function (fulfill, reject) {
            request(options, function (error, res, body) {
                if (body) {
                    fulfill(body);
                } else {
                    reject(error);
                }
            });
        }
        return new Promise(responseJSONPromise);
    }

    public makeHttpCallOccupancy(options) {
        var responseJSONPromise = function (fulfill, reject) {
            request(options, function (error, res, body) {
                if (body) {
                    fulfill(body);
                } else {
                    reject(error);
                }
            });
        }
        return new Promise(responseJSONPromise);
    }
}
