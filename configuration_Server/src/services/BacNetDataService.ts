import { Config } from "../config";
import { HttpRequest } from "../HttpRequest";
import ResponseFormatter from "../lib/helpers/ResponseFormatter";
import { APIConfiguration } from "../lib/collections/APIConfiguration";
import { DataCollectionManager } from "../lib/processManager/dataCollectionManager";
var path = require("path");
var fs = require("fs");
const fileType = require('file-type');

export class BacNetDataService {
      
  private HttpRequest: HttpRequest;
  private Config: Config;  
  private configFilePath = path.join(__dirname, "../../appConfig.json");
  private APIConfiguration: APIConfiguration;
  private dataCollectionManager: DataCollectionManager;
  private gridDimensions: any[];
  
  public static getInstance(): BacNetDataService {
    return BacNetDataService.instance;
  }
  private static instance: BacNetDataService = new BacNetDataService();
  constructor() {
    BacNetDataService.instance = this;
    this.HttpRequest = new HttpRequest();
    this.APIConfiguration = APIConfiguration.getInstance();
    this.Config = Config.getInstance();
  }
  public initiateApplicationConfiguration(callback) {
    this.Config = Config.getInstance();
    this.Config.init(this.configFilePath, function(this) {
      callback();
    });
  }

  

  public setAPIPortData(requestData,callback): any {
    var options = {
        
        method: "GET",
        url: this.APIConfiguration.urlDir.apiConfigData,
        json: true,
        auth: this.APIConfiguration.authentication
      };
  
      this.HttpRequest.makeHttpCall(options, function(result) {
        
        let responseObj = {};
        if (!result) { 
            const errorObj = ResponseFormatter.getErrorObject("projectNotFound", result.locals);
            responseObj = ResponseFormatter.getResponseObject(false, "Unable to get Project data", errorObj);
            callback(responseObj);
        } else { 
                
            // console.log("result.result.data in service ", result.result.data);
            responseObj = ResponseFormatter.getResponseObject(true, "Project data successfully received",'');
            callback(responseObj);
         
        }
        // callback(result);
      });


    
  }

  public checkBacNetData(requestData, callback): any {
    var options = {        
        method: "GET",
        url: this.APIConfiguration.urlDir.getGWServiceStatusData,
        json: true,
        //auth: this.APIConfiguration.authentication
      };
    this.HttpRequest.makeHttpCall(options, function(result) {
      let responseObj = {};
      if (!result.Status) {
        const errorObj = ResponseFormatter.getErrorObject("projectNotFound", result.locals);
        responseObj = ResponseFormatter.getResponseObject(false, "Unable to get Project data", errorObj);
        callback(responseObj);
      } else {
          responseObj = ResponseFormatter.getResponseObject(true, "Project data successfully received",'');
          callback(responseObj);
       
      }
    });
}
public checkDashboardData(requestData, callback): any {
  console.log("CALLLL HUAA")
  var options = {        
      method: "GET",
      url: this.APIConfiguration.urlDir.getConfigDataURL,
      json: true,
      //auth: this.APIConfiguration.authentication
    };
  this.HttpRequest.makeHttpCall(options, function(result) {
    let responseObj = {};
   // console.log("call-hua",result)
    if (!result) {
      const errorObj = ResponseFormatter.getErrorObject("projectNotFound", result.locals);
      responseObj = ResponseFormatter.getResponseObject(false, "Unable to get Project data", errorObj);
      callback(responseObj);
    } else {
        responseObj = ResponseFormatter.getResponseObject(true, "Project data successfully received",'');
        callback(responseObj);
     
    }
  });
}

public getDashboardConfigData(requestData, callback): any {
  console.log("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^")
    var options = {        
      method: "GET",
      url: this.APIConfiguration.urlDir.getConfigDataURL,
      json: true,
     // auth: this.APIConfiguration.authentication
    };
    console.log("##################",options)
  this.HttpRequest.makeHttpCall(options, function(result) {
    console.log("###########################################requestData--###",result)
    let responseObj = {};
    if (!result) {
      const errorObj = ResponseFormatter.getErrorObject("projectNotFound", result.locals);
      responseObj = ResponseFormatter.getResponseObject(false, "Unable to get Project data", errorObj);
      callback(responseObj);
    } else {
        responseObj = ResponseFormatter.getResponseObject(true, "Project data successfully received",result);
        callback(responseObj);
    
    }
  });
}

public saveDashboardConfigData(requestData, callback): any {
  console.log("###########################################requestData--###",requestData.JsonData)
    var options = {        
      method: "POST",
      url: this.APIConfiguration.urlDir.saveConfigData,
      body:requestData.JsonData,
      json: true
      //auth: this.APIConfiguration.authentication
    };
    console.log("##################",options)
  this.HttpRequest.makeHttpCall(options, function(result) {
    console.log("SUCCESSS__________________",result)
    let responseObj = {};
    if (!result) {
      const errorObj = ResponseFormatter.getErrorObject("projectNotFound", result.locals);
      responseObj = ResponseFormatter.getResponseObject(false, "Unable to get Project data", errorObj);
      callback(responseObj);
    } else {
        responseObj = ResponseFormatter.getResponseObject(true, "Project data successfully received",result);
        callback(responseObj);
    
    }
  });
}
  public getBacNetData(requestData, callback): any {
      var options = {        
        method: "GET",
        url: this.APIConfiguration.urlDir.bacNetData,
        json: true,
        //auth: this.APIConfiguration.authentication
      };
   // console.log('options ', options)
    this.HttpRequest.makeHttpCall(options, function(result) {
      //console.log("Backen******************tData",result)
      let responseObj = {};
    //   console.log('result ', result)
      if (!result.Status) {
        // Logger.log("error", err.message);
        const errorObj = ResponseFormatter.getErrorObject("projectNotFound", result.locals);
        responseObj = ResponseFormatter.getResponseObject(false, "Unable to get Project data", errorObj);
        callback(responseObj);
      } else {
        
          // console.log("result.result.data in service ", result.result.data);
          responseObj = ResponseFormatter.getResponseObject(true, "Project data successfully received",result.JsonData);
         //console.log('responseObj ', responseObj)
          callback(responseObj);
       
      }
      // callback(result);
    });
  }
  public saveBacNetConfiguration(requestData, callback): any {
    var options = {        
      method: "POST",
      url: this.APIConfiguration.urlDir.saveBacNetData,
      body:requestData,
      json: true,
      //auth: this.APIConfiguration.authentication
    };
  this.HttpRequest.makeHttpCall(options, function(result) {
    let responseObj = {};
    if (!result.Status) {
      const errorObj = ResponseFormatter.getErrorObject("projectNotFound", result.locals);
      responseObj = ResponseFormatter.getResponseObject(false, "Unable to get Project data", errorObj);
      callback(responseObj);
    } else {
        responseObj = ResponseFormatter.getResponseObject(true, "Project data successfully received",result.JsonData);
        callback(responseObj);
     
    }
    // callback(result);
  });
}
public getGWServiceStatus(requestData, callback): any {
  var options = {        
    method: "GET",
    url: this.APIConfiguration.urlDir.getGWServiceStatusData,
    json: true,
    //auth: this.APIConfiguration.authentication
  };
this.HttpRequest.makeHttpCall(options, function(result) {
  let responseObj = {};
  if (!result.Status) {
    const errorObj = ResponseFormatter.getErrorObject("projectNotFound", result.locals);
    responseObj = ResponseFormatter.getResponseObject(false, "Unable to get Project data", errorObj);
    callback(responseObj);
  } else {
      responseObj = ResponseFormatter.getResponseObject(true, "Project data successfully received",result.JsonData);
      callback(responseObj);
   
  }
});
}
public downloadObjectMapping(requestData, callback): any {
    var options = {        
      method: "GET",
      url: this.APIConfiguration.urlDir.downloadObjectMappingData,
      json: true,
      //auth: this.APIConfiguration.authentication
    };
  this.HttpRequest.makeHttpCall(options, function(result) {
    let responseObj = {};
    if (!result.Status) {
      const errorObj = ResponseFormatter.getErrorObject("projectNotFound", result.locals);
      responseObj = ResponseFormatter.getResponseObject(false, "Unable to get Project data", errorObj);
      callback(responseObj);
    } else {
        responseObj = ResponseFormatter.getResponseObject(true, "Project data successfully received",result.JsonData);
        callback(responseObj);
     
    }
  });   
  } 
  public restoreDefaultConfigurations(requestData, callback): any {
    var options = {        
      method: "GET",
      url: this.APIConfiguration.urlDir.restoreDefaultConfigurationsData,
      json: true,
      //auth: this.APIConfiguration.authentication
    };
  this.HttpRequest.makeHttpCall(options, function(result) {
    let responseObj = {};
    if (!result.Status) {
      const errorObj = ResponseFormatter.getErrorObject("projectNotFound", result.locals);
      responseObj = ResponseFormatter.getResponseObject(false, "Unable to get Project data", errorObj);
      callback(responseObj);
    } else {
        responseObj = ResponseFormatter.getResponseObject(true, "Project data successfully received",result.JsonData);
        callback(responseObj);
     
    }
  });
  }

  public ToggleGWServiceStatus(requestData, callback): any {
    var options = {        
      method: "POST",
      url: this.APIConfiguration.urlDir.setToggleGWServiceStatusdata,
      json: true,
      body:requestData,
      //auth: this.APIConfiguration.authentication
    };
  this.HttpRequest.makeHttpCall(options, function(result) {
    let responseObj = {};
    if (!result.Status) {
      const errorObj = ResponseFormatter.getErrorObject("projectNotFound", result.locals);
      responseObj = ResponseFormatter.getResponseObject(false, "Unable to get Project data", errorObj);
      callback(responseObj);
    } else {
        responseObj = ResponseFormatter.getResponseObject(true, "Project data successfully received",result.JsonData);
        callback(responseObj);
     
    }
  });
  }


  public SetDefaultConfigurations(requestData, callback): any {
    var options = {        
      method: "GET",
      url: this.APIConfiguration.urlDir.setDefaultConfigurationsData ,
      json: true,
      //auth: this.APIConfiguration.authentication
    };
  this.HttpRequest.makeHttpCall(options, function(result) {
    let responseObj = {};
    if (!result.Status) {
      const errorObj = ResponseFormatter.getErrorObject("projectNotFound", result.locals);
      responseObj = ResponseFormatter.getResponseObject(false, "Unable to get Project data", errorObj);
      callback(responseObj);
    } else {
        responseObj = ResponseFormatter.getResponseObject(true, "Project data successfully received",result.JsonData);
        callback(responseObj);
     
    }
  });
  }
  
  public getProjectData(requestData, callback): any {
    console.log("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@")
    var options = {        
      method: "GET",
      url: this.APIConfiguration.urlDir.getProjects,
      json: true,
      auth: this.APIConfiguration.authentication
    };
    console.log("InGetProjects----",options)
    this.HttpRequest.makeHttpCall(options, function(result) {
      let responseObj = {};
      if (!result.status) {
        const errorObj = ResponseFormatter.getErrorObject("projectNotFound", result.locals);
        responseObj = ResponseFormatter.getResponseObject(false, "Unable to get Project data", errorObj);
        callback(responseObj);
      } else {       
          // console.log("result.result.data in service ", result.result.data);
          responseObj = ResponseFormatter.getResponseObject(true, "Project data successfully received",result.result.data);
          callback(responseObj);
       
      }
      // callback(result);
    });
  }

  public getTsConfigData(requestData, callback): any {
    var options = {
        
      method: "GET",
      url: this.APIConfiguration.urlDir.tsConfigData,
      json: true,
      auth: this.APIConfiguration.authentication
    };

    this.HttpRequest.makeHttpCall(options, function(result) {
      let responseObj = {};
      if (!result.status) {
        const errorObj = ResponseFormatter.getErrorObject("projectNotFound", result.locals);
        responseObj = ResponseFormatter.getResponseObject(false, "Unable to get Project data", errorObj);
        callback(responseObj);
      } else {       
          // console.log("result.result.data in service ", result.result.data);
          responseObj = ResponseFormatter.getResponseObject(true, "Project data successfully received",result.result.data);
          callback(responseObj);
       
      }
      // callback(result);
    });
  }
  public getAPIConfigData(requestData, callback): any {
    var options = {
        
      method: "GET",
      url: this.APIConfiguration.urlDir.apiConfigData,
      json: true,
      auth: this.APIConfiguration.authentication
    };

    this.HttpRequest.makeHttpCall(options, function(result) {
      let responseObj = {};
      if (!result.status) {
        const errorObj = ResponseFormatter.getErrorObject("projectNotFound", result.locals);
        responseObj = ResponseFormatter.getResponseObject(false, "Unable to get Project data", errorObj);
        callback(responseObj);
      } else {      
          // console.log("result.result.data in service ", result.result.data);
          responseObj = ResponseFormatter.getResponseObject(true, "Project data successfully received",result.result.data);
          callback(responseObj);
       
      }
      // callback(result);
    });
  }
  public getAuditData(requestData, callback): any {
    var options = {
        
      method: "GET",
      url: this.APIConfiguration.urlDir.apiConfigData,
      json: true,
      auth: this.APIConfiguration.authentication
    };

    this.HttpRequest.makeHttpCall(options, function(result) {
      let responseObj = {};
      if (!result.status) {
        const errorObj = ResponseFormatter.getErrorObject("projectNotFound", result.locals);
        responseObj = ResponseFormatter.getResponseObject(false, "Unable to get Project data", errorObj);
        callback(responseObj);
      } else {      
          // console.log("result.result.data in service ", result.result.data);
          responseObj = ResponseFormatter.getResponseObject(true, "Project data successfully received",result.result.data);
          callback(responseObj);
       
      }
      // callback(result);
    });
  }
  public removeAuditLogData(requestData, callback): any {
    var options = {       
      method: "PUT",
      url: this.APIConfiguration.urlDir.removeAuditLog,
      json: true,
      auth: this.APIConfiguration.authentication
    };
    this.HttpRequest.makeHttpCall(options, function(result) {
      let responseObj = {};
      if (!result.status) {
        const errorObj = ResponseFormatter.getErrorObject("projectNotFound", result.locals);
        responseObj = ResponseFormatter.getResponseObject(false, "Unable to get Project data", errorObj);
        callback(responseObj);
      } else {      
          // console.log("result.result.data in service ", result.result.data);
          responseObj = ResponseFormatter.getResponseObject(true, "Project data successfully received",result.result.data);
          callback(responseObj);
       
      }
      // callback(result);
    });
  }
  public getdownloadLoggerData(requestData, callback): any {
    var urlObject = [];
     var downloadURL = this.APIConfiguration.urlDir.downloadLogger;
     urlObject.push(downloadURL);
     callback(urlObject);
    // });

    
    

    
    
  }
  public getLightsceneData(requestData, callback): any {
      var urlParam = this.APIConfiguration.urlDir.lightsceneList;
    var options = {        
      method: "GET",
      url: urlParam +'?projectid='+requestData.query.id,
      json: true,
      contentType: 'application/json; charset=utf-8',
      dataType: 'json',
      async: false,
      auth: this.APIConfiguration.authentication
    };
    this.HttpRequest.makeHttpCall(options, function(result) {
        //console.log("#######",result)
      let responseObj = {};
      if (!result.status) {
        const errorObj = ResponseFormatter.getErrorObject("projectNotFound", result.locals);
        responseObj = ResponseFormatter.getResponseObject(false, "Unable to get Project data", errorObj);
        callback(responseObj);
      } else {      
          // console.log("result.result.data in service ", result.result.data);
          responseObj = ResponseFormatter.getResponseObject(true, "Project data successfully received",result.result.data);
          callback(responseObj);
       
      }
      // callback(result);
    });
  }
  public setTsConfigData(requestData, callback): any {
    console.log("requestData-------setTsConfigData",requestData.body)
    for(let key in requestData.body)
     {
        let obj = JSON.parse(key);
        var configIP =obj.newSmartIP;
        var configPort = obj.newSmartPort;
        var options = {    
        method: "PUT",
        url: this.APIConfiguration.urlDir.setTsConfig,
        json: true,
        body: { "ip": configIP, "port": configPort },
        auth: this.APIConfiguration.authentication
       };
    }
    console.log("OPTION--ConfigsetTsData",options)
    this.HttpRequest.makeHttpCall(options, function(result) {
        console.log("RESULTTTT",result)
      let responseObj = {};
      if (!result.status) {
        const errorObj = ResponseFormatter.getErrorObject("projectNotFound", result.locals);
        responseObj = ResponseFormatter.getResponseObject(false, "Unable to get Project data", errorObj);
        callback(responseObj);
      } else {      
          // console.log("result.result.data in service ", result.result.data);
          responseObj = ResponseFormatter.getResponseObject(true, "Project data successfully received",result.result.data);
          callback(responseObj);
       
      }
      // callback(result);
    });
  }
  public setDeactiveTS(requestData, callback): any {
      console.log("requestData-------Deactive",requestData.body)
    var urlParam = this.APIConfiguration.urlDir.setDeactive
    
    for(let key in requestData.body)
    {
       let obj = JSON.parse(key);
       var projectid = obj.deleteSCProjID;
        var options = {       
        method: "PUT",
        url: urlParam+'?projectid='+ projectid,
        json: true,
        contentType: 'application/javascript',
        auth: this.APIConfiguration.authentication
        };
    }
    console.log("OPtionsDeactivate",options)
    this.HttpRequest.makeHttpCall(options, function(result) {
        console.log("RESULT-Deactive",result)
      let responseObj = {};
      if (!result.status) {
        const errorObj = ResponseFormatter.getErrorObject("projectNotFound", result.locals);
        responseObj = ResponseFormatter.getResponseObject(false, "Unable to get Project data", errorObj);
        callback(responseObj);
      } else {      
          // console.log("result.result.data in service ", result.result.data);
          responseObj = ResponseFormatter.getResponseObject(true, "Project data successfully received",result.result.data);
          callback(responseObj);
       
      }
      // callback(result);
    });
  }
  public loadMqtt(requestData, callback): any {
      for(let key in requestData.body)
      {
         let obj = JSON.parse(key);
         var statusData = obj.statusData
        var options = {       
            method: "PUT",
            url: this.APIConfiguration.urlDir.setmqtt,
            json: true,
            body: {"data":statusData},
            contentType: 'application/javascript',
            auth: this.APIConfiguration.authentication
        };
   }
    this.HttpRequest.makeHttpCall(options, function(result) {
        console.log("Result",result)
      let responseObj = {};
      if (!result.status) {
        const errorObj = ResponseFormatter.getErrorObject("projectNotFound", result.locals);
        responseObj = ResponseFormatter.getResponseObject(false, "Unable to get Project data", errorObj);
        callback(responseObj);
      } else {      
          // console.log("result.result.data in service ", result.result.data);
          responseObj = ResponseFormatter.getResponseObject(true, "Project data successfully received",result.result.data);
          callback(responseObj);
       
      }
      // callback(result);
    });
  }
  public setAPIConfigData(requestData, callback): any {
    var options = {
        
    method: "PUT",
    url: this.APIConfiguration.urlDir.setTsConfig,
    json: true,
    data: JSON.stringify({ "ip": requestData.query.ip, "port": requestData.query.port }),
    contentType: 'application/json; charset=utf-8',
    dataType: 'JSON',
    async: false,
    auth: this.APIConfiguration.authentication
    };

    this.HttpRequest.makeHttpCall(options, function(result) {
        console.log("#######",result)
      let responseObj = {};
      if (!result.status) {
        const errorObj = ResponseFormatter.getErrorObject("projectNotFound", result.locals);
        responseObj = ResponseFormatter.getResponseObject(false, "Unable to get Project data", errorObj);
        callback(responseObj);
      } else {      
          // console.log("result.result.data in service ", result.result.data);
          responseObj = ResponseFormatter.getResponseObject(true, "Project data successfully received",result.result.data);
          callback(responseObj);
       
      }
      // callback(result);
    });
  }

}
