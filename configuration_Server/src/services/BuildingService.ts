import { Config } from "../config";
import { HttpRequest } from "../HttpRequest";
import ResponseFormatter from "../lib/helpers/ResponseFormatter";
import { APIConfiguration } from "../lib/collections/APIConfiguration";
import { DataCollectionManager } from "../lib/processManager/dataCollectionManager";
var path = require("path");
// var ProjectObject = require(path.resolve("mqttscript.config"));
var fs = require("fs");
const fileType = require('file-type');
export class BuildingService {
   
  private HttpRequest: HttpRequest;
  private Config: Config;  
  private configFilePath = path.join(__dirname, "../../appConfig.json");
  private APIConfiguration: APIConfiguration;
  private dataCollectionManager: DataCollectionManager;
  private gridDimensions: any[];
  public static getInstance(): BuildingService {
    return BuildingService.instance;
  }
  private static instance: BuildingService = new BuildingService();

  constructor() {
    BuildingService.instance = this;
    this.HttpRequest = new HttpRequest();
    this.APIConfiguration = APIConfiguration.getInstance();
    this.Config = Config.getInstance();
    this.dataCollectionManager = DataCollectionManager.getInstance();
    this.gridDimensions = [];
  }

  public initiateApplicationConfiguration(callback) {
    this.Config = Config.getInstance();
    this.Config.init(this.configFilePath, function(this) {
      callback();
    });
  }

    /**
   * getAllProjects
   */
  public getAllProjects(callback): any {
    let projectObj = this.dataCollectionManager.getAllProject();
    let responseObj = {};
    if (Object.keys(projectObj).length === 0 && projectObj.constructor === Object) {
      // Logger.log("error", err.message);
      const errorObj = ResponseFormatter.getErrorObject("projectNotFound", projectObj);
      responseObj = ResponseFormatter.getResponseObject(false, "Unable to get Project data", errorObj);

    } else {
      responseObj = ResponseFormatter.getResponseObject(true, "Project data successfully received", projectObj);

    }
    // console.log('responseObj ', responseObj)
    callback(responseObj);
  } // end getAllProjects method


  public getFloorsByProjectId(projectId: string, callback): any {
    let projectFloors = this.dataCollectionManager.getFloorsByProjectId(projectId);
    let responseObj = {};
    if (projectFloors == '') {
      // Logger.log("error", err.message);
      const errorObj = ResponseFormatter.getErrorObject("projectNotFound", projectFloors);
      responseObj = ResponseFormatter.getResponseObject(false, "Unable to get Project floor data", errorObj);

    } else {
      responseObj = ResponseFormatter.getResponseObject(true, "Project floor data successfully received", projectFloors);
    }
    callback(responseObj);
  }
  
  public getStats(callback): any {
    let projectStat = this.dataCollectionManager.getProjectStat();
    let responseObj = {};
    if (projectStat == '') {
      // Logger.log("error", err.message);
      const errorObj = ResponseFormatter.getErrorObject("projectNotFound", projectStat);
      responseObj = ResponseFormatter.getResponseObject(false, "Unable to get Project stat data", errorObj);
      
    } else {
      responseObj = ResponseFormatter.getResponseObject(true, "Project stat data successfully received", projectStat);
      
    }
    callback(responseObj);
    /* var options = {
      method: "GET",
      url: this.APIConfiguration.urlDir.stats,
      qs: { id: id, type: type, projectid: projectId },
      json: true,
      auth: this.APIConfiguration.authentication
    };
    // console.log('options ', options)
    this.HttpRequest.makeHttpCall(options, function(result) {
      let responseObj = {};
      if (!result.status) {
        // Logger.log("error", err.message);
        const errorObj = ResponseFormatter.getErrorObject("projectNotFound", result.locals);
        responseObj = ResponseFormatter.getResponseObject(false, "Unable to get Project stat data", errorObj);
        callback(responseObj);
      } else {
        if (result && Object.keys(result).length > 0) {
          responseObj = ResponseFormatter.getResponseObject(true, "Project stat data successfully received", result.result.data);
          callback(responseObj);
        } else {
          const errorObj = ResponseFormatter.getErrorObject("projectNotFound", result.locals);
          responseObj = ResponseFormatter.getResponseObject(false, "Unable to get Project stat data", errorObj);
          callback(responseObj);
        }
      }
    }); */
  }



  // public getStats(id, type, projectId, callback): any {
  //   var options = {
  //     method: "GET",
  //     url: this.APIConfiguration.urlDir.stats,
  //     qs: { id: id, type: type, projectid: projectId },
  //     json: true,
  //     auth: this.APIConfiguration.authentication
  //   };
  //   // console.log('options ', options)
  //   this.HttpRequest.makeHttpCall(options, function(result) {
  //     let responseObj = {};
  //     if (!result.status) {
  //       // Logger.log("error", err.message);
  //       const errorObj = ResponseFormatter.getErrorObject(
  //         "projectNotFound",
  //         result.locals
  //       );
  //       responseObj = ResponseFormatter.getResponseObject(
  //         false,
  //         "Unable to get Project stat data",
  //         errorObj
  //       );
  //       callback(responseObj);
  //     } else {
  //       if (result && Object.keys(result).length > 0) {
  //         responseObj = ResponseFormatter.getResponseObject(
  //           true,
  //           "Project stat data successfully received",
  //           result.result.data
  //         );
  //         callback(responseObj);
  //       } else {
  //         const errorObj = ResponseFormatter.getErrorObject(
  //           "projectNotFound",
  //           result.locals
  //         );
  //         responseObj = ResponseFormatter.getResponseObject(
  //           false,
  //           "Unable to get Project stat data",
  //           errorObj
  //         );
  //         callback(responseObj);
  //       }
  //     }
  //   });
  // }

  public getProject(requestData, callback): any {
    var options = {
      method: "GET",
      url: this.APIConfiguration.urlDir.projects,
      qs: { info: "detail" },
      json: true,
      auth: this.APIConfiguration.authentication
    };
    this.HttpRequest.makeHttpCall(options, function(result) {
      let responseObj = {};
      if (!result.status) {
        // Logger.log("error", err.message);
        const errorObj = ResponseFormatter.getErrorObject("projectNotFound", result.locals);
        responseObj = ResponseFormatter.getResponseObject(false, "Unable to get Project data", errorObj);
        callback(responseObj);
      } else {
        if (result && Object.keys(result).length > 0) {
          let location = {
            Goldleaf: { lat: "36.71829033", long: "-119.70289029" },
            Cecile: { lat: "36.69682155", long: "-119.70598019" },
            "Loan start": { lat: "36.7004001", long: "-119.66718472" },
            Malaga: { lat: "36.68828742", long: "-119.73035611" },
            "Loan start 1": { lat: "36.69461928", long: "-119.68469418" },
            "Cecile 1": { lat: "36.70618047", long: "-119.70220364" },
            "Goldleaf 1": { lat: "36.71966633", long: "-119.70392025" },
            "Malaga 1": { lat: "36.68993926", long: "-119.74923886" }
          };

          result.result.data.forEach(element => {
            let keys = Object.keys(location);
            let randomLocation = location[keys[(keys.length * Math.random()) << 0]];
            element.longitude = randomLocation.long;
            element.latitude = randomLocation.lat;
          });
          // console.log("result.result.data in service ", result.result.data);
          responseObj = ResponseFormatter.getResponseObject(true, "Project data successfully received",result.result.data);
          callback(responseObj);
        } else {
          const errorObj = ResponseFormatter.getErrorObject("projectNotFound", result.locals);
          responseObj = ResponseFormatter.getResponseObject(false, "Unable to get Project data",errorObj);
          callback(responseObj);
        }
      }
      // callback(result);
    });
  }

  getProjectMetaData(callback) {
    var optionsProject = {
      method: "GET",
      url: this.APIConfiguration.startupUrl.projects,
      json: true,
      auth: this.APIConfiguration.authentication
    };
    var optionsGetProject = {
      method: "GET",
      url: this.APIConfiguration.startupUrl.projectDetail,
      json: true,
      auth: this.APIConfiguration.authentication
    };
    var self = this;
    // console.log('reference', this.Config, this);
    
    var projectMapping = [];
    let smartCoreConfig = this.Config.getConfig("smartcoreConfig");
    let responseObj = {
      smartcoreConfig: {
        buildings: []
      }
    };
    self.HttpRequest.makeHttpCall(optionsGetProject, function (result) {
      // console.log('initializeProcess ', options)
      self.HttpRequest.makeHttpCall(optionsProject, function (r) {
        r.result.data.forEach( data => {
          let obj = {
            "ip": data.ip,
            "projectid": data.id
          }
          
          projectMapping.push(obj);
        });
        // console.log('big data', projectMapping)
        if (result.status) {
          result.result.data.forEach(project => {
            let latlang = {
              name: undefined,
              lat: undefined,
              lang: undefined,
              loc: undefined,
              ip: undefined
            };
            projectMapping.forEach(map => {
              if (map.projectid == project.id) {
                    latlang.name = project.buildingName;
                    latlang.lang = project.longitude;
                    latlang.lat = project.latitude;
                    latlang.loc = project.location;
                    latlang.ip = map.ip;
                    smartCoreConfig.buildings.forEach( b => {
                        if (b.ip == latlang.ip) {
                          latlang.name = b.name? b.name : project.buildingName;
                          latlang.lang = b.longitude;
                          latlang.lat = b.latitude;
                          latlang.loc = b.location;
                        }
                    });
                    responseObj.smartcoreConfig.buildings.push(latlang);
              }

            });
            
          });
          callback(responseObj);
        }
      }
      );
    });

  }

  public getPowerUsage(reqObj, callback): any {
    var qString = {};
    if (reqObj.pid && reqObj.id && reqObj.type) {
      qString = { projectid: reqObj.pid, id: reqObj.id, type: reqObj.type };
    }
    var options = {
      method: "GET",
      url: this.APIConfiguration.urlDir.getPowerUsage,
      qs: qString,
      json: true,
      auth: this.APIConfiguration.authentication
    };
    this.HttpRequest.makeHttpCall(options, function(result) {
      let responseObj = {};
      if (!result.status) {
        const errorObj = ResponseFormatter.getErrorObject("projectNotFound", result.locals);
        responseObj = ResponseFormatter.getResponseObject(false, "Unable to get Device data", errorObj);
        callback(responseObj);
      } else {
        if (result && Object.keys(result).length > 0) {
          responseObj = ResponseFormatter.getResponseObject(true, "Device data successfully received", result.result.data);
          callback(responseObj);
        } else {
          const errorObj = ResponseFormatter.getErrorObject("projectNotFound", result.locals);
          responseObj = ResponseFormatter.getResponseObject(false, "Unable to get Device data", errorObj);
          callback(responseObj);
        }
      }
    });
  }
  public getProjectStatus(reqObj, callback): any {
    var qString = {};
    if (reqObj.pid ) {
      qString = { projectid: reqObj.pid };
    }
    var options = {
      method: "GET",
      url: this.APIConfiguration.urlDir.projectStatus,
      qs: qString,
      json: true,
      auth: this.APIConfiguration.authentication
    };
   
    this.HttpRequest.makeHttpCall(options, function(result) {
      let responseObj = {};
      
      if (!result.status) {
        const errorObj = ResponseFormatter.getErrorObject("projectNotFound", result.locals);
        responseObj = ResponseFormatter.getResponseObject(false, "Unable to get Project Status", errorObj);
        callback(responseObj);
      } else {
        if (result && Object.keys(result).length > 0) {
          
          responseObj = ResponseFormatter.getResponseObject(true, "Device data successfully received", result.result.data);
          callback(responseObj);
        } else {
          const errorObj = ResponseFormatter.getErrorObject("projectNotFound", result.locals);
          responseObj = ResponseFormatter.getResponseObject(false, "Unable to get Project Status", errorObj);
          callback(responseObj);
        }
      }
    });
  }


  
  public getEnergySavings(reqObj, callback): any {
    var qString = {};
    if (reqObj.pid && reqObj.id && reqObj.type && reqObj.starttime && reqObj.endtime && reqObj.unit && reqObj.querystring) {
      qString = {
        projectid: reqObj.pid,
        id: reqObj.id,
        type: reqObj.type,
        starttime: reqObj.starttime,
        endtime: reqObj.endtime,
        unit: reqObj.unit,
        source: reqObj.querystring
      };
    }
    var options = {
      method: "GET",
      url: this.APIConfiguration.urlDir.getEnergySavings,
      qs: qString,
      json: true,
      auth: this.APIConfiguration.authentication
    };
   
    /* this.HttpRequest.makeHttpCall(options, function(result) {
      let responseObj = {};
      if (!result.status) {
        const errorObj = ResponseFormatter.getErrorObject("projectNotFound", result.locals);
        responseObj = ResponseFormatter.getResponseObject(false, "Unable to get power sensor data", errorObj);
        callback(responseObj);
      } else {
        if (result && Object.keys(result).length > 0) {
          responseObj = ResponseFormatter.getResponseObject(true, "Power sensor data successfully received", result.result.data);

          callback(responseObj);
        } else {
          const errorObj = ResponseFormatter.getErrorObject("projectNotFound", result.locals);
          responseObj = ResponseFormatter.getResponseObject(false, "Unable to get power sensor data", errorObj);
          callback(responseObj);
        }
      }
    }); */
    let projectsDataPromise = this.HttpRequest.makeHttpCallEnergySavings(options);
    let responseObj = {};
    projectsDataPromise.then(function(result){
      
      if (!result["status"]) {
      // Logger.log("error", err.message);
      const errorObj = ResponseFormatter.getErrorObject("projectNotFound", result["locals"]);
      responseObj = ResponseFormatter.getResponseObject(false, "Unable to get Sensor data", errorObj);
      callback(responseObj);
      } else {
      if (result && Object.keys(result).length > 0) {
      responseObj = ResponseFormatter.getResponseObject(true, "Sensor data successfully received", result["result"].data);
      callback(responseObj);
      } else {
      const errorObj = ResponseFormatter.getErrorObject("projectNotFound", result["locals"]);
      responseObj = ResponseFormatter.getResponseObject(false, "Unable to get Sensor data", errorObj);
      callback(responseObj);
      }
      } 
    }).catch((error)=>{
      responseObj = ResponseFormatter.getResponseObject(false, "Unable to get Sensor data", error);
      callback(responseObj);
    });
  }

  public calculateCO2Emmision(pid, bid, callback): any {
    let response = {};

    var options = {
      method: "GET",
      url: this.APIConfiguration.urlDir.getBuildingPowerUsage,
      qs: { projectid: pid, buildingid: bid },
      json: true,
      auth: this.APIConfiguration.authentication
    };
    this.HttpRequest.makeHttpCall(options, function(result) {
      if (!result.status) {
        const errorObj = ResponseFormatter.getErrorObject("projectNotFound", result.locals);
        response = ResponseFormatter.getResponseObject(false, "Unable to get Device data", errorObj);
        callback(response);
      } else {
        if (result && Object.keys(result).length > 0 && result.status && result.result.data.length > 0) {
          if (result.result.data[0]["buildingPower"]) {
            var usage = result.result.data[0]["buildingPower"];

            // converted watt into kw and kw into kwh
            let kwhUsage = usage / 1000 / 8;
            let emission = kwhUsage * 0.537;
            var resObj = {
              co2Emission: emission
            };
          }
          response = ResponseFormatter.getResponseObject(true, "Device data successfully received", resObj);
          callback(response);
        } else {
          const errorObj = ResponseFormatter.getErrorObject("projectNotFound", result.locals);
          response = ResponseFormatter.getResponseObject(false, "Unable to get Device data", errorObj);
          callback(response);
        }
      }
    });

    return response;
  }

  public getFloorCount(buildingId): any {
    let response = {};
    let floorCount = 0;
    /* if(ProjectObject && ProjectObject.length ){
            let buildings  = ProjectObject[0].buildings;
            for(let i=0;i< buildings.length;i++){
                if(buildings[i].id == buildingId){
                    floorCount = floorCount + buildings[i].floors.length;
                }
            }
        } */
    return floorCount;
  }

  public getSensorLiveData(projectId, id, type, sensor, starttime, endtime, unit, callback): any {
    var options = {
      method: "GET",
      url: this.APIConfiguration.urlDir.sensorLastValue,
      qs: {
        projectid: projectId,
        id: id,
        type: type,
        sensor: sensor,
        starttime: starttime,
        endtime: endtime,
        unit: unit
      },
      json: true,
      auth: this.APIConfiguration.authentication
    };

    this.HttpRequest.makeHttpCall(options, function(result) {
      let responseObj = {};
      if (!result.status) {
        // Logger.log("error", err.message);
        const errorObj = ResponseFormatter.getErrorObject(
          "projectNotFound",
          result.locals
        );
        responseObj = ResponseFormatter.getResponseObject(
          false,
          "Unable to get Sensor data",
          errorObj
        );
        callback(responseObj);
      } else {
        if (result && Object.keys(result).length > 0) {
          responseObj = ResponseFormatter.getResponseObject(
            true,
            "Sensor data successfully received",
            result.result.data
          );

          callback(responseObj);
        } else {
          const errorObj = ResponseFormatter.getErrorObject(
            "projectNotFound",
            result.locals
          );
          responseObj = ResponseFormatter.getResponseObject(
            false,
            "Unable to get Sensor data",
            errorObj
          );
          callback(responseObj);
        }
      }
    });
  }
  public getSensorChartData(
    projectId,
    id,
    type,
    sensor,
    starttime,
    endtime,
    unit,
    callback
  ): any {
    var options = {
      method: "GET",
      url: this.APIConfiguration.urlDir.sensorChartData,
      qs: {
        projectid: projectId,
        id: id,
        type: type,
        sensor: sensor,
        starttime: starttime,
        endtime: endtime,
        unit:unit
      },
      json: true,
      auth: this.APIConfiguration.authentication
    };
    this.HttpRequest.makeHttpCall(options, function(result) {
      let responseObj = {};
      if (!result.status) {
        // Logger.log("error", err.message);
        const errorObj = ResponseFormatter.getErrorObject(
          "projectNotFound",
          result.locals
        );
        responseObj = ResponseFormatter.getResponseObject(
          false,
          "Unable to get Sensor data",
          errorObj
        );
        callback(responseObj);
      } else {
        if (result && Object.keys(result).length > 0) {
          responseObj = ResponseFormatter.getResponseObject(
            true,
            "Sensor data successfully received",
            result.result.data
          );

          callback(responseObj);
        } else {
          const errorObj = ResponseFormatter.getErrorObject(
            "projectNotFound",
            result.locals
          );
          responseObj = ResponseFormatter.getResponseObject(
            false,
            "Unable to get Sensor data",
            errorObj
          );
          callback(responseObj);
        }
      }
    });
  }

  public getSensorHistoryData(
    projectId,
    id,
    type,
    sensor,
    starttime,
    endtime,
    unit,
    callback
  ): any {
    var options = {
      method: "GET",
      url: this.APIConfiguration.urlDir.sensorHistoryData,
      qs: {
        projectid: projectId,
        id: id,
        type: type,
        sensor: sensor,
        starttime: starttime,
        endtime: endtime,
        unit:unit
      },
      json: true,
      auth: this.APIConfiguration.authentication
    };
    this.HttpRequest.makeHttpCall(options, function(result) {
      let responseObj = {};
      if (!result.status) {
        // Logger.log("error", err.message);
        const errorObj = ResponseFormatter.getErrorObject(
          "projectNotFound",
          result.locals
        );
        responseObj = ResponseFormatter.getResponseObject(
          false,
          "Unable to get Sensor data",
          errorObj
        );
        callback(responseObj);
      } else {
        if (result && Object.keys(result).length > 0) {
          responseObj = ResponseFormatter.getResponseObject(
            true,
            "Sensor data successfully received",
            result.result.data
          );

          callback(responseObj);
        } else {
          const errorObj = ResponseFormatter.getErrorObject(
            "projectNotFound",
            result.locals
          );
          responseObj = ResponseFormatter.getResponseObject(
            false,
            "Unable to get Sensor data",
            errorObj
          );
          callback(responseObj);
        }
      }
    });
  }
  public getPowerConsumption(projectId, id, type, starttime, endtime, unit, callback): any {
    var options = {
    method: "GET",
    url: this.APIConfiguration.urlDir.getPowerConsumption,
    qs: {
    projectid: projectId,
    id: id,
    type: type,
    starttime: starttime,
    endtime: endtime,
    unit: unit
    },
    json: true,
    auth: this.APIConfiguration.authentication
    };
    /* this.HttpRequest.makeHttpCallPowerUsage(options, function(result) {
    console.log("***********",JSON.stringify(result))
    let responseObj = {};
    if (!result.status) {
    // Logger.log("error", err.message);
    const errorObj = ResponseFormatter.getErrorObject("projectNotFound", result.locals);
    responseObj = ResponseFormatter.getResponseObject(false, "Unable to get Sensor data", errorObj);
    callback(responseObj);
    } else {
    if (result && Object.keys(result).length 〉 0) {
    responseObj = ResponseFormatter.getResponseObject(true, "Sensor data successfully received", result.result.data);
    callback(responseObj);
    } else {
    const errorObj = ResponseFormatter.getErrorObject("projectNotFound", result.locals);
    responseObj = ResponseFormatter.getResponseObject(false, "Unable to get Sensor data", errorObj);
    callback(responseObj);
    }
    }
    }); */
    let projectsDataPromise = this.HttpRequest.makeHttpCallPowerUsage(options);
    let responseObj = {};
    projectsDataPromise.then(function(result){
      
      if (!result["status"]) {
      // Logger.log("error", err.message);
      const errorObj = ResponseFormatter.getErrorObject("projectNotFound", result["locals"]);
      responseObj = ResponseFormatter.getResponseObject(false, "Unable to get Sensor data", errorObj);
      callback(responseObj);
      } else {
      if (result && Object.keys(result).length > 0) {
      responseObj = ResponseFormatter.getResponseObject(true, "Sensor data successfully received", result["result"].data);
      callback(responseObj);
      } else {
      const errorObj = ResponseFormatter.getErrorObject("projectNotFound", result["locals"]);
      responseObj = ResponseFormatter.getResponseObject(false, "Unable to get Sensor data", errorObj);
      callback(responseObj);
      }
      } 
    }).catch((error)=>{
      responseObj = ResponseFormatter.getResponseObject(false, "Unable to get Sensor data", error);
      callback(responseObj);
    });
  
    }


  public  getScenesPalette(projectid, id, type, zonetype, callback): any {
    var options = {
      method: "GET",
      url: this.APIConfiguration.urlDir.getScenesPalette,
      qs: { projectid: projectid, id: id, type: type, zonetype: zonetype },
      json: true,
      auth: this.APIConfiguration.authentication
    };
     //console.log('options######################################### ', options)
    this.HttpRequest.makeHttpCall(options, function(result) {
      let responseObj = {};
      // console.log("result.status ", result.status);
      if (!result.status) {
        // Logger.log("error", err.message);
        const errorObj = ResponseFormatter.getErrorObject("projectNotFound", result.locals);
        responseObj = ResponseFormatter.getResponseObject(false, "Unable to get Beacon Scene data", errorObj);
        callback(responseObj);
      } else {
        if (result && Object.keys(result).length > 0) {
          responseObj = ResponseFormatter.getResponseObject(true, "Beacon Scene data successfully received", result.result.data);
          callback(responseObj);
        } else {
          const errorObj = ResponseFormatter.getErrorObject("projectNotFound", result.locals);
          responseObj = ResponseFormatter.getResponseObject(false, "Unable to get Beacon Scene data", errorObj);
          callback(responseObj);
        }
      }
    });
  }



  public getDevicesStat(projectId, id, type, device, callback): any {
    var options = {
      method: "GET",
      url: this.APIConfiguration.urlDir.getDevicesStat,
      qs: { projectid: projectId, id: id, type: type, device: device },
      json: true,
      auth: this.APIConfiguration.authentication
    };
    // console.log('options ', options)
    this.HttpRequest.makeHttpCall(options, function(result) {
      let responseObj = {};
      // console.log("result.status ", result.status);
      if (!result.status) {
        // Logger.log("error", err.message);
        const errorObj = ResponseFormatter.getErrorObject("projectNotFound", result.locals);
        responseObj = ResponseFormatter.getResponseObject(false, "Unable to get Device data", errorObj);
        callback(responseObj);
      } else {
        if (result && Object.keys(result).length > 0) {
          responseObj = ResponseFormatter.getResponseObject(true, "Device data successfully received", result.result.data);
          callback(responseObj);
        } else {
          const errorObj = ResponseFormatter.getErrorObject("projectNotFound", result.locals);
          responseObj = ResponseFormatter.getResponseObject(false, "Unable to get Device data", errorObj);
          callback(responseObj);
        }
      }
    });
  }

  public getZoneSensorData(projectId, id, type, callback): any {
    var options = {
      method: "GET",
      url: this.APIConfiguration.urlDir.getZoneSensorData,
      qs: { projectid: projectId, id: id, type: type},
      json: true,
      auth: this.APIConfiguration.authentication
    };
    // console.log('options ', options)
    this.HttpRequest.makeHttpCall(options, function(result) {
      let responseObj = {};
      // console.log("result.status ", result.status);
      if (!result.status) {
        // Logger.log("error", err.message);
        const errorObj = ResponseFormatter.getErrorObject("projectNotFound", result.locals);
        responseObj = ResponseFormatter.getResponseObject(false, "Unable to get Device data", errorObj);
        callback(responseObj);
      } else {
        if (result && Object.keys(result).length > 0) {
          responseObj = ResponseFormatter.getResponseObject(true, "Device data successfully received", result.result.data);
          callback(responseObj);
        } else {
          const errorObj = ResponseFormatter.getErrorObject("projectNotFound", result.locals);
          responseObj = ResponseFormatter.getResponseObject(false, "Unable to get Device data", errorObj);
          callback(responseObj);
        }
      }
    });
  }
  public getOccupancy(reqObj, callback): any {
    let options = {
      method: "GET",
      url: this.APIConfiguration.urlDir.getOccupancy,
      qs: {
        projectid: reqObj.projectId,
        id: reqObj.id,
        type: reqObj.type,
        starttime: reqObj.starttime,
        endtime: reqObj.endtime,
        unit: reqObj.unit
      },
      json: true,
      auth: this.APIConfiguration.authentication
    };
  /*   this.HttpRequest.makeHttpCall(options, function (result) {
      if (!result.status) {
        const errorObj = ResponseFormatter.getErrorObject("projectNotFound", result.locals);
        responseObj = ResponseFormatter.getResponseObject(false, "Unable to get occupancy data", errorObj);
        callback(responseObj);
      } else {
        if (result && result.result.data[0].e.length > 0) {
          responseObj = ResponseFormatter.getResponseObject(true, "Occupancy data successfully received", result.result.data);
          callback(responseObj);
        } else {
          const errorObj = ResponseFormatter.getErrorObject("projectNotFound", result.locals);
          responseObj = ResponseFormatter.getResponseObject(false, "Unable to get Occupancy data", errorObj);
          callback(responseObj);
        }
      }
    }); */
    let projectsDataPromise = this.HttpRequest.makeHttpCallOccupancy(options);
    let responseObj = {};
    projectsDataPromise.then(function(result){
      
      if (!result["status"]) {
      // Logger.log("error", err.message);
      const errorObj = ResponseFormatter.getErrorObject("projectNotFound", result["locals"]);
      responseObj = ResponseFormatter.getResponseObject(false, "Unable to get Occupancy data", errorObj);
      callback(responseObj);
      } else {
      if (result && Object.keys(result).length > 0) {
      responseObj = ResponseFormatter.getResponseObject(true, "Occupancy data successfully received", result["result"].data);
      callback(responseObj);
      } else {
      const errorObj = ResponseFormatter.getErrorObject("projectNotFound", result["locals"]);
      responseObj = ResponseFormatter.getResponseObject(false, "Unable to get Occupancy data", errorObj);
      callback(responseObj);
      }
      } 
    }).catch((error)=>{
      responseObj = ResponseFormatter.getResponseObject(false, "Unable to get Occupancy data", error);
      callback(responseObj);
    });
  }

  public getDeviceData(projectId, id, type, deviceType, callback): any {
    var options = {
      method: "GET",
      url: this.APIConfiguration.urlDir.deviceData,
      qs: { projectid: projectId, id: id, type: type, device: deviceType },
      json: true,
      auth: this.APIConfiguration.authentication
    };
    this.HttpRequest.makeHttpCall(options, function(result) {
      let responseObj = {};
      if (!result.status) {
        // Logger.log("error", err.message);
        const errorObj = ResponseFormatter.getErrorObject(
          "projectNotFound",
          result.locals
        );
        responseObj = ResponseFormatter.getResponseObject(
          false,
          "Unable to get Device data",
          errorObj
        );
        callback(responseObj);
      } else {
        if (result && Object.keys(result).length > 0) {
          responseObj = ResponseFormatter.getResponseObject(
            true,
            "Device data successfully received",
            result.result.data
          );

          callback(responseObj);
        } else {
          const errorObj = ResponseFormatter.getErrorObject(
            "projectNotFound",
            result.locals
          );
          responseObj = ResponseFormatter.getResponseObject(
            false,
            "Unable to get Device data",
            errorObj
          );
          callback(responseObj);
        }
      }
    });
  }

  public setBrightnessData(projectId, id, type, brightnesslevel, property, zonetype, callback): any {
    var options = {
      method: "PUT",
      url: "",
      qs: {},
      body: { value: brightnesslevel, property: property },
      json: true,
      auth: this.APIConfiguration.authentication
    };
    options.url = this.APIConfiguration.urlDir.setBrightness;
    options.qs = {
      projectid: projectId,
      id: id,
      type: type,
      zonetype: zonetype,
      property: property
    };
    this.HttpRequest.makeHttpCall(options, function(result) {
      let responseObj = {};
      if (!result.status) {
        // Logger.log("error", err.message);
        const errorObj = ResponseFormatter.getErrorObject(
          "projectNotFound",
          result.locals
        );
        responseObj = ResponseFormatter.getResponseObject(
          false,
          "Unable to get Brightness data",
          errorObj
        );
        callback(responseObj);
      } else {
        if (result && Object.keys(result).length > 0) {
          responseObj = ResponseFormatter.getResponseObject(
            true,
            "Brightness data successfully received",
            result.result.data
          );

          callback(responseObj);
        } else {
          const errorObj = ResponseFormatter.getErrorObject("projectNotFound", result.locals);
          responseObj = ResponseFormatter.getResponseObject(false, "Unable to get Brightness data", errorObj);
          callback(responseObj);
        }
      }
    });
  }
  public setPaletteColor(projectId, id, type, zonetype,dataparam, callback): any {
    var options = {
      method: "PUT",
      url: "",
      qs: {},
      body:[],
      json: true,
      auth: this.APIConfiguration.authentication
    };
    options.url = this.APIConfiguration.urlDir.setPaletteColor;
    options.qs = {
      projectid: projectId,
      id: id,
      type: type,
      zonetype: zonetype
    };

let datapam=[];
    let temp_array=JSON.parse(dataparam);

    //console.log(temp_array);
    for (let index = 0; index < temp_array.length; index++) {
      const element = temp_array[index];
      datapam.push(element);
    }
    options.body=datapam;

    //console.log(options);
    this.HttpRequest.makeHttpCall(options, function(result) {
      //console.log("error*************************************", result);
      let responseObj = {};
      if (!result.status) {
        
        const errorObj = ResponseFormatter.getErrorObject(
          "projectNotFound",
          result.locals
        );
        responseObj = ResponseFormatter.getResponseObject(
          false,
          "Unable to set Palette data",
          errorObj
        );
        callback(responseObj);
      } else {
        if (result && Object.keys(result).length > 0) {
          responseObj = ResponseFormatter.getResponseObject(
            true,
            "Palette successfully applied",
            result.result.data
          );

          callback(responseObj);
        } else {
          const errorObj = ResponseFormatter.getErrorObject("projectNotFound", result.locals);
          responseObj = ResponseFormatter.getResponseObject(false, "Unable to set Palette data", errorObj);
          callback(responseObj);
        }
      }
    });
  }
  public getBrightnessData(
    projectId,
    id,
    type,
    property,
    zonetype,
    callback
  ): any {
    var options = {
      method: "GET",
      url: this.APIConfiguration.urlDir.getBrightness,
      qs: {
        projectid: projectId,
        id: id,
        type: type,
        zonetype: zonetype,
        property: property
      },
      json: true,
      auth: this.APIConfiguration.authentication
    };
    this.HttpRequest.makeHttpCall(options, function(result) {
      let responseObj = {};
      if (!result.status) {
        // Logger.log("error", err.message);
        const errorObj = ResponseFormatter.getErrorObject(
          "projectNotFound",
          result.locals
        );
        responseObj = ResponseFormatter.getResponseObject(
          false,
          "Unable to get Brightness data",
          errorObj
        );
        callback(responseObj);
      } else {
        if (result && Object.keys(result).length > 0) {
          responseObj = ResponseFormatter.getResponseObject(
            true,
            "Brightness data successfully received",
            result.result.data
          );
          callback(responseObj);
        } else {
          const errorObj = ResponseFormatter.getErrorObject(
            "projectNotFound",
            result.locals
          );
          responseObj = ResponseFormatter.getResponseObject(
            false,
            "Unable to get Brightness data",
            errorObj
          );
          callback(responseObj);
        }
      }
    });
  }
  public getOnOffData(projectId, id, type, callback): any {
    var options = {
      method: "GET",
      url: "",
      qs: {},
      json: true,
      auth: this.APIConfiguration.authentication
    };
    switch (type) {
      case "building":
        options.url = this.APIConfiguration.urlDir.getBuildingOnOff;
        options.qs = { projectid: projectId, buildingid: id };

        break;
      case "floor":
        options.url = this.APIConfiguration.urlDir.getfloorOnOff;
        options.qs = { projectid: projectId, floorid: id };
        break;
      case "zone":
        options.url = this.APIConfiguration.urlDir.getzoneOnOff;
        options.qs = { projectid: projectId, zoneid: id };
        break;

      default:
        break;
    }
    this.HttpRequest.makeHttpCall(options, function(result) {
      let responseObj = {};
      if (!result.status) {
        // Logger.log("error", err.message);
        const errorObj = ResponseFormatter.getErrorObject(
          "projectNotFound",
          result.locals
        );
        responseObj = ResponseFormatter.getResponseObject(
          false,
          "Unable to get ON/OFF data",
          errorObj
        );
        callback(responseObj);
      } else {
        if (result && Object.keys(result).length > 0) {
          responseObj = ResponseFormatter.getResponseObject(
            true,
            "ON/OFF data successfully received",
            result.result.data
          );

          callback(responseObj);
        } else {
          const errorObj = ResponseFormatter.getErrorObject(
            "projectNotFound",
            result.locals
          );
          responseObj = ResponseFormatter.getResponseObject(
            false,
            "Unable to get ON/OFF data",
            errorObj
          );
          callback(responseObj);
        }
      }
    });
  }
  public setOnOffData(projectId, id, type, state, callback): any {
    var options = {
      method: "PUT",
      url: "",
      qs: {},
      body: { state: state },
      json: true,
      auth: this.APIConfiguration.authentication
    };
    switch (type) {
      case "building":
        options.url = this.APIConfiguration.urlDir.setBuildingOnOff;
        options.qs = { projectid: projectId, buildingid: id };

        break;
      case "floor":
        options.url = this.APIConfiguration.urlDir.setfloorOnOff;
        options.qs = { projectid: projectId, floorid: id };
        break;
      case "zone":
        options.url = this.APIConfiguration.urlDir.setzoneOnOff;
        options.qs = { projectid: projectId, zoneid: id };
        break;

      default:
        break;
    }
    this.HttpRequest.makeHttpCall(options, function(result) {
      let responseObj = {};
      if (!result.status) {
        // Logger.log("error", err.message);
        const errorObj = ResponseFormatter.getErrorObject(
          "projectNotFound",
          result.locals
        );
        responseObj = ResponseFormatter.getResponseObject(
          false,
          "Unable to get ON/OFF data",
          errorObj
        );
        callback(responseObj);
      } else {
        if (result && Object.keys(result).length > 0) {
          responseObj = ResponseFormatter.getResponseObject(
            true,
            "ON/OFF data successfully received",
            result.result.data
          );

          callback(responseObj);
        } else {
          const errorObj = ResponseFormatter.getErrorObject(
            "projectNotFound",
            result.locals
          );
          responseObj = ResponseFormatter.getResponseObject(
            false,
            "Unable to get ON/OFF data",
            errorObj
          );
          callback(responseObj);
        }
      }
    });
  }

  public setControlData(projectId, id, type, value, property, callback): any {
    var options = {
      method: "PUT",
      url: this.APIConfiguration.urlDir.setcontrol,
      qs: { projectid: projectId, id: id, type: type },
      body: { value: value, property: property },
      json: true,
      auth: this.APIConfiguration.authentication
    };
    this.HttpRequest.makeHttpCall(options, function(result) {
      let responseObj = {};
      if (!result.status) {
        // Logger.log("error", err.message);
        const errorObj = ResponseFormatter.getErrorObject(
          "projectNotFound",
          result.locals
        );
        responseObj = ResponseFormatter.getResponseObject(
          false,
          "Unable to get Control data",
          errorObj
        );
        callback(responseObj);
      } else {
        if (result && Object.keys(result).length > 0) {
          responseObj = ResponseFormatter.getResponseObject(
            true,
            "Control data successfully received",
            result.result.data
          );

          callback(responseObj);
        } else {
          const errorObj = ResponseFormatter.getErrorObject(
            "projectNotFound",
            result.locals
          );
          responseObj = ResponseFormatter.getResponseObject(
            false,
            "Unable to get Control data",
            errorObj
          );
          callback(responseObj);
        }
      }
    });
  }

  public setRGBData(projectId, zoneid, red, green, blue, callback): any {
    var options = {
      method: "PUT",
      url: this.APIConfiguration.urlDir.setRGB,
      qs: { projectid: projectId, zoneid: zoneid },
      body: { red: red, green: green, blue: blue },
      json: true,
      auth: this.APIConfiguration.authentication
    };
    this.HttpRequest.makeHttpCall(options, function(result) {
      let responseObj = {};
      if (!result.status) {
        // Logger.log("error", err.message);
        const errorObj = ResponseFormatter.getErrorObject(
          "projectNotFound",
          result.locals
        );
        responseObj = ResponseFormatter.getResponseObject(
          false,
          "Unable to get RGB data",
          errorObj
        );
        callback(responseObj);
      } else {
        if (result && Object.keys(result).length > 0) {
          responseObj = ResponseFormatter.getResponseObject(
            true,
            "RGB data successfully received",
            result.result.data
          );

          callback(responseObj);
        } else {
          const errorObj = ResponseFormatter.getErrorObject(
            "projectNotFound",
            result.locals
          );
          responseObj = ResponseFormatter.getResponseObject(
            false,
            "Unable to get RGB data",
            errorObj
          );
          callback(responseObj);
        }
      }
    });
  }

  public setBlindLevelData(projectId, id, type, level, callback): any {
    var options = {
      method: "PUT",
      url: this.APIConfiguration.urlDir.setBlindLevel,
      qs: { projectid: projectId, id: id, type: type },
      body: {
        all: { level: parseInt(level) },
        n: { level: 0 },
        ne: { level: 0 },
        e: { level: 0 },
        se: { level: 0 },
        s: { level: 0 },
        sw: { level: 0 },
        w: { level: 0 },
        nw: { level: 0 },
        c: { level: 0 }
      },
      json: true,
      auth: this.APIConfiguration.authentication
    };
    this.HttpRequest.makeHttpCall(options, function(result) {
      let responseObj = {};
      if (!result.status) {
        // Logger.log("error", err.message);
        const errorObj = ResponseFormatter.getErrorObject(
          "projectNotFound",
          result.locals
        );
        responseObj = ResponseFormatter.getResponseObject(
          false,
          "Unable to get level data",
          errorObj
        );
        callback(responseObj);
      } else {
        if (result && Object.keys(result).length > 0) {
          responseObj = ResponseFormatter.getResponseObject(
            true,
            "level data successfully received",
            result.result.data
          );

          callback(responseObj);
        } else {
          const errorObj = ResponseFormatter.getErrorObject(
            "projectNotFound",
            result.locals
          );
          responseObj = ResponseFormatter.getResponseObject(
            false,
            "Unable to get level data",
            errorObj
          );
          callback(responseObj);
        }
      }
    });
  }

  public setSensor(reqObj, callback): any {
   var query = {};
    var url = "";
    query = { projectid: reqObj.pid, id: reqObj.id, type: reqObj.type, zonetype: reqObj.zonetype };
    url = this.APIConfiguration.urlDir.setSensorState;
    let temp = new Array();
    temp = (reqObj.sensors).split(",");
    
    var options = {
      method: "PUT",
      url: url,
      qs: query,
      body:  { property: reqObj.property, value : temp},
      json: true,
      auth: this.APIConfiguration.authentication
    };
    
    this.HttpRequest.makeHttpCall(options, function(result) {
      let responseObj = {};
      if (!result.status) {
        // Logger.log("error", err.message);
        const errorObj = ResponseFormatter.getErrorObject(
          "projectNotFound",
          result.locals
        );
        responseObj = ResponseFormatter.getResponseObject(
          false,
          "Unable to get level data",
          errorObj
        );
        callback(responseObj);
      } else {
        if (result && Object.keys(result).length > 0) {
          responseObj = ResponseFormatter.getResponseObject(
            true,
            "level data successfully received",
            result.result.data
          );

          callback(responseObj);
        } else {
          const errorObj = ResponseFormatter.getErrorObject(
            "projectNotFound",
            result.locals
          );
          responseObj = ResponseFormatter.getResponseObject(
            false,
            "Unable to get level data",
            errorObj
          );
          callback(responseObj);
        }
      }
    });
  }




  public setMood(reqObj, callback): any {
   
    var query = {};
    var url = "";
    if (reqObj.pid && reqObj.mood && reqObj.fid) {
      query = { projectid: reqObj.pid, floorid: reqObj.fid };
      url = this.APIConfiguration.urlDir.setFloorMood;
    } else if (reqObj.pid && reqObj.bid && reqObj.mood) {
      query = { projectid: reqObj.pid, buildingid: reqObj.bid };
      url = this.APIConfiguration.urlDir.setBuildingMood;
    }else if (reqObj.pid && reqObj.zid && reqObj.mood){
      query = { projectid: reqObj.pid, zoneid: reqObj.zid };
      url = this.APIConfiguration.urlDir.setZoneMood;
    } else {
      let responseObj;
      const errorObj = ResponseFormatter.getErrorObject("invalidParams", {});
      responseObj = ResponseFormatter.getResponseObject(
        false,
        "Unable to get level data",
        errorObj
      );
      callback(responseObj);
    }


    var options = {
      method: "PUT",
      url: url,
      qs: query,
      body: { mood: reqObj.mood },
      json: true,
      auth: this.APIConfiguration.authentication
    };
    
    this.HttpRequest.makeHttpCall(options, function(result) {
      let responseObj = {};
      if (!result.status) {
        // Logger.log("error", err.message);
        const errorObj = ResponseFormatter.getErrorObject(
          "projectNotFound",
          result.locals
        );
        responseObj = ResponseFormatter.getResponseObject(
          false,
          "Unable to get level data",
          errorObj
        );
        callback(responseObj);
      } else {
        if (result && Object.keys(result).length > 0) {
          responseObj = ResponseFormatter.getResponseObject(
            true,
            "level data successfully received",
            result.result.data
          );

          callback(responseObj);
        } else {
          const errorObj = ResponseFormatter.getErrorObject(
            "projectNotFound",
            result.locals
          );
          responseObj = ResponseFormatter.getResponseObject(
            false,
            "Unable to get level data",
            errorObj
          );
          callback(responseObj);
        }
      }
    });
  }
  public setLightScene(reqObj, callback): any {
    var query = {};
    var url = "";
    if (reqObj.pid === "floor") {
      query = { projectid: reqObj.pid, floorid: reqObj.id };
      url = this.APIConfiguration.urlDir.setFloorLightScene;
    } else if (reqObj.pid === "building") {
      query = { projectid: reqObj.pid, buildingid: reqObj.id };
      url = this.APIConfiguration.urlDir.setBuildingLightScene;
    }  else if (reqObj.type === "zone") {
        query = { projectid: reqObj.pid, zoneid: reqObj.id };
        url = this.APIConfiguration.urlDir.setZoneLightScene;
    }
     else {
      let responseObj;
      const errorObj = ResponseFormatter.getErrorObject("invalidParams", {});
      responseObj = ResponseFormatter.getResponseObject(
        false,
        "Unable to get level data",
        errorObj
      );
      callback(responseObj);
    }
    var options = {
      method: "PUT",
      url: url,
      qs: query,
      body: { lightscene: reqObj.lightscene },
      json: true,
      auth: this.APIConfiguration.authentication
    };
    this.HttpRequest.makeHttpCall(options, function(result) {
      let responseObj = {};
      if (!result.status) {
        // Logger.log("error", err.message);
        const errorObj = ResponseFormatter.getErrorObject("projectNotFound", result.locals);
        responseObj = ResponseFormatter.getResponseObject(false, "Unable to get level data", errorObj);
        callback(responseObj);
      } else {
        if (result && Object.keys(result).length > 0) {
          responseObj = ResponseFormatter.getResponseObject(true, "level data successfully received", result.result.data);
          callback(responseObj);
        } else {
          const errorObj = ResponseFormatter.getErrorObject(
            "projectNotFound",
            result.locals
          );
          responseObj = ResponseFormatter.getResponseObject(
            false,
            "Unable to get level data",
            errorObj
          );
          callback(responseObj);
        }
      }
    });
  }

  public getCurrentData(reqObje, callback): any {
    let query = {};
    let url = "";

    if (reqObje.pid && reqObje.type && reqObje.id) {
      query = { projectid: reqObje.pid, type: reqObje.type, id: reqObje.id };
      url = this.APIConfiguration.urlDir.getCurrentData;
    } else {
      let responseObj;
      const errorObj = ResponseFormatter.getErrorObject("invalidParams", {});
      responseObj = ResponseFormatter.getResponseObject(false, "Unable to get current data", errorObj);
      callback(responseObj);
    }

    var options = {
      method: "GET",
      url: url,
      qs: query,
      json: true,
      auth: this.APIConfiguration.authentication
    };

    // console.log('kpiiiiiiiiiiiii',options);

    this.HttpRequest.makeHttpCall(options, function(result) {
      let responseObj = {};
      if (!result.status) {
        // Logger.log("error", err.message);
        const errorObj = ResponseFormatter.getErrorObject("projectNotFound",result.locals);
        responseObj = ResponseFormatter.getResponseObject(false, "Unable to get KPI data", errorObj);
        callback(responseObj);
      } else {
        if (result && Object.keys(result).length > 0) {
          responseObj = ResponseFormatter.getResponseObject(true, "KPI data successfully received", result.result.data);
          callback(responseObj);
        } else {
          const errorObj = ResponseFormatter.getErrorObject("projectNotFound", result.locals);
          responseObj = ResponseFormatter.getResponseObject(false, "Unable to get KPI data", errorObj);
          callback(responseObj);
        }
      }
    });
  }

  public getHumidexData(projectId, id, type, callback): any {
    var options = {
      method: "GET",
      url: this.APIConfiguration.urlDir.getHumidexData,
      qs: { projectid: projectId, id: id, type: type },
      json: true,
      auth: this.APIConfiguration.authentication
    };
    this.HttpRequest.makeHttpCall(options, function(result) {
      let responseObj = {};
      if (!result.status) {
        // Logger.log("error", err.message);
        const errorObj = ResponseFormatter.getErrorObject("projectNotFound", result.locals);
        responseObj = ResponseFormatter.getResponseObject(false, "Unable to get Humidex data", errorObj);
        callback(responseObj);
      } else {
        if (result && Object.keys(result).length > 0) {
          responseObj = ResponseFormatter.getResponseObject(true, "Humidex data successfully received", result.result.data);
          callback(responseObj);
        } else {
          const errorObj = ResponseFormatter.getErrorObject("projectNotFound", result.locals);
          responseObj = ResponseFormatter.getResponseObject(false, "Unable to get Humidex data", errorObj);
          callback(responseObj);
        }
      }
    });
  }
  public getSensorsEnabledDisable(object, callback): any {
    var options = {
      method: "GET",
      url: this.APIConfiguration.urlDir.getSensorsEnabledDisable,
      qs: { projectid: object.pid, id: object.id, type: object.type, property: object.property, zonetype: object.zonetype},
      json: true,
      auth: this.APIConfiguration.authentication
    };
    this.HttpRequest.makeHttpCall(options, function(result) {
      let responseObj = {};
      if (!result.status) {
        // Logger.log("error", err.message);
        const errorObj = ResponseFormatter.getErrorObject("projectNotFound", result.locals);
        responseObj = ResponseFormatter.getResponseObject(false, "Unable to get Sensor data", errorObj);
        callback(responseObj);
      } else {
        if (result && Object.keys(result).length > 0) {
          responseObj = ResponseFormatter.getResponseObject(true, "Senosr data successfully received", result.result.data);
          callback(responseObj);
        } else {
          const errorObj = ResponseFormatter.getErrorObject("projectNotFound", result.locals);
          responseObj = ResponseFormatter.getResponseObject(false, "Unable to get Sensor data", errorObj);
          callback(responseObj);
        }
      }
    });
  }
  public getMood(object, callback): any {
    var options = {
      method: "GET",
      url: this.APIConfiguration.urlDir.getMood,
      qs: { projectid: object.pid, id: object.id},
      json: true,
      auth: this.APIConfiguration.authentication
    };
    this.HttpRequest.makeHttpCall(options, function(result) {
      let responseObj = {};
      if (!result.status) {
        // Logger.log("error", err.message);
        const errorObj = ResponseFormatter.getErrorObject("projectNotFound", result.locals);
        responseObj = ResponseFormatter.getResponseObject(false, "Unable to get Mood data", errorObj);
        callback(responseObj);
      } else {
        if (result && Object.keys(result).length > 0) {
          responseObj = ResponseFormatter.getResponseObject(true, "Mood data successfully received", result.result.data);
          callback(responseObj);
        } else {
          const errorObj = ResponseFormatter.getErrorObject("projectNotFound", result.locals);
          responseObj = ResponseFormatter.getResponseObject(false, "Unable to get Mood data", errorObj);
          callback(responseObj);
        }
      }
    });
  }

  public getFloorPlan(object, callback): any {
    fs.readdir('./floorplans/', (err, files) => {
      let found = false;
      files.forEach(file => {
        if (file.includes(object.id)) {
          found = true;
          var mimetype;
          fs.readFile('./floorplans/'+file, function read(err, data) {
            mimetype = fileType(data);
            if (err) {
                fs.readFile('./floorplans/defaultfloormap.PNG', function read(err, data) { 
                  if (err) {
                    console.log('error found', err);
                  }
                  else {
                  
                    var content = data;
                    mimetype = fileType(data);
                    callback(content, mimetype.mime); 
                  }
                });

            } else {
              var content = data;
              // Invoke the next step here however you like
              callback(content, mimetype.mime);   // Put all of the code here (not the best solution)         // Or put the next step in a function and invoke it
            }
          
        });
        }
      });
      if (!found) {
        fs.readFile('./floorplans/defaultfloormap.PNG', function read(err, data) { 
          if (err) {
            console.log(err);
          }
          else {
          var content = data;
          var mimetype = fileType(data);
          callback(content, mimetype.mime); 
          }
        });
      }
    });

  }

  public pingDMS(reqObj, callback): any {
    var options = {
      method: "GET",
      url: this.APIConfiguration.dmsUrlDir.ping,
      json: true,
    };
    // console.log("options ",options)
    this.HttpRequest.makeHttpCall(options, function(result) {
      callback(result);
    });
  }

  public getFirmwareList(reqObj, callback): any {
    var options = {
      method: "GET",
      url: this.APIConfiguration.dmsUrlDir.getFirmwareList,
      json: true,
    };
    // console.log("options ",options)
    this.HttpRequest.makeHttpCall(options, function(result) {
      let responseObj = {};

      if (!result.status) {
        // Logger.log("error", err.message);
        const errorObj = ResponseFormatter.getErrorObject("projectNotFound", result.message);
        responseObj = ResponseFormatter.getResponseObject(false, "Unable to get Firmware List data", errorObj);
        callback(responseObj);
      } else {
        if (result && Object.keys(result).length > 0) {
          responseObj = ResponseFormatter.getResponseObject(true, "Firmware list successfully received", result.result.data);
          callback(responseObj);
        } else {
          const errorObj = ResponseFormatter.getErrorObject("projectNotFound", result.message);
          responseObj = ResponseFormatter.getResponseObject(false, "Unable to get Firmware List data", errorObj);
          callback(responseObj);
        }
      }

      // callback(result);
    });
  }

  public uploadFirmwareFile(gateway,file, callback): any {
    var options = {
      method: "POST",
      url: this.APIConfiguration.dmsUrlDir.uploadFirmwareFile,
      json: true,
      formData:  
        { file: 
        { value: fs.createReadStream(file.path),
          options: 
           { filename: file.filename,
             contentType: null } },
       gateway: gateway } };
    this.HttpRequest.makeHttpCall(options, function(result) {
      let responseObj = {};
      if (!result.status) {
        responseObj = ResponseFormatter.getResponseObject(false, "Unable to upload Firmware File", result.error);
        callback(responseObj);
      } else {
        if (result && Object.keys(result).length > 0) {
          responseObj = ResponseFormatter.getResponseObject(true, "Firmware File Uploaded Successfully", result.result.data);
          callback(responseObj);
        } else {
          responseObj = ResponseFormatter.getResponseObject(false, "Unable to upload Firmware File", result.error);
          callback(responseObj);
        }
      }

      // callback(result);
    });
  }

  public updateFirmware(body, callback): any {
    var options = {
      method: "POST",
      url: this.APIConfiguration.dmsUrlDir.firmwareUpdate,
      body: body,
      headers: {'content-type': 'application/json' },
      json: true,
    };
    // console.log("options ",options)
    this.HttpRequest.makeHttpCall(options, function(result) {
      let responseObj = {};

      if (!result.status) {
        // Logger.log("error", err.message);
        const errorObj = ResponseFormatter.getErrorObject("projectNotFound", result.message);
        responseObj = ResponseFormatter.getResponseObject(false, "Unable to update Firmware ", errorObj);
        callback(responseObj);
      } else {
        if (result && Object.keys(result).length > 0) {
          responseObj = ResponseFormatter.getResponseObject(true, "Firmware Update Successful", result.result.data);
          callback(responseObj);
        } else {
          const errorObj = ResponseFormatter.getErrorObject("projectNotFound", result.message);
          responseObj = ResponseFormatter.getResponseObject(false, "Unable to update Firmware", errorObj);
          callback(responseObj);
        }
      }

      // callback(result);
    });
  }

  public getDeviceList(reqObj, callback): any {
    var options = {
      method: "GET",
      url: this.APIConfiguration.dmsUrlDir.dmsDeviceList,
      json: true,
    };
    this.HttpRequest.makeHttpCall(options, function(result) {
      let responseObj = {};
      if (!result.status) {
        // Logger.log("error", err.message);
        // const errorObj = ResponseFormatter.getErrorObject("Error", result);
        responseObj = ResponseFormatter.getResponseObject(false, "Unable to get Device List data", result.errno);
        callback(responseObj);
      } else {
        if (result && Object.keys(result).length > 0) {
          responseObj = ResponseFormatter.getResponseObject(true, "Device list successfully received", result.result.data);
          callback(responseObj);
        } else {
          // const errorObj = ResponseFormatter.getErrorObject("Error", result);
          responseObj = ResponseFormatter.getResponseObject(false, "Unable to get Device List data",result.errno);
          callback(responseObj);
        }
      }

      // callback(result);
    });
  }


  public getVersionList(reqObj, callback): any {
    var options = {
      method: "GET",
      url: this.APIConfiguration.dmsUrlDir.getVersionList,
      json: true,
    };
    this.HttpRequest.makeHttpCall(options, function(result) {
      let responseObj = {};
      if (!result.status) {
        // Logger.log("error", err.message);
        // const errorObj = ResponseFormatter.getErrorObject("Error", result);
        responseObj = ResponseFormatter.getResponseObject(false, "Unable to get version List data", result.errno);
        callback(responseObj);
      } else {
        if (result && Object.keys(result).length > 0) {
          responseObj = ResponseFormatter.getResponseObject(true, "Version list successfully received", result.result.data);
          callback(responseObj);
        } else {
          // const errorObj = ResponseFormatter.getErrorObject("Error", result);
          responseObj = ResponseFormatter.getResponseObject(false, "Unable to get Version List data",result.errno);
          callback(responseObj);
        }
      }

      // callback(result);
    });
  }

  public getFirmwareStatus(reqObj, callback): any {
    var options = {
      method: "GET",
      url: this.APIConfiguration.dmsUrlDir.getFirmwareStatus,
      json: true,
    };
    this.HttpRequest.makeHttpCall(options, function(result) {
      let responseObj = {};
      if (!result.status) {
        // Logger.log("error", err.message);
        // const errorObj = ResponseFormatter.getErrorObject("Error", result);
        responseObj = ResponseFormatter.getResponseObject(false, "Unable to get firmware status", result.errno);
        callback(responseObj);
      } else {
        if (result && Object.keys(result).length > 0) {
          responseObj = ResponseFormatter.getResponseObject(true, "firmware status successfully received", result.result.data);
          callback(responseObj);
        } else {
          // const errorObj = ResponseFormatter.getErrorObject("Error", result);
          responseObj = ResponseFormatter.getResponseObject(false, "Unable to get firmware status",result.errno);
          callback(responseObj);
        }
      }

      // callback(result);
    });
  }

  public updateGridDimentsions(fid, gridW, gridH) {
    // console.log(floorid, gridW, gridH);
    // console.log('project data', this.dataCollectionManager.dataObject.floorObj);
    let floorObj =  this.dataCollectionManager.dataObject.floorObj;
    Object.keys(this.dataCollectionManager.dataObject.projectObj).forEach( key => {
      // console.log('projid', key);
      let projectId = key;
      // console.log('testtttt', Object.keys(this.dataCollectionManager.dataObject.projectObj[key]));
      let obj = Object.keys(this.dataCollectionManager.dataObject.projectObj[key]);
      let buildingId = obj[0];
      Object.keys(floorObj[projectId][buildingId]).forEach( floorid => {
        if (floorid == fid) {
          floorObj[projectId][buildingId][floorid].floorInfo.floorGridH = gridH;
          floorObj[projectId][buildingId][floorid].floorInfo.floorGridW = gridW;
        }
      });
    });
  }

  public getFloorDimensions(floorid) {
    let found = false;
    this.gridDimensions.forEach( dim => {
      console.log(dim.floorid, floorid)
      if (dim.floorid == floorid) {
        found = true;
        console.log('test', { 'gridW': dim.gridW, 'gridH': dim.gridH });
        return { 'gridW': dim.gridW, 'gridH': dim.gridH }
      }
    });
    if (!found) {
      console.log('test1', this.gridDimensions, { 'gridW': '100', 'gridH': '100' });
      return { 'gridW': '100', 'gridH': '100' }
      
    }
  }

}
