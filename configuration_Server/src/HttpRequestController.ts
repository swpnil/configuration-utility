// import {AuthenticationService} from "./AuthenticationService";
import ResponseFormatter from "./lib/helpers/ResponseFormatter";
import Validator from "./validator";
import {HttpRequest} from "./HttpRequest";

export class HttpRequestController {
    private HttpRequest: HttpRequest;
    constructor() {
        this.HttpRequest = new HttpRequest();
        this.initiateProcess();
    }

    public initiateProcess() : void {
        var initiateProcessObj = {host: 'ip',port: 80,path: '/getProjects',method: 'GET'};
        this.HttpRequest.makeHttpCall(initiateProcessObj, function(){

        });
    }
}