import * as express from "express";
import * as async from "async";
import * as cors from "cors";
import * as bodyParser from "body-parser";
import * as http from "http";
import * as path from "path";
// import * as request from "request"

// import {AuditLogger} from "./lib/auditLogger";
import {AuthenticationService} from "./lib/AuthenticationService";
import {Collections} from "./lib/collections";
import {CommunicationManager} from "./lib/communicationManager";
import { DataCollectionManager } from "./lib/processManager/dataCollectionManager";
import {Config} from "./config";
import Logger from "./lib/logger/Logger";
import {MailingService} from "./lib/mailingService";
import {JwtWrapper} from "./lib/middleware/jwtWrapper";
import { cache } from "./global";
import {MqttManager} from "./lib/mqttManager"
import {Controllers} from "./controllers";
var Request = require("request");
export class Server {
    private app: any;
    private Config: Config;
    private MailingService: MailingService;
    private configFilePath: string;
    private server: any;
    private CommunicationManager: CommunicationManager;
    private AuthenticationService: AuthenticationService;
    private Collections: Collections;
    private MqttManager: MqttManager;
    private Controllers: Controllers;
    // private processManager: ProcessManager;
    // private AuditLogger: AuditLogger;

    constructor() {
        this.configFilePath = path.join(__dirname, "../appConfig.json");
        this.init();
        this.initiateMiddleWare();
        this.createCollections();
        this.initiateModules();
    }

    private init(): void {
        this.app = express();
        this.server = http.createServer(this.app); // SERVER object is required for initializing the socket.
    }

    private initiateMiddleWare(): void {
        const JwtWrapperObj = JwtWrapper.getInstance();
        this.app.use(JwtWrapperObj.init.bind(JwtWrapperObj));


        this.app.use(bodyParser.urlencoded({
            extended: true,
        }));

        this.app.use(bodyParser.json());


        this.app.use(cors());
        this.app.use(express.static(__dirname + "/ui"));
    }

    private initiateModules(): void {
        async.waterfall([
            async.apply(this.initiateApplicationConfiguration, this),
            async.apply(this.initiateCommunicationManager, this),
            // async.apply(this.initiateMailingService, this),
            async.apply(this.initiateAuthenticationService, this),
            async.apply(this.initiateMqtt, this),
            // async.apply(this.initiateDataProcess, this)
            // async.apply(this.initiateProcessManager)
        ], (err, results) => {
                this.listen(() => {
                    let dataCollectionManager = DataCollectionManager.getInstance();
                    dataCollectionManager.init();
                });

            

        });
    }

    private initiateCommunicationManager(selfObj, callback: any) {
        selfObj.CommunicationManager = CommunicationManager.getInstance();
        selfObj.CommunicationManager.init(selfObj.server, selfObj.app);
        callback(null);
    }

    private initiateApplicationConfiguration(selfObj, callback) {
        selfObj.Config = Config.getInstance();
        selfObj.Config.init(selfObj.configFilePath, () => {
            callback(null);
        });
    }

    private initiateMailingService(selfObj, callback) {
        selfObj.MailingService = MailingService.getInstance();
        selfObj.MailingService.init( () => {
            callback(null);
        });
    }

    private initiateAuthenticationService(selfObj, callback) {
        selfObj.AuthenticationService = AuthenticationService.getInstance();
        selfObj.AuthenticationService.init();
        callback(null);
    }

    private initiateMqtt(selfObj, callback) {
        selfObj.MqttManager = MqttManager.getInstance();
        selfObj.MqttManager.init();
        callback(null);
    }

    // private initiateAuditLogger(selfObj, callback) {
        // selfObj.AuditLogger = AuditLogger.getInstance();
        // selfObj.AuditLogger.init();
        // callback();
    // }

    private createCollections(): void {
        this.Collections = Collections.getInstance();
        this.Collections.init();
    }

    private listen(callback) {
        // const listOfAPIsIpAddress = this.Config.getConfig("listOfAPIsIpAddress")
        const systemConfig = this.Config.getConfig("systemConfig");
        console.log("@@@@@@@@@@",systemConfig)
        const self = this;
        this.server.listen(888, (self) => {
        
            // Logger.log("info", "Server running at port:" + systemConfig.port);
            const apiConfig = this.Config.getConfig("apiConfig");
            const dmsConfig = this.Config.getConfig("dmsConfig");
            const requestData = {
                ip: apiConfig.IP,
                port: apiConfig.port,
                dmsIp : dmsConfig.IP,
                firmwarePort:dmsConfig.firmwarePort,
                dmsPort:dmsConfig.DMSport
            }
            Request.post({
                url: "http://localhost:" + systemConfig.port + "/api/setIp",
                json: true,
                headers: {
                    "content-type": "application/json",
                },
                body: requestData
            }, function(error, res, body) {
                console.log("BODYYYYYYYYYYY")
            if (error) {
                console.error(error)
                callback(error)
                // res.send(error);
            } else {
                callback(body)
                // res.send(body);
                // console.log('statusCode:', res.statusCode);
                // console.log(body)
            }
            
            });
        });
    }
        
}
